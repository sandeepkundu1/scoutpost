<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserGamesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserGamesTable Test Case
 */
class UserGamesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'UserGames' => 'app.user_games',
        'Users' => 'app.users',
        'Userprofiles' => 'app.userprofiles',
        'UserStats' => 'app.user_stats'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserGames') ? [] : ['className' => 'App\Model\Table\UserGamesTable'];
        $this->UserGames = TableRegistry::get('UserGames', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserGames);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test checkmydate method
     *
     * @return void
     */
    public function testCheckmydate()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AB method
     *
     * @return void
     */
    public function testFetchAB()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_HIT method
     *
     * @return void
     */
    public function testFetchHIT()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AVG method
     *
     * @return void
     */
    public function testFetchAVG()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_SLG method
     *
     * @return void
     */
    public function testFetchSLG()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_OBP method
     *
     * @return void
     */
    public function testFetchOBP()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_OPS method
     *
     * @return void
     */
    public function testFetchOPS()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_TB method
     *
     * @return void
     */
    public function testFetchTB()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_RC method
     *
     * @return void
     */
    public function testFetchRC()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_PA method
     *
     * @return void
     */
    public function testFetchPA()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_PIP method
     *
     * @return void
     */
    public function testFetchPIP()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_HIT_for_pitcher method
     *
     * @return void
     */
    public function testFetchHITForPitcher()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AVG_VS_RHP method
     *
     * @return void
     */
    public function testFetchAVGVSRHP()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AVG_VS_LHP method
     *
     * @return void
     */
    public function testFetchAVGVSLHP()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_QAB method
     *
     * @return void
     */
    public function testFetchQAB()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AVG_W_ROB method
     *
     * @return void
     */
    public function testFetchAVGWROB()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_AB_for_pitcher method
     *
     * @return void
     */
    public function testFetchABForPitcher()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_stats_type method
     *
     * @return void
     */
    public function testFetchStatsType()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fetch_sum_of_stats method
     *
     * @return void
     */
    public function testFetchSumOfStats()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
