<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserStatsMediaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserStatsMediaTable Test Case
 */
class UserStatsMediaTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'UserStatsMedia' => 'app.user_stats_media',
        'UserStats' => 'app.user_stats',
        'UserGames' => 'app.user_games',
        'Users' => 'app.users',
        'Userprofiles' => 'app.userprofiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserStatsMedia') ? [] : ['className' => 'App\Model\Table\UserStatsMediaTable'];
        $this->UserStatsMedia = TableRegistry::get('UserStatsMedia', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserStatsMedia);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
