<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Users' => 'app.users',
        'Userprofiles' => 'app.userprofiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'App\Model\Table\UsersTable'];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getUniqueUsernme method
     *
     * @return void
     */
    public function testGetUniqueUsernme()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test sendRegistrationMail method
     *
     * @return void
     */
    public function testSendRegistrationMail()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test forgotPasswordEmailTemplate method
     *
     * @return void
     */
    public function testForgotPasswordEmailTemplate()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test matchAge method
     *
     * @return void
     */
    public function testMatchAge()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
