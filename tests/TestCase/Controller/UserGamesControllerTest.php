<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UserGamesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UserGamesController Test Case
 */
class UserGamesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'UserGames' => 'app.user_games',
        'Users' => 'app.users',
        'Userprofiles' => 'app.userprofiles',
        'UserStats' => 'app.user_stats',
        'Tournaments' => 'app.tournaments'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
