<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserprofilesFixture
 *
 */
class UserprofilesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sport' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '1=>\'Baseball\',2=>\'Softball\'', 'precision' => null, 'fixed' => null],
        'pitcher' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'outfilder' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'infilder' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'catcher' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bats_from' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '1=>\'Right\',2=>\'Left\',3=>\'Both\'', 'precision' => null, 'fixed' => null],
        'through_with' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '1=>\'Right Hand\',2=>\'Left Hand\'', 'precision' => null, 'fixed' => null],
        'curr_team' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'division' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'curr_school' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'schooling' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'feb_team' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'feb_athlete' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'user_id' => 1,
            'sport' => 'Lorem ipsum dolor sit amet',
            'pitcher' => 1,
            'outfilder' => 1,
            'infilder' => 1,
            'catcher' => 1,
            'bats_from' => 'Lorem ipsum dolor sit amet',
            'through_with' => 'Lorem ipsum dolor sit amet',
            'curr_team' => 'Lorem ipsum dolor sit amet',
            'division' => 'Lorem ipsum dolor sit amet',
            'curr_school' => 'Lorem ipsum dolor sit amet',
            'schooling' => 'Lorem ipsum dolor sit amet',
            'feb_team' => 'Lorem ipsum dolor sit amet',
            'feb_athlete' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-06-29 07:14:42',
            'modified' => '2015-06-29 07:14:42'
        ],
    ];
}
