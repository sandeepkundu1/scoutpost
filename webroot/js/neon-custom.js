function fit_main_content_height() {
    if (public_vars.$sidebarMenu.length && 0 == public_vars.$sidebarMenu.hasClass("fixed")) {
        if (isxs()) return public_vars.$sidebarMenu.css("min-height", ""), public_vars.$mainContent.css("min-height", ""), void("undefined" != typeof reset_mail_container_height && reset_mail_container_height());
        var e = public_vars.$sidebarMenu.outerHeight(),
            t = (public_vars.$mainContent.outerHeight(), $(document).height()),
            a = $(window).height();
        if (a > t && (t = a), public_vars.$horizontalMenu.length > 0) {
            var n = public_vars.$horizontalMenu.outerHeight();
            t -= n, e -= n
        }
        //Use for scroll min height
        //public_vars.$mainContent.css("min-height", t), public_vars.$sidebarMenu.css("min-height", t), public_vars.$chat.css("min-height", t), "undefined" != typeof fit_mail_container_height && fit_mail_container_height(), "undefined" != typeof fit_calendar_container_height && fit_calendar_container_height()
    }
}

function setup_sidebar_menu() {
    var e = public_vars.$sidebarMenu.find("li:has(ul)"),
        t = {
            submenu_open_delay: .5,
            submenu_open_easing: Sine.easeInOut,
            submenu_opened_class: "opened"
        },
        a = "root-level",
        n = public_vars.$mainMenu.hasClass("multiple-expanded");
    public_vars.$mainMenu.find("> li").addClass(a), e.each(function(e, i) {
        var s = $(i),
            o = s.find("> a"),
            r = s.find("> ul");
        s.addClass("has-sub"), o.click(function(e) {
            if (e.preventDefault(), !n && s.hasClass(a)) {
                var i = public_vars.$mainMenu.find("." + a).not(s).find("> ul");
                i.each(function(e, a) {
                    var n = $(a);
                    menu_do_collapse(n, n.parent(), t)
                })
            }
            if (s.hasClass(t.submenu_opened_class)) menu_do_collapse(r, s, t);
            else {
                r.is(":visible") || menu_do_expand(r, s, t)
            }
            fit_main_content_height()
        })
    }), public_vars.$mainMenu.find("." + t.submenu_opened_class + " > ul").addClass("visible"), public_vars.$mainMenu.hasClass("auto-inherit-active-class") && menu_set_active_class_to_parents(public_vars.$mainMenu.find(".active"));
    var i = public_vars.$mainMenu.find('#search input[type="text"]'),
        s = public_vars.$mainMenu.find("#search");
    public_vars.$mainMenu.find("#search form").submit(function(e) {
        var t = public_vars.$pageContainer.hasClass("sidebar-collapsed");
        return t && 0 == s.hasClass("focused") ? (e.preventDefault(), s.addClass("focused"), i.focus(), !1) : void 0
    }), i.on("blur", function() {
        var e = public_vars.$pageContainer.hasClass("sidebar-collapsed");
        e && s.removeClass("focused")
    });
    var o = $("");
    public_vars.$sidebarMenu.find(".logo-env").append(o)
}

function menu_do_expand(e, t, a) {
    e.addClass("visible").height(""), current_height = e.outerHeight();
    var n = {
            opacity: .2,
            height: 0,
            top: -20
        },
        i = {
            height: current_height,
            opacity: 1,
            top: 0
        };
    isxs() && (delete n.opacity, delete n.top, delete i.opacity, delete i.top), TweenMax.set(e, {
        css: n
    }), t.addClass(a.submenu_opened_class), TweenMax.to(e, a.submenu_open_delay, {
        css: i,
        ease: a.submenu_open_easing,
        onComplete: function() {
            e.attr("style", ""), fit_main_content_height()
        }
    })
}

function menu_do_collapse(e, t, a) {
    public_vars.$pageContainer.hasClass("sidebar-collapsed") && t.hasClass("root-level") || (t.removeClass(a.submenu_opened_class), TweenMax.to(e, a.submenu_open_delay, {
        css: {
            height: 0,
            opacity: .2
        },
        ease: a.submenu_open_easing,
        onComplete: function() {
            e.removeClass("visible"), fit_main_content_height()
        }
    }))
}

function menu_set_active_class_to_parents(e) {
    if (e.length) {
        var t = e.parent().parent();
        t.addClass("active"), t.hasClass("root-level") || menu_set_active_class_to_parents(t)
    }
}

function setup_horizontal_menu() {
    var e = public_vars.$horizontalMenu.find(".navbar-nav"),
        t = e.find("li:has(ul)"),
        a = public_vars.$horizontalMenu.find("li#search"),
        n = a.find(".search-input"),
        i = a.find("form"),
        s = "root-level";
    is_multiopen = e.hasClass("multiple-expanded"), submenu_options = {
        submenu_open_delay: .5,
        submenu_open_easing: Sine.easeInOut,
        submenu_opened_class: "opened"
    }, e.find("> li").addClass(s), t.each(function(t, a) {
        var n = $(a),
            i = n.find("> a"),
            o = n.find("> ul");
        n.addClass("has-sub"), setup_horizontal_menu_hover(n, o), i.click(function(t) {
            if (isxs()) {
                if (t.preventDefault(), !is_multiopen && n.hasClass(s)) {
                    var a = e.find("." + s).not(n).find("> ul");
                    a.each(function(e, t) {
                        var a = $(t);
                        menu_do_collapse(a, a.parent(), submenu_options)
                    })
                }
                if (n.hasClass(submenu_options.submenu_opened_class)) menu_do_collapse(o, n, submenu_options);
                else {
                    o.is(":visible") || menu_do_expand(o, n, submenu_options)
                }
                fit_main_content_height()
            }
        })
    }), a.hasClass("search-input-collapsed") && (i.submit(function(e) {
        return a.hasClass("search-input-collapsed") ? (e.preventDefault(), a.removeClass("search-input-collapsed"), n.focus(), !1) : void 0
    }), n.on("blur", function() {
        a.addClass("search-input-collapsed")
    }))
}

function setup_horizontal_menu_hover(e, t) {
    var a = .5,
        n = -10,
        i = Quad.easeInOut;
    TweenMax.set(t, {
        css: {
            autoAlpha: 0,
            transform: "translateX(" + n + "px)"
        }
    }), e.hoverIntent({
        over: function() {
            return isxs() ? !1 : ("none" == t.css("display") && t.css({
                display: "block",
                visibility: "hidden"
            }), t.css({
                zIndex: ++public_vars.hover_index
            }), void TweenMax.to(t, a, {
                css: {
                    autoAlpha: 1,
                    transform: "translateX(0px)"
                },
                ease: i
            }))
        },
        out: function() {
            return isxs() ? !1 : void TweenMax.to(t, a, {
                css: {
                    autoAlpha: 0,
                    transform: "translateX(" + n + "px)"
                },
                ease: i,
                onComplete: function() {
                    TweenMax.set(t, {
                        css: {
                            transform: "translateX(" + n + "px)"
                        }
                    }), t.css({
                        display: "none"
                    })
                }
            })
        },
        timeout: 300,
        interval: 50
    })
}

function blockUI(e) {
    e.block({
        message: "",
        css: {
            border: "none",
            padding: "0px",
            backgroundColor: "none"
        },
        overlayCSS: {
            backgroundColor: "#fff",
            opacity: .3,
            cursor: "wait"
        }
    })
}

function unblockUI(e) {
    e.unblock()
}

function attrDefault(e, t, a) {
    return "undefined" != typeof e.data(t) ? e.data(t) : a
}

function callback_test() {
    alert("Callback function executed! No. of arguments: " + arguments.length + "\n\nSee console log for outputed of the arguments."), console.log(arguments)
}

function setCurrentProgressTab(e, t, a, n, i) {
    a.prevAll().addClass("completed"), a.nextAll().removeClass("completed");
    var s = t.children().length,
        o = (parseInt((i + 1) / s * 100, 10), t.find("li:first-child")),
        r = o.find("span").position().left + "px";
    n.width(o.hasClass("active") ? 0 : a.prev().position().left - a.find("span").width() / 2), n.parent().css({
        marginLeft: r,
        marginRight: r
    });
    var l = o.find("span").position().left - o.find("span").width() / 2;
    e.find(".tab-content").css({
        marginLeft: l,
        marginRight: l
    })
}

function replaceCheckboxes() {
    $(".checkbox-replace:not(.neon-cb-replacement), .radio-replace:not(.neon-cb-replacement)").each(function(e, t) {
        var a = $(t),
            n = a.find("input"),
            i = $('<label class="cb-wrapper" />'),
            s = $('<div class="checked" />'),
            o = "checked",
            r = n.is('[type="radio"]'),
            l = n.attr("name");
        a.addClass("neon-cb-replacement"), n.wrap(i), i = n.parent(), i.append(s).next("label").on("click", function() {
            i.click()
        }), n.on("change", function() {
            r && $(".neon-cb-replacement input[type=radio][name='" + l + "']").closest(".neon-cb-replacement").removeClass(o), n.is(":disabled") && i.addClass("disabled"), a[n.is(":checked") ? "addClass" : "removeClass"](o)
        }).trigger("change")
    })
}

function scrollToBottom(e) {
    "string" == typeof e && (e = $(e)), e.get(0).scrollTop = e.get(0).scrollHeight
}

function elementInViewport(e) {
    for (var t = e.offsetTop, a = e.offsetLeft, n = e.offsetWidth, i = e.offsetHeight; e.offsetParent;) e = e.offsetParent, t += e.offsetTop, a += e.offsetLeft;
    return t >= window.pageYOffset && a >= window.pageXOffset && t + i <= window.pageYOffset + window.innerHeight && a + n <= window.pageXOffset + window.innerWidth
}

function disableXOverflow() {
    public_vars.$body.addClass("overflow-x-disabled")
}

function enableXOverflow() {
    public_vars.$body.removeClass("overflow-x-disabled")
}

function init_page_transitions() {
    var e = ["page-fade", "page-left-in", "page-right-in", "page-fade-only"];
    for (var t in e) {
        var a = e[t];
        if (public_vars.$body.hasClass(a)) return public_vars.$body.addClass(a + "-init"), void setTimeout(function() {
            public_vars.$body.removeClass(a + " " + a + "-init")
        }, 850)
    }
}

function onPageAppear(e) {
    var t, a, n;
    "undefined" != typeof document.hidden ? (t = "hidden", n = "visibilitychange", a = "visibilityState") : "undefined" != typeof document.mozHidden ? (t = "mozHidden", n = "mozvisibilitychange", a = "mozVisibilityState") : "undefined" != typeof document.msHidden ? (t = "msHidden", n = "msvisibilitychange", a = "msVisibilityState") : "undefined" != typeof document.webkitHidden && (t = "webkitHidden", n = "webkitvisibilitychange", a = "webkitVisibilityState"), (document[a] || "undefined" == typeof document[a]) && e(), document.addEventListener(n, e, !1)
}
var public_vars = public_vars || {};
! function(e, t) {
    "use strict";
    e(document).ready(function() {
        if (public_vars.$body = e("body"), public_vars.$pageContainer = public_vars.$body.find(".page-container"), public_vars.$chat = public_vars.$pageContainer.find("#chat"), public_vars.$horizontalMenu = public_vars.$pageContainer.find("header.navbar"), public_vars.$sidebarMenu = public_vars.$pageContainer.find(".sidebar-menu"), public_vars.$mainMenu = public_vars.$sidebarMenu.find("#main-menu"), public_vars.$mainContent = public_vars.$pageContainer.find(".main-content"), e(t).on("error", function() {
                init_page_transitions()
            }), setup_sidebar_menu(), setup_horizontal_menu(), public_vars.$sidebarMenu.find(".sidebar-collapse-icon").on("click", function(t) {
                t.preventDefault();
                var a = e(this).hasClass("with-animation");
                toggle_sidebar_menu(a)
            }), public_vars.$sidebarMenu.find(".sidebar-mobile-menu a").on("click", function(t) {
                t.preventDefault();
                var a = e(this).hasClass("with-animation");
                a ? public_vars.$mainMenu.stop().slideToggle("normal", function() {
                    public_vars.$mainMenu.css("height", "auto")
                }) : public_vars.$mainMenu.toggle()
            }), public_vars.$horizontalMenu.find(".horizontal-mobile-menu a").on("click", function(t) {
                t.preventDefault();
                var a = public_vars.$horizontalMenu.find(".navbar-nav"),
                    n = e(this).hasClass("with-animation");
                n ? a.stop().slideToggle("normal", function() {
                    a.attr("height", "auto"), "none" == a.css("display") && a.attr("style", "")
                }) : a.toggle()
            }), public_vars.$sidebarMenu.data("initial-state", public_vars.$pageContainer.hasClass("sidebar-collapsed") ? "closed" : "open"), is("tabletscreen") && hide_sidebar_menu(!1), e.isFunction(e.fn.niceScroll)) {
            var a = {
                cursorcolor: "#d4d4d4",
                cursorborder: "1px solid #ccc",
                railpadding: {
                    right: 3
                },
                cursorborderradius: 1,
                autohidemode: !0,
                sensitiverail: !0
            };
            public_vars.$body.find(".dropdown .scroller").niceScroll(a), e(".dropdown").on("shown.bs.dropdown", function() {
                e(".scroller").getNiceScroll().resize(), e(".scroller").getNiceScroll().show()
            });
            var n = e(".sidebar-menu.fixed");
            if (1 == n.length) {
                var i = 0;
                n.niceScroll({
                    cursorcolor: "#454a54",
                    cursorborder: "1px solid #454a54",
                    railpadding: {
                        right: 3
                    },
                    railalign: "right",
                    cursorborderradius: 1
                }), n.on("click", "li a", function() {
                    n.getNiceScroll().resize(), n.getNiceScroll().show(), t.clearTimeout(i), i = setTimeout(function() {
                        n.getNiceScroll().resize()
                    }, 500)
                })
            }
        }
        if (e.isFunction(e.fn.slimScroll) && e(".scrollable").each(function(t, a) {
                var n = e(a),
                    i = attrDefault(n, "height", n.height());
                n.is(":visible") && (n.removeClass("scrollable"), n.height() < parseInt(i, 10) && (i = n.outerHeight(!0) + 10), n.addClass("scrollable")), n.css({
                    maxHeight: ""
                }).slimScroll({
                    height: i,
                    position: attrDefault(n, "scroll-position", "right"),
                    color: attrDefault(n, "rail-color", "#000"),
                    size: attrDefault(n, "rail-width", 6),
                    borderRadius: attrDefault(n, "rail-radius", 3),
                    opacity: attrDefault(n, "rail-opacity", .3),
                    alwaysVisible: 1 == parseInt(attrDefault(n, "autohide", 1), 10) ? !1 : !0
                })
            }), e("body").on("click", '.panel > .panel-heading > .panel-options > a[data-rel="reload"]', function(e) {
                e.preventDefault();
                var t = jQuery(this).closest(".panel");
                blockUI(t), t.addClass("reloading"), setTimeout(function() {
                    unblockUI(t), t.removeClass("reloading")
                }, 900)
            }).on("click", '.panel > .panel-heading > .panel-options > a[data-rel="close"]', function(t) {
                t.preventDefault();
                var a = e(this),
                    n = a.closest(".panel"),
                    i = new TimelineLite({
                        onComplete: function() {
                            n.slideUp(function() {
                                n.remove()
                            })
                        }
                    });
                i.append(TweenMax.to(n, .2, {
                    css: {
                        scale: .95
                    }
                })), i.append(TweenMax.to(n, .5, {
                    css: {
                        autoAlpha: 0,
                        transform: "translateX(100px) scale(.95)"
                    }
                }))
            }).on("click", 'a[data-rel="collapse"]', function(t) {
                t.preventDefault();
                var a = e(this),
                    n = a.closest(".panel"),
                    i = n.children(".panel-body, .table"),
                    s = !n.hasClass("panel-collapse");
                n.is('[data-collapsed="1"]') && (n.attr("data-collapsed", 0), i.hide(), s = !1), s ? (i.slideUp("normal", fit_main_content_height), n.addClass("panel-collapse")) : (i.slideDown("normal", fit_main_content_height), n.removeClass("panel-collapse"))
            }), e('[data-toggle="buttons-radio"]').each(function() {
                var t = e(this).children();
                t.each(function(a, n) {
                    var i = e(n);
                    i.click(function() {
                        t.removeClass("active")
                    })
                })
            }), e('[data-toggle="buttons-checkbox"]').each(function() {
                var t = e(this).children();
                t.each(function(t, a) {
                    var n = e(a);
                    n.click(function() {
                        n.removeClass("active")
                    })
                })
            }), e("[data-loading-text]").each(function(t, a) {
                var n = e(a);
                n.on("click", function() {
                    n.button("loading"), setTimeout(function() {
                        n.button("reset")
                    }, 1800)
                })
            }), e('[data-toggle="popover"]').each(function(t, a) {
                var n = e(a),
                    i = attrDefault(n, "placement", "right"),
                    s = attrDefault(n, "trigger", "click"),
                    o = n.hasClass("popover-secondary") ? "popover-secondary" : n.hasClass("popover-primary") ? "popover-primary" : n.hasClass("popover-default") ? "popover-default" : "";
                n.popover({
                    placement: i,
                    trigger: s
                }), n.on("shown.bs.popover", function() {
                    var e = n.next();
                    e.addClass(o)
                })
            }), e('[data-toggle="tooltip"]').each(function(t, a) {
                var n = e(a),
                    i = attrDefault(n, "placement", "top"),
                    s = attrDefault(n, "trigger", "hover"),
                    o = n.hasClass("tooltip-secondary") ? "tooltip-secondary" : n.hasClass("tooltip-primary") ? "tooltip-primary" : n.hasClass("tooltip-default") ? "tooltip-default" : "";
                n.tooltip({
                    placement: i,
                    trigger: s
                }), n.on("shown.bs.tooltip", function() {
                    var e = n.next();
                    e.addClass(o)
                })
            }), e.isFunction(e.fn.knob) && e(".knob").knob({
                change: function() {},
                release: function() {},
                cancel: function() {},
                draw: function() {
                    if ("tron" == this.$.data("skin")) {
                        var e, t = this.angle(this.cv),
                            a = this.startAngle,
                            n = this.startAngle,
                            i = n + t,
                            s = 1;
                        return this.g.lineWidth = this.lineWidth, this.o.cursor && (n = i - .3) && (i += .3), this.o.displayPrevious && (e = this.startAngle + this.angle(this.v), this.o.cursor && (a = e - .3) && (e += .3), this.g.beginPath(), this.g.strokeStyle = this.pColor, this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a, e, !1), this.g.stroke()), this.g.beginPath(), this.g.strokeStyle = s ? this.o.fgColor : this.fgColor, this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, n, i, !1), this.g.stroke(), this.g.lineWidth = 2, this.g.beginPath(), this.g.strokeStyle = this.o.fgColor, this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + 2 * this.lineWidth / 3, 0, 2 * Math.PI, !1), this.g.stroke(), !1
                    }
                }
            }), e.isFunction(e.fn.slider) && e(".slider").each(function(t, a) {
                var n = e(a),
                    i = e('<span class="ui-label"></span>'),
                    s = i.clone(),
                    o = 0 != attrDefault(n, "vertical", 0) ? "vertical" : "horizontal",
                    r = attrDefault(n, "prefix", ""),
                    l = attrDefault(n, "postfix", ""),
                    c = attrDefault(n, "fill", ""),
                    u = e(c),
                    d = attrDefault(n, "step", 1),
                    p = attrDefault(n, "value", 5),
                    f = attrDefault(n, "min", 0),
                    h = attrDefault(n, "max", 100),
                    m = attrDefault(n, "min-val", 10),
                    v = attrDefault(n, "max-val", 90),
                    g = n.is("[data-min-val]") || n.is("[data-max-val]");
                if (g) {
                    n.slider({
                        range: !0,
                        orientation: o,
                        min: f,
                        max: h,
                        values: [m, v],
                        step: d,
                        slide: function(e, t) {
                            var a = (n.data("uiSlider").options, (r ? r : "") + t.values[0] + (l ? l : "")),
                                o = (r ? r : "") + t.values[1] + (l ? l : "");
                            i.html(a), s.html(o), c && u.val(a + "," + o)
                        }
                    });
                    var b = n.find(".ui-slider-handle");
                    i.html((r ? r : "") + m + (l ? l : "")), b.first().append(i), s.html((r ? r : "") + v + (l ? l : "")), b.last().append(s)
                } else {
                    n.slider({
                        range: attrDefault(n, "basic", 0) ? !1 : "min",
                        orientation: o,
                        min: f,
                        max: h,
                        value: p,
                        step: d,
                        slide: function() {
                            var e = n.data("uiSlider").options,
                                t = (r ? r : "") + e.value + (l ? l : "");
                            i.html(t), c && u.val(t)
                        }
                    });
                    var b = n.find(".ui-slider-handle");
                    i.html((r ? r : "") + p + (l ? l : "")), b.html(i)
                }
            }), e.isFunction(e.fn.bootstrapSwitch) && e(".make-switch.is-radio").on("switch-change", function() {
                e(".make-switch.is-radio").bootstrapSwitch("toggleRadioState")
            }), e.isFunction(e.fn.select2) && (e(".select2").each(function(t, a) {
                var n = e(a),
                    i = {
                        allowClear: attrDefault(n, "allowClear", !1)
                    };
                n.select2(i), n.addClass("visible")
            }), e.isFunction(e.fn.niceScroll) && e(".select2-results").niceScroll({
                cursorcolor: "#d4d4d4",
                cursorborder: "1px solid #ccc",
                railpadding: {
                    right: 3
                }
            })), e.isFunction(e.fn.selectBoxIt) && e("select.selectboxit").each(function(t, a) {
                var n = e(a),
                    i = {
                        showFirstOption: attrDefault(n, "first-option", !0),
                        "native": attrDefault(n, "native", !1),
                        defaultText: attrDefault(n, "text", "")
                    };
                n.addClass("visible"), n.selectBoxIt(i)
            }), e.isFunction(e.fn.autosize) && e("textarea.autogrow, textarea.autosize").autosize(), e.isFunction(e.fn.tagsinput) && e(".tagsinput").tagsinput(), e.isFunction(e.fn.typeahead) && e(".typeahead").each(function(t, a) {
                var n = e(a),
                    i = {
                        name: n.attr("name") ? n.attr("name") : n.attr("id") ? n.attr("id") : "tt"
                    };
                if (!n.hasClass("tagsinput")) {
                    if (n.data("local")) {
                        var s = n.data("local");
                        s = s.replace(/\s*,\s*/g, ",").split(","), i.local = s
                    }
                    if (n.data("prefetch")) {
                        var o = n.data("prefetch");
                        i.prefetch = o
                    }
                    if (n.data("remote")) {
                        var r = n.data("remote");
                        i.remote = r
                    }
                    if (n.data("template")) {
                        var l = n.data("template");
                        i.template = l, i.engine = Hogan
                    }
                    n.typeahead(i)
                }
            }), e.isFunction(e.fn.datepicker) && e(".datepicker").each(function(t, a) {
                var n = e(a),
                    i = {
                        format: attrDefault(n, "format", "mm/dd/yyyy"),
                        startDate: attrDefault(n, "startDate", ""),
                        endDate: attrDefault(n, "endDate", ""),
                        daysOfWeekDisabled: attrDefault(n, "disabledDays", ""),
                        startView: attrDefault(n, "startView", 0)
                    },
                    s = n.next(),
                    o = n.prev();
                n.datepicker(i), s.is(".input-group-addon") && s.has("a") && s.on("click", function(e) {
                    e.preventDefault(), n.datepicker("show")
                }), o.is(".input-group-addon") && o.has("a") && o.on("click", function(e) {
                    e.preventDefault(), n.datepicker("show")
                })
            }), e.isFunction(e.fn.timepicker) && e(".timepicker").each(function(t, a) {
                var n = e(a),
                    i = {
                        template: attrDefault(n, "template", !1),
                        showSeconds: attrDefault(n, "showSeconds", !1),
                        defaultTime: attrDefault(n, "defaultTime", "current"),
                        showMeridian: attrDefault(n, "showMeridian", !0),
                        minuteStep: attrDefault(n, "minuteStep", 15),
                        secondStep: attrDefault(n, "secondStep", 15)
                    },
                    s = n.next(),
                    o = n.prev();
                n.timepicker(i), s.is(".input-group-addon") && s.has("a") && s.on("click", function(e) {
                    e.preventDefault(), n.timepicker("showWidget")
                }), o.is(".input-group-addon") && o.has("a") && o.on("click", function(e) {
                    e.preventDefault(), n.timepicker("showWidget")
                })
            }), e.isFunction(e.fn.colorpicker) && e(".colorpicker").each(function(t, a) {
                var n = e(a),
                    i = {},
                    s = n.next(),
                    o = n.prev(),
                    r = n.siblings(".input-group-addon").find(".color-preview");
                n.colorpicker(i), s.is(".input-group-addon") && s.has("a") && s.on("click", function(e) {
                    e.preventDefault(), n.colorpicker("show")
                }), o.is(".input-group-addon") && o.has("a") && o.on("click", function(e) {
                    e.preventDefault(), n.colorpicker("show")
                }), r.length && (n.on("changeColor", function(e) {
                    r.css("background-color", e.color.toHex())
                }), n.val().length && r.css("background-color", n.val()))
            }), e.isFunction(e.fn.daterangepicker) && e(".daterange").each(function(t, a) {
                var n = {
                        Today: [moment(), moment()],
                        Yesterday: [moment().subtract("days", 1), moment().subtract("days", 1)],
                        "Last 7 Days": [moment().subtract("days", 6), moment()],
                        "Last 30 Days": [moment().subtract("days", 29), moment()],
                        "This Month": [moment().startOf("month"), moment().endOf("month")],
                        "Last Month": [moment().subtract("month", 1).startOf("month"), moment().subtract("month", 1).endOf("month")]
                    },
                    i = e(a),
                    s = {
                        format: attrDefault(i, "format", "MM/DD/YYYY"),
                        timePicker: attrDefault(i, "timePicker", !1),
                        timePickerIncrement: attrDefault(i, "timePickerIncrement", !1),
                        separator: attrDefault(i, "separator", " - ")
                    },
                    o = attrDefault(i, "minDate", ""),
                    r = attrDefault(i, "maxDate", ""),
                    l = attrDefault(i, "startDate", ""),
                    c = attrDefault(i, "endDate", "");
                i.hasClass("add-ranges") && (s.ranges = n), o.length && (s.minDate = o), r.length && (s.maxDate = r), l.length && (s.startDate = l), c.length && (s.endDate = c), i.daterangepicker(s, function(e, t) {
                    var a = i.data("daterangepicker");
                    i.is("[data-callback]") && callback_test(e, t), i.hasClass("daterange-inline") && i.find("span").html(e.format(a.format) + a.separator + t.format(a.format))
                })
            }), e.isFunction(e.fn.inputmask) && e("[data-mask]").each(function(t, a) {
                var n = e(a),
                    i = n.data("mask").toString(),
                    s = {
                        numericInput: attrDefault(n, "numeric", !1),
                        radixPoint: attrDefault(n, "radixPoint", ""),
                        rightAlignNumerics: "right" == attrDefault(n, "numericAlign", "left")
                    },
                    o = attrDefault(n, "placeholder", ""),
                    r = attrDefault(n, "isRegex", "");
                switch (o.length && (s[o] = o), i.toLowerCase()) {
                    case "phone":
                        i = "(999) 999-9999";
                        break;
                    case "currency":
                    case "rcurrency":
                        var l = attrDefault(n, "sign", "$");
                        i = "999,999,999.99", "rcurrency" == n.data("mask").toLowerCase() ? i += " " + l : i = l + " " + i, s.numericInput = !0, s.rightAlignNumerics = !1, s.radixPoint = ".";
                        break;
                    case "email":
                        i = "Regex", s.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}";
                        break;
                    case "fdecimal":
                        i = "decimal", e.extend(s, {
                            autoGroup: !0,
                            groupSize: 3,
                            radixPoint: attrDefault(n, "rad", "."),
                            groupSeparator: attrDefault(n, "dec", ",")
                        })
                }
                r && (s.regex = i, i = "Regex"), n.inputmask(i, s)
            }), e.isFunction(e.fn.validate) && e("form.validate").each(function(t, a) {
                var n = e(a),
                    i = {
                        rules: {},
                        messages: {},
                        errorElement: "span",
                        errorClass: "validate-has-error",
                        highlight: function(t) {
                            e(t).closest(".form-group").addClass("validate-has-error")
                        },
                        unhighlight: function(t) {
                            e(t).closest(".form-group").removeClass("validate-has-error")
                        },
                        errorPlacement: function(e, t) {
                            e.insertAfter(t.closest(".has-switch").length ? t.closest(".has-switch") : t.parent(".checkbox, .radio").length || t.parent(".input-group").length ? t.parent() : t)
                        }
                    },
                    s = n.find("[data-validate]");
                s.each(function(t, a) {
                    var n = e(a),
                        s = n.attr("name"),
                        o = attrDefault(n, "validate", "").toString(),
                        r = o.split(",");
                    for (var l in r) {
                        var c, u, d = r[l];
                        "undefined" == typeof i.rules[s] && (i.rules[s] = {}, i.messages[s] = {}), -1 != e.inArray(d, ["required", "url", "email", "number", "date", "creditcard"]) ? (i.rules[s][d] = !0, u = n.data("message-" + d), u && (i.messages[s][d] = u)) : (c = d.match(/(\w+)\[(.*?)\]/i)) && -1 != e.inArray(c[1], ["min", "max", "minlength", "maxlength", "equalTo"]) && (i.rules[s][c[1]] = c[2], u = n.data("message-" + c[1]), u && (i.messages[s][c[1]] = u))
                    }
                }), n.validate(i)
            }), e("input.file2[type=file]").each(function(t, a) {
                var n = e(a),
                    i = attrDefault(n, "label", "Browse");
                n.bootstrapFileInput(i)
            }), e.isFunction(e.fn.fileinput) && e(".fileinput").fileinput(), e.isFunction(e.fn.multiSelect) && e(".multi-select").multiSelect(), e.isFunction(e.fn.bootstrapWizard) && e(".form-wizard").each(function(a, n) {
                var i = e(n),
                    s = i.find(".steps-progress div"),
                    o = i.find("> ul > li.active").index(),
                    r = function() {
                        if (i.hasClass("validate")) {
                            var e = i.valid();
                            if (!e) return i.data("validator").focusInvalid(), !1
                        }
                        return !0
                    };
                i.bootstrapWizard({
                    tabClass: "",
                    onTabShow: function(e, t, a) {
                        setCurrentProgressTab(i, t, e, s, a)
                    },
                    onNext: r,
                    onTabClick: r
                }), i.data("bootstrapWizard").show(o), e(t).on("neon.resize", function() {
                    i.data("bootstrapWizard").show(o)
                })
            }), e.isFunction(e.fn.wysihtml5) && e(".wysihtml5").wysihtml5(), e.isFunction(e.fn.ckeditor) && e(".ckeditor").ckeditor(), replaceCheckboxes(), e(".tile-progress").each(function(t, a) {
                var n = e(a),
                    i = n.find(".pct-counter"),
                    s = n.find(".tile-progressbar span"),
                    o = parseFloat(s.data("fill")),
                    r = o.toString().length;
                if ("undefined" == typeof scrollMonitor) s.width(o + "%"), i.html(o);
                else {
                    var l = scrollMonitor.create(a);
                    l.fullyEnterViewport(function() {
                        s.width(o + "%"), l.destroy();
                        var e = {
                            pct: 0
                        };
                        TweenLite.to(e, 1, {
                            pct: o,
                            ease: Quint.easeInOut,
                            onUpdate: function() {
                                var t = e.pct.toString().substring(0, r);
                                i.html(t)
                            }
                        })
                    })
                }
            }), e(".tile-stats").each(function(t, a) {
                var n = e(a),
                    i = n.find(".num"),
                    s = attrDefault(i, "start", 0),
                    o = attrDefault(i, "end", 0),
                    r = attrDefault(i, "prefix", ""),
                    l = attrDefault(i, "postfix", ""),
                    c = attrDefault(i, "duration", 1e3),
                    u = attrDefault(i, "delay", 1e3);
                if (o > s)
                    if ("undefined" == typeof scrollMonitor) i.html(r + o + l);
                    else {
                        var d = scrollMonitor.create(a);
                        d.fullyEnterViewport(function() {
                            var e = {
                                curr: s
                            };
                            TweenLite.to(e, c / 1e3, {
                                curr: o,
                                ease: Power1.easeInOut,
                                delay: u / 1e3,
                                onUpdate: function() {
                                    i.html(r + Math.round(e.curr) + l)
                                }
                            }), d.destroy()
                        })
                    }
            }), e.isFunction(e.fn.tocify) && e("#toc").length) {
            e("#toc").tocify({
                context: ".tocify-content",
                selectors: "h2,h3,h4,h5"
            });
            var s = e(".tocify"),
                o = scrollMonitor.create(s.get(0));
            s.width(s.parent().width()), o.lock(), o.stateChange(function() {
                e(s.get(0)).toggleClass("fixed", this.isAboveViewport)
            })
        }
        public_vars.$body.on("click", '.modal[data-backdrop="static"]', function() {
            var t = e(this).find(".modal-dialog .modal-content"),
                a = new TimelineMax({
                    paused: !0
                });
            a.append(TweenMax.to(t, .1, {
                css: {
                    scale: 1.1
                },
                ease: Expo.easeInOut
            })), a.append(TweenMax.to(t, .3, {
                css: {
                    scale: 1
                },
                ease: Back.easeOut
            })), a.play()
        }), fit_main_content_height();
        var r = 0,
            l = function() {
                t.clearTimeout(r), fit_main_content_height(), r = setTimeout(l, 800)
            };
        l(), onPageAppear(init_page_transitions)
    });
    var a = 0;
    e(t).resize(function() {
        clearTimeout(a), a = setTimeout(trigger_resizable, 200)
    })
}(jQuery, window), jQuery(public_vars, {
    hover_index: 4
});