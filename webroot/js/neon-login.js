var neonLogin = neonLogin || {};
! function(n) {
    "use strict";
    n(document).ready(function() {
        n(".email1").change(function() {
            n(".email2").val(n(".email1").val())
        }), neonLogin.$container = n("#form_login"), neonLogin.$container.validate({
            rules: {
                username: {
                    required: !0
                },
                password: {
                    required: !0
                }
            },
            highlight: function(o) {
                n(o).closest(".input-group").addClass("validate-has-error")
            },
            unhighlight: function(o) {
                n(o).closest(".input-group").removeClass("validate-has-error")
            },
            submitHandler: function() {
                n.ajax({
                    type: neonLogin.$container.attr("method"),
                    url: neonLogin.$container.attr("action"),
                    data: neonLogin.$container.serialize(),
                    success: function(o) {
                      
                        "success" == o ? (n("#messages").fadeOut(), n(".login-page").addClass("logging-in"), setTimeout(function() {
                            var n = 25 + Math.round(30 * Math.random());
                            neonLogin.setPercentage(n, function() {
                                setTimeout(function() {
                                    neonLogin.setPercentage(100, function() {
                                        setTimeout("window.location.href = '"+SITE_URL+"users/dashboard' ", 0)
                                    }, 2)
                                }, 0)
                            })
                        }, 0)) : (n("#form_login").effect("shake", {
                            times: 2,
                            distance: 10
                        }, 300), n('.messages').html("<div class='alert alert-danger'><strong>Oh snap!</strong> Email or password is invalid.</div>") )
                    }
                })
            }
        });
        var o = n(".login-page").hasClass("is-lockscreen");
        if (o && (neonLogin.$container = n("#form_lockscreen"), neonLogin.$ls_thumb = neonLogin.$container.find(".lockscreen-thumb"), neonLogin.$container.validate({
                rules: {
                    password: {
                        required: !0
                    }
                },
                highlight: function(o) {
                    n(o).closest(".input-group").addClass("validate-has-error")
                },
                unhighlight: function(o) {
                    n(o).closest(".input-group").removeClass("validate-has-error")
                },
                submitHandler: function() {
                    n(".login-page").addClass("logging-in-lockscreen"), setTimeout(function() {
                        var n = 25 + Math.round(30 * Math.random());

                        neonLogin.setPercentage(n, function() {
                            setTimeout(function() {
                                neonLogin.setPercentage(100, function() {
                                    setTimeout("window.location.href = '../../'", 600)
                                }, 2)
                            }, 820)
                        })
                    }, 650)
                }
            })), neonLogin.$body = n(".login-page"), neonLogin.$login_progressbar_indicator = n(".login-progressbar-indicator h3"), neonLogin.$login_progressbar = neonLogin.$body.find(".login-progressbar div"), neonLogin.$login_progressbar_indicator.html("0%"), neonLogin.$body.hasClass("login-form-fall")) {
            var e = !1;
            setTimeout(function() {
                neonLogin.$body.addClass("login-form-fall-init"), setTimeout(function() {
                    e || (neonLogin.$container.find("input:first").focus(), e = !0)
                }, 550)
            }, 0)
        } else neonLogin.$container.find("input:first").focus();
        if (neonLogin.$container.find(".form-control").each(function(o, e) {
                var i = n(e),
                    t = i.closest(".input-group");
                i.prev(".input-group-addon").click(function() {
                    i.focus()
                }), i.on({
                    focus: function() {
                        t.addClass("focused")
                    },
                    blur: function() {
                        t.removeClass("focused")
                    }
                })
            }), n.extend(neonLogin, {
                setPercentage: function(n, e) {
                    if (n = parseInt(n / 100 * 100, 10) + "%", o) {
                        neonLogin.$lockscreen_progress_indicator.html(n);
                        var i = {
                            pct: g
                        };
                        return void TweenMax.to(i, .7, {
                            pct: parseInt(n, 10),
                            roundProps: ["pct"],
                            ease: Sine.easeOut,
                            onUpdate: function() {
                                neonLogin.$lockscreen_progress_indicator.html(i.pct + "%"), l(parseInt(i.pct, 10) / 100)
                            },
                            onComplete: e
                        })
                    }
                    neonLogin.$login_progressbar_indicator.html(n), neonLogin.$login_progressbar.width(n);
                    var i = {
                        pct: parseInt(neonLogin.$login_progressbar.width() / neonLogin.$login_progressbar.parent().width() * 100, 10)
                    };
                    TweenMax.to(i, .7, {
                        pct: parseInt(n, 10),
                        roundProps: ["pct"],
                        ease: Sine.easeOut,
                        onUpdate: function() {
                            neonLogin.$login_progressbar_indicator.html(i.pct + "%")
                        },
                        onComplete: e
                    })
                }
            }), o) {
            neonLogin.$lockscreen_progress_canvas = n("<canvas></canvas>"), neonLogin.$lockscreen_progress_indicator = neonLogin.$container.find(".lockscreen-progress-indicator"), neonLogin.$lockscreen_progress_canvas.appendTo(neonLogin.$ls_thumb);
            var i = neonLogin.$ls_thumb.width();
            neonLogin.$lockscreen_progress_canvas.attr({
                width: i,
                height: i
            }), neonLogin.lockscreen_progress_canvas = neonLogin.$lockscreen_progress_canvas.get(0);
            var t = neonLogin.lockscreen_progress_canvas,
                r = r = t.getContext("2d"),
                a = null,
                s = 2 * Math.PI,
                c = Math.PI / 2,
                g = 0;
            r.beginPath(), r.strokeStyle = "#eb7067", r.lineCap = "square", r.closePath(), r.fill(), r.lineWidth = 3, a = r.getImageData(0, 0, i, i);
            var l = function(n) {
                r.putImageData(a, 0, 0), r.beginPath(), r.arc(i / 2, i / 2, 70, -c, s * n - c, !1), r.stroke(), g = 100 * n
            };
            l(0), neonLogin.$lockscreen_progress_indicator.html("0%")
        }
    })
}(jQuery, window);
