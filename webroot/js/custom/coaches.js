$(document).ready(function(){
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/topTen.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
            
        for(var i in obj.tournament_stats)
              {
                
               $('#results').append(
                    $('<tr>')
                        .append($('<td>').append(parseInt(i)+parseInt(1)))  
                        .append($('<td>').append(obj.tournament_stats[i].names))
                        .append($('<td>').append(obj.tournament_stats[i].bat))
                        .append($('<td>').append( parseFloat(Math.round(obj.tournament_stats[i].avgData * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].slgData * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].OPS * 100) / 100).toFixed(3)))
                        .append($('<td>').append(obj.tournament_stats[i].RBI))
                        .append($('<td>').append(obj.tournament_stats[i].HR))
                        .append($('<td>').append(obj.tournament_stats[i].one_B))
                        .append($('<td>').append(obj.tournament_stats[i].two_B))
                        .append($('<td>').append(obj.tournament_stats[i].three_B))
                        .append($('<td>').append(obj.tournament_stats[i].BB))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].OBP * 100) / 100).toFixed(3)))
                        .append($('<td>').append(obj.tournament_stats[i].QAB))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].PIP * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].rgtavg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].lftavg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].rob0avg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].rob1avg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].rob2avg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].rob3avg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].out2savg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].strick2avg * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].fsem * 100) / 100).toFixed(1)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].ssem * 100) / 100).toFixed(1)))
                );
             }
            
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

$(document).on('click','#search',function(){  
$("#search_form").ajaxSubmit({

          beforeSend : function(){
            $('.animation_image_work').show();
          },
            success:function(obj)
            {
              var str ="";
              str += "<tr>" ;  
              str += "<th>Rank";
              str += "<th>Player";
              str += "<th>AT BATS";
              str += "<th>AVG";
              str += "<th>SLG";
              str += "<th>OPS";
              str += "<th>RBI";
              str += "<th>HR";
              str += "<th>1B";
              str += "<th>2B";
              str += "<th>3B";
              str += "<th>BB";
              str += "<th>OBP";
              str += "<th>QAB";
              str += "<th>PIP";
              str += "<th>RHP";
              str += "<th>LHP";
              str += "<th>ROB-0";
              str += "<th>ROB-1";
              str += "<th>ROB-2";
              str += "<th>ROB-3";
              str += "<th>2OUTS";
              str += "<th>2STRICK";
              str += "<th>GPA(1<sup>st</sup> Semester)";
              str += "<th>GPA(2<sup>nd</sup> Semester)";

              if(obj.tournament_stats.length==0){
               str += "<tr>" ;
               str += "<td>NO Result Found !!" ; 
               $('#results').html(str); 
              }else{ 
              for(var i in obj.tournament_stats)
              {
               
              str += "<tr>" ;
              str += "<td>"+(parseInt(i)+parseInt(1));  
              str += "<td>"+obj.tournament_stats[i].names;
              str += "<td>"+obj.tournament_stats[i].bat;
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].avgData * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].slgData * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].OPS * 100) / 100).toFixed(3);
              str += "<td>"+obj.tournament_stats[i].RBI;
              str += "<td>"+obj.tournament_stats[i].HR;
              str += "<td>"+obj.tournament_stats[i].one_B;
              str += "<td>"+obj.tournament_stats[i].two_B;
              str += "<td>"+obj.tournament_stats[i].three_B;
              str += "<td>"+obj.tournament_stats[i].BB;
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].OBP * 100) / 100).toFixed(3);
              str += "<td>"+obj.tournament_stats[i].QAB;
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].PIP * 100) / 100).toFixed(3);

              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].rgtavg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].lftavg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].rob0avg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].rob1avg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].rob2avg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].rob3avg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].out2savg * 100) / 100).toFixed(3);
              str += "<td>"+parseFloat(Math.round(obj.tournament_stats[i].strick2avg * 100) / 100).toFixed(3);

              str += "<td><center>"+parseFloat(Math.round(obj.tournament_stats[i].fsem * 100) / 100).toFixed(1);
              str += "<td><center>"+parseFloat(Math.round(obj.tournament_stats[i].ssem * 100) / 100).toFixed(1);

             }
               $('#results').html(str); 
             }
            },
            error:function(errorObj)
            {
              error_handle(errorObj,'.blank');
              
            },
          complete : function(){
            $('.animation_image_work').hide();
          }
        });
          
    });

$(function() {
    $( ".picdate" ).datepicker({
      maxDate: 0,
      changeMonth: true,
      changeYear: true,
      yearRange: "-50:+0",
     dateFormat: 'yy-mm-dd', 
    });
  });

});


function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}