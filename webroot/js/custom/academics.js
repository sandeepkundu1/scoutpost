$(document).ready(function(){
        
var now = new Date();
var years = now.getFullYear();
  get_academics(years);
  get_school();
  $('.bxslider').bxSlider({
    minSlides: 4,
            maxSlides: 4,
            slideWidth: 170,
            slideMargin: 10,
            controls: true,
            stopAuto: false,
            pager: false,
      auto: false

  });  
});


function get_academics(year)
{ 
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/fetch_academics_info.json',
          data:{'year':year},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
           var str="";
           
           if(obj.user_academic.id) {
           	    var first_gpa=obj.user_academic.first_sem_gpa;
           	    first_gpa=parseFloat(first_gpa);
           	    var scnd_gpa=obj.user_academic.second_sem_gpa;
           	    scnd_gpa=parseFloat(scnd_gpa);
               $("#first_sem_gpa").html("First Sem GPA: "+first_gpa.toPrecision(3));
               $("#second_sem_gpa").html("Second Sem GPA: "+scnd_gpa.toPrecision(3));
           } else {
               
               $("#first_sem_gpa").html("No Results found.");
           }
          },
          error:function(errorObj)
          {
             //alert('hi');
             $("#first_sem_gpa").html("No Results found.");
                //error_handle(errorObj,'.TerrorObj');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

    });
}

/*function get_school()
{ 

       $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_school_gallery.json',
          success: function(obj){
             if((obj.schoolDetail.length==0)) {
                          $('#getMoreFeature').hide();
              $('#no_rslt_load').show(); 
                }
              for(var i in obj.schoolDetail)
              {
                if(i<4){
                $('#getSchool').append(
                 $('<li style="display:inline-block;">').html('<img id="theImg" src="'+SITE_URL+"img/school_pic/"+obj.schoolDetail[i].photo+'" height="50" width="80"/>')
                );
              }else{
                $('#getSchool').append(
                 $('<li style="display:inline-block;">').html('<img id="theImg" src="'+SITE_URL+"img/school_pic/"+obj.schoolDetail[i].photo+'" height="50" width="80"/>')
                );
              }
             }
          },
          error:function(errorObj)
          {  
             $("#first_sem_gpa").html("No Results found.");
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

    });
}
*/

function get_school()
{ 

       $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_school_gallery.json',
          success: function(obj){


            for(var i in obj.schoolDetail){
                //alert(obj.schoolDetail[i].photo);
              $('.getSchool').append('<img src="'+SITE_URL+"img/school_pic/"+obj.schoolDetail[i].photo+'" height="150" width="270" id="big'+[i]+'"/>');
              $('.getSchools').append('<a href="#big'+[i]+'"><img src="'+SITE_URL+"img/school_pic/"+obj.schoolDetail[i].photo+'" id="big'+[i]+'"/></a>');
          }
              
             //$('#getSchool').src(SITE_URL+"img/school_pic");
          },
          error:function(errorObj)
          {  
             $("#first_sem_gpa").html("No Results found.");
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

    });
}