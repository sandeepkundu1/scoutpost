$(document).ready(function(){
$.ajax({
          type: "GET",
          url: SITE_URL+'api/market_feature.json',
          data:{'page':1,'limit':10},
          beforeSend : function(){
            $('.animation_image').show();
          },
          success: function(obj){
              
               if((obj.markets.length==0)) {
                          $('#getMoreFeature').hide();
                          $('#no_rslt_load').show(); 
                }
                       
              for(var i in obj.markets)
              {

               $('#results').append(
                    $('<tr>')
                        .append($('<td>').append((parseInt([i])+parseInt(1))))
                        .append($('<td>').append('<img id="theImg" src="'+obj.image_path+"/"+obj.markets[i].photo+'" height="150" width="200"/>'))
                        .append($('<td>').append(obj.markets[i].description))
                );
             } 
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.errorObj');
          },
          complete : function(){
            $('.animation_image').hide();
          }


});
  var srInterval =1;
  var track_load =1; //total loaded record group(s)
    $(document).on('click','#getMoreFeature',function() { //detect page scroll
       track_load++;
       srInterval+=10;

      if(track_load <= 1000000) //there's more data to load
            {
                loading = true; //prevent further ajax loading
                var loader = '<img src="'+SITE_URL+'img/loadinga.gif">';
                
                $('.animation_image').show(loader); //show loading image

           //load data from the server using a HTTP POST request
                $.get(''+SITE_URL+'api/market_feature.json',{'page': track_load,'limit': 10}, function(data){

                       if((data.markets.length==0)) {
                          $('#getMoreFeature').hide();
                          $('#no_rslt_dv').show();
                          
                       }

                       for(var i in data.markets)
                        {
                          var srNo = srInterval;
                        $('#results').append(
                          $('<tr>')
                          .append($('<td>').append((parseInt([i])+parseInt(srNo))))
                          .append($('<td>').append('<img id="theImg" src="'+data.image_path+"/"+data.markets[i].photo+'" height="150" width="200"/>'))
                          .append($('<td>').append(data.markets[i].description))
                          );
                        srNo++;
                        } 
                      

                    //hide loading image
                   $('.animation_image').hide(loader); //hide loading image once data is received

                    

                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?

                    //alert(thrownError); //alert with HTTP error
                    $('.animation_image').hide(); //hide loading image
                   

                });

            }

    });


});