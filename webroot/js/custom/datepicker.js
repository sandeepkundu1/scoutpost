$(document).ready(function(){
$(function() {
    $( "#date-picker-1" ).datepicker({
    	maxDate: 0,
      changeMonth: true,
      changeYear: true,
      yearRange: "-50:+0",
     dateFormat: 'yy-mm-dd', 
    });
  });
});