$(document).ready(function(){

  //window.location.href=SITE_URL+'api/paypalForm.json';

  $(document).on('click','#button_show',function(){
        $("#registration_form").ajaxSubmit({
           beforeSend : function(){
            $('.animation_image').show();
          },
          success:function(obj)
            {
              
              $("#button_show").button('reset');

               sendType(obj.lastId); 
            },
            error:function(errorObj)
            {
              $("#button_show").button('reset');
                error_handle(errorObj,'.errorObj');
                setTimeout(function() {
            $(".errorObj").hide('blind', {}, 500)
              }, 5000);    
            },
          complete : function(){
            $('.animation_image').hide();
          }
        });
          });

$(document).on('change','#passingYear',function(){
var passingYear = $('#passingYear').val();
if(passingYear==0){
  $('#passingYearbox').show();
  $('#passingYear').hide();
}
});

  
$('#button_show').attr('disabled','disabled');

 $('#agree').click(function(){

 var set = $('#agree').is(":checked");

if(set==true){
 $('#button_show').removeAttr('disabled');
 } else{

$('#button_show').attr('disabled','disabled');

 }

 });

 $('#shownext').hide();
$('#nextFormFill').click(function(){
$('#shownext').show();
$('#previous').hide();
});

$('#backs').click(function(){
$('#previous').show();
$('#shownext').hide();
});

/*
$( "#loginFormId" ).submit(function( event ) {
event.preventDefault();

var str = $( "form" ).serialize();
    
 
        $.ajax({
          type: "POST",
          url: SITE_URL+'api/login.json',
          data: str,
          dataType: 'json',
          success: function(obj){
              
              alert('login successfully');
               window.location.href=SITE_URL+'categories/profile';
             
            
          },
          error:function(errorObj)
          {
             
                 alert('fail! username/password is incorrect');
          }
         
        });
  });
*/

// forgot password 

$( "#forgetPassword" ).submit(function( event ) {
event.preventDefault();

var str = $( "form" ).serialize();
$.ajax({
          type: "POST",
          url: SITE_URL+'api/forgot_password.json',
          data: str,
          dataType: 'json',
          beforeSend : function(){
            $('.animation_image').show();
          },
          success: function(obj){
              
              alert('Please check your mail for reset your password');
               window.location.href=SITE_URL+'categories/userLogin';
             
            
          },
          error:function(errorObj)
          {
                var error='<span><em class="text-red">*</em>';
                alert(errorObj.responseJSON.message.error);
                
          },
          complete : function(){
            $('.animation_image').hide();
          }
         
        });

});

$( "#resetPassword" ).submit(function( event ) {
event.preventDefault();

var ident = $("#identId").val()
var activate = $("#activateCode").val()
var str = $( "form" ).serialize();
console.log(str);
$.ajax({
          type: "POST",
          url: SITE_URL+'api/reset_password.json?ident='+ident+'&activate='+activate,
          data: str,
          dataType: 'json',
           beforeSend : function(){
            $('.animation_image').show();
          },
          success: function(obj){
              
               window.location.href=SITE_URL+'categories/userLogin';
             
            
          },
          error:function(errorObj)
          {
              error_handle(errorObj,'.errorObj');
          },
          complete : function(){
            $('.animation_image').hide();
          }
         
        });

});

function sendType(id){

var userId = id;
  $.ajax({
          type: "GET",
          url: SITE_URL+'api/paypalForm.json',
          data: {id:userId},
          dataType: 'json',
          beforeSend : function(){
            $('.animation_image').show();
            $('#button_show').attr('disabled','disabled');
            $('#backs').attr('disabled','disabled');
            $('#reset').attr('disabled','disabled');
          },
          success: function(obj){

         //console.log(obj.plan); 
          window.location.href=obj.plan; 
          },
          error : function(error){
            alert('error');
          }
        });
}
//sendType(53) ;
var inkr=0;
$("#addivisions").click(function(){
        inkr++;

        $("#selectDiv").append('<div class="form-group"><div class="col-sm-12 nopading-spc"><div class="input text"><input type="text" id="userprofiles-curr-team" placeholder="Current Teams" rows="2" class="form-control siming-spc curTeam'+inkr+'" name="teamdivision[current_team][]" onkeyup="autocomplet('+inkr+');"></div><div><select id="divisions" class="form-control siming-spc" name="teamdivision[division][]"><option value="">(select Division)</option><option value="Major">Major</option><option value="Minor">Minor</option><option value="AAA">AAA</option><option value="AA">AA</option><option value="All-Star">All-Star</option><option value="Senior">Senior</option><option value="Show-Case">Show Case</option><option value="Freshman">Freshman</option><option value="JV">JV</option><option value="Varsity">Varsity</option><option value="Bronco">Bronco</option><option value="Mustang">Mustang</option><option value="Pinto">Pinto</option></select></div></div><div class="col-sm-12 nopading-spc"><ul class="list listitem'+inkr+'"></ul><a href="javascript:void(0);" class="remCF">Remove</a></div></div>');
        //$("#add-new-div").append('<tr valign="top"><td><input type="text" class="form-control" id="customFieldName" name="customFieldName[]" value="" placeholder="Enter subject Name" /> &nbsp; <input type="text" class="code" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Input Value" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
      
    });
    $("#selectDiv").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });

});
function autocomplet(inkr) {
  
 var min_length = 0; // min caracters to display the autocomplete
  
    var make="Teamdivision";
    var filter="current_team";
    var keyword = $('.curTeam'+inkr).val();
    var urlForThis='api/autocomplete.json'; 
 
  if (keyword.length > min_length) {
    $.ajax({
      url: urlForThis,
      type: 'GET',
      data: {q:keyword,types:make,filter:filter,inkr:inkr},
      dataType: 'html',
      beforeSend:function(){
        $('.curTeam'+inkr).addClass('loadinggif');
      },
     success:function(data){

        $('.listitem'+inkr).show();
        $('.listitem'+inkr).html(data);
         setTimeout(function() {
          $('.listitem'+inkr).hide('blind', {}, 500)
           }, 5000);

      },
      error:function(errorObj){
        console.log('No Matching Data');
      },
      complete : function(){
     
          $('.curTeam'+inkr).removeClass('loadinggif');
     }
    });
  } else {
    $('.listitem'+inkr).hide();
  }
}

function set_item(item,type,number) {
  // change input value
  var t = item.toUpperCase();
  
  
  $('.curTeam'+number).val(t);
  $('.listitem'+number).hide();
}



 

