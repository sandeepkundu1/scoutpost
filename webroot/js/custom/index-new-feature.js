$(document).ready(function(){
$.ajax({
          type: "GET",
          data: {'page':1,'limit':2},
          url: SITE_URL+'/api/get_features.json',
          success: function(obj){
              
            $('.images').prepend("<p>"+obj.features[1].description+"</p>");
            $('.images').prepend('<img id="theImg" src="'+obj.image_path+"/"+obj.features[1].photo+'" height="100" width="100"/>');
            $('.images2').prepend("<p>"+obj.features[0].description+"</p>");
            $('.images2').prepend('<img id="theImg" src="'+obj.image_path+"/"+obj.features[0].photo+'" height="100" width="100"/>');  
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.errorObj');
          }

});

$.ajax({
          type: "GET",
          url: SITE_URL+'api/topTen.json',
          success: function(obj){
                 
        for(var i in obj.tournament_stats)
              {
                
               $('#results').append(
                    $('<tr>')
                        .append($('<td>').append(parseInt(i)+parseInt(1)))  
                        .append($('<td>').append(obj.tournament_stats[i].names))
                        .append($('<td>').append(obj.tournament_stats[i].bat))
                        .append($('<td>').append( parseFloat(Math.round(obj.tournament_stats[i].avgData * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].slgData * 100) / 100).toFixed(3)))
                        .append($('<td>').append(parseFloat(Math.round(obj.tournament_stats[i].OPS * 100) / 100).toFixed(3)))
                        .append($('<td>').append(obj.tournament_stats[i].RBI))
                        .append($('<td>').append(obj.tournament_stats[i].HR))
                );
             }
          },
          error:function(errorObj)
          {
             
                console.log('error');
          }

});
});


