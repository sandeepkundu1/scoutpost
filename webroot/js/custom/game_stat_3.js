function urlParam(name){
    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    return results[1] || 0;
}
var match_type_url = urlParam('tournament_id');

var game_id = urlParam('game_id');
var tournament_id = urlParam('tournament_id');
$(document).ready(function(){
        $.ajax({
          type: "GET",
          url: SITE_URL+'api/fetch_at_bat_total_per_tounament.json',
          data:{'tounament_id':tournament_id,'game_id':game_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){

  var total_hits=parseInt(obj.AtbatTotal.no_of_hr)+parseInt(obj.AtbatTotal.no_of_3b)+parseInt(obj.AtbatTotal.no_of_2b)+parseInt(obj.AtbatTotal.no_of_1b);
             
              $('#txtfortype').val(obj.AtbatTotal.game_type);
              if(obj.AtbatTotal.game_type==1){
                var Type = 'tournament';
              }else if(obj.AtbatTotal.game_type==2){
              var Type = 'League'; 
              }

              $('.gameType').html(Type);
             
              var options = obj.AtbatTotal.game_date;
				
       if(options != '' || options != null || options !=NaN || options !=undefined) {
           var split_arr=options.split("T");
            var my_date=split_arr[0];
          
           
         if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[0]+'-'+arr[1]+'-'+arr[2];
              
              var options = d;
              var yyyy=arr[0];
            }
}
           $('.gameDate').html(options);


              $('.team1').html(obj.AtbatTotal.team_1);
              $('.team2').html(obj.AtbatTotal.oponent);
              $('#ab').html(obj.AtbatTotal.total_at_bat);
              $('#1b').html(obj.AtbatTotal.no_of_1b);
              $('#2b').html(obj.AtbatTotal.no_of_2b);
              $('#3b').html(obj.AtbatTotal.no_of_3b);
              $('#hr').html(obj.AtbatTotal.no_of_hr);
              $('#bb').html(obj.AtbatTotal.no_of_bb);
              $('#hits').html(total_hits);
              $('#runs').html(obj.AtbatTotal.runs);
              $('#rbi').html(obj.AtbatTotal.rbi);

              

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj1');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

        $.ajax({
          type: "GET",
          url: SITE_URL+'api/single_game_video.json',
          data:{'tounament_id':tournament_id,'game_id':game_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){

             var row="";
             var str="";
             var video_name = "";
             var id = "";
            var video_str='';
            var str=1;    
                

              for(var i in obj.GameDetail){
                

              //str  = obj.GameDetail[i].at_bat;
              
              row  = obj.GameDetail[i].at_bat_result;
              video_name =obj.GameDetail[i].file_name;
               id+=obj.GameDetail[i].stat_id;
              $(".getVideo").append('<p>AT BAT#'+str+' - '+'result'+' '+row);
              
              if(obj.GameDetail[i].file_name !='') { 
                
                var vardiv ='<iframe frameborder="0" height="281"  allowfullscreen="" src="https://www.youtube.com/embed/'+obj.GameDetail[i].file_name+'?rel=0"  class="video_mang"></iframe>';  
              $('.getVideo').append(vardiv);
              
                }else {
                    $('.getVideo').append('No Video Found...');
                }
                ++str;
              }
             
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj2');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

var obj=new Date();
var date = obj.getFullYear();
var requested_url=(match_type_url!="null") ? SITE_URL+'api/fetch_tournament_stats.json':SITE_URL+'api/fetch_league_stats.json';
          $.ajax({
          type: "GET",
          url: requested_url,
          data:{'year':date,'tournament_id':tournament_id,'game_id':game_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
          $('#avg2').html(obj.tournament_stats.avg);
          $('#slg2').html(obj.tournament_stats.slg);
          $('#obp2').html(obj.tournament_stats.obp);
          $('#ops2').html(obj.tournament_stats.ops);
          $('#tb2').html(obj.tournament_stats.tb);
          $('#rc2').html(obj.tournament_stats.rc);
          $('#pa2').html(obj.tournament_stats.pa);
          $('#pip2').html(obj.tournament_stats.pip);
          $('#rhp2').html(obj.tournament_stats.AVG_VS_RHP);
          $('#lhp2').html(obj.tournament_stats.AVG_VS_LHP);
          $('#avgw2').html(obj.tournament_stats.wrob);
          $('#qab2').html(obj.tournament_stats.qab);

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj3');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});




});
function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}

 

