$(function () {
    
   load_workout_graph('week');
   load_practice_graph('week');
});


function load_workout_graph(type) {
    
     $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_skills.json',
          data: {"skill_type":1,"day_type":type},
          dataType: 'json',
          beforeSend : function(){
            //$('.animation_image_acadmic').show();
          },
          success: function(obj){
            if(type=="week") {
                categories_arr= [
                        'Sun',
                        'Mon',
                        'Tue',
                        'Wed',
                        'Thu',
                        'Fri',
                        'Sat',
                         ];
                 title='Weekly Workout Chart';
                 sub_title='Time in minutes';
            } else if(type=="month") {
                title='Monthly Workout Chart';
                sub_title='Time in hours';
                categories_arr= ['First Week','Second Week','Third Week','Fourth Week','Fifth Week'];
                
            } else if(type=="year") {
                title='Yearly Workout Chart';
                sub_title='Time in hours';
                categories_arr=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            }
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: title
                },
                /*subtitle: {
                    text: 'Source: WorldClimate.com'
                },*/
                xAxis: {
                    categories: categories_arr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: sub_title
                    }
                },
               
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: obj.skills
              
            });
           
           
           
          },
          error:function(errorObj)
          {
             
             console.log(errorObj);
                //error_handle_acadmic(errorObj,'.errorObj');
                
          },
          complete : function(){
            //$('.animation_image_acadmic').hide();
          }
         
     });
    
}


function load_practice_graph(type) {
    
     $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_practice_skills.json',
          data: {"skill_type":1,"day_type":type},
          dataType: 'json',
          beforeSend : function(){
            //$('.animation_image_acadmic').show();
          },
          success: function(obj){
              
             // alert(obj.skills.length)
               if(type=="week") {
                categories_arr= [
                        'Sun',
                        'Mon',
                        'Tue',
                        'Wed',
                        'Thu',
                        'Fri',
                        'Sat',
                         ];
                 title='Weekly Practice Chart';
                 sub_title='Time in minutes';
            } else if(type=="month") {
                
                title='Monthly Practice Chart';
                sub_title='Time in hours';
                
                categories_arr= ['First Week','Second Week','Third Week','Fourth Week','Fifth Week'];
                
            } else if(type=="year") {
                title='Yearly Practice Chart';
                sub_title='Time in hours';
                categories_arr=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            }
            $('#container_two').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: title
                },
                /*subtitle: {
                    text: 'Source: WorldClimate.com'
                },*/
                xAxis: {
                    categories: categories_arr,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: sub_title
                    }
                },
               
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: obj.skills
              
            });
           
           
           
          },
          error:function(errorObj)
          {
             
             console.log(errorObj);
                //error_handle_acadmic(errorObj,'.errorObj');
                
          },
          complete : function(){
            //$('.animation_image_acadmic').hide();
          }
         
     });
    
}