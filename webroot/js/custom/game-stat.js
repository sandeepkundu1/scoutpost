$(document).ready(function(){
var years = new Date().getFullYear();
$('.year_prev').html(years);
getStats(years);
$('#previousYear').show();
$('#previousYear1').hide();
});

$(document).ready(function(){
$('#previousYear').on('click' ,function(){
  var now = new Date();
var past = now.setFullYear(now.getFullYear() - 1, 1);
var years = now.getFullYear();
$('.year_prev').html(years);
getStats(years);
$('#previousYear1').show();
$('#previousYear').hide();
});
});

$(document).ready(function(){
$('#previousYear1').on('click' ,function(){

location.reload();
 });
 }); 
function getStats(year)
{ 
var stat_type = 'stat_type';
var game_type = 1;  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_games_count_of_stats.json',
          data:{'year':year,'stat_type':stat_type,'game_type':game_type},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
              var str="";
            for(var i in obj.StatsDetail){
              if(obj.StatsDetail[i].no_of_games==1 || obj.StatsDetail[i].no_of_games==0){
                var NO = 'Game';
              }else if(obj.StatsDetail[i].no_of_games){
              var NO = 'Games'; 
              }
              
               var options = obj.StatsDetail[i].UserGames.game_date;
               
if(options != '' || options != null || options !=NaN || options !=undefined) {
           var split_arr=options.split("T");
            var my_date=split_arr[0];
          
           
         if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[2]+'-'+arr[1]+'-'+arr[0];
              
              var options = d;
              var yyyy=arr[0];
            }
}
            str +="<li><a href='"+SITE_URL+"Categories/game_stat_2?date="+obj.StatsDetail[i].UserGames.game_date+"&id="+obj.StatsDetail[i].tournaments_id+"&type=1' class='tournament-link'>"+obj.StatsDetail[i].tour_name+'  -  '+obj.StatsDetail[i].no_of_games+ ' ' +NO;
            }
            $('#getTDate').html(str);

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.TerrorObj');
                $('#getTDate').hide();                
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});


var stat_type = 'stat_type';
var date_link = '';
var game_type = 2;  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_games_count_of_stats_league.json',
          data:{'year':year,'stat_type':stat_type,'game_type':game_type},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
            var strL="";
            for(var i in obj.StatsDetail){

              if(obj.StatsDetail[i].no_of_games==1 || obj.StatsDetail[i].no_of_games==0){
                var NO = 'Game';
              }else if(obj.StatsDetail[i].no_of_games){
              var NO = 'Games'; 
              }
              
               var options = obj.StatsDetail[i].game_date;
			   if(options != '' || options != null || options !=NaN || options !=undefined) {
				   var split_arr=options.split("T");
				    var my_date=split_arr[0];
					
					  /*var dte = new Date(my_date);
					  var dd = dte.getDate();
					  var mm = dte.getMonth()+1; //January is 0!
					  var yyyy = dte.getFullYear();
					  if(dd<10){
						  dd='0'+dd
					  } 
					  if(mm<10){
						  mm='0'+mm
					  } 
					  options = mm+'-'+dd+'-'+yyyy;*/
            
            if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[1]+'-'+arr[2]+'-'+arr[0];
              options =d;
              date_link = d;
              var d2= arr[0]+'-'+arr[1]+'-'+arr[2];
              date_link2 = d2;    

            }
			   }
			 // alert(options);
			 
		  
         // var date_link = yyyy+'-'+mm+'-'+dd;
            strL += "<li><a href='"+SITE_URL+"Categories/game_stat_2?date="+date_link2+"&id=2&type=2' class='tournament-link'>"+options+'  -  '+obj.StatsDetail[i].no_of_games+ ' ' +NO;
            
            }
            $('#getLDate').html(strL);
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj1');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

var game_id=0;
var game_type = 1;  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/fetch_tournament_stats.json',
          data:{'year':year,'tournament_id':0,'game_type':game_type},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
          $('#avg1').html(obj.tournament_stats.avg);
          $('#slg1').html(obj.tournament_stats.slg);
          $('#obp1').html(obj.tournament_stats.obp);
          $('#ops1').html(obj.tournament_stats.ops);
          $('#tb1').html(obj.tournament_stats.tb);
          $('#rc1').html(obj.tournament_stats.rc);
          $('#pa1').html(obj.tournament_stats.pa);
          $('#pip1').html(obj.tournament_stats.pip);
          $('#rhp1').html(obj.tournament_stats.AVG_VS_RHP);
          $('#lhp1').html(obj.tournament_stats.AVG_VS_LHP);
          $('#avgw1').html(obj.tournament_stats.wrob);
          $('#qab1').html(obj.tournament_stats.qab);

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj2');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
var game_id = 0;
var game_type = 2;  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/fetch_league_stats.json',
          data:{'year':year,'tournament_id':0,'game_type':game_type,'game_id':game_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
          $('#avg2').html(obj.tournament_stats.avg);
          $('#slg2').html(obj.tournament_stats.slg);
          $('#obp2').html(obj.tournament_stats.obp);
          $('#ops2').html(obj.tournament_stats.ops);
          $('#tb2').html(obj.tournament_stats.tb);
          $('#rc2').html(obj.tournament_stats.rc);
          $('#pa2').html(obj.tournament_stats.pa);
          $('#pip2').html(obj.tournament_stats.pip);
          $('#rhp2').html(obj.tournament_stats.AVG_VS_RHP);
          $('#lhp2').html(obj.tournament_stats.AVG_VS_LHP);
          $('#avgw2').html(obj.tournament_stats.wrob);
          $('#qab2').html(obj.tournament_stats.qab);

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj3');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});


function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}

var tournament_id = 0;
var game_id= 0;
var game_type = 1;  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_games_at_bat_total.json',
          data:{'year':year,'game_type':game_type,'tournament_id':tournament_id,'game_id':game_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){


              var total_hits=parseInt(obj.AtbatTotal.no_of_hr)+parseInt(obj.AtbatTotal.no_of_3b)+parseInt(obj.AtbatTotal.no_of_2b)+parseInt(obj.AtbatTotal.no_of_1b);
            $("#abc").html(
              "<td>"+obj.AtbatTotal.ab+
              "<td>"+obj.AtbatTotal.one_b+
              "<td>"+obj.AtbatTotal.two_b+
              "<td>"+obj.AtbatTotal.three_b+
              "<td>"+obj.AtbatTotal.hr+
              "<td>"+obj.AtbatTotal.hits+
              "<td>"+obj.AtbatTotal.runs
              );
            $("#def").html(
              "<td>"+obj.AtbatTotal.bb+
              "<td>"+obj.AtbatTotal.hbp+
              "<td>"+obj.AtbatTotal.roe+
              "<td>"+obj.AtbatTotal.so+
              "<td>"+obj.AtbatTotal.sac+
              "<td>"+obj.AtbatTotal.sb+
              "<td>"+obj.AtbatTotal.rbi
              );
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj4');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

var tournament_id = 0;
var game_type = 2; 
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_games_at_bat_total_league.json',
          data:{'year':year,'game_type':game_type,'tournament_id':tournament_id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){

  var total_hits=parseInt(obj.AtbatTotal.no_of_hr)+parseInt(obj.AtbatTotal.no_of_3b)+parseInt(obj.AtbatTotal.no_of_2b)+parseInt(obj.AtbatTotal.no_of_1b);
             $("#abc1").html(
              "<td>"+obj.AtbatTotal.ab+
              "<td>"+obj.AtbatTotal.one_b+
              "<td>"+obj.AtbatTotal.two_b+
              "<td>"+obj.AtbatTotal.three_b+
              "<td>"+obj.AtbatTotal.hr+
              "<td>"+obj.AtbatTotal.hits+
              "<td>"+obj.AtbatTotal.runs
              );
            $("#def1").html(
              "<td>"+obj.AtbatTotal.bb+
              "<td>"+obj.AtbatTotal.hbp+
              "<td>"+obj.AtbatTotal.roe+
              "<td>"+obj.AtbatTotal.so+
              "<td>"+obj.AtbatTotal.sac+
              "<td>"+obj.AtbatTotal.sb+
              "<td>"+obj.AtbatTotal.rbi
              );
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj5');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

}