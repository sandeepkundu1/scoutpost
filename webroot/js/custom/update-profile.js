$(document).ready(function(){

      var flag='pause';
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/temp_game_check.json',
          data:{'flag':flag},
          dataType:'json',
          
            success:function(obj)
            {
              $('#pause').hide();
              $('#user_stats_save').hide();
              $("#dialog").dialog({
              title: "Game State is Pause .",
            buttons: {
                  Close: function () {
                    $(this).dialog('close');
                  },
                    Open: function () {
                      $( "#resume" ).trigger( "click" );
                      $('#pause').show();
                      $('#user_stats_save').show();
                      $(this).dialog('close');
                      }
                   }
                });
            }
        });
  
        
$('#hide-gpa2').hide();
$('#calGpa').click(function(){
var sum=0;
var count="";
$(".gpatotal").each( function(i,n){
//alert(n.value);
var selName = $(n).attr('name');

if(n.value>=0 && selName=='sub[grade]'){
  
  count = i;
}

sum = sum+parseFloat(n.value);

});

if(count==0 && count<1){
count =parseInt(count)+1;
}else if(count>1){
count = (parseInt(count)+2)/2;
}

var result="";
result=sum/count;

var r = $('#userSem').val();

if(r==1){
$('#Gpa1').val(result);
}else if(r==2){
$('#Gpa2').val(result);
}
});
$("#userSem").change(function() {
     var tab=$("#userSem").val();

     if(tab==1){
     $('#hide-gpa1').show();
     $('#hide-gpa2').hide();
      }else if(tab==2){
     $('#hide-gpa1').hide();
     $('#hide-gpa2').show();
   } 
});

var mkId =1;
    $("#addCF").click(function(){
        
        $("#add-new-div").append('<div class="form-group"><div class="col-sm-3"><div class="input text"><input type="text" placeholder="Enter subject name" id="usersub" class="form-control" name="sub[subject]"></div></div><div class="col-sm-3"><select id="userSems" class="userSem_'+mkId+' form-control gpatotal" name="sub[grade]"><option value="4">A</option><option value="3">B</option><option value="2">C</option><option value="1">D</option><option value="0">F</option></select></div><div class="col-sm-3"><select id="userWts" class="userWt_'+mkId+' form-control gpatotal" name="sub[weight]"><option value="0">Regular</option><option value="0.5">Honors</option><option value="1">College</option></select></div><div class="col-sm-3"><a href="javascript:void(0);" class="remCF">Remove</a></div></div>');
        //$("#add-new-div").append('<tr valign="top"><td><input type="text" class="form-control" id="customFieldName" name="customFieldName[]" value="" placeholder="Enter subject Name" /> &nbsp; <input type="text" class="code" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Input Value" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
    ++mkId;   
    });
    $("#add-new-div").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });


$( "#acadmics_form" ).submit(function( event ) {
event.preventDefault();
var str = $( "form" ).serialize();    
 
        $.ajax({
          type: "POST",
          url: SITE_URL+'api/save_academics.json',
          data: str,
          dataType: 'json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
            $('.succobj').text(obj.message.success);
            $('.errorObj').hide();
            $('.succobj').show();
            setTimeout(function() {
            $(".succobj").hide('blind', {}, 500)
              }, 5000);
          },
          error:function(errorObj)
          {
             
                error_handle_acadmic(errorObj,'.errorObj');
                $('.succobj').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }
         
          });
       });

      var year = $('#userYear').val();  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/get_academics_info.json',
          data:{'year':year},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
               $('#Gpa1').val(obj.acedimics[0].first_sem_gpa);
               $('#Gpa2').val(obj.acedimics[0].second_sem_gpa);
          },
          error:function(errorObj)
          {
             
                error_handle_acadmic(errorObj,'.errorObj');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

$(function() {
    $( ".workdateClass" ).datepicker({ 
      dateFormat: 'yy-mm-dd',
      maxDate: 0,
       });
    $( ".workdate" ).datepicker({ 
      dateFormat: 'yy-mm-dd',
      maxDate: 0,
      minDate:-dateTime
       });
    $(".worktimeClass").timepicker({
    'minTime': '00:00am',
    'maxTime': '11:30pm',
    'showDuration': true
  });
  });

$(document).on('change','#userYear',function(){
var year = $('#userYear').val();  
$.ajax({
          type: "GET",
          url: SITE_URL+'api/get_academics_info.json',
          data:{'year':year},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
              
               for(var i in obj.acedimics){
               $('#Gpa1').val(obj.acedimics[i].first_sem_gpa);
               $('#Gpa2').val(obj.acedimics[i].second_sem_gpa);
             }
          },
          error:function(errorObj)
          {
             
                error_handle_acadmic(errorObj,'.errorObj');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
});
 //function add route
    $(document).on('click','#work_save',function(){
        $("#workout_form").ajaxSubmit({
          beforeSend : function(){
            $('.animation_image_work').show();
          },
            success:function(obj)
            {
              
              $('#shnm').hide();
              $('#fileh').show();
              $("#workcard").flip(false);
              $('#success_work').text(obj.message.success);
              $('#success_work').show();  
              $('.errorObj_work').hide();
              $( '#workout_form').each(function(){
              this.reset();
              setTimeout(function() {
            $("#success_work").hide('blind', {}, 500)
              }, 5000);
          });  
            },
            error:function(errorObj)
            {
                error_handle_acadmic(errorObj,'.errorObj_work');
                $('#success_work').hide();  
            },
          complete : function(){
            $('.animation_image_work').hide();
          }
        });
          
    });
 
 //function add route
    $(document).on('click','#prac_save',function(){
        $("#practic_form").ajaxSubmit({
            beforeSend : function(){
            $('.animation_image_prac').show();
          },
          success:function(obj)
            {
              $('#prcvhs').hide();
              $('#prcvh').show();
               $("#praccard").flip(false);
               $('#success_prac').text(obj.message.success);
               $('#success_prac').show();  
               $('.errorObj_prac').hide();
               $( '#practic_form').each(function(){
              this.reset();
              setTimeout(function() {
            $("#success_prac").hide('blind', {}, 500)
              }, 5000);
          });   
            },
            error:function(errorObj)
            {
                error_handle_acadmic(errorObj,'.errorObj_prac');
                $('#success_prac').hide();
            },
          complete : function(){
            $('.animation_image_prac').hide();
          }
        });
    });
 
     $("#Balls").change(function() {
        
        var bt= this.value;
        var Strikes = $("#Strikes").val();
      
  if(Strikes==null || Strikes== "" || Strikes==undefined) {
  
  Strikes=0;
  }
        sum(parseInt(Strikes)+parseInt(bt));
      });

    $("#Strikes").change(function() {
        
        var bs= this.value;
        var Balls = $("#Balls").val();
      
  if(Balls==null || Balls== "" || Balls==undefined) {
  
  Balls=0;
  }
        sum(parseInt(Balls)+parseInt(bs));
      });

function sum(total){
  $('#pitchCount').val(total);
}

  $("#gameType").change(function() {
     var tab=$("#gameType").val();
     if(tab==2){
     $('#tourDiv').hide();
   }else
     $('#tourDiv').show();
});

  //$('#SaveGameId').val('0');
 // $('#at_bat_value').val('1');
  //var count = $('#at_bat_value').val();
    
  ///alert("page load==>"+count);

 //function add route
    $(document).on('click','#user_stats_save',function(){


        blockStats();
        
        if($('#ROB').val()==""){
          alert('kindly select rob');
           return false ;
            }
            else if($('#AT-BAT-Results').val()==""){
          alert('kindly select AT-BAT-Results');
           return false ;
            }
            else if($('#Pitcher').val()==""){
          alert('kindly select Pitcher');
           return false ;
            }

        $("#user_stats").ajaxSubmit({
          beforeSend : function(){
            $('.animation_image_state').show();
          },
            success:function(obj)
            {

              $('#stprcvh').hide();
              $('#stapr').show();
              var count = $('#at_bat_value').val();
              count=parseInt(count);
              
              $('#success_state').text(obj.message.success);
              var datestr = obj.game_info.game_date;
              var seperateDate= datestr.replace(/[-,:\sT]/g, "-");
              var dateFrmt = seperateDate.split('-');
              $('#gameDate').val(dateFrmt[0]+'-'+dateFrmt[1]+'-'+dateFrmt[2]);
              $('#gameDate').prop('readonly', true);
              $("#gameDate").datepicker("destroy");
              $('#locationa').val(obj.game_info.game_location);
              $('#locationa').attr('readonly', true);
              //$('#tourName').val(obj.game_info.game_location);
              $('#tourName').attr('readonly', true);
              $('#gameType').val(obj.game_info.game_type);
              $('#gameType').attr('disabled', true);
              $('#myteama').val(obj.game_info.my_team_name);
              $('#myteama').attr('readonly', true);
              $('#opponenta').val(obj.game_info.oponent);  
              $('#opponenta').attr('readonly', true);
              $('#SaveGameId').val(obj.game_info.id);
              $('#SaveTourId').val(obj.game_info.tournament_id);
              $('#at_bat_value').val(count+1);

              $('#success_state').show();
              $('.errorObj_state').hide();

              $( '#Number-of-Outs').val("");
              $( '#Pitcher').val("");
              $( '#ROB').val("");
              $( '.fnames').val("");
              $( '.types').val("");
              $( '.sizes').val("");
              $( '#AT-BAT-Results').val("");
              $( '#Stolen-Bases').val("");
              $( '#RBIs').val("");
              $( '#Runs-Scored').val("");
              $( '#stat-file').val("");
              $( '#pitchCount').val("");
              $( '#Balls').val("");
              $( '#Strikes').val("");
              setTimeout(function() {
            $("#success_state").hide('blind', {}, 500)
              }, 5000);
            },
              
            error:function(errorObj)
            {
                error_handle(errorObj,'.errorObj_state');
                $('#success_state').hide();
                
            },
          complete : function(){
            $('.animation_image_state').hide();
          }
        });

    });

/*----------------Pause DATA function starts from here---------------------*/

    $(document).on('click','#pause',function(){
      
     var buttoName= 'pause';
     $('#gameType').removeAttr('disabled');
    var formdata = $('#user_stats').serialize();
        formdata += '&buttoName='+buttoName;

      $.ajax({
          type: "POST",
          url: SITE_URL+'api/save_user_stats.json',
          data:formdata,
        
          beforeSend : function(){
            $('.animation_image_state').show();
          },
            success:function(obj)
            {
                console.log('Game is on pause Mode');
                $('#pause').hide();
                $('#user_stats_save').hide();
                


            },
              
            error:function(errorObj)
            {
                error_handle(errorObj,'.errorObj_state');
                 
            },
          complete : function(){
            $('.animation_image_state').hide();

          }
        });
    });
 


/**-------------------------------------------------------------------------*/
/**-----------------Resume Game Js Code Start form here-------------------------------*/
    $(document).on('click','#resume',function(){
        var flag='pause';
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/temp_game.json',
          data:{'flag':flag},
          dataType:'json',
          beforeSend : function(){
            $('.animation_image_state').show();
          },
            success:function(obj)
            {
              var a = obj.tempdata[0].formdata;
              var data = JSON.parse(a);
              
              $('#gameDate').val(data.game.game_date);
              $('#locationa').val(data.game.game_location);
              $('#gameType').val(data.game.game_type);
              $('#myteama').val(data.game.my_team_name);
              $('#opponenta').val(data.game.oponent);  
              $('#tourName').val(data.tour.name);
              $('#at_bat_value').val(data.stat.at_bat);
              $('#at_bat_value').attr('readonly', true);
              $( '#Number-of-Outs').val(data.stat.no_of_out);
              $( '#Pitcher').val(data.stat.pitcher);
              $( '#ROB').val(data.stat.rob);
              $( '#AT-BAT-Results').val(data.stat.at_bat_result);
              $( '#Stolen-Bases').val(data.stat.stolen_bases);
              $( '#RBIs').val(data.stat.rbis);
              $( '#Runs-Scored').val(data.stat.runs_scored);
              $( '#stat-file').val("");
              $('#SaveGameId').val(data.game.saved_game_id);
              $( '#pitchCount').val(data.stat.pitch_thrown);
              $( '#Balls').val(data.stat.pitch_count_bals);
              $( '#Strikes').val(data.stat.pitch_count_strikes);
                      $('#pause').show();
                      $('#user_stats_save').show();
              
              //$('#user_stats').populate(obj.tempdata[0].formdata);
            },
              
            error:function(errorObj)
            {
               
                alert("No Pause data");
                
            },
          complete : function(){
            $('.animation_image_state').hide();
          }
        });
    });



$.ajax({
          type: "GET",
          url: SITE_URL+'api/get_memory_limit.json',
          dataType:'json',
          
            success:function(obj)
            {
              if(obj.memory_allot[0] == null){
                var avlmemry = parseInt(2500000000)-parseInt(obj.memory_allot);
                $('#chkUploadSize').val(avlmemry);  
              if(avlmemry<0){

              $('#fileh').attr("disabled", true);
              $('#prcvh').attr("disabled", true);
              $('#stapr').attr("disabled", true);
              alert('memory Full , you cant upload any video or file on this page');
             }
              }else{

              $('#chkUploadSize').val(obj.memory_allot[0].available_memory);  
              if(obj.memory_allot[0].available_memory==0 || obj.memory_allot[0].available_memory < 0){

              $('#fileh').attr("disabled", true);
              $('#prcvh').attr("disabled", true);
              $('#stapr').attr("disabled", true);
              alert('memory Full , you cant upload any video or file on this page');
            }
          }

              
            },
              
            error:function(errorObj)
            {
                //alert(errorObj.memory_allot.media_size);
                
                //console.log(errorObj.memory_allot);

                console.log("default memory assign user...");
                
            }
        });


    

    $(document).on('click','#endGame',function(){
             
             var phno4 = $('#ph4').val();
             var phno5 = $('#ph5').val();
             var phno6 = $('#ph6').val();
           

            if(phno4!=0){
                  var phno1 = $('#ph1').val(); 
             }else{
                var phno1 = "";  
             }if(phno5!=0){
                  var phno2 = $('#ph2').val(); 
             }else{
                var phno2 = "";  
             }if(phno6!=0){
                  var phno3 = $('#ph3').val(); 
             }else{
                var phno3 = "";  
             }

        $.ajax({
          type: "POST",
          url: SITE_URL+'api/sendsms.json',
          data:{'phone_no1':phno1,'phone_no2':phno2,'phone_no3':phno3},
          beforeSend : function(){
            $('.animation_image').show();
            alert('a sms will be send on your filled phone number');
          },
          success: function(obj){
             alert(obj.message.success);
             location.reload();
            },
          error:function(errorObj)
          {
            
             error_handle_acadmic(errorObj,'.errorsms');
          },
          complete : function(){
            $('.animation_image').hide();
          }

});  
});

    $(document).on('click','#exit_btn',function(){
      window.location.href=SITE_URL+'gamesStatus'; 
});

$.ajax({
          type: "GET",
          url: SITE_URL+'api/getPhno.json',
          beforeSend : function(){
            $('.animation_image').show();
          },
          success: function(obj){
             
             $('#ph1').val(obj.phno[0].phone_no1);
             $('#ph2').val(obj.phno[0].phone_no2);
             $('#ph3').val(obj.phno[0].phone_no3);
             $('#ph4').val(obj.phno[0].notify_one);
             $('#ph5').val(obj.phno[0].notify_two);
             $('#ph6').val(obj.phno[0].notify_three);
             $('#getnoId').val(obj.phno[0].id);
             
            },
          error:function(errorObj)
          {
              error_handle(errorObj,'.PerrorObj2');
          },
          complete : function(){
            $('.animation_image').hide();
          }

});
       
  $(".skilImgcls").change(function() {
    $("#message").empty(); // To remove the previous error message
    
    var file = this.files[0];
    var imagefile = file.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
    {
    $('#previewing1').hide();
    $("#message").html("<p style='color:red'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
    $("#skills_img1").css("color","red");
    return false;
    }
    else
    {
      
    var reader = new FileReader();
    reader.onload = imageIsLoaded;
    reader.readAsDataURL(this.files[0]);
    
    $('#previewing1').show();
    }
  });


  $(".skilImgcls2").change(function() {
    $("#message2").empty(); // To remove the previous error message
    var file = this.files[0];
    var imagefile = file.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
    {
    $('#previewing2').hide();
    $("#message2").html("<p style='color:red'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
    $("#skills_img2").css("color","red");
    return false;
    }
    else
    {
    var reader = new FileReader();
    reader.onload = imageIsLoadedss;
    reader.readAsDataURL(this.files[0]);
    $('#previewing2').show();
    }
  });


    });

function autocomplet(type) {
  var type;
  var urlForThis;
  var keyword;
  var make;
  var filter;
  var inkr=0;
 var min_length = 0; // min caracters to display the autocomplete
  if(type=="Tournaments"){
    make="Tournaments";
    filter="name";
    keyword = $('#tourName').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="myteam"){
    make="UserGames";
    filter="my_team_name";
    keyword = $('#myteama').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="opponent"){
    make="UserGames";
    filter="oponent";
    keyword = $('#opponenta').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="location"){
    make="UserGames";
    filter="game_location";
    keyword = $('#locationa').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if (keyword.length > min_length) {
    $.ajax({
      url: urlForThis,
      type: 'GET',
      data: {q:keyword,types:make,filter:filter,inkr:inkr},
       dataType: 'html',
       beforeSend:function(){
       
        if(filter=="name"){
          $('#tourName').addClass('loadinggif');
        }
        else if(filter=="my_team_name"){
          $('#myteama').addClass('loadinggif');
        }else if(filter=="oponent"){
          $('#opponenta').addClass('loadinggif');
        }else if(filter=="game_location"){
          $('#locationa').addClass('loadinggif');
        }
        
      
    },
success:function(data){

        $('#'+filter).show();
        $('#'+filter).html(data);
          setTimeout(function() {
            $(".list").hide('blind', {}, 800)
              }, 8000);

      },
      error:function(errorObj){
        console.log('No Matching Data');
      },
      complete : function(){
        if(filter=="name"){
          $('#tourName').removeClass('loadinggif');
        }
        else if(filter=="my_team_name"){
          $('#myteama').removeClass('loadinggif');
        }else if(filter=="oponent"){
          $('#opponenta').removeClass('loadinggif');
        }else if(filter=="game_location"){
          $('#locationa').removeClass('loadinggif');
        }
    }
    });
  } else {
    $('#'+filter).hide();
  }
}
 
// set_item : this function will be executed when we select an item
function set_item(item,type) {
  // change input value
  var t = item.toUpperCase();
  if(type=="name"){
  $('#tourName').val(t);
}
 if(type=="my_team_name"){
  $('#myteama').val(t);
}
 if(type=="oponent"){
  $('#opponenta').val(t);
}
 if(type=="game_location"){
  $('#locationa').val(t);
}
  // hide proposition list
  $('#'+type).hide();
}

function blockStats(){

$.ajax({
          type: "GET",
          url: SITE_URL+'api/allowStats.json',
          
          success: function(obj){
               console.log(obj.message.success);
          },
          error:function(errorObj)
          {
             console.log('you have taken plan $10 ');  
          }

});


}

function error_handle_acadmic(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}



function imageIsLoaded(e) {
  $("#skills_img1").css("color","green");
  $('#image_preview1').css("display", "block");
  $('#previewing1').attr('src', e.target.result);
  $('#previewing1').attr('width', '150px');
  $('#previewing1').attr('height', '130px');
};


function imageIsLoadedss(e) {
  $("#skills_img2").css("color","green");
  $('#image_preview2').css("display", "block");
  $('#previewing2').attr('src', e.target.result);
  $('#previewing2').attr('width', '150px');
  $('#previewing2').attr('height', '130px');
};