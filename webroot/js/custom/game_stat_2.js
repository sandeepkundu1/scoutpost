
$(document).ready(function(){
var id = $.urlParam('id');
var date = $.urlParam('date'); 
var match_type_url = $.urlParam('type'); 

var request_url_match =(match_type_url==1)?SITE_URL+'api/single_game_detail.json':SITE_URL+'api/single_game_detail_league.json';
  $.ajax({
          type: "GET",
          url: request_url_match,
          data:{'game_date':date,'tournament_id':id},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){

            for(var i in obj.GameDetail){

              if(obj.GameDetail[i].UserGames.game_type==1){
                var Type = 'tournament';
              }else if(obj.GameDetail[i].UserGames.game_type==2){
              var Type = 'League'; 
              }

              $('.gameType').html(Type);
             
              var options = obj.GameDetail[i].UserGames.game_date;
				
			 if(options != '' || options != null || options !=NaN || options !=undefined) {
				   var split_arr=options.split("T");
				    var my_date=split_arr[0];
         if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[1]+'-'+arr[2]+'-'+arr[0];
              options =d;
              date_link = d;
            }
}
          var leagueDate = options;
          var options = obj.GameDetail[i].tour_name;
           if(obj.GameDetail[i].tour_name!=null){
           $('.gameDate').html(options);
           }
         else if(obj.GameDetail[i].tour_name==null)
         {
          $('.gameDate').html(leagueDate);
         }

           var my_href='';
           if(match_type_url==1) {
               my_href=SITE_URL+'Categories/game_stat_3?game_id='+obj.GameDetail[i].UserGames.id+'&tournament_id='+obj.GameDetail[i].UserGames.tournament_id;
            } else {
                my_href=SITE_URL+'Categories/game_stat_3?game_id='+obj.GameDetail[i].UserGames.id+'&tournament_id='+null;
            }
           $('.listLink').append("<li><a href="+my_href+">"+obj.GameDetail[i].UserGames.my_team_name +" ~Vs~ "+obj.GameDetail[i].UserGames.oponent+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>(AT-"+obj.GameDetail[i].UserGames.game_location+")");

            }
          },
          error:function(errorObj)
          {
             error_handle(errorObj,'.PerrorObj1');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
$(document).ready(function(){

var stat_type = date;
var tounament_id = id;  
var request_url_ab_total =(match_type_url==1)?SITE_URL+'api/get_games_at_bat_total.json':SITE_URL+'api/get_games_at_bat_total_league.json';
var request_param_ab_total =(match_type_url==1)?id:date;

          $.ajax({
          type: "GET",
          url: request_url_ab_total,
          data:{'year':2015,'game_type':match_type_url,'tournament_id':request_param_ab_total},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){

  var total_hits=parseInt(obj.AtbatTotal.no_of_hr)+parseInt(obj.AtbatTotal.no_of_3b)+parseInt(obj.AtbatTotal.no_of_2b)+parseInt(obj.AtbatTotal.no_of_1b);
             $("#abc").html(
              "<td>"+obj.AtbatTotal.ab+
              "<td>"+obj.AtbatTotal.one_b+
              "<td>"+obj.AtbatTotal.two_b+
              "<td>"+obj.AtbatTotal.three_b+
              "<td>"+obj.AtbatTotal.hr+
              "<td>"+obj.AtbatTotal.hits+
              "<td>"+obj.AtbatTotal.runs
              );
            $("#def").html(
              "<td>"+obj.AtbatTotal.bb+
              "<td>"+obj.AtbatTotal.hbp+
              "<td>"+obj.AtbatTotal.roe+
              "<td>"+obj.AtbatTotal.so+
              "<td>"+obj.AtbatTotal.sac+
              "<td>"+obj.AtbatTotal.sb+
              "<td>"+obj.AtbatTotal.rbi
              );

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj2');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
if(date != '' || date != null || date !=NaN || date !=undefined) {
           var split_arr=date.split("T");
            var my_date=split_arr[0];
          
           
         if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[0]+'-'+arr[1]+'-'+arr[2];
              
              var given_date = d;
              var yyyy=arr[0];
            }
}
          
          var requested_url=(match_type_url==1) ? SITE_URL+'api/fetch_tournament_stats.json':SITE_URL+'api/fetch_league_stats.json';
          var second_param_value=(match_type_url==1)? id:given_date;
          
          $.ajax({
          type: "GET",
          url: requested_url,
          data:{'year':yyyy,'tournament_id':second_param_value,'game_type':match_type_url},
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
          $('#avg1').html(obj.tournament_stats.avg);
          $('#slg1').html(obj.tournament_stats.slg);
          $('#obp1').html(obj.tournament_stats.obp);
          $('#ops1').html(obj.tournament_stats.ops);
          $('#tb1').html(obj.tournament_stats.tb);
          $('#rc1').html(obj.tournament_stats.rc);
          $('#pa1').html(obj.tournament_stats.pa);
          $('#pip1').html(obj.tournament_stats.pip);
          $('#rhp1').html(obj.tournament_stats.AVG_VS_RHP);
          $('#lhp1').html(obj.tournament_stats.AVG_VS_LHP);
          $('#avgw1').html(obj.tournament_stats.wrob);
          $('#qab1').html(obj.tournament_stats.qab);

          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj3');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
});
});


$.urlParam = function(name){
    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    return results[1] || 0;
}

function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}