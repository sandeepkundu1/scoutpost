$(document).ready(function(){
  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/history.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
            var str="";

            var j=1;
            var add1 =0;
            var add2=0;
            var add3=0;
            var add4=0;
            var add5=0;
            var add6=0;
            var add7=0;
            var add8=0;
            var add9=0;
            var add10=0;
            var add11=0;
            var add12=0;
            var add13=0;
            var add14=0;
            var add15=0;
            var add16=0;
            var add17=0;
            var add18=0;
            var add19=0;
            


            for(var i in obj.tournament_stats){
             
              if(j==3) {
                break;
              }

              add1  += parseFloat(obj.tournament_stats[i].no_of_games);
              add2  += parseFloat(obj.tournament_stats[i].PA);  
              add3  += parseFloat(obj.tournament_stats[i].AB);    
              add4  += parseFloat(obj.tournament_stats[i].hits);  
              add5  += parseFloat(obj.tournament_stats[i].one_b);  
              add6  += parseFloat(obj.tournament_stats[i].two_b);  
              add7  += parseFloat(obj.tournament_stats[i].three_b);  
              add8  += parseFloat(obj.tournament_stats[i].hr);  
              add9  += parseFloat(obj.tournament_stats[i].rbi);  
              add10 += parseFloat(obj.tournament_stats[i].bb);  
              add11 += parseFloat(obj.tournament_stats[i].sb);  
              add12 += parseFloat(obj.tournament_stats[i].runs_scored);  
              add13 += parseFloat(obj.tournament_stats[i].avg);  
              add14 += parseFloat(obj.tournament_stats[i].obp);  
              add15 += parseFloat(obj.tournament_stats[i].slg);  
              add16 += parseFloat(obj.tournament_stats[i].ops);  
              add17 += parseFloat(obj.tournament_stats[i].rc);  
              add18 += parseFloat(obj.tournament_stats[i].tb);
              add19 += parseFloat(obj.tournament_stats[i].pip);
                

              str +=
              "<tr>"+
              "<td>"+obj.tournament_stats[i].year_of_games+  
              "<td>"+obj.tournament_stats[i].age+
              "<td>"+obj.tournament_stats[i].team+
              "<td>"+obj.tournament_stats[i].no_of_games+
              "<td>"+obj.tournament_stats[i].PA+
              "<td>"+obj.tournament_stats[i].AB+
              "<td>"+obj.tournament_stats[i].hits+
              "<td>"+obj.tournament_stats[i].one_b+
              "<td>"+obj.tournament_stats[i].two_b+
              "<td>"+obj.tournament_stats[i].three_b+
              "<td>"+obj.tournament_stats[i].hr+
              "<td>"+obj.tournament_stats[i].rbi+
              "<td>"+obj.tournament_stats[i].bb+
              "<td>"+obj.tournament_stats[i].sb+
              "<td>"+obj.tournament_stats[i].runs_scored+
              "<td>"+obj.tournament_stats[i].avg+
              "<td>"+obj.tournament_stats[i].obp+
              "<td>"+obj.tournament_stats[i].slg+
              "<td>"+obj.tournament_stats[i].ops+
              "<td>"+obj.tournament_stats[i].rc+
              "<td>"+obj.tournament_stats[i].tb+
              "<td>"+obj.tournament_stats[i].pip;

              j++;

            }
            
            $("#data_table").html(str);
            $("#add1").html(add1);
            $("#add2").html(add2);
            $("#add3").html(add3);
            $("#add4").html(add4);
            $("#add5").html(add5);
            $("#add6").html(add6);
            $("#add7").html(add7);
            $("#add8").html(add8);
            $("#add9").html(add9);
            $("#add10").html(add10);
            $("#add11").html(add11);
            $("#add12").html(add12.toPrecision(3));
            $("#add13").html(add13.toPrecision(3));
            $("#add14").html(add14.toPrecision(3));
            $("#add15").html(add15.toPrecision(3));
            $("#add16").html(add16.toPrecision(3));
            $("#add17").html(add17.toPrecision(3));
            $("#add18").html(add18.toPrecision(3));
            $("#add19").html(add19.toPrecision(3));
            


            
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
});


function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}