$(document).ready(function(){

  var tab=$("#gameType").val();
    if(tab==2){
        $('#tourDiv').hide();
         }

});

function stat_val(){

if($("#Pitcher").val()==''){
    alert("Please Select Pitcher");
    return false;
  }
 if($("#ROB").val()==''){
   alert("Please Select ROB");
   return false;
  }     
  if($("#AT-BAT-Results").val()==''){
        alert("Please Select At Bat Result");
        return false;
  }

}


function autocomplet(type) {
  var type;
  var urlForThis;
  var keyword;
  var make;
  var filter;
  var inkr=0;
 var min_length = 0; // min caracters to display the autocomplete
  if(type=="Tournaments"){
    make="Tournaments";
    filter="name";
    keyword = $('#tourName').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="myteam"){
    make="UserGames";
    filter="my_team_name";
    keyword = $('#myteama').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="opponent"){
    make="UserGames";
    filter="oponent";
    keyword = $('#opponenta').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }
  if(type=="location"){
    make="UserGames";
    filter="game_location";
    keyword = $('#locationa').val();
    urlForThis=SITE_URL+'/api/autocomplete.json'; 
  }

  if (keyword.length > min_length) {
    $.ajax({
      url: urlForThis,
      type: 'GET',
      data: {q:keyword,types:make,filter:filter,inkr:inkr},
       dataType: 'html',
       beforeSend:function(){
       
        if(filter=="name"){
          $('#tourName').addClass('loadinggif');
        }
        else if(filter=="my_team_name"){
          $('#myteama').addClass('loadinggif');
        }else if(filter=="oponent"){
          $('#opponenta').addClass('loadinggif');
        }else if(filter=="game_location"){
          $('#locationa').addClass('loadinggif');
        }
        
      
    },
success:function(data){

        $('#'+filter).show();
        $('#'+filter).html(data);
          setTimeout(function() {
            $(".list").hide('blind', {}, 1200)
              }, 12000);

      },
      error:function(errorObj){
        console.log('No Matching Data');
      },
      complete : function(){
        if(filter=="name"){
          $('#tourName').removeClass('loadinggif');
        }
        else if(filter=="my_team_name"){
          $('#myteama').removeClass('loadinggif');
        }else if(filter=="oponent"){
          $('#opponenta').removeClass('loadinggif');
        }else if(filter=="game_location"){
          $('#locationa').removeClass('loadinggif');
        }
    }
    });
  } else {
    $('#'+filter).hide();
  }
}
 
// set_item : this function will be executed when we select an item
function set_item(item,type) {
  // change input value
  var t = item.toUpperCase();
  if(type=="name"){
  $('#tourName').val(t);
}
 if(type=="my_team_name"){
  $('#myteama').val(t);
}
 if(type=="oponent"){
  $('#opponenta').val(t);
}
 if(type=="game_location"){
  $('#locationa').val(t);
}
  // hide proposition list
  $('#'+type).hide();
}

  
  
