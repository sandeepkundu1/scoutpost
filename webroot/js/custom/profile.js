$(function() {
    setTimeout(function() {
        $(".success").hide('blind', {}, 500)
    }, 5000);
});

$(document).ready(function(){
  
          $.ajax({
          type: "GET",
          url: SITE_URL+'api/weekly_game_info.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },

          success: function(obj){
            var videos="";
            var htmls='';
            for(i in obj.skillDetail){
                var id= obj.skillDetail[i].um.id;
                var options = obj.skillDetail[i].workout_date;
                options = new Date(options);
                D = options.getUTCDay();
                if(D==0){
                   var s="Sunday";
                } else if(D==1){
                   var s="Monday";
                } else if(D==2){
                    var s="Tuesday";
                } else if(D==3){
                   var s="Wednesday";
                } else if(D==4){
                   var s="Thursday";
                } else if(D==5){
                   var s="Friday";
                } else if(D==6){
                var	s="Saturday";
                }
                //alert(id);
                var video_str='';
                if(obj.skillDetail[i].skills_img !='') { 
                  
                  
                  var imgdiv = '<img src="img/skills_pic/'+obj.skillDetail[i].skills_img+'" alt="image not found" width="150" height="150" />'; 
                    
                } else {
                    var imgdiv ='No Image Found...';
                }

                if(obj.skillDetail[i].um.media_name !='') { 

                  
                  var vardiv = '<iframe frameborder="0" height="281"  allowfullscreen="" src="https://www.youtube.com/embed/'+obj.skillDetail[i].um.media_name+'?rel=0"  class="video_mang"></iframe>'; 
                    
                } else {
                    var vardiv ='No Video Found...';
                }
                
                
                htmls +='<h3 id="dayName">'+s+'</h3>Time Spend: <span id="Wtime">'+obj.skillDetail[i].workout_time+"Mins"+'</span><p id="desc">'+obj.skillDetail[i].description+'</p><p>'+imgdiv+'</p><table border="0" id="results"></table></div><div class="col-sm-12 no-padding-right"><p class="video"><div>'+vardiv+'</div></p></div>';

            }
            
            $('#data_div').html(htmls);
            $(function() {
                    $('.video_player').myPlayer();
                });
          	
            },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj');
                
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});

$.ajax({
          type: "GET",
          url: SITE_URL+'api/history.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },


          success: function(obj){
          		var j=1;
          var str="";

          	for(var i in obj.tournament_stats){
             
              if(j==2) {
                break;
              }
              $('#yer').html(obj.tournament_stats[i].year_of_games);
              $("#avg1").html(obj.tournament_stats[i].avg);
              $("#avg2").html(obj.tournament_stats[i].AB);
              $("#avg3").html(obj.tournament_stats[i].hits);
              $("#avg4").html(obj.tournament_stats[i].rbi);
              $("#avg5").html(obj.tournament_stats[i].slg);
              $("#avg6").html(obj.tournament_stats[i].obp);
              $("#avg7").html(obj.tournament_stats[i].pip);

              $("#avg8").html(obj.tournament_stats[i].age);
              $("#avg9").html(obj.tournament_stats[i].team);
              $("#avg10").html(obj.tournament_stats[i].sex);
              $("#avg11").html(obj.tournament_stats[i].division);

              if(obj.tournament_stats[i].pitcher==1)
              {
              var pitcher = 'Pitcher' ;
              }
              else if(obj.tournament_stats[i].pitcher==0 || obj.tournament_stats[i].pitcher==null)
              {
              var pitcher = '' ;
              }
              if(obj.tournament_stats[i].infilder==1)
              {
              var infilder = 'infielder' ;
              }
              else if(obj.tournament_stats[i].infilder==0 || obj.tournament_stats[i].infilder==null)
              {
              var infilder = '' ;
              }
              if(obj.tournament_stats[i].catcher==1)
              {
              var catcher = 'catcher' ;
              }
              else if(obj.tournament_stats[i].catcher==0 || obj.tournament_stats[i].catcher==null)
              {
              var catcher = '' ;
              }
              if(obj.tournament_stats[i].outfilder==1)
              {
              var outfilder = 'outfielder' ;
              }
              else if(obj.tournament_stats[i].outfilder==0 || obj.tournament_stats[i].outfilder==null)
              {
              var outfilder = '' ;
              }



              $("#avg12").html(pitcher+'<br>'+infilder+'<br>'+catcher+'<br>'+outfilder);
              if(obj.tournament_stats[i].through==1)
              {
              $("#avg15").html('Right Hand');
              }
              else if(obj.tournament_stats[i].through==2)
              {
              $("#avg15").html('Left Hand');
              }

              if(obj.tournament_stats[i].bats==1)
              {
              $("#avg16").html('Right');
              }
              else if(obj.tournament_stats[i].bats==2)
              {
              $("#avg16").html('Left');
              }
              else if(obj.tournament_stats[i].bats==3)
              {
              $("#avg16").html('Both');
              }
              $("#avg17").html(obj.tournament_stats[i].favorite_team);
              $("#avg14").html(obj.tournament_stats[i].favorite_player);

              j++;

            }
            
            
            },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj');
                $('#getLDate').hide();
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
var game_id= "order by us.id desc limit 1";

$.ajax({
          type: "GET",
          url: SITE_URL+'api/get_recent_game.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){



              var options = obj.gamedetail[0].game_date; 
                 
                  if(options != '' || options != null || options !=NaN || options !=undefined) {
           var split_arr=options.split("T");
            var my_date=split_arr[0];
          
           
         if(my_date !='') {
            var arr=my_date.split('-');
            var d= arr[0]+'-'+arr[1]+'-'+arr[2];
              
              var options = d;
              
            }
      }
      if(obj.gamedetail[0].game_type==1){
        var type='Tournament'; 
      }else{
        var type='League'; 
      }

            $('.gameDate').html(options);
            $('.gameType').html(type);
            $('.team1').html(obj.gamedetail[0].my_team_name);
              $('.team2').html(obj.gamedetail[0].oponent);

              
             var row="";
             var str="";
             var video_name = "";
             var id = "";
            var video_str='';
            var str=1;    
                

              for(var i in obj.gamedetail){
              //str  = obj.gamedetail[i].at_bat;
              
              row  = obj.gamedetail[i].us.at_bat_result;
              video_name =obj.gamedetail[i].usm.media_name;
               id+=obj.gamedetail[i].us.id;
              $(".getVideo").append('<p>AT BAT#'+str+' - '+'result'+' '+row);
              //$(".getVideo").append('<p>result'+' '+row);
              if(obj.gamedetail[i].usm.media_name !='') { 
                var vardiv = '<iframe frameborder="0" height="281"  allowfullscreen="" src="https://www.youtube.com/embed/'+obj.gamedetail[i].usm.media_name+'?rel=0"  class="video_mang"></iframe>';

              $('.getVideo').append(vardiv);
              
                }else {
                    $('.getVideo').append('No Video Found...');
                }
                ++str;
              }
             
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj2');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});


$.ajax({
          type: "GET",
          url: SITE_URL+'api/get_profile_pic.json',
          beforeSend : function(){
            $('.animation_image_acadmic').show();
          },
          success: function(obj){
             
             var data1 = 'img/profile_pic/'+obj.img[0].profile_pic1;
             var data2 = 'img/profile_pic/'+obj.img[0].profile_pic2;
             var data3 = 'img/profile_pic/'+obj.img[0].profile_pic3;
             
             $('#picProfile').prepend('<img id="theImg" src="'+data1+'" height="150" width="150"/>')
             $('#picProfile').prepend('<img id="theImg" src="'+data2+'" height="150" width="150"/>')
             $('#picProfile').prepend('<img id="theImg" src="'+data3+'" height="150" width="150"/>')
             
             /*
             $('#getpic1').attr('src', data1);
             $('#getpic2').attr('src', data2);
             $('#getpic3').attr('src', data3);
             */

            },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.PerrorObj2');
          },
          complete : function(){
            $('.animation_image_acadmic').hide();
          }

});
$.ajax({
          type: "GET",
          data: {'page':1,'limit':2},
          url: SITE_URL+'/api/market_feature.json',
          success: function(obj){
              
            $('.images').prepend(obj.markets[1].description);
            $('.images').prepend('<img id="theImg" src="'+obj.image_path+"/"+obj.markets[1].photo+'" height="100" width="100"/>');
            $('.images2').prepend(obj.markets[0].description);
            $('.images2').prepend('<img id="theImg" src="'+obj.image_path+"/"+obj.markets[0].photo+'" height="100" width="100"/>');  
          },
          error:function(errorObj)
          {
             
                error_handle(errorObj,'.errorObj');
          }

});


});


function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '<span><em class="text-red">*</em>'+err+'</span>'
        $(o).show();
         $(o).html(div);
    }
        
}