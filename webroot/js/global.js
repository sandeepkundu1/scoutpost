/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getParameterByName(a) {
    a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var o = new RegExp("[\\?&]" + a + "=([^&#]*)"),
            e = o.exec(location.search);
    return null === e ? "" : decodeURIComponent(e[1].replace(/\+/g, " "))
}
function resetForm(a) {
    return a ? void $(a).closest("form").find("input[type=text], textarea,input[type=password]").val("") : !1
}


function logOut(a) {
    if (a) {
        var a = "You have no such permissions please try again later";
        $("#show_danger").modal("hide"), $("#show_danger").remove();
        var o = '<div  class="modal fade" id="show_warning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        o += '<div class="modal-dialog">', o += '<div class="alert alert-danger" role="alert">' + a + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>', o += "</div></div>", $("body").append(o), $("#show_danger").modal("show")
    } else
        show_warning("Your login session has been expired please login again");
    var e = SITE_URL + "users/login?redirect_url=" + location.href;
    show_warning("Your login session has been expired please login again", e)
}


function error_handle(a, o) {

    if (405 === a.status)
        logOut("please login to continue");
    else if (400 === a.status) {
        var e = "You have no such permissions please try again later";
        $("#show_danger").modal("hide");
        $("#show_danger").remove();
        var t = '<div  class="modal fade" id="show_danger" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
        t += '<div class="modal-dialog">';
        t += '<div class="alert alert-danger" role="alert">' + e + '<span style="float:right"><button data-dismiss="modal" class="btn label label-warning btn-shadow" type="button">OK</button></span></div>';
        t += "</div></div>";
        $("body").append(t);
        $("#show_danger").modal("show");
    } else
    {
      
        var err = a.responseJSON.message.error;
       var div = '';
        for(var i in err)
        {
            
           div += '<span><em class="text-red">*</em>'+err[i]+'</span>'
           
        }
        $(o).show();
         $(o).html(div);
    }
        
}
