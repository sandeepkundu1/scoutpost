$(function (){
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#workfileupload').fileupload({
        url: SITE_URL+'api/serverVideo',
        maxChunkSize: 10240000000000000000000000000000000000000,
        //maxFileSize: 41943040,
        acceptFileTypes: /(\.|\/)(mp4|mov|avi|riff|mpg|vob|m2ts|mkv|3gp|flv|rec|264|dv4|m4v|3gpp|mpeg4|mpegps|webm|wmv|mpeg4)$/i,
        beforeSend : function(){
            $('#work_video').show();
            $('#work_save').attr("disabled", true);
          },
          success: function(obj){
              var s = JSON.parse(obj);
              
              //alert($('#chkUploadSize').val());

              if($('#chkUploadSize').val() != null && $('#chkUploadSize').val()<=s.size){
               
                alert('Error : memory limit cross , contact administration');
                $('#shnm').hide();
                $('#fileh').show();
                $('#work_video').hide();
                $('#work_save').attr("disabled", false);
                exit();
                return false;
               }else{ 
                
              $('.fnames').val(s.files);
              $('.sizes').val(s.size);
              $('.types').val(s.type);
              $('#work_save').attr("disabled", false);
              $('#work_video').hide();
              $('#fileh').hide();
              $('#shnm').html(s.files+'<br>(video added successfully ,please click on upload button)');

            }
              
          },
          error:function(errorObj)
          {
            alert('Error : uploading fail weak connection');
        
        $('#work_video').hide();
          },
          complete : function(){
            
            
          }
    });


    $('#pracfileupload').fileupload({
        url: SITE_URL+'api/serverVideo',
        maxChunkSize: 10240000000000000000000000000000000000000,
        //maxFileSize: 41943040,
        acceptFileTypes: /(\.|\/)(mp4|mov|avi|riff|mpg|vob|m2ts|mkv|3gp|flv|rec|264|dv4|m4v|3gpp|mpeg4|mpegps|webm|wmv|mpeg4)$/i,
        beforeSend : function(){
            $('#prac_video').show();
            $('#prac_save').attr("disabled", true);
          },
          success: function(obj){
              var s = JSON.parse(obj);
              
               if($('#chkUploadSize').val()<=s.size){
               
                
                alert('Error : memory limit cross , contact administration');
                $('#prcvhs').hide();
                $('#prcvh').show();
                $('#prac_video').hide();
                $('#prac_save').attr("disabled", false);
                exit();
                return false;
               }else{
              
              $('.fnames').val(s.files);
              $('.sizes').val(s.size);
              $('.types').val(s.type);
              $('#prac_save').attr("disabled", false);
              $('#prac_video').hide();
              $('#prcvh').hide();
              $('#prcvhs').html(s.files+'<br>(video added successfully ,please click on upload button)'); 
            
              }
          },
          error:function(errorObj)
          {
        alert('Error : uploading fail weak connection');
        $('#prac_video').hide();
          },
          complete : function(){
            
            
          }
    });

   $('#fileupload').fileupload({
      
        url: SITE_URL+'api/serverVideo',
        maxChunkSize: 10240000000000000000000000000000000000000,
        //maxFileSize: 62914560,
        acceptFileTypes: /(\.|\/)(mp4|mov|avi|riff|mpg|vob|m2ts|mkv|3gp|flv|rec|264|dv4|m4v|3gpp|mpeg4|mpegps|webm|wmv|mpeg4)$/i,
        beforeSend : function(){
            $('#stat_video').show();
            $('#user_stats_save').attr("disabled", true);
          },
          success: function(obj){
              //console.log(obj);
               var s = JSON.parse(obj);
               
               
               if($('#chkUploadSize').val()<=s.size){
               alert('Error : memory limit cross , contact administration');
                
                $('#stprcvh').hide();
                $('#stapr').show();
                $('#stat_video').hide();
                $('#user_stats_save').attr("disabled", false);
                exit();
                return false;
               }else{
                
              $('.fnames').val(s.files);
              $('.sizes').val(s.size);
              $('.types').val(s.type);
              $('#user_stats_save').attr("disabled", false);
              $('#stat_video').hide();
              $('#stapr').hide();
              $('#stprcvh').html(s.files+'<br>(video added successfully ,please click on upload button)'); 
              
            }
          },
          error:function(errorObj)
          {
            alert('Error : uploading fail weak connection');
            $('#stat_video').hide(); 
          },
          complete : function(){
            
            
          }
    });





$('#adminFileupload').fileupload({
      
        url: SITE_URL+'api/serverVideo',
        maxChunkSize: 10240000000000000000000000000000000000000,
        //maxFileSize: 62914560,
        acceptFileTypes: /(\.|\/)(mp4|mov|avi|riff|mpg|vob|m2ts|mkv|3gp|flv|rec|264|dv4|m4v|3gpp|mpeg4|mpegps|webm|wmv|mpeg4)$/i,
        beforeSend : function(){
          
            $('#stat_video').show();
            $('#user_stats_save').attr("disabled", true);
          },
          success: function(obj){
              //console.log(obj);
               var s = JSON.parse(obj);
               
               
               if($('#chkUploadSize').val()<=s.size){
               alert('Error : memory limit cross , contact administration');
                
                $('#stprcvh').hide();
                $('#stapr').show();
                $('#stat_video').hide();
                $('#user_stats_save').attr("disabled", false);
                exit();
                return false;
               }else{
                
              $('.fnames').val(s.files);
              $('.sizes').val(s.size);
              $('.types').val(s.type);
              $('#user_stats_save').attr("disabled", false);
              $('#stat_video').hide();
              $('#stapr').hide();
              $('#stprcvh').html(s.files+'<br>(video added successfully ,please click on upload button)'); 
              
            }
          },
          error:function(errorObj)
          {
            alert('Error : uploading fail weak connection');
            $('#stat_video').hide(); 
          },
          complete : function(){
            
            
          }
    });

});
