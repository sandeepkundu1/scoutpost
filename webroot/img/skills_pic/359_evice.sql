-- phpMyAdmin SQL Dump
-- version 4.0.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2015 at 12:51 PM
-- Server version: 5.5.33-cll-lve
-- PHP Version: 5.4.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `evice`
--
CREATE DATABASE IF NOT EXISTS `evice` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `evice`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parent_id` int(30) NOT NULL DEFAULT '0' COMMENT '0 for main category and if there is an id then subcategory',
  `parent_name` varchar(100) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parent_id`, `parent_name`, `status`, `slug`, `created`, `updated`) VALUES
(10, 'Academics', 0, '', 1, 'academics', '2015-06-24 12:26:54', NULL),
(11, 'Electronics', 0, NULL, 1, 'electronics', '2015-06-24 12:27:07', NULL),
(19, 'radio', 11, 'Electronics', 1, 'radio', '2015-06-30 15:38:52', NULL),
(20, 'Business', 0, NULL, 1, 'business', '2015-06-30 16:21:45', NULL),
(23, 'arts', 20, 'Business', 1, 'arts', '2015-06-30 16:27:06', NULL),
(25, 'Science', 10, 'Academics', 1, 'science', '2015-06-30 16:41:53', NULL),
(26, 'AC', 11, 'Electronics', 1, 'ac', '2015-07-01 10:00:07', NULL),
(27, 'test', 20, 'Business', 1, 'test', '2015-07-01 10:09:51', NULL),
(28, 'Math', 10, 'Academics', 1, 'math', '2015-07-03 10:15:08', NULL),
(29, 'English', 10, 'Academics', 1, 'english', '2015-07-03 10:15:22', NULL),
(30, 'History', 10, 'Academics', 1, 'history', '2015-07-03 10:15:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE IF NOT EXISTS `guest` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`id`, `name`, `email`, `type`, `status`, `created`, `updated`) VALUES
(18, NULL, 'mohit.verma@udaantechnologies.com', 'learner', 1, '2015-07-03 14:33:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) DEFAULT NULL COMMENT 'user id of teacher',
  `learner_id` int(30) DEFAULT NULL COMMENT 'learner id who give rating',
  `knowledge_rate` double DEFAULT NULL,
  `communication_rate` double DEFAULT NULL,
  `help_rate` double DEFAULT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `user_id`, `learner_id`, `knowledge_rate`, `communication_rate`, `help_rate`, `comment`, `status`, `created`, `updated`) VALUES
(1, 9, 11, 2, 3, 4, 'nice teacher', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(2, 9, 16, 3, 4, 5, 'nice teacher and supportive', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(3, 9, 17, 5, 3, 4, 'nice teacher and supportive very much', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) DEFAULT NULL,
  `learner_id` int(30) DEFAULT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `user_id`, `learner_id`, `comment`, `status`, `created`, `updated`) VALUES
(1, 9, 11, 'very nice teacher', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(2, 9, 16, 'very supportive teacher', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(3, 9, 19, 'very supportive teacher', 1, '2015-06-26 00:00:00', '2015-06-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'teacher'),
(3, 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) DEFAULT NULL COMMENT 'user id of teacher',
  `subcategory_id` int(30) DEFAULT NULL,
  `skill_name` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `user_id`, `subcategory_id`, `skill_name`, `created`, `updated`) VALUES
(1, 9, 13, 'alzebra', '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(2, 9, 13, 'geometry', '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(3, 9, 13, 'statistics', '2015-06-26 00:00:00', '2015-06-26 00:00:00'),
(4, 9, 13, 'number stat', '2015-06-26 00:00:00', '2015-06-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `profile` text COLLATE utf8_unicode_ci,
  `paypal_account` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci,
  `short_summary` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(30) DEFAULT NULL,
  `status` int(10) DEFAULT NULL COMMENT '0 for inactive and 1 for active',
  `price` int(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `slug`, `password`, `salt`, `email`, `profile`, `paypal_account`, `summary`, `short_summary`, `image`, `role_id`, `status`, `price`, `created`, `updated`) VALUES
(1, 'admin', 'admin', '123456', NULL, 'admin@admin.com', 'decent profile', '2147483647', 'hello this is test', NULL, 'cat-demo.jpg', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'mohit verma', 'mohit-verma', 'a93ce2662e6ead76ae0a2d1cd1c00be3', 'ead56ef50260cccc6bfc4e3e4171f597', 'mohit.verma@webenturetech.com', NULL, '2147483647', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. ', 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'cat-demo.jpg', 2, 1, 9, '2015-06-30 10:15:14', NULL),
(11, 'ashok', 'ashok', 'b25c206d0ba58009e282bad173f22d99', '1b7f8615a5938b083ebd598f769ee65c', 'a@a.com', NULL, NULL, NULL, NULL, 'cat-demo.jpg', 2, 1, NULL, '2015-06-24 11:24:27', NULL),
(16, 'sandeep', 'sandeep', '84df0e3a9f4206cb6d3f26eeb481319f', '8506a1d8cf04a2fe3e94ef08bec9d5b8', 's@s.com', NULL, '445454', 'scsdccdcdscdc', NULL, 'cat-demo.jpg', 2, 1, 60, '2015-06-25 12:46:33', NULL),
(19, 'monu', 'monu', '537464e479a1317a0f0080530f430453', 'a9f9faa8df99ce2ba8f9a302609a7f56', 'mohit.verma@udaantechnologies.com', NULL, '155515215', 'csdcscdes', NULL, 'cat-demo.jpg', 2, 1, 50, '2015-06-25 12:53:58', NULL),
(20, 'sandeep kundu', 'sandeep-kundu', 'f1e9ca6b7dd53788346f0f8eb6eaa25a', '72ef3e8e8391be0cb1c7e3bc2f1758f9', 'sandeep.kundu@udaantechnologies.com', NULL, NULL, '', NULL, 'cat-demo.jpg', 2, 0, NULL, '2015-07-07 10:49:45', NULL),
(25, 'zuhed123', 'zuhed123', '6ea9417f62becd7492346d05fc8251ee', '8b746aaa0d9b27803d1b0d8d2a4504f7', 'zuhed.anwar@gmail.com', NULL, 'mohd.sayeed@webenturetech.com', 'this is for testing the functionality.', NULL, 'bnjhg.jpg', 2, 1, NULL, '2015-07-09 16:15:50', NULL),
(27, 'zuhed', 'zuhed', 'dd9dd526e07fe09d8314be6b74c501b3', '7c5d3f2f9c1b266d966d1ec4dc60486e', 'destinykingik@gmail.com', NULL, 'no@no.com', 'no any summary', NULL, 'default.jpg', 3, 1, NULL, NULL, NULL),
(29, 'zuhed121', 'zuhed121', 'f60a348f680f7eb76105e13df673075f', 'b0a7af8feef16705ce03ba80978ac85e', 'ankit.jain@udaantechnologies.com', NULL, 'mohd.sayeed@webenturetech.com', 'this is for testing', NULL, '1391513692_58123-250x250.jpg', 2, 1, NULL, '2015-07-09 16:50:49', NULL),
(30, 'dsfd', 'dsfd', '799d024ec92a7eb5ad9571ece58d91ba', '55ec35a017adce5bb4536c255a57119a', 'sdfsd@fdgdf.ry', NULL, 'no@no.com', 'no any summary', NULL, 'default.jpg', 3, 0, NULL, NULL, NULL),
(31, 'fsdfs', 'fsdfs', '9e2f362355f1e6164f41619b64817d15', '4ff5aff18cdc5d6743ee26580621b3e7', 'wesdfs@dhbfg.fghfg', NULL, 'no@no.com', 'no any summary', NULL, 'default.jpg', 3, 0, NULL, NULL, NULL),
(32, 'test', 'test', '228cb545fe4acd0c389cb49dafb8f0fc', '49e46dbcf35b6f02330870f203c9199c', 'kiran.sharmawctm@gmail.com', NULL, 'mohd.sayeed@webenturetech.com', 'this is for testing.', NULL, 'article-2486855-192ACC5200000578-958_964x682.jpg', 2, 1, NULL, '2015-07-09 17:39:41', NULL),
(33, 'tester123', 'tester123', '662fcd49964647f027a1f340fc0f320f', 'ea9d39a7f86b07647ae7e6c7bf4fe500', 'tester@mail.com', NULL, 'test@mailer.com', 'tjhis is foemner`', NULL, '1391513692_58123-250x250.jpg', 2, 0, NULL, '2015-07-09 18:17:25', NULL),
(34, 'fdgdf', 'fdgdf', '14881204408cb82fa93dbaff63a5b139', 'bed481290347e4d67e9dce1957790c7e', 'sds@dfgdf.ergfer', NULL, 'sds@dfgdf.ergfer', 'this is for testing', NULL, 'fsdf.jpg', 2, 0, NULL, '2015-07-09 18:32:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_category_mapping`
--

CREATE TABLE IF NOT EXISTS `user_category_mapping` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `category_id` int(30) DEFAULT NULL,
  `main_cat_id` int(30) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `user_id` int(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `user_category_mapping`
--

INSERT INTO `user_category_mapping` (`id`, `category_id`, `main_cat_id`, `status`, `user_id`, `created`, `updated`) VALUES
(1, 28, 10, 1, 9, '2015-07-04 00:00:00', '2015-07-04 00:00:00'),
(2, 25, 10, 1, 9, '2015-07-04 00:00:00', '2015-07-04 00:00:00'),
(3, 28, 10, 1, 19, '2015-07-04 00:00:00', '2015-07-04 00:00:00'),
(4, 28, 10, 1, 16, '2015-07-04 00:00:00', '2015-07-04 00:00:00'),
(5, 19, 11, 1, 23, '2015-07-09 00:00:00', NULL),
(6, 26, 11, 1, 23, '2015-07-09 00:00:00', NULL),
(7, 23, 20, 1, 23, '2015-07-09 00:00:00', NULL),
(8, 28, 10, 1, 24, '2015-07-09 00:00:00', NULL),
(9, 19, 11, 1, 24, '2015-07-09 00:00:00', NULL),
(10, 30, 10, 1, 25, '2015-07-09 00:00:00', NULL),
(11, 28, 10, 1, 29, '2015-07-09 00:00:00', NULL),
(12, 29, 10, 1, 29, '2015-07-09 00:00:00', NULL),
(13, 30, 10, 1, 29, '2015-07-09 00:00:00', NULL),
(14, 25, 10, 1, 32, '2015-07-09 00:00:00', NULL),
(15, 28, 10, 1, 32, '2015-07-09 00:00:00', NULL),
(16, 28, 10, 1, 33, '2015-07-09 00:00:00', NULL),
(17, 28, 10, 1, 34, '2015-07-09 00:00:00', NULL),
(18, 28, 10, 1, 35, '2015-07-09 00:00:00', NULL),
(19, 19, 11, 1, 35, '2015-07-09 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_skills_mapping`
--

CREATE TABLE IF NOT EXISTS `user_skills_mapping` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `skills_id` int(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
