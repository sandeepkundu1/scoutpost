<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

public $helpers = ['Form', 'Html', 'Time'];

public function beforeFilter(\Cake\Event\Event $event)
    {
         $this->layout='admin';
         $user = $this->Auth->user();
        
    }
    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
/**
     * Display all pages in a Table form View
     * @params null
     * @return array 
     */
    
    
    public function index()
    {   
        $pages = TableRegistry::get('Pages');
        
       
            if(isset($this->request->params['pass'][0]) && $this->request->params['pass'][0]!=''){
              
                $pageTitle = $this->request->params['pass'][0];

                $result =array();
                $pageDetails=$pages->find('all',['conditions'=>['Pages.pname'=>$pageTitle]]);
                $pageDetail=$pageDetails->toArray();
                

                $this->set('pageDetail',$pageDetail);
                $this->set('page_title',$this->request->params['pass'][0]);
            }else{
                $this->set('page_title','');
            }
        
    }

    public function add_page()
    {
        $this->autoRender=false;
        $pages= TableRegistry::get('Pages');
        $page = $pages->newEntity();
        if ($this -> request -> is(['post', 'put'])) {
            
            $data=array();
        
        $this->request->data['pname']=$this->request->data['data']['Page']['pname'];

        $entity=$pages->patchEntity($page,$this->request->data);
        if ($entity -> errors()) {
                    foreach ($entity->errors() as $key => $val) {
                        foreach ($val as $k => $d) {
                            $vl[] = $d;
                        }
                    }
                    $response_array['error'] = $vl;
                    $this->Flash->error("Kindly fill Page-contants field or select title to change Contant");
                    $this -> response -> statusCode(403);
                    $this->redirect('/pages/index/');
                }else{
              
            if($pages->save($entity)){
                $this->Flash->success('successfully saved');                   
             $this->redirect('/pages/index/'.$this->request->data['data']['Page']['pname']);
            }else{
                $this->Flash->error('error while save');
                $this->redirect('/pages/index/'.$this->request->data['data']['Page']['pname']);
            }
          }  

        }
            
    }
}
