<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
class ExtraComponent extends Component {
	
	public function getUser() {
		return $this->Session->read('UserAuth');
	}
	/**
	 * Used to get user id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getUserId() {
		return $this->Session->read('UserAuth.User.id');
	}
	/**
	 * Used to get group id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getProfileName() {
		
		return $this->Session->read('UserAuth.User.username');
	}
	/**
	 * Used to get group id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getUserName() {
		
		
		return $this->Session->read('UserAuth.User.first_name')." ".$this->Session->read('UserAuth.User.last_name');
	}
	/**
	 * Used to get group id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getUserFame() {
		
		return $this->Session->read('UserAuth.User.first_name');
	}
	
	/**
	 * Used to get email id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getUserEmail() {
		
		return $this->Session->read('UserAuth.User.email');
	}
	
	
	
	/**
	 * Used to get group id from session
	 *
	 * @access public
	 * @return integer
	 */
	public function getGroupId() {
		return $this->Session->read('UserAuth.User.user_group_id');
	}
	/**
	 * Used to get group name from session
	 *
	 * @access public
	 * @return string
	 */
	public function getGroupName() {
		return $this->Session->read('UserAuth.UserGroup.name');
	}
	/**
	 * Used to check is admin logged in
	 *
	 * @access public
	 * @return string
	 */
	public function isAdmin() {
		$groupId = $this->Session->read('UserAuth.User.user_group_id');
		if($groupId==ADMIN_GROUP_ID) {
			return true;
		}
		return false;
	}
	/**
	 * Used to check is guest logged in
	 *
	 * @access public
	 * @return string
	 */
	public function isGuest() {
		$groupId = $this->Session->read('UserAuth.User.user_group_id');
		if(empty($groupId)) {
			return true;
		}
		return false;
	}
	/**
	 * Used to make password in hash format
	 *
	 * @access public
	 * @param string $pass password of user
	 * @return hash
	 */
	public function makePassword($pass, $salt=null) {
		return md5(md5($pass).md5($salt));
	}
	/**
	 * Used to make salt in hash format
	 *
	 * @access public
	 * @return hash
	 */
	public function makeSalt() {
		
		$rand = mt_rand(0, 32);
		$salt = md5($rand . time());
		return $salt;
	}
	
	/*
	 * this function used for checking unique username in user model
	 * */

	public function isUserNameUnique($userName) {
		$result = $this -> find('count', array('conditions' => array('User.username' => $userName)));
		if ($result) {
			return false;
		} {
			return $userName;
		}
	}

	public function getUniqueUsernme($firstName) {
		$userName = $firstName . "." . rand(101, 999);
		return $userName;
		/*$confirmUsername = self::isUserNameUnique($userName);
		if ($confirmUsername) {
			return $confirmUsername;
		} else {
			getUniqueUsernme($firstName);

		}*/
	}
	
	/**
	 * Used to get login as guest
	 *
	 * @access private
	 * @return array
	 */
	private function __useGuestAccount() {
		return $this->login('guest');
		
	}
	
	
	
}
