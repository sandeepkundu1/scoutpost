<?php
namespace App\Controller;

use App\Controller\AppController;

class CoachesController extends AppController
{


  public $helpers = ['Form', 'Html', 'Time'];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');  //defined component here  ... 
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow(['coachesLogin']);  // Define method here to fetch without authentication

    }

  public function coachesLogin(){

  	$this->layout= "user";
     $user = $this->Auth->user();

        if(!empty($user)){
            $this->Auth->setUser($user);

            if($user['role_id']==1 || $user['role_id']==3){
                $this->Flash->error('Email Password Incorrect');
                  $this->Auth->logout();  
            }else{
                return $this->redirect(['controller'=>'Coaches','action' => 'index']);
            }
          }
         if($this->request->is('post')){

             $user = $this->Auth->identify();

             if(!empty($user)){
             $this->Auth->setUser($user);

             if($user['role_id']==2){
                  
               $this->Flash->success('login successfull');
               return $this->redirect(['controller'=>'Coaches','action' => 'index']);
                    
              }else if($user==null){
                  $this->Flash->error('Email Password Incorrect');
                }else{
                  $this->Flash->error('Error while login');
                  $this->Auth->logout();
                }
          }else{
            $this->Flash->error('Incorrect Email password !!');
          }
     }      
  }

  public function index(){
    
    $this->layout='coaches';
    $user = $this->Auth->user();
   $this->set('user',$user); 
  }

  public function logout(){

    $this->autoRender = false;
   $this->Auth->logout();
   $this->Flash->success('successfully logout');
   return $this->redirect(['controller'=>'Categories','action' => 'index']); 
  
  }



}

