<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * UserGames Controller
 *
 * @property \App\Model\Table\UserGamesTable $UserGames
 */
class UserGamesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
    	$this->layout= "admin";
        $this->paginate = [
            'contain' => ['Users', 'Tournaments','UserStats'],

        ];
     
        $this->set('userGames', $this->paginate($this->UserGames));
        $this->set('_serialize', ['userGames']);
    }
    /**
     * Edit method
     *
     * @param string|null $id User Game id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null,$stat_id=null)
    {
        $this->layout= "admin";
        $userGame = $this->UserGames->get($id, [
                           'contain' => []
        ]);
        $UserStats = TableRegistry::get('UserStats');
        $StatsMedia=TableRegistry::get('UserStatsMedia');
        
        if($stat_id !=null)
        {
             $query=$UserStats -> find()-> where(['user_games_id'=>$id])-> andwhere(['id'=>$stat_id]) -> first();
             $query2=$StatsMedia->find()->where(['user_stats_id'=>$stat_id])->first(); 
             $next=$UserStats -> find()->where((['id >'=>$stat_id]))-> andwhere(['user_games_id'=>$id])->first();
             $previous=$UserStats -> find()->where((['id <'=>$stat_id]))-> andwhere(['user_games_id'=>$id])->first();
             
        }else{

                $query=$UserStats -> find()-> where(['user_games_id'=>$id]) -> first();
             }
         
         if ($this->request->is(['patch', 'post', 'put'])) {
            
            $UserStatId = $this->request->data;
            $statId=$UserStatId['id'];
            $userGame = $this->UserGames->patchEntity($userGame, $this->request->data);

            $query1=$UserStats -> find()-> where(['id'=>$statId]) -> first();
            $UserStats = TableRegistry::get('UserStats'); 
            $UserStat = $UserStats -> patchEntity($query1,$this -> request -> data['stat']);
            $StatsMedia=TableRegistry::get('UserStatsMedia');

             
            if($stat_id !=null){
            $query3=$StatsMedia -> find()-> where(['id'=>$this->request->data['statmedia']['id']]) -> first();
            $StatMedia = $StatsMedia -> patchEntity($query3,$this -> request -> data['statmedia']);
            }
            if($userGame -> errors() ){

                  
                foreach ($userGame->errors() as $key => $val) {

                    foreach ($val as $k => $d) {
                        $vl[] = $d;
                    }
                    
                    $this->set('error_list',$vl);
                }

            }
            
            if($stat_id !=null){
                $StatsMedia->save($StatMedia);
            }
              

              if ($this->UserGames->save($userGame)) {
                  if($UserStats->save($UserStat)  ){
                      
                     $this->Flash->success('The user game has been saved.');
                     return $this->redirect(['action' => 'index']);
               } else {
                $this->Flash->error('The user game could not be saved. Please, try again.');
             }
          } 
        }
        if($stat_id !=null)
        {
            $this->set('next',$next['id']);
            $this->set('previous',$previous['id']);
            $this->set('UserMedia',$query2);
        }
        $users = $this->UserGames->Users->find('list', ['limit' => 200]);
        $tournaments = $this->UserGames->Tournaments->find('list', ['limit' => 200]);
        $this->set(compact('userGame', 'users', 'tournaments'));

        $this->set('_serialize', ['userGame']);  
        $this->set('games_id',$id);
        $this->set('UserStatData',$query);
        
    }
    /**
     * Delete method
     *
     * @param string|null $id User Game id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    public function delete($id = null,$stat_id = null)
    {

        $StatsMedia=TableRegistry::get('UserStatsMedia');
        $Tment=TableRegistry::get('Tournaments');

        $this->request->allowMethod(['post', 'delete']);
        if($stat_id !=null)
        {
             
             $query=$StatsMedia->find()->where(['user_stats_id'=>$stat_id])->toArray(); 
             foreach ($query as $key => $value) {

                $StatsMedia->delete($value);
             }
        }
            
        $userGame = $this->UserGames->get($id);
            
            if($userGame['tournament_id']!=null){
                $TmentId = $Tment->get($userGame['tournament_id']);
                $Tment->delete($TmentId);
            }
        if ($this->UserGames->delete($userGame)) {
            $this->Flash->success('The user game has been deleted.');
        } else {
            $this->Flash->error('The user game could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
