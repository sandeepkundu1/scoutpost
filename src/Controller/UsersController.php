<?php
 
namespace App\Controller;


use App\Controller\AppController;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{

	public $helpers = ['Form', 'Html', 'Time'];

	public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow(['login']);
        $user = $this->Auth->user();
        if(isset($user) && $user['role_id']!=1){
            $this->Auth->logout();
             return $this->redirect(['controller'=>'Categories','action' => 'index']);
            }

    
    }


	
    public function login() {

        $this->layout = 'login';
         $user = $this->Auth->user();        
        if(!empty($user)){
            $this->Auth->setUser($user);
            if($user['role_id']==1){
             return $this->redirect(['controller'=>'Users','action' => 'dashboard']);
            }else{
                return $this->redirect(['controller'=>'Categories','action' => 'index']);
            }
         }
        if ($this->request->is('post')) {
        
           $user = $this->Auth->identify();
            $this->autoRender = false;
            $this->Auth->setUser($user);
            if ($user['email_verified']==0) {
                $this->Auth->logout();
                $this->Flash->success(__('Sorry your account is not active, please contact to Administrator'),'database_error_flash');
                }else if($user['role_id'] == 1){
                echo 'success';
                }else {
                   echo 'false';
                 }
        }
    }
    
    public function dashboard()
    {
        
        $this->layout = 'admin';
       // pr($this->layout); die; 
    }

    public function logout() {
        $this->layout = 'front';
         $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function myprofile(){

        echo 'my profile'; die;
    }
    
    public function index()
	{
		//$this->layout="login";
        $user = $this->Users->newEntity();

		if($this->request->is('post'))
		{

			$this->request->data['name']=$this->request->data['Name'];
			$this->request->data['age']=$this->request->data['Age'];
			$this->request->data['gender']=strtoupper($this->request->data['Gender']);
			
			$user = $this->Users->patchEntity($user, $this->request->data);

			if ($this->Users->save($user)) {
        		$this->Flash->success('The user has been saved.');
            	return $this->redirect(['action' => 'viewUser']);
        	} else {
                $this->Flash->error('The user could not be saved. Please, try again.');
    	    }
 		}
			$this->set(compact('user'));
			$this->set('_serialize', ['user']);
	}


    public function viewUser()
    {

        $this->layout= "admin";

        $query=!empty($this->request->query['query'])?$this->request->query['query']:'';

         if($query!=''){
         $this->paginate = [
         'fields'=>[
         "Users.id",
         "Users.first_name",
         "Users.last_name",
         "Users.username",
         "Users.email",
         "Users.email_verified",
         "Users.active",
         "Users.profile_image"
         ],
        'limit' => 20,
        'conditions'=>[
        'OR'=>[
        'Users.email LIKE'=>'%'.$query.'%',
        'Users.first_name LIKE'=>'%'.$query.'%',
        'Users.last_name LIKE'=>'%'.$query.'%'
        ]]
        
    ];
        }else{
         $this->paginate = [
         'fields'=>[
         "Users.id",
         "Users.first_name",
         "Users.last_name",
         "Users.username",
         "Users.email",
         "Users.email_verified",
         "Users.active",
         "Users.profile_image"
         ],
        'limit' => 20,
        'order'=>['Users.id'=>'DESC'],
        
    ];
       }

        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    
    }

    public function redirect_url()
    {
         $this->autoRender = false;
            if($this->request->isPost()){
               
                    $this->redirect([
                            'controller'=>'Users',
                            'action'=>'viewUser',
                            'query'=>!empty($this->request->data['email'])?$this->request->data['email']:''
                            ]);
            }
    }

    public function edit($id = null)
    {
    	
    	$user = $this->Users->get($id);

    	if ($this->request->is(['patch', 'post', 'put'])) {

    		$this->request->data['name']=$this->request->data['Name'];
			$this->request->data['age']=$this->request->data['Age'];
			$this->request->data['gender']=strtoupper($this->request->data['Gender']);
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
		$this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function viewProfile($id = null)
    {
        $this->layout= "admin"; 
        $userprofiles = TableRegistry::get('Userprofiles');
        $teamdivision = TableRegistry::get('teamdivision');
        $this->paginate = [
         'conditions'=>['Users.id'=>$id],
        
    ];
    
        $query = $userprofiles-> find() -> where(['user_id' => $id]) -> first();
        $query2 = $teamdivision-> find('all') -> where(['user_id' => $id]);
        foreach ($query2 as $key => $value) {
            $teamArr[]=$value;
        }
        
        $this->set('users', $this->paginate($this->Users));
        $this->set('userprofiles', $query);
        $this->set('teamdivision', $teamArr);
        $this->set('_serialize', ['users']);
    }


    public function editProfile($id = null)
    {
        $this->layout= "admin"; 
         $userprofiles = TableRegistry::get('Userprofiles');
        $teamdivision = TableRegistry::get('teamdivision');

        $user = $this->Users->get($id);

         $query = $userprofiles-> find() -> where(['user_id' => $id]) -> first();
        $query2 = $teamdivision-> find('all') -> where(['user_id' => $id]);
        foreach ($query2 as $key => $value) {
            $teamArr[]=$value;
        }

        $this->set('userprofiles', $query);
        $this->set('teamdivision', $teamArr);

        $ImgPath = WWW_ROOT."img/profile_pic/";
    if ($this->request->is(['patch', 'post', 'put'])) {
        
            if(!empty($this->request->data['profile_image']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['profile_image']['name'];
                            move_uploaded_file($this->request->data['profile_image']['tmp_name'], $ImgPath .$imageName);
                            
                            if($this->request->data['profile_image_old'] !='default.png')
                            {
                            @unlink($ImgPath.$this->request->data['profile_image_old']);
                            }
                            $this->request->data['profile_image']=$imageName; 
                        }else{
                            
                            $this->request->data['profile_image']= $user['profile_image']; 
                            }


            $user = $this->Users->patchEntity($user, $this->request->data);

            if($user -> errors()){

                foreach ($user->errors() as $key => $val) {

                    foreach ($val as $k => $d) {
                        $vl[] = $d;
                    }
                    $this->set('error_list',$vl);
                }

            }else{

                $userprofiles = TableRegistry::get('Userprofiles');
                 
                 
                $query = $userprofiles-> find() -> where(['user_id' => $id]) -> first();

                $profile_entity = $userprofiles -> patchEntity($query,$this -> request -> data) ;
                    if($profile_entity -> errors()) {
                foreach ($profile_entity->errors() as $key => $val) {

                    foreach ($val as $k => $d) {
                        $vlz[] = $d;
                    }
                    $this->set('error_listz',$vlz);
                }
                }else{

                $this->Users->save($user);
                     $data = $this -> request -> data['teamdivision'];
            
                    $a = $data['current_team'];
                    $b = $data['division'];
                    $arr = array();
                    $teamdivision = TableRegistry::get('teamdivision');
                for($i = 0;$i < count($a); $i++)

                {
                    
                    $this -> request -> data['teamdivision']['current_team'] = $a[$i];
                    $this -> request -> data['teamdivision']['division'] = $b[$i];
                    $query2 = $teamdivision-> find('all') -> where(['user_id' => $id]);
                     foreach($query2 as $tre){
                    $team_entity = $teamdivision -> patchEntity($tre,$this -> request -> data);    
                     }   
                    
                    $teamdivision -> save($team_entity);
                }
                    
                    $userprofiles = TableRegistry::get('Userprofiles');
                    $query = $userprofiles-> find() -> where(['user_id' => $id]) -> first();
                    
                    if(isset($this -> request -> data['Userprofiles']['pitcher']) && $this -> request -> data['Userprofiles']['pitcher'] ==1){
                        $this -> request -> data['Userprofiles']['pitcher']=1;

                    }else{
                        $this -> request -> data['Userprofiles']['pitcher']=0;
                    }
                    if(isset($this -> request -> data['Userprofiles']['outfilder']) && $this -> request -> data['Userprofiles']['outfilder'] ==1){
                        $this -> request -> data['Userprofiles']['outfilder']=1;

                    }else{
                        $this -> request -> data['Userprofiles']['outfilder']=0;
                    }
                    if(isset($this -> request -> data['Userprofiles']['infilder']) && $this -> request -> data['Userprofiles']['infilder'] ==1){
                        $this -> request -> data['Userprofiles']['infilder']=1;

                    }else{
                        $this -> request -> data['Userprofiles']['infilder']=0;
                    }
                    if(isset($this -> request -> data['Userprofiles']['catcher']) && $this -> request -> data['Userprofiles']['catcher'] ==1){
                        $this -> request -> data['Userprofiles']['catcher']=1;

                    }else{
                        $this -> request -> data['Userprofiles']['catcher']=0;
                    }
                    $profile_entity = $userprofiles -> patchEntity($query,$this -> request -> data);

            if ($userprofiles -> save($profile_entity)) {
                $this->Flash->success('The user has been updated.');
                return $this->redirect(['action' => 'viewUser']);
            } else {

                $this->Flash->error('The user could not be updated. Please, try again.');
            }

                     }
                        }
                 
              }


        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function addUser()
    {
        $this->layout= "admin"; 
        $ImgPath = WWW_ROOT."img/profile_pic/";
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            if(!empty($this->request->data['profile_image']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['profile_image']['name'];
                            move_uploaded_file($this->request->data['profile_image']['tmp_name'], $ImgPath .$imageName);
                            
                            $this->request->data['profile_image']=$imageName; 
                        }else{
                            
                            $this->request->data['profile_image']= 'default.png'; 
                            }
               $salt = md5(rand(9,99999));
               $this -> request -> data['salt'] = $salt;
               $this -> request -> data['role_id'] = 3;
               $this -> request -> data['username'] = $this -> request -> data['first_name'].'_'.rand(1,9999);
               $this -> request -> data['activation_key'] = $salt;             

            $user = $this->Users->patchEntity($user, $this->request->data);

            if($user -> errors()){

                foreach ($user->errors() as $key => $val) {

                    foreach ($val as $k => $d) {
                        $vl[] = $d;
                    }
                    $this->set('error_list',$vl);
                }

            }else{

                $userprofiles = TableRegistry::get('Userprofiles');
                    $profile_entity = $userprofiles -> newEntity($this -> request -> data);
                    if($profile_entity -> errors()) {
                foreach ($profile_entity->errors() as $key => $val) {

                    foreach ($val as $k => $d) {
                        $vlz[] = $d;
                    }
                    $this->set('error_listz',$vlz);
                }
                }else{

                $this->Users->save($user);
                     $data = $this -> request -> data['teamdivision'];
            
                    $a = $data['current_team'];
                    $b = $data['division'];
                    $arr = array();
                    $teamdivision = TableRegistry::get('teamdivision');
                for($i = 0;$i < count($a); $i++)

                {
                    
                    $this -> request -> data['teamdivision']['current_team'] = $a[$i];
                    $this -> request -> data['teamdivision']['division'] = $b[$i];
                    $this -> request -> data['teamdivision']['user_id'] = $user -> id;
                    $team_entity = $teamdivision -> newEntity($this -> request -> data);
                    $teamdivision -> save($team_entity);
                }
                    
                    $userprofiles = TableRegistry::get('Userprofiles');
                    $this -> request -> data['Userprofiles']['user_id'] = $user -> id;
                    $this -> request -> data['Userprofiles']['curr_team'] = $user -> id;
                    $profile_entity = $userprofiles -> newEntity($this -> request -> data);
            
            if ($userprofiles -> save($profile_entity)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'viewUser']);
            } else {

                $this->Flash->error('The user could not be saved. Please, try again.');
            }

                     }
                        }
                 
              }
             
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function changePassword($id =null)
    {
            $this->layout= "admin"; 
            $user = $this->Users->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

       		if($this->request->data['password']==$this->request->data['cpassword']){

        $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The Password has been changed.');
                return $this->redirect(['action' => 'viewUser']);
            } else {
                $this->Flash->error('The Password could not be changed. Please, try again.');
            }
        }
    else{
    		$this->Flash->error("The Password didn't matched.");
                return $this->redirect(['action' => 'changePassword/'.$id]);
          }
      }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function deleteProfile($id = null)
    {
        $this->autoRender= false;
        $userprofiles = TableRegistry::get('Userprofiles');
        $teamdivision = TableRegistry::get('teamdivision');
        $ImgPath = WWW_ROOT."img/profile_pic/";
        $this->request->allowMethod(['get', 'delete']);
        $user = $this->Users->get($id);
        $query = $userprofiles-> find() -> where(['user_id' => $id]) -> first();
        $query2 = $teamdivision-> find('all') -> where(['user_id' => $id]);
        if ($this->Users->delete($user)) {
            $userprofiles->delete($query);
            foreach ($query2 as $key => $value) {
                $teamdivision->delete($value);
            }
            
        	if($user['prifile_image']!='default.png'){
        	@unlink($ImgPath.$user['prifile_image']);
           }
            $this->Flash->success('The user has been deleted.');
        } else {
            $this->Flash->error('The user could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'viewUser']);

    }

    public function viewFeature()
    {
        $features = TableRegistry::get('features');

            $this->layout= "admin";

            $this->paginate = [
            
        'limit' => 20,
        'order'=>['Features.id'=>'DESC'],
        
    ];
       
        $this->set('features', $this->paginate($features));
        $this->set('_serialize', ['features']); 
    }

    public function addFeature()
    {

        $features = TableRegistry::get('features');

            $this->layout= "admin";

            $ImgPath = WWW_ROOT."img/feature_pic/";
        $user = $features->newEntity();
        if ($this->request->is('post')) {

            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            $this->request->data['photo']=$imageName; 
                        }else{
                            
                            $this->request->data['photo']= 'default.png'; 
                            }

            $user = $features->patchEntity($user, $this->request->data);
            if ($features->save($user)) {
                $this->Flash->success('The features has been saved.');
                return $this->redirect(['controller'=>'Users','action' => 'viewFeature']);
            } else {
                $this->Flash->error('The features detail could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
	
    }
     public function editFeature($id= null)
    {
    		$this->layout= "admin";

    		$features = TableRegistry::get('features');

    		$user = $features->get($id);
        $ImgPath = WWW_ROOT."img/feature_pic/";
    if ($this->request->is(['patch', 'post', 'put'])) {

            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            
                            if($this->request->data['profile_image_old'] !='default.png')
                            {
                            @unlink($ImgPath.$this->request->data['feature_image_old']);
                            }
                            $this->request->data['photo']=$imageName; 
                        }else{
                            
                            $this->request->data['photo']= $user['photo']; 
                            }


            $user = $features->patchEntity($user, $this->request->data);
            if ($features->save($user)) {
                $this->Flash->success('The data has been saved.');
                return $this->redirect(['action' => 'viewFeature']);
            } else {
                $this->Flash->error('The data could not be saved. Please, try again.');
            }
        }


        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function deleteFeature($id = null)
    {
        $this->autoRender= false;
        $features = TableRegistry::get('features');
        $ImgPath = WWW_ROOT."img/feature_pic/";

        $this->request->allowMethod(['get', 'delete']);
        $user = $features->get($id);
        
        if ($features->delete($user)) {

        	if($user['photo']!='default.png'){
        	@unlink($ImgPath.$user['photo']);
        }
            $this->Flash->success('The features has been deleted.');
        } else {
            $this->Flash->error('The features could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'viewFeature']);

    }

    public function viewGallery()
    {
    	$this->layout= "admin";
        $gallery = TableRegistry::get('galleries');
        $this->paginate = [
        'limit' => 20,
        'order'=>['galleries.id'=>'DESC'],
        
    ];
       
        $this->set('galleries', $this->paginate($gallery));
        $this->set('_serialize', ['galleries']); 
    }

    public function addGallary()
    {
        $this->layout= "admin";

        $gallery = TableRegistry::get('galleries');
        $ImgPath = WWW_ROOT."img/gallery_pic/";
        $user = $gallery->newEntity();
        if ($this->request->is('post')) {

            if(!empty($this->request->data['gallery_image']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['gallery_image']['name'];
                            move_uploaded_file($this->request->data['gallery_image']['tmp_name'], $ImgPath .$imageName);
                            
                            $this->request->data['gallery_image']=$imageName; 
                        }else{
                            
                            $this->request->data['gallery_image']= 'default.png'; 
                            }

            $user = $gallery->patchEntity($user, $this->request->data);
            if ($gallery->save($user)) {
                $this->Flash->success('The gallery has been saved.');
                return $this->redirect(['controller'=>'Users','action' => 'viewGallery']);
            } else {
                $this->Flash->error('The gallery detail could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function verifyEmail($id = null)
    {
         $this->autoRender= false;       
         $user = $this->Users->get($id);
            $user['email_verified']=1;
            if ($this->Users->save($user)) {
                $this->Flash->success('The Email has been veryFied.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            } else {
                $this->Flash->error('The Email has not been veryFied.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            }
    
    }
    public function unVerifyEmail($id = null)
    {
         $this->autoRender= false;       
         $user = $this->Users->get($id);
            $user['email_verified']=0;
            if ($this->Users->save($user)) {
                $this->Flash->success('The Email has been Unverified.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            } else {
                $this->Flash->error('The Email has not been UnveriFied.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            }
    }

    public function makeActive($id = null)
    {
         $this->autoRender= false;       
         $user = $this->Users->get($id);
            $user['active']=1;
            if ($this->Users->save($user)) {
                $this->Flash->success('This User is Active now.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            } else {
                $this->Flash->error('Error.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            }
    
    }
    public function makeinActive($id = null)
    {
         $this->autoRender= false;       
         $user = $this->Users->get($id);
            $user['active']=0;
            if ($this->Users->save($user)) {
                $this->Flash->success('This User is inactive now.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            } else {
                $this->Flash->error('Error.');
                return $this->redirect(['controller'=>'Users','action' => 'viewUser']);
            }
    
    }
    public function deteteGalery($id=null)
    {
        $this->autoRender=false;
        $gallery=TableRegistry::get('galleries');

        if(isset($id)){
         $result=$gallery->get($id);
         $row=$gallery->delete($result);
         if($row){
            $this->Flash->success('successfully deleted');
            $this->redirect(['controller'=>'Users','action'=>'viewGallery']);
         }else{

            $this->Flash->success('Error while deleting');
            $this->redirect(['controller'=>'Users','action'=>'viewGallery']);
         }   

        }
    }

    /*
            * 1000000000 byte = 1 gb 
            * in database we are saving data in byte.
    */
   public function checkSize($id=null)
     {
         $this->layout= "admin";   
        $user_media = TableRegistry::get('user_media_info');   
        $user_stats = TableRegistry::get('user_stats_media');  
        
        $memory = TableRegistry::get('memory_allocation');
        $result= $memory->find()
                        ->where(['user_id'=>$id])->First();

        $res1= $user_media->find()
                        ->select(['media_size'=>'sum(media_size)'])
                        ->where(['user_id'=>$id])->First()->toArray();

        $res3= $user_stats->find()
                        ->select(['media_size'=>'sum(media_size)'])
                        ->where(['user_id'=>$id])->First()->toArray();
                                        
        if(!empty($result)){
                
                $totalmemory = $result['total_memory'];
                $memory_used = $result['memory_size'];            
        }else{
        $memory_used = $res1['media_size']+$res3['media_size'];    
        $totalmemory= "2500000000";
    }
        
                       $this->set('usedmemory',$memory_used);    
                       $this->set('totalmemory',$totalmemory);
                       $this->set('id',$id);
                       
        
    }

        public function changePermission($id = null, $totalmemory= null,$usedmemory= null)
        {
            $this->layout= "admin";

            $memory = TableRegistry::get('memory_allocation'); 

              $id = $this->request->query['id'];
              $totalmemory = $this->request->query['totalmemory'];
              $usedmemory = $this->request->query['usedmemory'];  
                
            
            if(!empty($id)){

                 $user = $this->Users->get($id);

                        $result= $memory->find()
                        ->where(['user_id'=>$id])->First();

                        $this->set('result',$result);
                        $this->set('totalmemory',$totalmemory);
                        $this->set('usedmemory',$usedmemory);

            }

            if ($this->request->is(['patch', 'post', 'put'])) {

                    $tableid = $this->request->data['id'];
                    if(!empty($tableid)){
                    $user = $memory->get($tableid);


                    $this->request->data['user_id']=$user['user_id'];
                 $user = $memory->patchEntity($user, $this->request->data);
             }else{
                
                $this->request->data['user_id']=$id;
                $user = $memory->newEntity($this->request->data);
             }

            if ($memory->save($user)) {
                $this->Flash->success('The data has been saved.');
                return $this->redirect(['action' => 'checkSize',$id]);
            } else {
                $this->Flash->error('The data could not be saved. Please, try again.');
            }


        } 
    }

    /*
     * Created By Anshul
     * Created On 11-06-2015
     * use to View School galery images for acadmics profile . 
    */



    public function viewSchool()
    {
        $school = TableRegistry::get('school_galeries');

            $this->layout= "admin";

            $this->paginate = [
            
        'limit' => 20,
        'order'=>['school_galeries.id'=>'DESC'],
        
    ];
       
        $this->set('school', $this->paginate($school));
        $this->set('_serialize', ['school']); 
    }

    /*
     * Created By Anshul
     * Created On 11-06-2015
     * use to add School galery images for acadmics profile . 
    */

    public function addSchool()
    {
        $this->layout= "admin";
        $school = TableRegistry::get('school_galeries');
        $ImgPath = WWW_ROOT."img/school_pic/";

        if ($this->request->is('post')) {
            
            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            $this->request->data['photo']=$imageName; 
                        }else{
                            
                            $this->request->data['photo']= 'default.png'; 
                            }

             $newentry = $school->newEntity($this->request->data);
            
            if ($school->save($newentry)) {
                $this->Flash->success('The school Galery has been saved.');
                return $this->redirect(['controller'=>'Users','action' => 'viewSchool']);
            } else {
                $this->Flash->error('The school Galery could not be saved. Please, try again.');
            }
        }
        $this->set(compact('school'));
        $this->set('_serialize', ['school']);
    
    }

    /*
     * Created By Anshul
     * Created On 11-06-2015
     * use to Edit School galery images for acadmics profile . 
    */

     public function editSchool($id= null)
    {
            $this->layout= "admin";

            $school = TableRegistry::get('school_galeries');

            $schoolDetail = $school->get($id);
        $ImgPath = WWW_ROOT."img/school_pic/";
    if ($this->request->is(['patch', 'post', 'put'])) {

            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            if($this->request->data['school_image_old'] !='default.png')
                            {
                            @unlink($ImgPath.$this->request->data['school_image_old']);
                            }
                            $this->request->data['photo']=$imageName; 
                            }else{
                            $this->request->data['photo']= $schoolDetail['photo']; 
                            }


            $schools = $school->patchEntity($schoolDetail, $this->request->data);
            if ($school->save($schools)) {
                $this->Flash->success('The data has been updated.');
                return $this->redirect(['action' => 'viewSchool']);
            } else {
                $this->Flash->error('The data could not be saved. Please, try again.');
            }
        }


        $this->set(compact('schoolDetail'));
        $this->set('_serialize', ['schoolDetail']);
    }

    /*
     * Created By Anshul
     * Created On 11-06-2015
     * use to delete School galery images for acadmics profile . 
    */

    public function deleteSchool($id = null)
    {
        $this->autoRender= false;
        $school = TableRegistry::get('school_galeries');
        $ImgPath = WWW_ROOT."img/school_pic/";

        $this->request->allowMethod(['get', 'delete']);
        $school_id = $school->get($id);
        
        if ($school->delete($school_id)) {

            if($school_id['photo']!='default.png'){
            @unlink($ImgPath.$school['photo']);
        }
            $this->Flash->success('The school Gallery has been deleted.');
            return $this->redirect(['action' => 'viewSchool']);
        } else {
            $this->Flash->error('The School Gallery could not be deleted. Please, try again.');
            return $this->redirect(['action' => 'viewSchool']);
        }
        

    }

    /*
     * Created By Anshul
     * Created On 29-07-2015
     * use to View Market galery images for acadmics profile . 
    */



    public function viewMarket()
    {
        $market = TableRegistry::get('market_galeries');

            $this->layout= "admin";

            $this->paginate = [
            
        'limit' => 20,
        'order'=>['market_galeries.id'=>'DESC'],
        
    ];
       
        $this->set('market', $this->paginate($market));
        $this->set('_serialize', ['market']); 
    }

    /*
     * Created By Anshul
     * Created On 29-07-2015
     * use to add Market galery images for acadmics profile . 
    */

    public function addMarket()
    {
        $this->layout= "admin";
        $market = TableRegistry::get('market_galeries');
        $ImgPath = WWW_ROOT."img/market_pic/";

        if ($this->request->is('post')) {
            
            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            $this->request->data['photo']=$imageName; 
                        }else{
                            
                            $this->request->data['photo']= 'default.png'; 
                            }

             $newentry = $market->newEntity($this->request->data);
            
            if ($market->save($newentry)) {
                $this->Flash->success('The market Galery has been saved.');
                return $this->redirect(['controller'=>'Users','action' => 'viewMarket']);
            } else {
                $this->Flash->error('The Market Galery could not be saved. Please, try again.');
            }
        }
        $this->set(compact('market'));
        $this->set('_serialize', ['market']);
    
    }

    /*
     * Created By Anshul
     * Created On 29-07-2015
     * use to Edit Market galery images for acadmics profile . 
    */

     public function editMarket($id= null)
    {
            $this->layout= "admin";

            $market = TableRegistry::get('market_galeries');

            $marketDetail = $market->get($id);
        $ImgPath = WWW_ROOT."img/market_pic/";
    if ($this->request->is(['patch', 'post', 'put'])) {

            if(!empty($this->request->data['photo']['tmp_name'])){
                            $imageName = rand(5,10000).'_'.$this->request->data['photo']['name'];
                            move_uploaded_file($this->request->data['photo']['tmp_name'], $ImgPath .$imageName);
                            
                            if($this->request->data['market_image_old'] !='default.png')
                            {
                            @unlink($ImgPath.$this->request->data['market_image_old']);
                            }
                            $this->request->data['photo']=$imageName; 
                            }else{
                            $this->request->data['photo']= $marketDetail['photo']; 
                            }


            $markets = $market->patchEntity($marketDetail, $this->request->data);
            if ($market->save($markets)) {
                $this->Flash->success('The data has been updated.');
                return $this->redirect(['action' => 'viewMarket']);
            } else {
                $this->Flash->error('The data could not be saved. Please, try again.');
            }
        }


        $this->set(compact('marketDetail'));
        $this->set('_serialize', ['marketDetail']);
    }

    /*
     * Created By Anshul
     * Created On 29-07-2015
     * use to delete Market galery images for acadmics profile . 
    */

    public function deleteMarket($id = null)
    {
        $this->autoRender= false;
        $market = TableRegistry::get('market_galeries');
        $ImgPath = WWW_ROOT."img/market_pic/";

        $this->request->allowMethod(['get', 'delete']);
        $market_id = $market->get($id);
        if ($market->delete($market_id)) {

            if($market_id['photo']!='default.png'){
            @unlink($ImgPath.$market['photo']);
        }
            $this->Flash->success('The market Gallery has been deleted.');
            return $this->redirect(['action' => 'viewMarket']);
        } else {
            $this->Flash->error('The market Gallery could not be deleted. Please, try again.');
            return $this->redirect(['action' => 'viewMarket']);
        }

    }
}