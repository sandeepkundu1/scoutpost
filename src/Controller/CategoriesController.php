<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Crypto\Mcrypt;
use Cake\Network\Email\Email;
use Cake\Routing\Router;
use Cake\Utility\Security;

class CategoriesController extends AppController
{
    public $helpers = ['Form', 'Html', 'Time'];

    public function initialize()
    {

        parent::initialize();
        $this->loadComponent('Paginator');  //defined component here  ... 
         $user = $this->Auth->user();
          $this->set('checkplan',$user['check_plan']);    
         
    }

    public function isAuthorized($user)
    {
        if($user['check_plan']==1 && $this->request->params['action']=='updateProfile'){
                
            return false;
          }else{
            return true;
          }
        
    }
    

    public function beforeFilter(\Cake\Event\Event $event)
    {

        $this->Auth->allow(['index','signup','userLogin','viewMarkets','userVerification','resetPassword','profile_url','viewFeatures','aboutUs','termsCondition','mailPage','mailPage2','view_player_post']);  // Define method here to fetch without authentication
    }   
    /**
     * Index method
     * created by anshul
     * date 07-04-2015 
     * @return void Redirects on successful .
     */
    public function index()
    {

        $this->layout= "user";
        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'index']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $this->set('pagecontant',$row);
    }

    /**
     * signup method
     * created by anshul
     * date 07-04-2015 
     * @return void Redirects on successful registration, renders userlogin otherwise.
     */

    public function signup()
     {
       $this->layout= "user";
     }

     /**
     * signup method
     * created by anshul
     * date 07-04-2015 
     * @return void Redirects on successful registration, renders userlogin otherwise.
     */


    public function userLogin()
    {
         $this->layout= "user";
        $user = $this->Auth->user();
        if(!empty($user)){
            $this->Auth->setUser($user);
            if($user['role_id']==1 || $user['role_id']==1){
            return $this->redirect(['controller'=>'Users','action' => 'dashboard']);
            }else{
                return $this->redirect(['controller'=>'Categories','action' => 'profile']);
            }
         }
         if($this->request->is('post')){

             $user = $this->Auth->identify();
             
             if(!empty($user)){
             $this->Auth->setUser($user);
             
             if($user){
                if($user['email_verified']==0){
                    
                    $this->Auth->logout();
                    $this->Flash->success('Please verify Your Email And activate your code');
                    $this->redirect(['action' => 'userLogin']);
                
                }else{

                	if($user['role_id']==1 || $user['role_id']==2){
                		$this->Flash->error('you are not a authorized user');
                		$this->Auth->logout();	
                		
                	}else{
                    $this->Flash->success('success login');
                       return $this->redirect(['controller'=>'Categories','action' => 'profile']);
                    }

                    }
                }else{
                    $this->Flash->error('Error');
                    }
                }else{
                    $this->Flash->set('Somthing going wrong Check Your email & Password');
                    }
                $this->set(compact('user'));
                }
    }

    /**
     * signup method
     * created by anshul
     * date 08-04-2015 
     * @return void Redirects on successful registration, renders userlogin otherwise.
     */


    public function profile()
    {
        $this->layout= "profile";
        $user= $this->Auth->user();
        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'profile']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);
        $users = TableRegistry::get('Users');
        $userId= $this->Auth->user('id');
        $profileImg = $users->find('all')->where(['Users.id'=>$userId]);
        $profileImgs = $profileImg->toArray();
        $img = $profileImgs[0]['profile_image'];
        
        $this->set('user_img',$img);
        $this->set('pagecontent',$row);
        
        
    }
    /*   
    *  created for send sms alert with this action as URL 
    *  created on 26-06-2015
    *  created on anshul  
    */ 

public function profile_url($id= null)
    {
        $id = $this->request->query['id'];
        $this->layout= "profileUrl";
        $users = TableRegistry::get('users');
        $profiles = TableRegistry::get('userprofiles');
        $user_id = $users->get($id);
        
        
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user_id['id']])->First();

        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user_id);
        
        $profileImg = $users->find('all')->where(['users.id'=>$id]);
        //echo ($profileImg); die;
        $profileImgs = $profileImg->toArray();
        $img = $profileImgs[0]['profile_image'];
        
        $this->set('user_img',$img);
        
        
    }
    public function updateProfile($id= null)
    {
         $this->layout= "profile";
         $user= $this->Auth->user('id');
         $this->set('userId',$user);
         $user= $this->Auth->user();
         $profiles = TableRegistry::get('userprofiles');
         $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
         $this->set('sport',$sportType['sport']);
         $this->set('userDetail',$user);
         $users = TableRegistry::get('Users');
        $userId= $this->Auth->user('id');
        $profileImg = $users->find('all')->where(['Users.id'=>$userId]);
        $profileImgs = $profileImg->toArray();
        $img = $profileImgs[0]['profile_image'];
        
        $this->set('user_img',$img);
        
    }

     public function aboutUs()
    {
        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'about']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $this->set('pagecontant',$row);
        $this->layout= "user";

    }


    public function gamesStatus()
    {
         $this->layout= "profile";
         $user= $this->Auth->user();
         $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);
    }

    public function skills()
    {
         $this->layout= "profile";
         $user= $this->Auth->user();
         $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);
    }

    public function academic()
    {
         $this->layout= "profile";
         $user= $this->Auth->user();
         $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
         $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'acadmic']]);

        foreach ($result as $key => $value) {
           $row[]=$value;
        }
       
        $this->set('pagecontant',$row);
        $this->set('userDetail',$user);
    }
    public function palyerLogout()
    {
        $this->autoRender=false;
         $this->Auth->logout();
         $this->redirect(['action' => 'index']);
    }

    public function termsCondition()
    {
        $this->layout= "user";
        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'termscondition']]);
    }
        public function privacy()
    {
        $this->layout= "profile";
        $profiles = TableRegistry::get('userprofiles');
        $user= $this->Auth->user();
        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'privacy']]);
        
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);
        $this->set('pagecontant',$row);
    }
    

    public function mailPage()
    {

         $this->layout= "user";
         $page=TableRegistry::get('Pages');
         $user=TableRegistry::get('Users');
         $id = $this->request->query['id'];
        include(ROOT.'/vendor/paypal/common/ExecuteAgreement.php'); 
        $users = $user->get($id);
            
         $users['plan_id']='F';

         if($user->save($users)){

        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'mailPage']]);

        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $this->Auth->logout();
        $this->set('pagecontant',$row);

        }
       else{
        $this->Auth->logout();
        $this->Flash->error('error while sending mail');
       }

    }

    public function mailPage2()
    {

         $this->layout= "user";
         $page=TableRegistry::get('Pages');
         $user=TableRegistry::get('Users');
         $id = $this->request->query['id'];
         include(ROOT.'/vendor/paypal/common/ExecuteAgreement.php');
        $users = $user->get($id);
            
         $users['plan_id']='P';
         $users['check_plan']='0';

         if($user->save($users)){
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'mailPage2']]);

        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $this->Auth->logout();
        $this->set('pagecontant',$row);
        }
       else{
        $this->Auth->logout();
        $this->Flash->error('error while sending mail');
       }
    }

    public function resetPassword()
    {
         $this->layout= "user";
         $this->set('ident',$this->request->query['ident']);
         $this->set('activate',$this->request->query['activate']);       
    }
    public function viewFeatures()
    {
        $this->layout= "user"; 
    }


    /*
     * created by anshul
     * created on 29-07-2015 
     *  created for view all market adds features and description 
     */
    public function viewMarkets()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();
        $this->set('userDetail',$user);
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
    }

    
    public function userVerification() {
        $this->autoRender = false;

        if(isset($this->request->query['active'])) {

             $activateKey= $this->request->query['active'];
             $Users = TableRegistry::get('Users');
             $userdata = $Users->find('all')
             ->where(['Users.activation_key'=>$activateKey]);
               
               foreach ($userdata as $key => $data) {
               $userdetail=$data->toArray();
             
               }
             
            if (!empty($userdetail)) {
                        
                    
                if ($userdetail['email_verified'] == 0) {
                    
                    $theKey = $userdetail['activation_key'];
                      $user=array();

                    if ($activateKey==$theKey) {
                       
                        $query = $Users->query();
                        $query->update()
                        ->set(['email_verified' => 1,'active'=>1])
                        ->where(['activation_key' => $theKey])
                        ->execute();

                         $this->Flash->success(__('Thank you, your account is activated now'));
                         $this->redirect(['action' => 'UserLogin']);
                    }
                } else {
                $this->Flash->success(__('Thank you, your account is already activated'));
                $this->redirect(['action' => 'UserLogin']);
            }
        }else{
            $this->Flash->success(__('Sorry something went wrong, please click on the link again'));
                $this->redirect(['action' => 'UserLogin']);
        
            }   
        }
    }

    public function game_stat_2()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();

        $this->set('userDetail',$user);
        $profiles = TableRegistry::get('userprofiles');
        
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
    }

    public function game_stat_3()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();
        $this->set('userDetail',$user);
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
    }

    public function game_stat_history()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();
        $this->set('userDetail',$user);
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
    }


    public function profile_image()
    {
        $this->autoRender=false;
        $users = TableRegistry::get('Users');
        $user= $this->Auth->user('id');
        $ImgPath = WWW_ROOT . "img/profile_pic/";

        if(isset($_FILES["profile_image"]["type"]))
                {

                $validextensions = array("jpeg", "jpg", "png","JPG","PNG","JPEG");
                $temporary = explode(".", $_FILES["profile_image"]["name"]);
                $file_extension = end($temporary);
               
                if(in_array($file_extension, $validextensions)) {
                    
                if ($_FILES["profile_image"]["error"] > 0)
                {
                 echo json_encode(['status'=>'error','message'=>'error while uploading']); die;      
                //echo "Return Code: " .  . "<br/><br/>";
                }
                else
                {
                if (file_exists($ImgPath . $_FILES["profile_image"]["name"])) {
                echo json_encode(['status'=>'error','message'=>' file already exists']); die;      
                }
                else
                {
                    
                $sourcePath = $_FILES['profile_image']['tmp_name']; // Storing source path of the file in a variable
                $targetPath = $ImgPath.$_FILES['profile_image']['name']; // Target path where file is to be stored
                move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
                /*echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
                echo "<br/><b>File Name:</b> " . $_FILES["profile_image"]["name"] . "<br>";
                echo "<b>Type:</b> " . $_FILES["profile_image"]["type"] . "<br>";
                echo "<b>Size:</b> " . ($_FILES["profile_image"]["size"] / 1024) . " kB<br>";
                echo "<b>Temp file:</b> " . $_FILES["profile_image"]["tmp_name"] . "<br>";*/
                echo json_encode(['status'=>'success','message'=>'Successfully uploaded']);   
                $imgnames = $_FILES['profile_image']['name'];
                //@unlink($ImgPath.$this->request->data['feature_image_old']);
                    
                    $user = $users->get($this->Auth->user('id'));
                    $user['profile_image']=$imgnames;
                    
                    if ($users->save($user)) {
                        $this->Flash->success('Successfully uploaded.');
                        
                    } else {
                        $this->Flash->error('The image could not be saved. Please, try again.');
                    }
                        
                }
                }
                }
                else
                {
                 echo json_encode(['status'=>'error','message'=>'Invalid file Type only support-jpeg/jpg/png/JPG/PNG/JPEG/ format']); die;   
                //echo "<span id='invalid'>***Invalid file Size or Type***<span>";
                }
                }


    }     
    
    public function view_player_post() {
       $this->layout= "user";
        $gallery = TableRegistry::get('galleries');
        $this->paginate = [
        'limit' => 20,
        'order'=>['galleries.id'=>'DESC'],
        
    ];
       
        $this->set('galleries', $this->paginate($gallery));
        $this->set('_serialize', ['galleries']); 
    }

    public function workoutsGuide()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();

        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'workoutsGuide']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('pagecontant',$row);
        $this->set('userDetail',$user);

    }

    public function practiceGuide()
    {
        $this->layout= "profile";
         $user= $this->Auth->user();

        $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'practiceGuide']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('pagecontant',$row);
        $this->set('userDetail',$user);

    }

    public function serverVideo()
    {
        $this->autoRender=false;    
        
    include(ROOT.'/vendor/UploadHandler.php');
        error_reporting(E_ALL | E_STRICT);
        $upload_handler = new \UploadHandler();
        die;
    }


    public function profileSetting()
    {

        $this->layout= "profile";
         $user = $this->Auth->user();

        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);

    }

    public function canGo(){

            $this->layout= "profile";
            $user = $this->Auth->user();
            $page=TableRegistry::get('Pages');
        $row=array();
        $result=$page->find('all',['conditions'=>['Pages.pname'=>'canGo']]);
        foreach ($result as $key => $value) {
           $row[]=$value;
        }
             $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user); 
        $this->set('pagecontant',$row);              

    }

     public function gallery()
    {
         $this->layout= "profile";
         $user = $this->Auth->user();

        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);

    }  

     public function videoGallery()
    {
         $this->layout= "profile";
         $user = $this->Auth->user();

        $profiles = TableRegistry::get('userprofiles');
        $sportType = $profiles->find()->select(['user_id','sport'])->where(['user_id'=>$user['id']])->First();
        $this->set('sport',$sportType['sport']);
        $this->set('userDetail',$user);

    }  

    

}

     