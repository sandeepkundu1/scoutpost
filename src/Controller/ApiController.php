<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Crypto\Mcrypt;
use Cake\Network\Email\Email;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

class ApiController extends AppController {
	
	private $user_id = null;
	public $helpers = ['Form', 'Html', 'Time'];
	//public $uses=array('Users');
	public $components = array('RequestHandler', 'Auth');

	public function initialize() {
		parent::initialize();
		$this -> loadComponent('RequestHandler');
		$this -> loadComponent('Extra');
		$this->checkAuth();
		
	}

	public function beforeFilter(\Cake\Event\Event $event) {
		if (!$this->Auth->user()) {
		    $this->Auth->config('authError', false);
		}
		$this -> Auth -> allow(['index', 'sign_up','autocomplete', 'login','market_feature','serverVideo','weekly_game_info','history','single_game_video','get_recent_game','get_profile_pic','forgot_password','reset_password','user_verification','topTen','get_features','get_gallries','paypalForm']);
		
		

	}
	
	public function checkAuth() {
		
		$user_id=$this->Auth->user('id');
		if(isset($user_id)) {
			return $this->user_id=$user_id;
		} else if(isset($this -> request -> data['token'])) {
			$users= TableRegistry::get('Users');
			$user = $users->find()
					->select(['id'])
			    	->where([
			            'Users.salt' => $this -> request -> data['token'],  //we change salt by token
			            'Users.active' => 1,
			            'Users.email_verified' => 1,
			        ])->first();
			return $this->user_id=$user->id;
		} else {
			$this->userId=null;
			
		}
	}

	public function index() {
		$this -> loadModel('Users');
		$users = $this -> Users -> find('all');
		$this -> set(array('users' => $users, '_serialize' => array('users')));
	}

	public function view($id) {
		$recipe = $this -> Recipes -> get($id);
		$this -> set(['recipe' => $recipe, '_serialize' => ['recipe']]);
	}

	public function sign_up() {

		if ($this -> request -> is('post')) {
			$response_array = array();
			$ImgPath = WWW_ROOT . "img/profile_pic/";
			$users = TableRegistry::get('Users');

			$salt = $this -> Extra -> makeSalt();
			$this -> request -> data['salt'] = $salt;
			$this -> request -> data['check_plan'] = 0;
			$this -> request -> data['username'] = $users -> getUniqueUsernme($this -> request -> data['first_name'], $users);
			$this -> request -> data['role_id'] = 3;
			$this -> request -> data['activation_key'] = $salt;
				if(!empty($this->request->data['profile_image']['tmp_name'])){
                $imageName = rand(5,10000).'_'.$this->request->data['profile_image']['name'];
                move_uploaded_file($this->request->data['profile_image']['tmp_name'], $ImgPath .$imageName);
                $this->request->data['profile_image']=$imageName; 
              }else{
                $this->request->data['profile_image']= 'default.png'; 
              }
			$user_entity = $users -> newEntity($this -> request -> data);

			if ($user_entity -> errors()) {
				foreach ($user_entity->errors() as $key => $val) {

					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$data='';	
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			
			}else {
				
				$userprofiles = TableRegistry::get('Userprofiles');
					$profile_entity = $userprofiles -> newEntity($this -> request -> data);
					if($profile_entity -> errors()) {
				foreach ($profile_entity->errors() as $key => $val) {

					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$data='';
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			    }else{

				$users -> save($user_entity);
                    
                                //pr($user_entity);die;
				if ($user_entity -> id) {

					 
					$data = $this -> request -> data['teamdivision'];
			
					$a = $data['current_team'];
					$b = $data['division'];
					$arr = array();
					$teamdivision = TableRegistry::get('teamdivision');
				for($i = 0;$i < count($a); $i++)

				{
					
					$this -> request -> data['teamdivision']['current_team'] = $a[$i];
					$this -> request -> data['teamdivision']['division'] = $b[$i];
					$this -> request -> data['teamdivision']['user_id'] = $user_entity -> id;
					$team_entity = $teamdivision -> newEntity($this -> request -> data);
					$teamdivision -> save($team_entity);
				}
					
					$userprofiles = TableRegistry::get('Userprofiles');
					$this -> request -> data['Userprofiles']['user_id'] = $user_entity -> id;
					$this -> request -> data['Userprofiles']['curr_team'] = $user_entity -> id;
					$profile_entity = $userprofiles -> newEntity($this -> request -> data);
					$userprofiles -> save($profile_entity);
					$activate_key = $user_entity -> activation_key;

					$link = Router::url(['controller' => 'categories', 'action' => 'userVerification', '?' => ['active' => $activate_key]],true);
					
					//$users->sendRegistrationMail($user_entity,$link);

					$body = "Hi " . $user_entity -> first_name . ", You are successfully registered with us,\n\n Click the link below to complete your registration \n\n " . $link;

					$email = new Email('default');

					$email -> from([EMAIL_FROM_ADDRESS => 'Mail From ScoutPost']) -> to($user_entity -> email) -> subject('Scoutpost SignUp') -> send($body);
				}

				$response_array['success'] = 'successfully registerd please check your email';
				$data = $user_entity -> id;
				$this -> response -> statusCode(200);

			 } 
                         
                         
		   }

			$this -> set([
				'message' => $response_array,
				'lastId' => $data,
			 '_serialize' => ['message','lastId']]);

		} else {
			$this -> response -> statusCode(403);
		}

	}

	public function login() {
		$response_array = array();
		if ($this -> request -> is('post')) {

			if (isset($this -> request -> data['email']) && isset($this -> request -> data['password'])) {

				$email = $this -> request -> data['email'];
				$password = $this -> request -> data['password'];

				$users = TableRegistry::get('Users');
				$user = $users -> find() -> where(['email' => $email]) -> first();

				if (empty($user)) {
					$response_array['error'] = 'Incorrect login detail';
					$this -> response -> statusCode(403);
				} else if ($user -> active == 0 || $user -> email_verified == 0) {
					$response_array['error'] = 'Sorry your account is not active, please contact to Administrator';
					$this -> response -> statusCode(403);
				} else {

					$user = $this -> Auth -> identify();
					if ($user) {
						$this -> Auth -> setUser($user);
						$response_array['success'] = 'login successfully';
						$this -> response -> statusCode(200);
					} else {
						$response_array['error'] = 'fail! username/password is incorrect';
						$this -> response -> statusCode(403);
					}

				}

			} else {
				$response_array['error'] = 'params error';
				$this -> response -> statusCode(403);
			}

		} else {
			$response_array['error'] = 'request error';
			$this -> response -> statusCode(403);
		}

		$this -> set(['message' => $response_array, '_serialize' => ['message']]);

	}

	/**
	 * Used to send forgot password email to user
	 *
	 * @access public
	 * @return void
	 */
	public function forgot_password() {
		
		$response_array = array();
		if ($this->request -> isPost()) {
			$users = TableRegistry::get('Users');
			if ($this->request->data['email']) {
				$email  = $this->request->data['email'];
				$user = $users -> find() -> where(['email' => $email]) -> first();
				if (empty($user)) {
					$response_array['error'] = 'This email Address does not exists';
					$this -> response -> statusCode(403);
				}
				// check for inactive account
				else if ($user -> active == 0 || $user -> email_verified == 0) {
					$response_array['error'] = 'Your registration has not been confirmed yet please verify your email before reset password';
					$this -> response -> statusCode(403);
				} else {
					$users->forgotPasswordEmailTemplate($user);
					$response_array['success'] = 'Please check your mail for reset your password';
					$this -> response -> statusCode(200);
				}
			} else {
				$response_array['error'] = 'Please fill up email';
				$this -> response -> statusCode(403);
			}
			$this -> set(['message' => $response_array, '_serialize' => ['message']]);
		}
	}

	/**
	 *  Used to reset password when user comes on the by clicking the password reset link from their email.
	 *
	 * @access public
	 * @return void
	 */
	public function reset_password() {
		$response_array = array();
		if ($this->request -> isPost()) {
			if (!empty($this->request->query['ident']) && !empty($this->request->query['activate'])) {
				$userId= $this->request->query['ident'];
				$activateKey=$this->request->query['activate'];
				$users= TableRegistry::get('Users');
				
				$user = $users->find()
				    	->where([
				            'Users.id' => $userId,
				            'Users.activation_key' => $activateKey
				        ])->first();
				
				if (!empty($user)) {
					$this -> request -> data['id']=$user->id;
					$user_entity = $users -> newEntity($this -> request -> data);
					if ($user_entity -> errors()) {
						foreach ($user_entity->errors() as $key => $val) {
		
							foreach ($val as $k => $d) {
								$vl[] = $d;
							}
						}
						$response_array['error'] = $vl;
						$this -> response -> statusCode(403);
					} else {
						$user->password = $this->request->data['password'];
						$users->save($user);
						$response_array['success'] = 'Your password has been reset successfully';
						$this -> response -> statusCode(200);
					} 
				} else {
					$response_array['error'] = 'Something went wrong, please click again on the link in email';
					$this -> response -> statusCode(403);
				}
				
				
			} else {
				$response_array['error'] = 'Something went wrong, please click again on the link in email';
				$this -> response -> statusCode(403);
			}
		} else {
			$response_array['error'] = 'Something went wrong, please click again on the link in email';
			$this -> response -> statusCode(403);
		}
		
		
		$this -> set(['message' => $response_array, '_serialize' => ['message']]);
		
		
	}

	
	
	/**
	 *  Used to verify user when user comes on the by clicking the sign up email link from their email.
	 *
	 * @access public
	 * @return void
	 */
	public function user_verification() {
		$response_array = array();
		if ($this->request -> isPost()) {
			if (!empty($this->request->query['ident']) && !empty($this->request->query['activate'])) {
				$userId= $this->request->query['ident'];
				$activateKey=$this->request->query['activate'];
				$users= TableRegistry::get('Users');
				
				$user = $users->find()
				    	->where([
				            'Users.id' => $userId,
				            'Users.activation_key' => $activateKey
				        ])->first();
				
				if ((!empty($user)) && ($user->active==0 || $user->email_verified==0)) {
					$user->active = 1;
					$user->email_verified = 1;
					$users->save($user);
					$response_array['success'] = 'Your account has been activate successfully';
					$this -> response -> statusCode(200); 
				} else {
					$response_array['error'] = 'Something went wrong, please click again on the link in email';
					$this -> response -> statusCode(403);
				}
				
				
			} else {
				$response_array['error'] = 'Something went wrong, please click again on the link in email';
				$this -> response -> statusCode(403);
			}
		} else {
			$response_array['error'] = 'Something went wrong, please click again on the link in email';
			$this -> response -> statusCode(403);
		}
		
		
		$this -> set(['message' => $response_array, '_serialize' => ['message']]);
		
		
	}

	/**
	 * Used to fetch features records.
	 * request: GET
	 * Params: page and limit
	 */
	public function get_features() {
		$picture_url=Router::url('/',TRUE)."img/feature_pic/";
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
			$page=(isset($this->request->query['page']))?$this->request->query['page']:1;
			$limit=(isset($this->request->query['limit']))?$this->request->query['limit']:5;
			$features= TableRegistry::get('Features');
			$result = $features->find()->order(['id' => 'DESC'])->limit($limit)->page($page);
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'features' => $data,
            'image_path'=>$picture_url,
            '_serialize' => ['message', 'features','image_path']
        ]);
	}


	/**
	 * Used to fetch user acedmics info.
	 * request: GET
	 * Params: token or session id
	 */
	 
	 public function get_academics_info() {
	 	
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
			$user_id=$this->user_id;
			$year=$this->request->query['year'];
			$userAcademic= TableRegistry::get('UserAcademics');
			$result = $userAcademic->find()->where([
				            'UserAcademics.user_id' => $user_id,'UserAcademics.year' => $year,
				        ])->order(['year' => 'DESC']);
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'acedimics' => $data,
            '_serialize' => ['message', 'acedimics']
        ]);
	 }

	/**
	 * Used to fetch save user academics info.
	 * request: post
	 * Post json type: {{
		*"year":"2015",
	*	"first_sem_gpa":"10",
	*	"second_sem_gpa":"20"
	*   }
	 */
	public function save_academics() {
		$response_array = array();
		$data=array();
		
		if (($this->request->is('post'))) {

		    $UserAcademicDetails= TableRegistry::get('UserAcademicDetails');
			$existsubject = $UserAcademicDetails->find()->where(['user_id' => $this -> request -> data['user_id'],'year'=>$this -> request -> data['year']]);
			
			if($existsubject->count()>0) {
				$UserAcademicDetails->delete($existsubject->first());
			}
			if($this -> request -> data['sub']){

			$this -> request -> data['sub']['user_id']=$this->user_id;
			$this -> request -> data['sub']['year']=$this -> request -> data['year'];
			$this -> request -> data['sub']['semester']=$this -> request -> data['semester'];
			
			$entitysub = $UserAcademicDetails -> newEntity($this -> request -> data['sub']);
			if ($entitysub -> errors()) {
				foreach ($entitysub->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$UserAcademicDetails->save($entitysub);
				$data=$entitysub;
				$response_array['success'] = 'User acedimics save successfully';
				$this -> response -> statusCode(200);
			}

			$saved_sub_id=$entitysub->id;
			}
			
			if(!empty($saved_sub_id)){
			$userAcademics= TableRegistry::get('UserAcademics');
			$this -> request -> data['user_id']=$this->user_id;
			$this -> request -> data['use_acadmic_details_id']=$saved_sub_id;
			$this -> request -> data['workout_date']=date("Y-m-d H:i:s",strtotime($this -> request -> data['workout_date']));
			$exist = $userAcademics->find()->where(['user_id' => $this -> request -> data['user_id'],'year'=>$this -> request -> data['year']]);
			
			if($exist->count()>0) {
				$userAcademics->delete($exist->first());
			} 
			
			$entity = $userAcademics -> newEntity($this -> request -> data);
			
			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$userAcademics->save($entity);
				$data=$entity;
				$response_array['success'] = 'User acedimics save successfully';
				$this -> response -> statusCode(200);
			}
			
		} else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'acedimics' => $data,
            '_serialize' => ['message', 'acedimics']
        ]);
		}
	}

	/*
	 * use for save workout/practice	
	 * Request type: POST
	 * POST data items : input fields and multipart data for video
	 * flag=> 1 for workspace and 2 for practice
	 * */

	public function save_workout() {

		$response_array = array();
		$data=array();
		$ImgPath = WWW_ROOT . "img/skills_pic/";
		if (($this->request->is('post'))) {
			$userSkills= TableRegistry::get('UserSkills');
			$this -> request -> data['user_id']=$this->user_id;
			//pr($this -> request -> data); die;

			if(!empty($this->request->data['skills_img']['tmp_name'])){
                $imageName = rand(5,10000).'_'.$this->request->data['skills_img']['name'];
                move_uploaded_file($this->request->data['skills_img']['tmp_name'], $ImgPath .$imageName);
                $this->request->data['skills_img']=$imageName; 
              }else{
                $this->request->data['skills_img']= ''; 
              }


			$entity = $userSkills -> newEntity($this -> request -> data);
			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				
				
				$userSkills->save($entity);
				$data=$entity;
				
				if($entity->id){
					
					if(!empty($this -> request -> data['file'])) // if video exist
					{
						
						$error = 0;
						$tempFileName = $this -> request -> data['file']["name"];
						$file_type = $this -> request -> data['file']['type'];
						$file_size = $this -> request -> data['file']['size'];
	
						if (isset($tempFileName)) {
							
							$tempFileName = $tempFileName;
							$userMediaInfo= TableRegistry::get('UserMediaInfo');
							$this -> request -> data['user_skills_id']=$entity->id;
							$this -> request -> data['user_id']=$this->user_id;
							$this -> request -> data['media_name']=$tempFileName;
							$this -> request -> data['media_type']=$file_type;
							$this -> request -> data['media_size']=$file_size;
							$media_entity = $userMediaInfo -> newEntity($this -> request -> data);
							$userMediaInfo->save($media_entity);
							
						}
					}
				}
				$response_array['success'] = 'User skills information saved successfully';
				$this -> response -> statusCode(200);
			}
		} else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		
		 $this->set([
            'message' => $response_array,
            'skills' => $data,
            '_serialize' => ['message', 'skills']
        ]);
		
		
	}

	/**
	 * Used to fetch save user academics info.
	 * url: http://localhost/scoutpost/api/save_user_stats.json
	 * request: post
	 * params: game[saved_game_id]
	 * Note :Used to fetch save user academics info. by default it became zero but when first stats save then its value became the game id which have been saved recently
	 * 
	 **/
	public function save_user_stats() {
               //$conn = ConnectionManager::get('default');
	  	//$conn->logQueries(true);
		$response_array = array();
		$data=array();
		$saved_tour_id=0;
			
				
		if(!empty($this -> request -> data['buttoName']) && $this -> request -> data['buttoName']=='pause'){

				$tempdata= TableRegistry::get('TempGameRecord');
				$user_id = $this->user_id;
				$data = json_encode($this -> request -> data);
				//pr($data); die;
				$this -> request -> data['flag']='pause';
				$this -> request -> data['formdata']=$data;
				$this -> request -> data['user_id'] = $user_id;
				$entitytemp = $tempdata -> newEntity($this -> request -> data);
					
				$tempdata->save($entitytemp); //insert
				die; 
				}
			

		if (($this->request->is('post'))) {
			$tournament= TableRegistry::get('Tournaments');
			$user_id=$this->user_id;
                        
           	if(!empty($this -> request -> data['tour']['name'])) {
				$this -> request -> data['tour']['flag']=1;
				$this -> request -> data['tour']['user_id']=$user_id;
				$entitytour = $tournament -> newEntity($this -> request -> data['tour']);
				
				if ($entitytour -> errors()) {

					foreach ($entitytour->errors() as $key => $val) {
						foreach ($val as $k => $d) {
							$vl[] = $d;
						}
					}
					$response_array['error'] = $vl;
					$this -> response -> statusCode(403);

				} else{
                                    $tournament_name=$this -> request -> data['tour']['name'];
                                    $check_tournament_exist=$tournament->find()->where(['name'=>$tournament_name,'user_id'=>$user_id])->first();
                                    if($check_tournament_exist['id']) {
                                        $entitytour->id=$check_tournament_exist['id'];
                                    }
                                    $tournament->save($entitytour); //insert

                                  }
			}

			$userGames= TableRegistry::get('UserGames');
			
			if(!empty($entitytour->id)){
                            $saved_tour_id=$entitytour->id;
                        }else{
                        $saved_tour_id=0;
                         }
			if($this -> request -> data['game']) {
                            
				$this -> request -> data['game']['user_id']=$user_id;
                                if($saved_tour_id>0 || $saved_tour_id != null) {
                                     $this -> request -> data['game']['tournament_id']=$saved_tour_id;
                                } 
				$entity = $userGames -> newEntity($this -> request -> data['game']);

				if ($entity -> errors()) {
					foreach ($entity->errors() as $key => $val) {
						foreach ($val as $k => $d) {
							$vl[] = $d;
						}
					}
					$response_array['error'] = $vl;
					$this -> response -> statusCode(403);
				} else {
					$saved_game_post_id=$this -> request -> data['game']['saved_game_id'];
					if($saved_game_post_id>0) {
						
						$already_exist_games_detail = $userGames->get($saved_game_post_id); // games with id like 12

						$userGames->save($already_exist_games_detail);
							$data=$already_exist_games_detail;
					} else {
						$userGames->save($entity); //insert
							
							$data=$entity;
					}
							
					$saved_game_id=($saved_game_post_id >0)?$saved_game_post_id:$entity->id;
                                        
					
					if($saved_game_id) {
						$userStats= TableRegistry::get('UserStats');
						$this -> request -> data['stat']['user_games_id']=$saved_game_id;
						$this -> request -> data['stat']['user_id']=$user_id;
                                                
                                                /*
                                                 * we apply this condition to check wheather if at_bat_result
                                                 * is coming SAC, BB or HBP 
                                                 * because  At-Bat is NOT counted (AB= 0) if the result is a SAC, BB or HBP
                                                 */
                                                if(($this -> request -> data['stat']['at_bat_result']=='SAC') ||($this -> request -> data['stat']['at_bat_result']=='BB') || ($this -> request -> data['stat']['at_bat_result']=='HBP')) {
                                                    $this -> request -> data['stat']['at_bat']=0;//we do not care for what value are coming by post because at bat value will be either 1 or 0
                                                } else {
                                                    $this -> request -> data['stat']['at_bat']=1;//we do not care for what value are coming by post because at bat value will be either 1 or 0
                                                }
                                                
                                                if(($this -> request -> data['stat']['pitch_thrown'] >7 || $this -> request -> data['stat']['rbis'] >0)) {
                                                    $this -> request -> data['stat']['qab']=1;//we do not care for what value are coming by post because at bat value will be either 1 or 0
                                                } else {
                                                    $this -> request -> data['stat']['qab']=0;//we do not care for what value are coming by post because at bat value will be either 1 or 0
                                                }
                                                
                                                if(($this -> request -> data['stat']['at_bat_result']=='Outs') || ($this -> request -> data['stat']['at_bat_result']=='Ground Out') ||($this -> request -> data['stat']['at_bat_result']=='SO') || ($this -> request -> data['stat']['at_bat_result']=='Line Out') || ($this -> request -> data['stat']['at_bat_result']=='Fly Out') || ($this -> request -> data['stat']['at_bat_result']=='FC') || ($this -> request -> data['stat']['at_bat_result']=='ROE')) {
                                                    $this -> request -> data['stat']['outs']=1;
                                                } else {
                                                    $this -> request -> data['stat']['outs']=0;
                                                }
                                                
						$entity_stats = $userStats -> newEntity($this -> request -> data['stat']);
						$userStats->save($entity_stats); //insert into user_stats
						
						if($entity_stats->id) {
							
							if(isset($this -> request -> data['stat']['file']) && ($this -> request -> data['stat']['file'] !=null)) // if video exist
							{
								$mediaArr=$this -> request -> data['stat']['file'];
								$error = 0;
								$tempFileName = $mediaArr["name"];
								$file_type = $mediaArr['type'];
								$file_size = $mediaArr['size'];
			
								if (isset($tempFileName)) {
									$c=strtotime("now");
									$tempFileName = $tempFileName;
									$userStatsMedia= TableRegistry::get('UserStatsMedia');
									$this -> request -> data['media']['user_stats_id']=$entity_stats->id;
									$this -> request -> data['media']['user_id']=$user_id;
									$this -> request -> data['media']['media_name']=$tempFileName;
									$this -> request -> data['media']['media_type']=$file_type;
									$this -> request -> data['media']['media_size']=$file_size;
									$media_entity = $userStatsMedia -> newEntity($this -> request -> data['media']);
									$userStatsMedia->save($media_entity);
									
								}
							}
						}
						
						
					}
					
				
					$response_array['success'] = 'User games saved successfully';
					$this -> response -> statusCode(200);
				}
				
			}
			
			
			
			
		} else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'game_info' => $data,
            '_serialize' => ['message', 'game_info']
        ]);
		
	}
	
	public function edit($id) {
		$recipe = $this -> Recipes -> get($id);
		if ($this -> request -> is(['post', 'put'])) {
			$recipe = $this -> Recipes -> patchEntity($recipe, $this -> request -> data);
			if ($this -> Recipes -> save($recipe)) {
				$message = 'Saved';
			} else {
				$message = 'Error';
			}
		}
		$this -> set(['message' => $message, '_serialize' => ['message']]);
	}

	public function delete($id) {
		$recipe = $this -> Recipes -> get($id);
		$message = 'Deleted';
		if (!$this -> Recipes -> delete($recipe)) {
			$message = 'Error';
		}
		$this -> set(['message' => $message, '_serialize' => ['message']]);
	}

	/*
	 * Used to fetch user games state info.
	 * request: GET
	 * params query string: game_date
	 * Params: token or session id
	 */
	 
	 public function get_gamestats_info() {
	 	//$this->autoRender=false;
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
			$user_id=$this->user_id;
			$UserGames= TableRegistry::get('UserGames');
			$game_date = $this->request->query['game_date'];
			$result = $UserGames->find()->where([
				'date(UserGames.game_date)' => $game_date,'UserGames.user_id'=>$user_id]);
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'StatsDetail' => $data,
            '_serialize' => ['message', 'StatsDetail']
        ]);
	 }
	 
	 /*
	  * Used to fetch number of games in tounament in a year
	  * params query string: (1)param: year 
	  * Request: GET
	  * */
	  
	  public function get_games_count_of_stats() {
                
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
                        $tournaments= TableRegistry::get('Tournaments');
			$year = $this->request->query['year'];

						$result = $tournaments->find()
                          ->select(['tournaments_id'=>'Tournaments.id','tour_name'=>'Tournaments.name','UserGames.id','UserGames.game_date','UserGames.game_date','UserGames.game_location','no_of_games'=>'count(UserGames.id)'])  
                        ->Where(['year(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>1])
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ]
                         ])
                         ->group('Tournaments.id');
                      
                
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'StatsDetail' => $data,
            '_serialize' => ['message', 'StatsDetail']
        ]);
	  }
          
          
          /*
	  * Used to fetch number of games in tounament in a year
	  * params query string: (1)param: year 
	  * Request: GET
	  * */
	  
	  public function get_games_count_of_stats_league() {
                //Note: these two lines are used for to print query debug
	  	//$conn = ConnectionManager::get('default');
	  	//$conn->logQueries(true);
        $response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$year = $this->request->query['year'];
			$stat_type = $this->request->query['stat_type'];
			$game_type = $this->request->query['game_type'];
			
			
                        
                        
                        $result = $user_games->find()
                         ->select(['UserGames.id','UserGames.game_location','game_date','UserGames.id','no_of_games'=>'count(UserGames.id)'])  
                        ->Where(['year(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type])
                        ->group('UserGames.game_date');
                        /*->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ]);*/
			
			//pr($result->toArray()); die;
                   
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'StatsDetail' => $data,
            '_serialize' => ['message', 'StatsDetail']
        ]);
		}
	  /*
	  * Used to fetch At bat total in all tounament in a year
	  * params query string: 
           * (1)year 
	  * (2) game_type=1 or 2, 1 for tournament, 2 for league
           * (3) tournament_id param is for identify all record or per tournament record or for a single match
           * if tournament_id value is zero than all record (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=0)
           * if not zero then for per match bases (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=1&game_id=2)
            * if tournament_id is an date then record will be filter on the basis of date or we can say for a tournament date
           * (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=1)
           * in Y-m-d format
           * (4) game_id
           * 
	  * */
	  
	  public function get_games_at_bat_total() {
                //$conn = ConnectionManager::get('default');
	        //$conn->logQueries(true);
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
			$year = $this->request->query['year'];
			$game_type = $this->request->query['game_type'];
                        $tournament_id =(isset($this->request->query['tournament_id']))? $this->request->query['tournament_id']:0;
                        $option=$tournament_id;
                        $game_id =(isset($this->request->query['game_id']))? $this->request->query['game_id']:0;
			$conn = ConnectionManager::get('default');
                           
                           //fetch AB for Tournament
                           $ab_tournament_arr=$user_games->fetch_AB($game_type,$year,$user_id,$option,$game_id);
                           $ab=$ab_tournament_arr['at_bat'];
                           $data['ab']=$ab;
                           
                           //fetch HITS for Tournament
                           $hits=$user_games->fetch_HIT($game_type,$year,$user_id,$option,$game_id);
                           $data['hits']=$hits; 
                           
                           //fetch 1B for Tournament
                           $one_b=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"1B",$game_id); //fetch hits
                           $data['one_b']=$one_b; 
                           
                           //fetch 2B for Tournament
                           $two_b=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"2B",$game_id); //fetch hits
                           $data['two_b']=$two_b; 
                           
                           //fetch 3B for Tournament
                           $three_b=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"3B",$game_id); //fetch hits
                           $data['three_b']=$three_b; 
                           
                           //fetch HR for Tournament 
                           $hr=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"HR",$game_id); //fetch hits
                           $data['hr']=$hr; 
                           
                           //fetch RBI for Tournament and league
                           $rbi=$user_games->fetch_sum_of_stats($game_type,$year,$user_id,$option,"rbis",$game_id); 
                           $data['rbi']=$rbi;
                           
                            //fetch BB for Tournament and league
                           $bb=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"BB",$game_id); //fetch hits
                           $data['bb']=$bb; 
                           
                            //fetch HBP for Tournament and league
                           $hbp=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"HBP",$game_id); //fetch hits
                           $data['hbp']=$hbp; 
                            
                            //fetch ROE for Tournament and league //Client want ROE now 
                           $roe=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"ROE",$game_id); //fetch hits
                           $data['roe']=$roe;  
                           
                           //fetch SO for Tournament and league
                           $so=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"SO",$game_id); //fetch hits
                           $data['so']=$so; 
                           
                            //fetch SAC for Tournament and league
                           $sac=$user_games->fetch_stats_type($game_type,$year,$user_id,$option,"SAC",$game_id); //fetch hits
                           $data['sac']=$sac; 
                           
                            //fetch SB for Tournament and league
                           $sb=$user_games->fetch_sum_of_stats($game_type,$year,$user_id,$option,"stolen_bases",$game_id); 
                           $data['sb']=$sb;
                           
                           //fetch runs_scored  for Tournament and league
                           $runs_scored=$user_games->fetch_sum_of_stats($game_type,$year,$user_id,$option,"runs_scored",$game_id); 
                           $data['runs']=$runs_scored;
                           
                          if((!empty($data)>0)) {
                                    $response_array['success'] = 'success';
                                    $this -> response -> statusCode(200); 
                            } else {
                                    $response_array['error'] = 'No results found';
                                    $this -> response -> statusCode(403);
                            }
                    
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'AtbatTotal' => $data,
            '_serialize' => ['message', 'AtbatTotal']
        ]);
               
    }
        
    /*
	  * Used to fetch At bat total for a league
	  * params query string: 
           * (1)year 
	  * (2) game_type=1 or 2, 1 for tournament, 2 for league
           * (3) tournament_id param is for identify all record or per tournament record or for a single match
           * if tournament_id value is zero than all record (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=0)
           * if not zero then for per match bases (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=1)
            * if tournament_id is an date then record will be filter on the basis of date or we can say for a tournament date
           * (http://localhost/scoutpost/api/get_games_at_bat_total.json?year=2015&game_type=1&tournament_id=2015-04-29)
           * in Y-m-d format
           * 
	  * */
	  
	  public function get_games_at_bat_total_league() {
                //$conn = ConnectionManager::get('default');
	        //$conn->logQueries(true);
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
			$year = $this->request->query['year'];
			$game_type = $this->request->query['game_type'];
                        $tournament_id =(isset($this->request->query['tournament_id']))? $this->request->query['tournament_id']:0;
                        $option=$tournament_id;
			$conn = ConnectionManager::get('default');
                           
                           //fetch AB for Tournament and league
                           $ab_tournament_arr=$user_games->fetch_AB_league($game_type,$year,$user_id,$option);
                           $ab=$ab_tournament_arr['at_bat'];
                           $data['ab']=$ab;
                           
                           //fetch HITS for Tournament and league
                           $hits=$user_games->fetch_HIT_league($game_type,$year,$user_id,$option);
                           $data['hits']=$hits; 
                           
                           //fetch 1B for Tournament and league
                           $one_b=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"1B"); //fetch hits
                           $data['one_b']=$one_b; 
                           
                           //fetch 2B for Tournament and league
                           $two_b=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"2B"); //fetch hits
                           $data['two_b']=$two_b; 
                           
                           //fetch 3B for Tournament and league
                           $three_b=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"3B"); //fetch hits
                           $data['three_b']=$three_b; 
                           
                           //fetch HR for Tournament and league
                           $hr=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"HR"); //fetch hits
                           $data['hr']=$hr; 
                           
                           //fetch RBI for Tournament and league
                           $rbi=$user_games->fetch_sum_of_stats_league($game_type,$year,$user_id,$option,"rbis"); 
                           $data['rbi']=$rbi;
                           
                            //fetch BB for Tournament and league
                           $bb=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"BB"); //fetch hits
                           $data['bb']=$bb; 
                           
                            //fetch HBP for Tournament and league
                           $hbp=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"HBP"); //fetch hits
                           $data['hbp']=$hbp; 
                            
                            //fetch ROE for Tournament and league //Client deny for ROE
                           $roe=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"ROE"); //fetch hits
                           $data['roe']=$roe; 
                           
                           //fetch SO for Tournament and league
                           $so=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"SO"); //fetch hits
                           $data['so']=$so; 
                           
                            //fetch SAC for Tournament and league
                           $sac=$user_games->fetch_stats_type_league($game_type,$year,$user_id,$option,"SAC"); //fetch hits
                           $data['sac']=$sac; 
                           
                            //fetch SB for Tournament and league
                           $sb=$user_games->fetch_sum_of_stats_league($game_type,$year,$user_id,$option,"stolen_bases"); 
                           $data['sb']=$sb;
                           
                           //fetch runs_scored  for Tournament and league
                           $runs_scored=$user_games->fetch_sum_of_stats_league($game_type,$year,$user_id,$option,"runs_scored"); 
                           $data['runs']=$runs_scored;
                           
                          if((!empty($data)>0)) {
                                    $response_array['success'] = 'success';
                                    $this -> response -> statusCode(200); 
                            } else {
                                    $response_array['error'] = 'No results found';
                                    $this -> response -> statusCode(403);
                            }
                    
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'AtbatTotal' => $data,
            '_serialize' => ['message', 'AtbatTotal']
        ]);
               
    }
     /*
	  * Used to fetch At bat total in a tounament in a year
	  * params query string: 
           * (1)year 
	  * (2) game_type=1 or 2, 1 for tournament, 2 for league
           * (3)tounament_id: for all year tounament we do not pass it in url, and for specific tounament we pass it on url
	  * Request: GET
	  * */
	  
	  public function fetch_at_bat_total_per_tounament() {
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
			
             $tounament_id =(isset($this->request->query['tounament_id']))? $this->request->query['tounament_id']:"";
             $game_id =(isset($this->request->query['game_id']))? $this->request->query['game_id']:"";
			//pr($this->request->query); die;
			$conn = ConnectionManager::get('default');


                    $query="SELECT 
                                        ug.id as game_id,ug.my_team_name as team_1,ug.oponent as oponent,ug.game_location as location,ug.user_id,ug.game_date,ug.game_type,
                                        sum(us.at_bat) as 'total_at_bat',sum(us.runs_scored)  as 'runs',sum(us.stolen_bases)  as 'sb',sum(us.rbis)  as 'rbi',
                                        (select count(id) from user_stats where `at_bat_result`='1B'   and user_id=$user_id and user_games_id=$game_id ) as 'no_of_1b',
                                        (select count(id) from user_stats where `at_bat_result`='2B'   and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_2b',
                                        (select count(id) from user_stats where `at_bat_result`='3B'   and user_id=$user_id and user_games_id=$game_id ) as 'no_of_3b',
                                        (select count(id) from user_stats where `at_bat_result`='HR'   and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_hr',
                                        (select sum(`runs_scored`) from user_stats where  user_id=$user_id and user_games_id=$game_id) as `runs`,
                                        (select count(id) from user_stats where `at_bat_result`='BB'   and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_bb',
                                        (select count(id) from user_stats where `at_bat_result`='HBP'  and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_hbp',
                                        (select count(id) from user_stats where `at_bat_result`='SO'   and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_so',
                                        (select count(id) from user_stats where `at_bat_result`='SF'   and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_sf',
                                        (select count(id) from user_stats where `at_bat_result`='SAC'  and user_id=$user_id and user_games_id=$game_id  ) as 'no_of_sac'
                                    FROM `user_games` as ug 
                                        left join user_stats as us on us.user_games_id=ug.id
                                    where  ug.user_id=$user_id and ug.id=$game_id";   //ug.game_type=$game_type and year(ug.game_date)=$year and

                      
                        //echo $query;die;

                        $query_return = $conn->execute($query);

                        foreach($query_return as $item)
                        {

                                $result = $item;

                        }
                       
			if((count($result)>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'AtbatTotal' => $data,
            '_serialize' => ['message', 'AtbatTotal']
        ]);
               
    }



    /*
	  * Used to fetch At bat total in tounament in a date
	  * params query string: 
           * 
           * (3)tounament_id: for all year tounament we do not pass it in url, and for specific tounament we pass it on url
	  * Request: GET
	  * */
	  
	  public function fetch_at_bat_total_per_date() {
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                        if(!empty($this->request->query)) {
                        
			$user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
			
             $splDate=$this->request->query['tounament_date'];

              
             //pr($dt); die;
             $stat_type=$this->request->query['stat_type'];
             $dt= date('Y-m-d',strtotime($stat_type));
			$conn = ConnectionManager::get('default');


                    $query="SELECT 
                                        ug.id as game_id,ug.user_id,ug.game_date,ug.game_type,
                                        sum(us.at_bat) as 'total_at_bat',sum(us.runs_scored)  as 'runs',sum(us.stolen_bases)  as 'sb',sum(us.rbis)  as 'rbi',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='1B' and user_stats.user_id=$user_id ) as 'no_of_1b',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='2B'  and user_stats.user_id=$user_id) as 'no_of_2b',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='3B'  and user_stats.user_id=$user_id) as 'no_of_3b',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='HR'  and user_stats.user_id=$user_id) as 'no_of_hr',
                                        (select sum(`runs_scored`) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'."  and user_stats.user_id=$user_id) as `runs`,
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='BB'  and user_stats.user_id=$user_id) as 'no_of_bb',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='HBP'  and user_stats.user_id=$user_id) as 'no_of_hbp',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='SO'  and user_stats.user_id=$user_id) as 'no_of_so',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='SF'  and user_stats.user_id=$user_id) as 'no_of_sf',
                                        (select count(at_bat_result) from user_stats inner join user_games on user_games.id= user_stats.user_games_id where `game_date` = ".'"'.$dt.'"'." and `at_bat_result`='SAC' and user_stats.user_id=$user_id ) as 'no_of_sac'
                                    FROM `user_games` as ug 
                                        left join user_stats as us on us.user_games_id=ug.id
                                    where  ug.user_id=$user_id and ug.game_date=".'"'.$dt.'"';   //ug.game_type=$game_type and year(ug.game_date)=$year and

                      
                        //echo $query;die;

                        $query_return = $conn->execute($query);

                        foreach($query_return as $item)
                        {

                                $result = $item;

                        }
                       
			if((count($result)>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
                        
                } else {
                    $response_array['error'] = 'wrong request';
                    $this -> response -> statusCode(403);
                }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
            'message' => $response_array,
            'AtbatTotal' => $data,
            '_serialize' => ['message', 'AtbatTotal']
        ]);
               
    }
    
    
     /*
	  * Used to fetch tournament stats
	  *  params query string: 
            * (1)year 
            * (2) game_type=1 , 1 for tournament
            * (3)option: for see year stats we pass in option param 0
            *for see tournament stats on perticular date we pass in option, date like 2015-04-23 in 'Y-m-d' format
            *for see specific game stats we pass game_id
            * Request: GET
	  * 
	  */	
	  
          public function fetch_tournament_stats() { 
                
                $conn = ConnectionManager::get('default');
	  		//$conn->logQueries(true);
                
                $response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                    if(!empty($this->request->query)) {
                        $user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
                        $year = $this->request->query['year'];
			//$game_type = $this->request->query['game_type'];
                        $game_type = 1;
                        $option = isset($this->request->query['tournament_id'])?$this->request->query['tournament_id']:0;
                        $game_id =(isset($this->request->query['game_id']))? $this->request->query['game_id']:0;
                        
                        
                        $avg=$user_games->fetch_AVG($game_type,$year,$user_id,$option,$game_id);
                        $data['avg']=$avg;  //fetch avg
                        
                           
                        $slg=$user_games->fetch_SLG($game_type,$year,$user_id,$option,$game_id);
                        $data['slg']=$slg;  //fetch SLG
                        
                        $obp=$user_games->fetch_OBP($game_type,$year,$user_id,$option,$game_id);
                        $data['obp']=$obp;
                        
                        //fetch ops
                        $ops=$user_games->fetch_OPS($game_type,$year,$user_id,$option,$game_id);
                        $data['ops']=$ops;
                        
                        //fetch TB
                        $tb=$user_games->fetch_TB($game_type,$year,$user_id,$option,$game_id);
                        $data['tb']=$tb;
                        
                        //fetch RC
                        $rc=$user_games->fetch_RC($game_type,$year,$user_id,$option,$game_id);
                        $data['rc']=$rc;
                        
                         //fetch PA
                        $pa=$user_games->fetch_PA($game_type,$year,$user_id,$option,$game_id);
                        $data['pa']=$pa;
                        
                         //fetch PIP
                        $pip=$user_games->fetch_PIP($game_type,$year,$user_id,$option,$game_id);
                        $data['pip']=$pip;
                        
                        

                         $AVG_VS_RHP=$user_games->fetch_AVG_VS_RHP($game_type,$year,$user_id,$option,$game_id);

                         $data['AVG_VS_RHP']=$AVG_VS_RHP;
                        
                         $AVG_VS_LHP=$user_games->fetch_AVG_VS_LHP($game_type,$year,$user_id,$option,$game_id);
                         $data['AVG_VS_LHP']=$AVG_VS_LHP;
                         
                         
                         $wrob=$user_games->fetch_AVG_W_ROB($game_type,$year,$user_id,$option,$game_id);
                         $data['wrob']=$wrob;
                         
                         
                         $qab=$user_games->fetch_QAB($game_type,$year,$user_id,$option,$game_id);
                         $data['qab']=$qab;
                        
                        //die;
                        
                        
                        $response_array['success'] = 'success';
                        $this -> response -> statusCode(200); 

                    } else {
                        $response_array['error'] = 'wrong request';
                        $this -> response -> statusCode(403);
                    }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
                'message' => $response_array,
                        'tournament_stats' => $data,
                        '_serialize' => ['message', 'tournament_stats']
                    ]);
          }
          
          
          /*
	  * Used to fetch league stats
	  *  params query string: 
            * (1)year 
            * (2) game_type=1 or 2, 1 for tournament, 2 for league
            * (3)option: for see year stats we pass in option param 0
            *for see tournament stats on perticular date we pass in option, date like 2015-04-23 in 'Y-m-d' format
            *for see specific game stats we pass game_id
            * Request: GET
	  * */
	  
          public function fetch_league_stats() { 
                
                //$conn = ConnectionManager::get('default');
	  			//$conn->logQueries(true);
                
                $response_array = array();
				$data=array();
		if (($this->request->is('get'))) {
               if(!empty($this->request->query)) {
                        $user_id=$this->user_id;
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
                        $year = $this->request->query['year'];
			//$game_type = $this->request->query['game_type'];
                        $game_type = 2;
                        $option = isset($this->request->query['game_id'])?$this->request->query['game_id']:$this->request->query['tournament_id'];
                        
                        
                        $avg=$user_games->fetch_AVG_league($game_type,$year,$user_id,$option);
                        $data['avg']=$avg;  //fetch avg
                        
                           
                        $slg=$user_games->fetch_SLG_league($game_type,$year,$user_id,$option);
                        $data['slg']=$slg;  //fetch SLG
                        
                        $obp=$user_games->fetch_OBP_league($game_type,$year,$user_id,$option);
                        $data['obp']=$obp;
                        
                        //fetch ops
                        $ops=$user_games->fetch_OPS_league($game_type,$year,$user_id,$option);
                        $data['ops']=$ops;
                        
                        //fetch TB
                        $tb=$user_games->fetch_TB_league($game_type,$year,$user_id,$option);
                        $data['tb']=$tb;
                        
                        //fetch RC
                        $rc=$user_games->fetch_RC_league($game_type,$year,$user_id,$option);
                        $data['rc']=$rc;
                        
                         //fetch PA
                        $pa=$user_games->fetch_PA_league($game_type,$year,$user_id,$option);
                        $data['pa']=$pa;
                        
                         //fetch PIP
                        $pip=$user_games->fetch_PIP_league($game_type,$year,$user_id,$option);
                        $data['pip']=$pip;
                        
                        
                         $AVG_VS_RHP=$user_games->fetch_AVG_VS_RHP_league($game_type,$year,$user_id,$option);
                         $data['AVG_VS_RHP']=$AVG_VS_RHP;
                        
                         $AVG_VS_LHP=$user_games->fetch_AVG_VS_LHP_league($game_type,$year,$user_id,$option);
                         $data['AVG_VS_LHP']=$AVG_VS_LHP;
                         
                         
                         $wrob=$user_games->fetch_AVG_W_ROB_league($game_type,$year,$user_id,$option);
                         $data['wrob']=$wrob;
                         
                         
                         $qab=$user_games->fetch_QAB_league($game_type,$year,$user_id,$option);
                         $data['qab']=$qab;
                        
                        //die;
                        
                        
                        $response_array['success'] = 'success';
                        $this -> response -> statusCode(200); 

                    } else {
                        $response_array['error'] = 'wrong request';
                        $this -> response -> statusCode(403);
                    }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
                'message' => $response_array,
                        'tournament_stats' => $data,
                        '_serialize' => ['message', 'tournament_stats']
                    ]);
          }
          
          
          public function history() { 
            
            $response_array = array();
			$data=array();
		if (($this->request->is('get'))) {
         if(!empty($this->user_id)){

         	$user_id=$this->user_id;

         }else{
         		//pr($this->request);die;
         		$user_id=$this->request->query['id'];
         	}      
                        

                        $users= TableRegistry::get('Users');
                        $userprofiles= TableRegistry::get('Userprofiles');
                        $Teamdiv= TableRegistry::get('Teamdivision');
			$user_games= TableRegistry::get('UserGames');
			$user_stat= TableRegistry::get('UserStats');
                        $tournaments= TableRegistry::get('Tournaments');
                        
                        
                        
                        $condition=['Users.id'=>$user_id];
                        $result = $users->find()
                            ->select(['Users.id','Users.first_name','Users.last_name','Users.age','Users.dob','Users.sex','team'=>'td.current_team','division'=>'td.division','no_of_games'=>'count(ug.id)','year_of_games'=>'year(ug.game_date)','sport'=>'up.sport','pitcher'=>'up.pitcher','infilder'=>'up.infilder','catcher'=>'up.catcher','outfilder'=>'up.outfilder','through'=>'up.through_with','bats'=>'up.bats_from','favorite_team'=>'up.feb_team','favorite_player'=>'up.feb_athlete'])
                            ->Where($condition)
                            ->group('year(ug.game_date)')
                            ->order(['year(ug.game_date)'=>'desc'])
                            ->hydrate(false)
                            ->join([
                                'up' => [
                                    'table' => 'userprofiles',
                                    'type' => 'LEFT',
                                    'conditions' => 'up.user_id = Users.id',
                                ],
                                'td' => [
                                    'table' => 'teamdivision',
                                    'type' => 'LEFT',
                                    'conditions' => 'td.user_id = Users.id',
                                ],
                                'ug' => [
                                    'table' => 'user_games',
                                    'type' => 'LEFT',
                                    'conditions' => 'ug.user_id = Users.id',
                                ]
                            ])->toArray();
                        
                        //pr($result);die;
                        
                      
                        foreach($result as $k=>$v) {
                            
                            //$data['']
                        
                           $year= $v['year_of_games'];
                           
                           $data[$year]['year_of_games']=$year;
                           $data[$year]['user_id']=$v['id'];
                           $data[$year]['sex']=$v['sex'];
                           $data[$year]['sport']=$v['sport'];
                           $data[$year]['pitcher']=$v['pitcher'];
                           $data[$year]['infilder']=$v['infilder'];
                           $data[$year]['catcher']=$v['catcher'];
                           $data[$year]['outfilder']=$v['outfilder'];
                           $data[$year]['through']=$v['through'];
                           $data[$year]['bats']=$v['bats'];
                           $data[$year]['favorite_team']=$v['favorite_team'];
                           $data[$year]['favorite_player']=$v['favorite_player'];
                           $data[$year]['first_name']=$v['first_name'];
                           $data[$year]['last_name']=$v['last_name'];
                           $data[$year]['age']=$v['age'];
                           $data[$year]['dob']=$v['dob'];
                           $data[$year]['team']=$v['team'];
                           $data[$year]['division']=$v['division'];
                           $data[$year]['no_of_games']=$v['no_of_games'];
                           
                           
                           //fetch PA for Tournament and league
                           $pa_tournament=$user_games->fetch_PA(1,$year,$user_id,0,0);
                           $pa_league=$user_games->fetch_PA_league(2,$year,$user_id,0);
                           $pa=$pa_tournament+$pa_league;
                           $data[$year]['PA']=$pa;
                           
                           //fetch AB for Tournament and league
                           $ab_tournament_arr=$user_games->fetch_AB(1,$year,$user_id,0,0);
                           $ab_tournament=$ab_tournament_arr['at_bat'];
                           $ab_league_arr=$user_games->fetch_AB_league(2,$year,$user_id,0);
                           $ab_league=$ab_league_arr['at_bat'];
                           $AB=$ab_tournament+$ab_league;
                           $data[$year]['AB']=$AB;
                           
                           //fetch HITS for Tournament and league
                           $hits_tournament=$user_games->fetch_HIT(1,$year,$user_id,0,0);
                           $hits_league=$user_games->fetch_HIT_league(2,$year,$user_id,0);
                           $hits=$hits_tournament+$hits_league;
                           $data[$year]['hits']=$hits; 
                           
                           //fetch 1B for Tournament and league
                           $one_b_tournament=$user_games->fetch_stats_type(1,$year,$user_id,0,"1B",0); //fetch hits
                           $one_b_league=$user_games->fetch_stats_type_league(2,$year,$user_id,0,"1B"); //fetch hits
                           $one_b=$one_b_tournament+$one_b_league;
                           $data[$year]['one_b']=$one_b; 
                           
                           //fetch 2B for Tournament and league
                           $two_b_tournament=$user_games->fetch_stats_type(1,$year,$user_id,0,"2B",0); //fetch hits
                           $two_b_league=$user_games->fetch_stats_type_league(2,$year,$user_id,0,"2B"); //fetch hits
                           $two_b=$two_b_tournament+$two_b_league;
                           $data[$year]['two_b']=$two_b; 
                           
                           //fetch 3B for Tournament and league
                           $three_b_tournament=$user_games->fetch_stats_type(1,$year,$user_id,0,"3B",0); //fetch hits
                           $three_b_league=$user_games->fetch_stats_type_league(2,$year,$user_id,0,"3B"); //fetch hits
                           $three_b=$three_b_tournament+$three_b_league;
                           $data[$year]['three_b']=$three_b; 
                           
                           //fetch HR for Tournament and league
                           $hr_tournament=$user_games->fetch_stats_type(1,$year,$user_id,0,"HR",0); //fetch hits
                           $hr_league=$user_games->fetch_stats_type_league(2,$year,$user_id,0,"HR"); //fetch hits
                           $hr=$hr_tournament+$hr_league;
                           $data[$year]['hr']=$hr; 
                           
                           //fetch RBI for Tournament and league
                           $rbi_tournament=$user_games->fetch_sum_of_stats(1,$year,$user_id,0,"rbis",0); 
                           $rbi_league=$user_games->fetch_sum_of_stats_league(2,$year,$user_id,0,"rbis"); 
                           $rbi=$rbi_tournament+$rbi_league;
                           $data[$year]['rbi']=$rbi;
                           
                            //fetch BB for Tournament and league
                           $bb_tournament=$user_games->fetch_stats_type(1,$year,$user_id,0,"BB",0); //fetch hits
                           $bb_league=$user_games->fetch_stats_type_league(2,$year,$user_id,0,"BB"); //fetch hits
                           $bb=$bb_tournament+$bb_league;
                           $data[$year]['bb']=$bb; 
                           
                            //fetch SB for Tournament and league
                           $sb_tournament=$user_games->fetch_sum_of_stats(1,$year,$user_id,0,"stolen_bases",0); 
                           $sb_league=$user_games->fetch_sum_of_stats_league(2,$year,$user_id,0,"stolen_bases"); 
                           $sb=$sb_tournament+$sb_league;
                           $data[$year]['sb']=$sb;
                           
                           //fetch runs_scored  for Tournament and league
                           $runs_scored_tournament=$user_games->fetch_sum_of_stats(1,$year,$user_id,0,"runs_scored",0); 
                           $runs_scored_league=$user_games->fetch_sum_of_stats_league(2,$year,$user_id,0,"runs_scored"); 
                           $runs_scored=$runs_scored_tournament+$runs_scored_league;
                           $data[$year]['runs_scored']=$runs_scored;
                           
                           //fetch agregrate functions
                           
                            
                        $avg_tournament=$user_games->fetch_AVG(1,$year,$user_id,0,0);
                        $avg_league=$user_games->fetch_AVG_league(2,$year,$user_id,0);
                        if($avg_league!=0 && $avg_tournament!=0){
                        $avg=($avg_tournament+$avg_league)/2;	
                    	}else
                        $avg=($avg_tournament+$avg_league);
                        $avg=number_format($avg,3);
                        $data[$year]['avg']=$avg;
                        
                           
                        $slg_tournament=$user_games->fetch_SLG(1,$year,$user_id,0,0);
                        $slg_league=$user_games->fetch_SLG_league(2,$year,$user_id,0);
                        if($slg_tournament!=0 && $slg_league!=0){
                        $slg=($slg_tournament+$slg_league)/2;
                    	}else
                        $slg=($slg_tournament+$slg_league);
                        $slg=number_format($slg,3);
                        $data[$year]['slg']=$slg;
                        //fetch obp
                        $obp_tournament=$user_games->fetch_OBP(1,$year,$user_id,0,0);
                        $obp_league=$user_games->fetch_OBP_league(2,$year,$user_id,0);
                        if($obp_tournament!=0 && $obp_league!=0){
                        $obp=($obp_tournament+$obp_league)/2;
                    	}else
                        $obp=($obp_tournament+$obp_league);
                        $obp=number_format($obp,3);
                        $data[$year]['obp']=$obp;
                        
                        //fetch ops
                        $ops_tournament=$user_games->fetch_OPS(1,$year,$user_id,0,0);
                        $ops_league=$user_games->fetch_OPS_league(2,$year,$user_id,0);
                        if($ops_tournament!=0 && $ops_league!=0){
                        $ops=($ops_tournament+$ops_league)/2;
                    	}else
                        $ops=($ops_tournament+$ops_league);
                        $ops=number_format($ops,3);
                        $data[$year]['ops']=$ops;
                        
                        //fetch TB
                        $tb_tournament=$user_games->fetch_TB(1,$year,$user_id,0,0);
                        $tb_league=$user_games->fetch_TB_league(2,$year,$user_id,0);
                        if($tb_tournament!=0 && $tb_league!=0){
                        $tb=($tb_tournament+$tb_league)/2;
                    	}else
                        $tb=($tb_tournament+$tb_league);
                        $tb=number_format($tb,3);
                        $data[$year]['tb']=$tb;
                        
                        //fetch RC
                        $rc_tournament=$user_games->fetch_RC(1,$year,$user_id,0,0);
                        $rc_league=$user_games->fetch_RC_league(2,$year,$user_id,0);
                        if($rc_tournament!=0 && $rc_league!=0){
                        $rc=($rc_tournament+$rc_league)/2;
                    	}else
                        $rc=($rc_tournament+$rc_league);
                        $rc=number_format($rc,3);
                        $data[$year]['rc']=$rc;
                        
                       
                        
                         //fetch PIP
                        $pip_tournament=$user_games->fetch_PIP(1,$year,$user_id,0,0);
                        $pip_league=$user_games->fetch_PIP_league(2,$year,$user_id,0);
                        if($pip_tournament!=0 && $pip_league!=0){
                        $pip=($pip_tournament+$pip_league)/2;
                    	}else
                    	$pip=($pip_tournament+$pip_league);
                        $pip=number_format($pip,3);
                        $data[$year]['pip']=$pip;
                        
                        }
                        
                        $response_array['success'] = 'success';
                        $this -> response -> statusCode(200); 
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		$this->set([
                'message' => $response_array,
                        'tournament_stats' => $data,
                        '_serialize' => ['message', 'tournament_stats']
                    ]);
          }
    
     /*
	  * Used to fetch games in one tournament
	  * params query string: 
      * tournament_id 
	  * Request: GET
	  *
	  */

	  public function single_game_detail(){
	  		       
			//$this->autoRender=false;
			$response_array = array();
			$user_id=$this->user_id;
			$data=array();
			if (($this->request->is('get'))) {
				$tournaments= TableRegistry::get('Tournaments');
				$tournament_id=$id =$this->request->query['tournament_id'];
				
				$game_type=1;
			    $result= $tournaments->find()
                         ->select(['UserGames.id','UserGames.user_id','tour_name'=>'Tournaments.name','UserGames.tournament_id','UserGames.game_date','UserGames.game_type','UserGames.my_team_name','UserGames.oponent','UserGames.game_location'])
                        ->Where(['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>1])
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ]
                         ]);
       

			   if(($result->count()>0)) {
					$data=$result;
					$response_array['success'] = 'success';
					$this -> response -> statusCode(200); 
				} else {
					$response_array['error'] = 'No results found';
					$this -> response -> statusCode(403);
				}
				
			}else{
				$response_array['error'] = 'wrong request';
				$this -> response -> statusCode(403);
			}

			$this->set([
				'message' => $response_array,
				'GameDetail' => $data,
				'_serialize' => ['message', 'GameDetail']
			]);    
  		  }
  		  
  		  
  		  /*
		  * Used to leagues match per date wise
		  * params query string: 
			   * game_id
			   * game_date
		  * Request: GET
		  * */
  		 
			public function single_game_detail_league(){
	  		       
			//$this->autoRender=false;
			$response_array = array();
			$data=array();
			if (($this->request->is('get'))) {

				
				$UserGames= TableRegistry::get('UserGames');
				$game_date =$this->request->query['game_date'];
				$user_id=$this->user_id;
			   $result= $UserGames->find()->where(['UserGames.game_date'=>$game_date,'UserGames.game_type'=>2,'UserGames.user_id'=>$user_id])->toArray();
                           
                           
			   if((count($result)>0)) {
					//$data['UserGames']=$result;
                                        foreach($result as $k=>$v) {
                                            $data[$k]['UserGames']['id']=$v['id'];
                                            $data[$k]['UserGames']['user_id']=$v['user_id'];
                                            $data[$k]['UserGames']['tournament_id']=$v['tournament_id'];
                                            $data[$k]['UserGames']['game_date']=$v['game_date'];
                                            $data[$k]['UserGames']['game_location']=$v['game_location'];
                                            $data[$k]['UserGames']['game_type']=$v['game_type'];
                                            $data[$k]['UserGames']['my_team_name']=$v['my_team_name'];
                                            $data[$k]['UserGames']['oponent']=$v['oponent'];
                                            $data[$k]['UserGames']['created']=$v['created'];
                                            $data[$k]['UserGames']['modified']=$v['modified'];
                                        }
                                        
                                        //echo json_encode($v)
					$response_array['success'] = 'success';
					$this -> response -> statusCode(200); 
				} else {
					$response_array['error'] = 'No results found';
					$this -> response -> statusCode(403);
				}
				
			}else{
				$response_array['error'] = 'wrong request';
				$this -> response -> statusCode(403);
			}

			$this->set([
				'message' => $response_array,
				'GameDetail' =>$data,
				'_serialize' => ['message', 'GameDetail']
			]);    
  		  }


  		  /*
	  * Used to fetch single game  tounament videos serialized at bat 
	  * params query string: 
           * user_game_id 
	  * Request: GET
	  * */

	  public function single_game_video(){
	  		       
	  	//$this->autoRender=false;
	  	$response_array = array();
		$data=array();
		$result=array();
		if(!empty($this->user_id)){

         		$user_id=$this->user_id;

         }else{
         		//pr($this->request);die;
         		$user_id=$this->request->query['id'];
         	}
		
		if (($this->request->is('get'))) {
		   $id =$this->request->query['game_id'];
		   	if($id=="order by us.id desc limit 1"){
		   $condition = "where us.user_id = ".$user_id. " order by us.id desc limit 1";
		   }else{
		   		$condition = "where us.user_games_id=".$id; 
		   }
		   //pr($condition) ; die;
	  	  $conn = ConnectionManager::get('default');

            $query="SELECT us.id as stat_id,us.at_bat,us.at_bat_result,us.user_games_id,user_stats_media.id as media_id,user_stats_media.media_type as media_type,user_stats_media.media_name as file_name
						FROM user_stats as us
						left join user_stats_media on user_stats_media.user_stats_id=us.id ".$condition; 
			//pr($query); die;			
						
						$query_return = $conn->execute($query);
                        
                        foreach($query_return as $item)
                        {

                                $result[] = $item;

                        }
                       
			if((count($result)>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}

		$this->set([
            'message' => $response_array,
            'GameDetail' => $data,
            '_serialize' => ['message', 'GameDetail']
        ]);    
  		  }
                  
        /*
         * Def: Api use for to show records on workout skills chart
         * 2) day_type means (pass weak for fetch current week data, and pass month, and year to fetch similar data)
         * 
         *          */          
        public function get_skills() {
              // $conn = ConnectionManager::get('default');
                //$conn->logQueries(true);
	  	$response_array = array();
		$data=array();
                $userSkills= TableRegistry::get('UserSkills');
                $user_id=$this->user_id;
		if (($this->request->is('get'))) {
		   $skill_type =1; //for workout
                   $day_type =$this->request->query['day_type'];
	  	   $conn = ConnectionManager::get('default');
                   
                   
                   $myarr=[];
                   if($day_type=="week") {
                        $skill_array=['Agility Training','Distance Running','Weight Lifting','Swimming'];
                       
                        
                        foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                         $week_array=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                        foreach($week_array as $k=>$v) {
                          $result_agility_training =$userSkills->fetch_data_skills($skill_type,$user_id,"agility_training",$v);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(int) $agility_training;
                          
                          //2 for distance running
                          $distance_running =$userSkills->fetch_data_skills($skill_type,$user_id,"distance_running",$v);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(int) $distance;
                          
                          //3 for weight_lifting
                          $weight_lifting =$userSkills->fetch_data_skills($skill_type,$user_id,"weight_lifting",$v);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(int) $weight;
                          
                           //4 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills($skill_type,$user_id,"swimming",$v);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(int) $swim;

                        }
                        
                        
                   } else if($day_type=="month") {
                       
                       $no_of_weeks=$userSkills->weeks_in_month(date('Y'),date('m'),1);
                       $skill_array=['Agility Training','Distance Running','Weight Lifting','Swimming'];
                       $week_array=[];
                       if($no_of_weeks==4){
                           $week_array=['First Week','Second Week','Third Week','Fourth Week'];
                       } else {
                           $week_array=['First Week','Second Week','Third Week','Fourth Week','Fifth Week'];
                       }
                      
                       foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                       
                        foreach($week_array as $k=>$v) {
                            //echo date("W");die;
                          $my_date_start_with_fist_date=date('Y-m')."-01";
                          $nth_week=date("W",strtotime($my_date_start_with_fist_date));
                          $nth_week=$nth_week-1; //I reduce -1 because mysql give nth week number less then -1 then php
                          
                          $nth_week_no =($k==0)? date("Y").($nth_week):date("Y").($nth_week+$k);
                            
                            
                            
                          $result_agility_training =$userSkills->fetch_data_skills_month($skill_type,$user_id,"agility_training",$nth_week_no);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(float) $agility_training;
                          
                          //2 for distance running
                          $distance_running =$userSkills->fetch_data_skills_month($skill_type,$user_id,"distance_running",$nth_week_no);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(float) $distance;
                          
                          //3 for weight_lifting
                          $weight_lifting =$userSkills->fetch_data_skills_month($skill_type,$user_id,"weight_lifting",$nth_week_no);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(float) $weight;
                          
                           //4 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_month($skill_type,$user_id,"swimming",$nth_week_no);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(float) $swim;
                         

                        }
                       
                       
                   } else if($day_type=='year') {
                       $skill_array=['Agility Training','Distance Running','Weight Lifting','Swimming'];
                       
                        
                        foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                         $week_array=['January','February','March','April','May','June','July','August','September','October','November','December'];
                        foreach($week_array as $k=>$v) {
                          $result_agility_training =$userSkills->fetch_data_skills_year($skill_type,$user_id,"agility_training",$v);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(float) $agility_training;
                          
                          //2 for distance running
                          $distance_running =$userSkills->fetch_data_skills_year($skill_type,$user_id,"distance_running",$v);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(float) $distance;
                          
                          //3 for weight_lifting
                          $weight_lifting =$userSkills->fetch_data_skills_year($skill_type,$user_id,"weight_lifting",$v);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(float) $weight;
                          
                           //4 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_year($skill_type,$user_id,"swimming",$v);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(float) $swim;

                        }
                   }

                 
                   if((!empty($myarr))) {
                                $data=$myarr;
                                $response_array['success'] = 'success';
                                $this -> response -> statusCode(200); 
                        } else {
                                $response_array['error'] = 'No results found';
                                $this -> response -> statusCode(403);
                        }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}

		$this->set([
            'message' => $response_array,
            'skills' => $data,
            '_serialize' => ['message', 'skills']
        ]);
        }
        
        
        /*
         * Def: Api use for to show records on practice skills chart
         * 2) day_type means (pass weak for fetch current week data, and pass month, and year to fetch similar data)
         * 
         *          */          
        public function get_practice_skills() {
                $conn = ConnectionManager::get('default');
                //$conn->logQueries(true);
	  	$response_array = array();
		$data=array();
                $userSkills= TableRegistry::get('UserSkills');
                $user_id=$this->user_id;
		if (($this->request->is('get'))) {
		   $skill_type =2; //for practice
                   $day_type =$this->request->query['day_type'];
	  	   $conn = ConnectionManager::get('default');
                   
                   
                   $myarr=[];
                   if($day_type=="week") {
                        $skill_array=['Hitting Drills','Offensive Drills','Defensive Drills','Pitching','Catching'];
                       
                        
                        foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                         $week_array=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
                        foreach($week_array as $k=>$v) {
                          //1 for hitting_drills
                          $result_agility_training =$userSkills->fetch_data_skills($skill_type,$user_id,"hitting_drills",$v);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(int) $agility_training;
                          
                          //2 for offensive_drills
                          $distance_running =$userSkills->fetch_data_skills($skill_type,$user_id,"offensive_drills",$v);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(int) $distance;
                          
                          //3 for defensive_drills
                          $weight_lifting =$userSkills->fetch_data_skills($skill_type,$user_id,"defensive_drills",$v);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(int) $weight;
                          
                           //4 for pitching
                          $swimming =$userSkills->fetch_data_skills($skill_type,$user_id,"pitching",$v);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(int) $swim;
                          
                          //5 for catching
                          $catching =$userSkills->fetch_data_skills($skill_type,$user_id,"catching",$v);
                          $catch=($catching['time_in_minutes']==null)?0:$catching['time_in_minutes'];
                          $myarr[4]['data'][$k]=(int) $catch;

                        }
                        
                   } else if($day_type=="month") {
                       
                       
                       
                       $no_of_weeks=$userSkills->weeks_in_month(date('Y'),date('m'),1);
                       $skill_array=['Hitting Drills','Offensive Drills','Defensive Drills','Pitching','Catching'];
                       $week_array=[];
                       if($no_of_weeks==4){
                           $week_array=['First Week','Second Week','Third Week','Fourth Week'];
                       } else {
                           $week_array=['First Week','Second Week','Third Week','Fourth Week','Fifth Week'];
                       }
                      
                       foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                       
                        foreach($week_array as $k=>$v) {
                            //echo date("W");die;
                            
                          $my_date_start_with_fist_date=date('Y-m')."-01";
                          $nth_week=date("W",strtotime($my_date_start_with_fist_date));
                          $nth_week=$nth_week-1; //I reduce -1 because mysql give nth week number less then -1 then php
                          
                          $nth_week_no =($k==0)? date("Y").($nth_week):date("Y").($nth_week+$k);
                         
                            
                            
                          $result_agility_training =$userSkills->fetch_data_skills_month($skill_type,$user_id,"hitting_drills",$nth_week_no);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(float) $agility_training;
                          
                          //2 for distance running
                          $distance_running =$userSkills->fetch_data_skills_month($skill_type,$user_id,"offensive_drills",$nth_week_no);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(float) $distance;
                          
                          //3 for weight_lifting
                          $weight_lifting =$userSkills->fetch_data_skills_month($skill_type,$user_id,"defensive_drills",$nth_week_no);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(float) $weight;
                          
                           //4 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_month($skill_type,$user_id,"pitching",$nth_week_no);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(float) $swim;
                         
                          
                            //5 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_month($skill_type,$user_id,"catching",$nth_week_no);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[4]['data'][$k]=(float) $swim;

                        }
                       
                   } else if($day_type=='year') {
                       
                       $skill_array=['Hitting Drills','Offensive Drills','Defensive Drills','Pitching','Catching'];
                       
                        
                        foreach($skill_array as $key=>$val) {
                            
                             $myarr[$key]['name']=$val;
                        }
                        
                         $week_array=['January','February','March','April','May','June','July','August','September','October','November','December'];
                        foreach($week_array as $k=>$v) {
                          $result_agility_training =$userSkills->fetch_data_skills_year($skill_type,$user_id,"hitting_drills",$v);
                          $agility_training=($result_agility_training['time_in_minutes']==null)?0:$result_agility_training['time_in_minutes'];
                          $myarr[0]['data'][$k]=(float) $agility_training;
                          
                          //2 for distance running
                          $distance_running =$userSkills->fetch_data_skills_year($skill_type,$user_id,"offensive_drills",$v);
                          $distance=($distance_running['time_in_minutes']==null)?0:$distance_running['time_in_minutes'];
                          $myarr[1]['data'][$k]=(float) $distance;
                          
                          //3 for weight_lifting
                          $weight_lifting =$userSkills->fetch_data_skills_year($skill_type,$user_id,"defensive_drills",$v);
                          $weight=($weight_lifting['time_in_minutes']==null)?0:$weight_lifting['time_in_minutes'];
                          $myarr[2]['data'][$k]=(float) $weight;
                          
                           //4 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_year($skill_type,$user_id,"pitching",$v);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[3]['data'][$k]=(float) $swim;
                          
                           //5 for weight_lifting
                          $swimming =$userSkills->fetch_data_skills_year($skill_type,$user_id,"catching",$v);
                          $swim=($swimming['time_in_minutes']==null)?0:$swimming['time_in_minutes'];
                          $myarr[4]['data'][$k]=(float) $swim;

                        }
                   }

                 
                   if((!empty($myarr))) {
                                $data=$myarr;
                                $response_array['success'] = 'success';
                                $this -> response -> statusCode(200); 
                        } else {
                                $response_array['error'] = 'No results found';
                                $this -> response -> statusCode(403);
                        }
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}

		$this->set([
            'message' => $response_array,
            'skills' => $data,
            '_serialize' => ['message', 'skills']
        ]);
        }



  		  /*
	  * Used to fetch weekly  games activity with videos serialized at bat 
	  * params query string: 
           * user_game_id 
	  * Request: GET
	  * */

	  public function weekly_game_info(){
	  	//$conn = ConnectionManager::get('default');
	  	//$conn->logQueries(true);

	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
					

                $userSkills= TableRegistry::get('UserSkills');
                $userMedia= TableRegistry::get('UserMediaInfo');
                if(!empty($this->user_id)){

         		$user_id=$this->user_id;

         }else{
         		//pr($this->request);die;
         		$user_id=$this->request->query['id'];
         	}


                $str=date('Y').date('W')-1;
                $condition=[
                'UserSkills.user_id'=>$user_id,
                //'UserSkills.workout_date'=>date('Y')
                'YEARWEEK(workout_date)'=>trim($str)
                ];

                 $query_return = $userSkills->find()
                         ->select(['um.id','um.media_name','um.media_type','um.user_skills_id','UserSkills.skills_img','UserSkills.workout_date','UserSkills.description','UserSkills.flag','workout_time'=>'sum(UserSkills.workout_time)','day_name'=>'dayname(UserSkills.workout_date)'])
                        ->Where($condition)
                         ->group('UserSkills.description,dayname(UserSkills.workout_date)')
                        ->order(['UserSkills.workout_date'=>'asc'])
                         ->limit(10)
                         ->hydrate(false)
                ->join([
                    'table' => 'user_media_info',
                    'alias' => 'um',
                    'type' => 'LEFT',
                    'conditions' => 'um.user_skills_id = UserSkills.id',
                ]);
		  
                
		foreach($query_return as $item)
                {

                   $result[] = $item;

                }
               // die;	
                 
			if((count(@$result)>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'No results found';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}

		$this->set([
            'message' => $response_array,
            'skillDetail' => $data,
            '_serialize' => ['message', 'skillDetail']
        ]);

  		  }
                  
                  
          /*
	  * Used to academics info 
	  * params query string: 
           * year
	  * Request: GET
	  * */

	  public function fetch_academics_info(){
	  	$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
                    
                    $user_academic= TableRegistry::get('UserAcademics');
                     $user_id=$this->user_id;
                    $year =$this->request->query['year'];

                    $result= $user_academic->find()->where(['user_id'=>$user_id,'year'=>$year])->first();
                  
                   
                    if(!empty($result)) {
                            $data=$result;
                            $response_array['success'] = 'success';
                            $this -> response -> statusCode(200); 
                    } else {
                            $response_array['error'] = 'No results found';
                            $this -> response -> statusCode(403);
                    }
                    
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}

		$this->set([
                    'message' => $response_array,
                    'user_academic' => $data,
                    '_serialize' => ['message', 'user_academic']
                ]);

  	}
        
        
        /**
	 * Used to fetch features records.
	 * request: GET
	 * Params: page and limit
	 */
	public function get_gallries() {
		$picture_url=Router::url('/',TRUE)."img/gallery_pic/";
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
			$page=(isset($this->request->query['page']))?$this->request->query['page']:1;
			$limit=(isset($this->request->query['limit']))?$this->request->query['limit']:5;
			$gallery = TableRegistry::get('galleries');
			$result = $gallery->find()->order(['id' => 'DESC'])->limit($limit)->page($page);
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'gallery' => $data,
            'image_path'=>$picture_url,
            '_serialize' => ['message', 'gallery','image_path']
        ]);
	}

	/*
	used to fetch tournament name for autocomplete name 
	created by anshul
	params: "q" for query , "type" for model ,"filter" for condition.  
	created on 29-05-2015 
	*/

	public function autocomplete(){
        $this->autoRender=false;

        $this->layout= "profile";
        
        //$tournament= TableRegistry::get('Tournaments');
        //$usergames= TableRegistry::get('UserGames');

		$q = $_GET["q"];
		$type = $_GET["types"];
		$filter = $_GET["filter"];
		$inkr = $_GET["inkr"];
		
		$obj=$this->return_factory_obj($type);
        $tname=array();
        $values=array();


        $res= $obj->find('all')
        ->select([$filter])
        ->where(["$filter LIKE"=>'%'.$q.'%'])->limit(50)->toArray();
        
   	   foreach ($res as $key => $value) {
   	   	$tname[]=$value[$filter];
   	   }

	   	if($tname){   
	   	   foreach ($tname as $rs) {
		
			$row = str_replace($q, '<b>'.$q.'</b>', $rs);
			
			// add new option

		    echo '<li onclick="set_item(\''.str_replace("'", "\'", $rs)."','".$filter."','".$inkr.'\')">'.$row.'</li>';
			}
		}else{
			echo "No Previous Entry";
		}

   }

   public function return_factory_obj($class_name) {

   		 $obj= TableRegistry::get($class_name);
   		 return $obj;


   }

   public function temp_game(){
   		$user_id=$this->Auth->user('id');

   		$response_array = array();
		$data=array();
		$result=array();
		if (($this->request->is('get'))) {
			$flag = $this->request->query['flag'];
			$tempdata= TableRegistry::get('TempGameRecord');
			$results = $tempdata->find()->where(['user_id'=>$user_id ,'flag'=>$flag])->order(['id' => 'DESC'])->toArray();
			//pr($result); die;
			foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				foreach ($data as $key => $value) {
					$id=$value->id;
				}
				$deltemp = $tempdata -> get($id);
				$tempdata-> delete($deltemp);
			  
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'tempdata' => $data,
            '_serialize' => ['message', 'tempdata']
        ]);
   }


   /*
   *  created to check pause data on load page 
   *  created on 06-05-2015
   *  created by anshul
   */

   public function temp_game_check(){
   		$user_id=$this->Auth->user('id');
		$response_array = array();
		$data=array();
		$result=array();
		if (($this->request->is('get'))) {
			$flag = $this->request->query['flag'];
			$tempdata= TableRegistry::get('TempGameRecord');
			$results = $tempdata->find()->where(['user_id'=>$user_id ,'flag'=>$flag])->order(['id' => 'DESC'])->toArray();
			foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
			
			}
		
		 $this->set([
            'message' => $response_array,
            '_serialize' => ['message']
        ]);
   }

   public function get_recent_game (){


   	$conn = ConnectionManager::get('default');
	 //$conn->logQueries(true); 	


   		//$user_id = $this->user_id;

   		if(!empty($this->user_id)){

         		$user_id=$this->user_id;

         }else{
         		//pr($this->request);die;
         		$user_id=$this->request->query['id'];
         	}
        //print_r($user_id."dddddddddd"); die; 	
   		$response_array = array();
		$data=array();
		$result=array();
   		$userGames= TableRegistry::get('UserGames');
   		if (($this->request->is('get'))) {
   		$results =$userGames->find()
   		->select(['UserGames.id','game_date','game_type','my_team_name','oponent','us.id','us.user_id','us.at_bat','us.at_bat_result','us.user_games_id','usm.id','usm.media_type','usm.media_name'])
   		->where(['UserGames.user_id' =>  $user_id,'UserGames.id'=>$userGames->find()->select(['UserGames.id'])->where(['UserGames.user_id'=>$user_id])->order(['UserGames.id'=>'DESC'])->limit(1)])
   		->order(['UserGames.id'=>'DESC'])
   		->hydrate(false)
   		->join([
   			'table' => 'user_stats',
                 'type' => 'LEFT',
                 'alias' => 'us',
                 'conditions' => 'us.user_games_id = UserGames.id',
             
         ])
        ->join([
            
                 'table' => 'user_stats_media',
                 'type' => 'LEFT',
                 'alias' => 'usm',
                 'conditions' => 'usm.user_stats_id = us.id',
            
         ]);

        //echo $results; die;
   		foreach ($results as $key => $value){
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'gamedetail' => $data,
            '_serialize' => ['message', 'gamedetail']
        ]);
   }

   public function get_memory_limit (){
   	 
   		$user_id = $this->user_id;
   		$response_array = array();
		$data=array();
		$result=array();
   		$memory_allot= TableRegistry::get('memory_allocation');
		$user_media = TableRegistry::get('user_media_info');   
        $user_stats = TableRegistry::get('user_stats_media');

   		if (($this->request->is('get'))) {
   		$results =$memory_allot->find()->select(['available_memory','memory_size','total_memory','id'])->where(['user_id' =>  $user_id]);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {

				$res1 = $user_media->find()
                        ->select(['media_size'=>'sum(media_size)'])
                        ->where(['user_id'=>$user_id])->First()->toArray();

        		$res3 = $user_stats->find()
                        ->select(['media_size'=>'sum(media_size)'])
                        ->where(['user_id'=>$user_id])->First()->toArray();
        
        		$data=$res1['media_size']+$res3['media_size'];	
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(200);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'memory_allot' => $data,
            '_serialize' => ['message', 'memory_allot']
        ]);
   }

   /*
   * created by anshul
   * created on 11-06-2015
   * method for the school galery  
   * 
   */

   public function get_school_gallery (){
   	 
   		$user_id = $this->user_id;
   		$response_array = array();
		$data=array();
		$result=array();
   		$schoolDetail= TableRegistry::get('school_galeries');
   		if (($this->request->is('get'))) {
   		$results =$schoolDetail->find()->select(['id','photo','description']);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
			}
		
		 $this->set([
            'message' => $response_array,
            'schoolDetail' => $data,
            '_serialize' => ['message', 'schoolDetail']
        ]);
   }

	/*
		created for getting topTen player ranking
		created on 20-06-2015
		created by anshul 
	*/
   public function topTen() { 
               
   		
   		$condition1 = "";
   		$condition3 = "";
   		$condition4 = "";
   		$condition5 = "";
   		$condition2 = "";

   			if(!empty($this->request->query)){
   				
		$query1 =$this->request->query['query1'];
		
		if($query1=='Baseball'){
		$condition1 ="AND up.sport=1"; 	
		}else if($query1=='Softball'){
		$condition1 ="AND up.sport=2";		
		}
		$query2 =$this->request->query['query2'];
		if(!empty($query2)){
		$condition3 ="AND u.age = ".$query2;
		}
		$query3 =$this->request->query['query3'];
		if($query3=='Right'){
		$condition4 ="AND up.bats_from=1";

		}else if($query3=='Left'){
		$condition4 ="AND up.bats_from=2";		
		}else if($query3=='Both'){
		$condition4 ="AND up.bats_from=3";		
		}
		//$query4 =$this->request->query['query4'];
		$query5 =$this->request->query['query5'];
		 if($query5=='AVG'){
			$condition2 = "order by avgData DESC" ;
		}else if($query5=='SLG'){
			$condition2 = "order by slgData DESC" ;
		}else if($query5=='OPS'){
			$condition2 = "order by OPS DESC" ;
		}else if($query5=='OBP'){
			$condition2 = "order by OBP DESC" ;
		}else if($query5=='PIP'){
			$condition2 = "order by PIP DESC" ;		
		}else if($query5=='RBI'){
			$condition2 = "order by RBI DESC" ;
		}else if($query5=='HR'){
			$condition2 = "order by HR DESC" ;
		}else if($query5=='BB'){
			$condition2 = "order by BB DESC" ;
		}else if($query5=='QAB'){
			$condition2 = "order by QAB DESC" ;
		}else if($query5=='RHP'){
			$condition2 = "order by rgtavg DESC" ;
		
		}else if($query5=='LHP'){
			$condition2 = "order by lftavg DESC" ;
		
		}else if($query5=='ROB-0'){
			$condition2 = "order by rob0avg DESC" ;
		
		}else if($query5=='ROB-1'){
			$condition2 = "order by rob1avg DESC" ;
		
		}else if($query5=='ROB-2'){
			$condition2 = "order by rob2avg DESC" ;
		
		}else if($query5=='ROB-3'){
			$condition2 = "order by rob3avg DESC" ;
		
		}else if($query5=='2-outs'){
			$condition2 = "order by out2savg DESC" ;
		
		}else if($query5=='2-strick'){
			$condition2 = "order by strick2avg DESC" ;
		}

		$datefrom=$this->request->query['datep1'];
		$dateto=$this->request->query['datep2'];
		if(!empty($datefrom)){
			$condition5 = "AND (ug.game_date BETWEEN '".$datefrom."' AND '".$dateto."')";
		}
		
		//$query6 =$this->request->query['query6']; 	
		}
		
		$conn = ConnectionManager::get('default');
      	//$conn->logQueries(true);
      	$response_array = array();
    	$data=array();
    	$result=array();

    	if (($this->request->is('get'))) {

        $query='Select  sum(d2) as hits,
			resDada.names,
		(sum(d3)/sum(resDada.atBat))  as slgData,
		(sum(d2)/sum(resDada.atBat))  as avgData,
		(sum(d21)/sum(rgtatbat))  as rgtavg,
		(sum(d211)/sum(lftatbat))  as lftavg,
		(sum(rob10)/sum(0robatbat))  as rob0avg,
		(sum(rob11)/sum(1robatbat))  as rob1avg,
		(sum(rob12)/sum(2robatbat))  as rob2avg,
		(sum(rob13)/sum(3robatbat))  as rob3avg,
		(sum(outs02)/sum(outs2))  as out2savg,
		(sum(pstrs02)/sum(pcs2))  as strick2avg,
		sum(d3),resDada.user_id,
		sum(resDada.atBat) as bat,
		sum(resDada.BB) as BB,
		sum(resDada.qab) as QAB,
		1-sum(resDada.SO)/sum(resDada.atBat) as PIP,
        sum(resDada.1B) as one_B,
        sum(resDada.2B) as two_B,
        sum(resDada.3B) as three_B,
        sum(resDada.HBP) as HBP,
		sum(resDada.SAC) as SAC,
		sum(resDada.rbis) as RBI,
		sum(resDada.HR) as HR,
		resDada.1st_sem as fsem,
		resDada.2nd_sem as ssem,
		((sum(d2) + sum(resDada.BB) + sum(resDada.HBP)) / (sum(resDada.atBat) + sum(resDada.BB) + sum(resDada.HBP) + sum(resDada.SAC)) + (sum(d3)/sum(resDada.atBat))) as OPS,
		((sum(d2) + sum(resDada.BB) + sum(resDada.HBP)) / (sum(resDada.atBat) + sum(resDada.BB) + sum(resDada.HBP) + sum(resDada.SAC))) as OBP
	from (	
		select count(us.user_id),
		       us.rbis,
		       sum(dataList.f_sem) as 1st_sem,
		       sum(dataList.s_sem) as 2nd_sem,
			   sum(us.at_bat) as atBat,
			   sum(us.qab) as qab,
			   IF(us.pitcher=1,sum(us.at_bat),0)as rgtatbat,
			   IF(us.pitcher=2,sum(us.at_bat),0)as lftatbat,
			   IF(us.rob=0,sum(us.at_bat),0)as 0robatbat,
			   IF(us.rob=1,sum(us.at_bat),0)as 1robatbat,
			   IF(us.rob=2,sum(us.at_bat),0)as 2robatbat,
			   IF(us.rob=3,sum(us.at_bat),0)as 3robatbat,
			   IF(us.outs=2,sum(us.at_bat),0)as outs2,
			   IF(us.pitch_count_strikes=2,sum(us.at_bat),0)as pcs2,
			   IF(us.at_bat_result = "HR", count(us.user_id)*4, 	
			   IF(us.at_bat_result = "1B", count(us.user_id)*1,
			   IF(us.at_bat_result = "3B", count(us.user_id)*3,
			   IF(us.at_bat_result = "2B", count(us.user_id)*2, "Rakesh2")
			   ))
			   ) as d3,
			   IF(us.at_bat_result = "BB", count(us.user_id)*1, 0) as BB,
			   IF(us.at_bat_result = "1B", count(us.user_id)*1, 0) as 1B,
			   IF(us.at_bat_result = "2B", count(us.user_id)*1, 0) as 2B,
			   IF(us.at_bat_result = "3B", count(us.user_id)*1, 0) as 3B,
			   IF(us.at_bat_result = "HBP", count(us.user_id)*1, 0) as HBP,
			   IF(us.at_bat_result = "SAC", count(us.user_id)*1, 0) as SAC,
			   IF(us.at_bat_result = "HR", count(us.user_id)*1, 0) as  HR,
			   IF(us.at_bat_result = "SO", count(us.user_id)*1, 0) as SO,
			   IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh")))) as d2,
			   IF(us.pitcher=1,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"ssssss") as d21,
IF(us.pitcher=2,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"ss") as d211,

IF(us.rob=0,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"robfalse0") as rob10,
IF(us.rob=1,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"robfalse1") as rob11,
IF(us.rob=2,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"robfalse2") as rob12,
IF(us.rob=3,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"robfalse3") as rob13,

IF(us.outs=2,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"outs2false") as outs02,

IF(us.pitch_count_strikes=2,IF(us.at_bat_result = "HR", count(us.user_id),IF(us.at_bat_result = "1B", count(us.user_id), IF
(us.at_bat_result = "3B", count(us.user_id), IF(us.at_bat_result = "2B", count(us.user_id), "Rakesh"
)))),"pstr3") as pstrs02,
			   us.at_bat_result,us.at_bat ,dataList.username as names,
			   us.user_id from (

			    SELECT u.id as uId,u.first_name as name,u.*,ua.first_sem_gpa as f_sem,ua.second_sem_gpa as s_sem FROM users as u
					left join `userprofiles` as up on u.id=up.user_id
					left join user_academics as ua on u.id=ua.user_id
					left join user_games as ug on u.id=ug.user_id where 1=1
			 		 '." ".$condition1." ".$condition3." ".$condition4." ".$condition5.' group by u.id     
			    ) as dataList
				left join user_stats as us on dataList.uId=us.user_id group by us.user_id,us.at_bat_result,us.at_bat

	)

		 as resDada  where resDada.user_id <> "" 
		group by resDada.user_id '.$condition2.' LIMIT 10';
             
				//echo $query; die;
                $query_return = $conn->execute($query);			                 
              foreach($query_return as $item)
                {

                        $result[] = $item;

                }
                       	
			if((count($result)>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
                
		}else{
			$response_array['error'] = 'no data found';
			$this -> response -> statusCode(403);
		}

		$this->set([
                'message' => $response_array,
                        'tournament_stats' => $data,
                        '_serialize' => ['message', 'tournament_stats']
                    ]);
          }


        }
        
    public function paypalForm()
	    {
	       $users = TableRegistry::get('Users');
	        $response_array = array();
	        $data = array();
	       if ($this->request->is('get')){

	       	$id=$this->request->query['id'];
         	 
			$row = $users ->get($id);
			if($row['plan_id']=='F'){
	           $response_array['success'] = 'FreeUser' ;
	           $data = "http://".$_SERVER['SERVER_NAME']."/categories/mailPage?success=false&id=".$id;   
	           $this->response->statusCode(200); 
	    	}else if($row['plan_id']=='P'){

	    	include(ROOT.'/vendor/paypal/common/CreateBillingAgreementWithPayPal.php');
		       	   $response_array['success'] = 'PaidUser' ;
	               $data = $approvalUrl ;   
	               $this->response->statusCode(200); 
	        }else{      		
				   $response_array['error'] = 'error' ;
	               $data = 'plan not found' ;   
	               $this->response->statusCode(403);
	    		 }	

	   $this->set(['message'=>$response_array,
	   	'plan'=>$data,
	   	'_serialize'=>['message','plan']
	    ]); 	  
	  }
	}

	  public function upgradepaypal()
	    {
	        $users = TableRegistry::get('Users');
	        $response_array = array();
	        $data = array();
	      if ($this->request->is('get')){

	       	$id=$this->request->query['id'];
	       	$row = $users ->get($id);
			if(!empty($row['plan_id'])){

	    	include(ROOT.'/vendor/paypal/common/CreateBillingAgreementWithPayPal.php');
	       	   $response_array['success'] = 'PaidUser';
               $data = $approvalUrl ;   
               $this->response->statusCode(200); 
	        }else{      		
				   $response_array['error'] = 'error' ;
	               $data = 'plan not found' ;   
	               $this->response->statusCode(403);
	    		 }	

	   $this->set(['message'=>$response_array,
	   	'plan'=>$data,
	   	'_serialize'=>['message','plan']
	    ]); 	  
	  }  

	}


	public function pausepaypal()
	    {
	        $users = TableRegistry::get('Users');
	        $response_array = array();
	        $data = array();
	      if ($this->request->is('get')){

	       	$id=$this->request->query['id'];
	       	$row = $users ->get($id);
			if(!empty($row['plan_id'])){

	    	include(ROOT.'/vendor/paypal/common/DeletePlan.php');
	    	
            	
         		$row['plan_id']='F';

         		$users->save($row);
				$response_array['success'] = 'Now You are a FreeUser';
               $data = $id ;   
               $this->response->statusCode(200); 
	        }else{      		
				   $response_array['error'] = 'error' ;
	               $data = 'plan not found' ;   
	               $this->response->statusCode(403);
	    		 }	

	   $this->set(['message'=>$response_array,
	   	'plan'=>$data,
	   	'_serialize'=>['message','plan']
	    ]); 	  
	  }  

	}


/*
created for saving profile pic 
created by anshu 
created on 22-06-2015  
*/

	public function profile_pic(){

		$response_array = array();
	        $data = array();
	     $ImgPath = WWW_ROOT . "img/profile_pic/";
 
	        
	        if ($this->request->is(['patch', 'post', 'put'])) {
			$Profile = TableRegistry::get('Profiledetails');
			$this -> request -> data['user_id']=$this->user_id;
			if(!empty($this->request->data['profile_pic1']['tmp_name'])){

                $imageName = rand(5,10000).'_'.$this->request->data['profile_pic1']['name'];
                move_uploaded_file($this->request->data['profile_pic1']['tmp_name'], $ImgPath .$imageName);
                $this->request->data['profile_pic1']=$imageName; 
              }else{
                $this->request->data['profile_pic1']= ''; 
              }
			
			if(!empty($this->request->data['profile_pic2']['tmp_name'])){
                $imageName = rand(5,10000).'_'.$this->request->data['profile_pic2']['name'];
                move_uploaded_file($this->request->data['profile_pic2']['tmp_name'], $ImgPath .$imageName);
                $this->request->data['profile_pic2']=$imageName; 
              }else{
                $this->request->data['profile_pic2']= 'default.png'; 
              }

			if(!empty($this->request->data['profile_pic3']['tmp_name'])){
                $imageName = rand(5,10000).'_'.$this->request->data['profile_pic3']['name'];
                move_uploaded_file($this->request->data['profile_pic3']['tmp_name'], $ImgPath .$imageName);
                $this->request->data['profile_pic3']=$imageName; 
              }else{
                $this->request->data['profile_pic3']= 'default.png'; 
              }

			
			$id = $this->request->data['id']; 	
			if(!empty($id)){
			$user = $Profile->get($id);
			$entity = $Profile->patchEntity($user, $this->request->data);
		}else{
			$entity = $Profile -> newEntity($this -> request -> data);
		}
			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$Profile->save($entity);
				$data=$entity;
				$response_array['success'] = 'Profile images saved successfully';
				$this -> response -> statusCode(200);
			}
		} else{
			$response_array['error'] = 'Error while Saving';
			$this -> response -> statusCode(403);
		}
		$this->set(['message'=>$response_array,
	   	'getdata'=>$data,
	   	'_serialize'=>['message','getdata']
	    ]);
	}

/*
created for getting profile pic 
created by anshu 
created on 23-06-2015  
*/

	public function get_profile_pic (){
   		
   		if(!empty($this->user_id)){

         		$user_id=$this->user_id;

         }else{
         		//pr($this->request);die;
         		$user_id=$this->request->query['id'];
         	}

   		$response_array = array();
		$data=array();
		$result=array();
   		$Profiles= TableRegistry::get('Profiledetails');
   		if (($this->request->is('get'))) {
   		$results =$Profiles->find()->select(['profile_pic1','profile_pic2','profile_pic3','user_id','id'])->where(['user_id' =>  $user_id])->limit(1);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'img' => $data,
            '_serialize' => ['message', 'img']
        ]);
   }


/*
created for saving profile pic 
created by anshu 
created on 22-06-2015  
*/

	public function setPhone(){

		$response_array = array();
	        $data = array();
	     
 		if ($this->request->is(['patch', 'post', 'put'])) {
 			$smsalert = TableRegistry::get('SmsAlert');
			$this -> request -> data['user_id']=$this->user_id;
			$id = $this->request->data['id'];

			if(!empty($id)){
			$user = $smsalert->get($id);
			$entity = $smsalert->patchEntity($user, $this->request->data);
		}else{
			$entity = $smsalert -> newEntity($this -> request -> data);
		}

			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$smsalert->save($entity);
				$data=$entity;
				$response_array['success'] = 'Phone Number saved successfully';
				$this -> response -> statusCode(200);
			}
		} else{
			$response_array['error'] = 'Error while Saving';
			$this -> response -> statusCode(403);
		}
		$this->set(['message'=>$response_array,
	   	'getdata'=>$data,
	   	'_serialize'=>['message','getdata']
	    ]);
	}

	/*
created for saving profile information 
created by anshu 
created on 03-07-2015  
*/

	public function personal_info(){

		$response_array = array();
	        $data = array();
	     
 		if ($this->request->is(['patch', 'post', 'put'])) {
 			$smsalert = TableRegistry::get('Users');
			$this -> request -> data['user_id']=$this->user_id;
			$id = $this->request->data['id'];

			if(!empty($id)){
			$user = $smsalert->get($id);
			$entity = $smsalert->patchEntity($user, $this->request->data);
		}else{
			$entity = $smsalert -> newEntity($this -> request -> data);
		}

			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$smsalert->save($entity);
				$data=$entity;
				$response_array['success'] = 'PERSONAL INFO UPDATED';
				$this -> response -> statusCode(200);
			}
		} else{
			$response_array['error'] = 'Error while Saving';
			$this -> response -> statusCode(403);
		}
		$this->set(['message'=>$response_array,
	   	'getdata'=>$data,
	   	'_serialize'=>['message','getdata']
	    ]);
	}

	public function profile_info(){

		$response_array = array();
	        $data = array();
	     
 		if ($this->request->is(['patch', 'post', 'put'])) {
 			$smsalert = TableRegistry::get('Teamdivision');
			$this -> request -> data['user_id']=$this->user_id;
			$id = $this->request->data['id'];

			if(!empty($id)){
			$user = $smsalert->get($id);
			$entity = $smsalert->patchEntity($user, $this->request->data);
		}else{
			$entity = $smsalert -> newEntity($this -> request -> data);
		}

			if ($entity -> errors()) {
				foreach ($entity->errors() as $key => $val) {
					foreach ($val as $k => $d) {
						$vl[] = $d;
					}
				}
				$response_array['error'] = $vl;
				$this -> response -> statusCode(403);
			} else {
				$smsalert->save($entity);
				$data=$entity;
				$response_array['success'] = 'PERSONAL INFO UPDATED';
				$this -> response -> statusCode(200);
			}
		} else{
			$response_array['error'] = 'Error while Saving';
			$this -> response -> statusCode(403);
		}
		$this->set(['message'=>$response_array,
	   	'getdata'=>$data,
	   	'_serialize'=>['message','getdata']
	    ]);
	}

/*
created for getting profile pic 
created by anshu 
created on 23-06-2015  
*/

	public function getPhno(){
   		$user_id = $this->user_id;
   		$response_array = array();
		$data=array();
		$result=array();
   		$Profiles= TableRegistry::get('SmsAlert');
   		if (($this->request->is('get'))) {
   		$results =$Profiles->find()->select(['phone_no1','phone_no2','phone_no3','notify_one','notify_two','notify_three','user_id','id'])->where(['user_id' =>  $user_id])->limit(1);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'phno' => $data,
            '_serialize' => ['message', 'phno']
        ]);
   }

   /*
created for getting getPerinfo
created by anshu 
created on 23-06-2015  
*/

	public function getPerinfo(){
   		$user_id = $this->user_id;
   		$response_array = array();
		$data=array();
		$result=array();
   		$Profiles= TableRegistry::get('Users');
   		if (($this->request->is('get'))) {
   		$results =$Profiles->find()->select(['address','phone','email','id'])->where(['id' =>  $user_id])->limit(1);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'phno' => $data,
            '_serialize' => ['message', 'phno']
        ]);
   }

 /*
created for getting getPerinfo
created by anshu 
created on 23-06-2015  
*/

	public function getProinfo(){
   		$user_id = $this->user_id;
   		$response_array = array();
		$data=array();
		$result=array();
   		$Profiles= TableRegistry::get('Teamdivision');
   		if (($this->request->is('get'))) {
   		$results =$Profiles->find()->select(['current_team','division','user_id','id'])->where(['user_id' =>  $user_id])->limit(1);
   		foreach ($results as $key => $value) {
				$result[]=$value;
			}
			if((!empty($result))) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200);
				
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'phno' => $data,
            '_serialize' => ['message', 'phno']
        ]);
   }

   public function sendsms(){
   
   	include(ROOT.'/vendor/plivo.php');

		$response_array = array();
	        $data = array();
	    $this -> request -> data['user_id']=$this->user_id;
	        
	 		$auth_id = "MAOWFHNTA4NDEYZWNIMG";
    		$auth_token = "N2QzODZlMjc2OTQ1ZWQzNmNjODA5ZDA1NGEyMzFl";
	  $p = new \RestAPI($auth_id, $auth_token);
	
 		if ($this->request->is(['patch', 'post', 'put'])) {
			
 			
			if($this -> request -> data['phone_no1'] !=0){

									
			$check = '<'.$this -> request -> data['phone_no1'];	
			}else if($this -> request -> data['phone_no2']!=0){

			$check = '<'.$this -> request -> data['phone_no2'];	
				
			}else if($this -> request -> data['phone_no3']!=0){

				$check = '<'.$this -> request -> data['phone_no3'];
			}else{
				$check = '';
			}

			if($check==null){
				$response_array['success'] = 'mobile number was empty';
				$this -> response -> statusCode(200);

			}else{
				


					$id = $this->user_id;
					$crs = $this->Auth->user();
		  			$name  = $crs['first_name']." ".$crs['last_name'];
					$params = array(
		            'src' => '+13305203988', // Sender's phone number with country code
		            'dst' => $check, // Receiver's phone number with country code
		            'text' => 'Hi,'.$name.' has just posted game results Click on link to view    http://'.$_SERVER['HTTP_HOST'].'/current_stats?id='.$id, // Your SMS text message
		            'url' => 'https://glacial-harbor-8656.herokuapp.com/report', // The URL to which with the status of the message is sent
		            'method' => 'POST' // The method used to call the url
		        	);

					$response = $p->send_message($params);

					if($response['status']==400){
					
						$response_array['error'] = $response['response']['error'];
					    $this -> response -> statusCode(403);

					}else if($response['status']==202){

						$response_array['success'] = 'message send successfully';
						$this -> response -> statusCode(200);
					}	
					
				}

	    }

		$this->set(['message'=>$response_array,
	   	'getdata'=>$response,
	   	'_serialize'=>['message','getdata']
	    ]);
	}


	/*
     * created for checking a user having one time permission of
     * feeding stats for free plan
     * created on 01-07-2015
     * created by anshul  
    */  
    public function allowStats(){

        	
         $user = $this->Auth->user();
		$usergames = TableRegistry::get('UserGames');
         $users = TableRegistry::get('Users');
         $sportType = $usergames->find()->select(['user_id'])->where(['user_id'=>$user['id']])->First()->toArray();
         
         if(!empty($sportType['user_id']) && $user['plan_id'] =='F' && $user['check_plan'] ==0){

             $id = $user['id'];
             $user = $users->get($id);
             $user['check_plan']=1;
             
             	$users->save($user);
                $response_array['success'] = 'stats locked now as you are free user !!';
				$this -> response -> statusCode(200);
			
			} else{
					$response_array['error'] = 'you are free to use limited memory';
					$this -> response -> statusCode(403);
		     	}

		$this->set(['message'=>$response_array,
	   	'_serialize'=>['message','getdata']
	    ]);

	   
       }

       /*
		* created for uploding file on youtube server
		* created on 17 -07-2015
		* created by anshul
		*/

		public function serverVideo()
    {
        $this->autoRender=false;    
    		    
    include(ROOT.'/vendor/video/upload_video.php');
        error_reporting(E_ALL | E_STRICT);
        //$upload_handler = new \upload_video();
        die;
    }

    /**
	 * Used to fetch marketing add gallery.
	 * request: GET
	 * Params: page and limit
	 */
	public function market_feature() {
		$picture_url=Router::url('/',TRUE)."img/market_pic/";
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {
			$page=(isset($this->request->query['page']))?$this->request->query['page']:1;
			$limit=(isset($this->request->query['limit']))?$this->request->query['limit']:5;
			$features= TableRegistry::get('MarketGaleries');
			$result = $features->find()->order(['id' => 'DESC'])->limit($limit)->page($page);
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'markets' => $data,
            'image_path'=>$picture_url,
            '_serialize' => ['message', 'markets','image_path']
        ]);
	}

	/**
	 * Used to fetch skill gallery.
	 * request: GET
	 * Params: page and limit
	 */
	public function get_skills_images() {

		$user = $this->Auth->user();

		$picture_url=Router::url('/',TRUE)."img/skills_pic/";
		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {

			$userSkills= TableRegistry::get('UserSkills');
			$result = $userSkills->find()->select(['id','user_id','skills_img','flag','created'])->where(['user_id'=>$user['id']])->order(['id' => 'DESC']);
			
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'skillsimg' => $data,
            'image_path'=>$picture_url,
            '_serialize' => ['message', 'skillsimg','image_path']
        ]);
	}


	/**
	 * Used to fetch skill gallery video .
	 * request: GET
	 * Params: page and limit
	 */
	public function get_skills_videos() {

		$user = $this->Auth->user();

		$response_array = array();
		$data=array();
		if (($this->request->is('get'))) {

			$userMediaInfo= TableRegistry::get('UserMediaInfo');
			$result = $userMediaInfo->find()->select(['id','user_id','media_name','media_type','created'])->where(['user_id'=>$user['id']])->order(['id' => 'DESC']);
			
			if(($result->count()>0)) {
				$data=$result;
				$response_array['success'] = 'success';
				$this -> response -> statusCode(200); 
			} else {
				$response_array['error'] = 'results set is empty';
				$this -> response -> statusCode(403);
			}
			
		}else{
			$response_array['error'] = 'wrong request';
			$this -> response -> statusCode(403);
		}
		
		 $this->set([
            'message' => $response_array,
            'skillsvideo' => $data,
            
            '_serialize' => ['message', 'skillsvideo']
        ]);
	}


}

