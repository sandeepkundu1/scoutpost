<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SchoolGalery Entity.
 */
class SchoolGalery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'description' => true,
        'photo' => true,
    ];
}
