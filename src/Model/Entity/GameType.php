<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GameType Entity.
 */
class GameType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'game_name' => true,
        'description' => true,
    ];
}
