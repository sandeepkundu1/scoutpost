<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tournament Entity.
 */
class Tournament extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'user_id' => true,
        'flag' => true,
        'user_games' => true,
    ];
}
