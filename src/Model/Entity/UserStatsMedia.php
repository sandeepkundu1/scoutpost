<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserStatsMedia Entity.
 */
class UserStatsMedia extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_stats_id' => true,
        'user_id' => true,
        'media_name' => true,
        'media_type' => true,
        'media_size' => true,
        'user_stat' => true,
        'user' => true,
    ];
}
