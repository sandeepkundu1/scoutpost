<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gallery Entity.
 */
class Gallery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [

        
        'id' => true,
        'gallery_image' => true,
        
         ];
}
