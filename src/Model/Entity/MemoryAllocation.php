<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MemoryAllocation Entity.
 */
class MemoryAllocation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'memory_size' => true,
        'total_memory' => true,
        'available_memory' => true,
        'user' => true,
    ];
}
