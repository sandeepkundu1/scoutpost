<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserMediaInfo Entity.
 */
class UserMediaInfo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_skills_id' => true,
        'user_id' => true,
        'media_name' => true,
        'media_type' => true,
        'media_size' => true,
        'user_skill' => true,
        'user' => true,
    ];
}
