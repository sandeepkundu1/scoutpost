<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserGame Entity.
 */
class UserGame extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'tournament_id' => true,
        'game_date' => true,
        'game_location' => true,
        'game_type' => true,
        'my_team_name' => true,
        'oponent' => true,
        'user' => true,
        'user_stats' => true,
    ];
}
