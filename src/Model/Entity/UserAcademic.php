<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserAcademic Entity.
 */
class UserAcademic extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'year' => true,
        'first_sem_gpa' => true,
        'second_sem_gpa' => true,
        'user' => true,
    ];
}
