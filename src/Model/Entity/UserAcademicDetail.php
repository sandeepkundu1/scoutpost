<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserAcademic Entity.
 */
class UserAcademicDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'year' => true,
        'subject'=>true,
        'grade' => true,
        'weight' => true,
        'user' => true,
    ];
}
