<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SmsAlert Entity.
 */
class SmsAlert extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'phone_no1' => true,
        'phone_no2' => true,
        'phone_no3' => true,
        'notify_one' => true,
        'notify_two' => true,
        'notify_three' => true,
        'user' => true,
    ];
}
