<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity.
 */
class Page extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'pname' => true,
        'id'=>true,
        'meta_title' => true,
        'meta_link' => true,
        'subject' => true,
        'link_title' => true,
        'meta_keyword' => true,
        'meta_description' => true,
        'page_content' => true,
        
    ];
}
