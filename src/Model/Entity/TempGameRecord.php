<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TempGameRecord Entity.
 */
class TempGameRecord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'flag' => true,
        'formdata' => true,
        'user' => true,
    ];
}
