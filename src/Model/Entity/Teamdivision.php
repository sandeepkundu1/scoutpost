<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Teamdivision Entity.
 */
class Teamdivision extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'current_team' => true,
        'division' => true,
        'user' => true,
    ];
}
