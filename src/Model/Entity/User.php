<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'role_id' => true,
        'plan_id' => true,
        'last_name' => true,
        'email' => true,
        'profile_image' => true,
        'username' => true,
        'password' => true,
        'age' => true,
        'sex' => true,
        'dob' => true,
        'address' => true,
        'city' => true,
        'state' => true,
        'zip' => true,
        'activation_key' => true,
        'salt' => true,
        'email_verified' => true,
        'phone' => true,
        'active' => true,
        'check_plan' => true,
        'userProfiles' => true,
        'cpassword' => true,
         ];
    
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
    
    
    

}
