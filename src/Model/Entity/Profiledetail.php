<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Profiledetail Entity.
 */
class Profiledetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'profile_pic1' => true,
        'profile_pic2' => true,
        'profile_pic3' => true,
        'user' => true,
    ];
}
