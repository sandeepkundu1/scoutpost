<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserStat Entity.
 */
class UserStat extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_games_id' => true,
        'user_id' => true,
        'at_bat' => true,
        'no_of_out' => true,
        'pitcher' => true,
        'rob' => true,
        'at_bat_result' => true,
        'stolen_bases' => true,
        'rbis' => true,
        'runs_scored' => true,
        'select_hbp' => true,
        'pitch_count_bals' => true,
        'pitch_count_strikes' => true,
        'pitch_thrown' => true,
        'qab' => true,
        'outs' => true,
        'user_game' => true,
        'user' => true,
    ];
}
