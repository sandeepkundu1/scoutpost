<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserSkill Entity.
 */
class UserSkill extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'workout_date' => true,
        'workout_time' => true,
        'description' => true,
        'skills_img' => true,
        'flag' => true,
        'user' => true,
    ];
}
