<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class Userprofile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'sport' => true,
        'pitcher' => true,
        'outfilder' => true,
        'infilder' => true,
        'catcher' => true,
        'bats_from' => true,
        'through_with' => true,
        'curr_team' => true,
        'curr_school'=>true,
        'schooling'=>true,
        'feb_team'=>true,
        'feb_athlete'=>true,
       
         ];
    
}
