<?php
namespace App\Model\Table;

use App\Model\Entity\UserStat;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserStats Model
 */
class UserStatsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('user_stats');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('UserGames', [
            'foreignKey' => 'user_games_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->allowEmpty('at_bat')
            ->add('no_of_out', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('no_of_out')
            ->allowEmpty('pitcher')
            ->allowEmpty('rob')
            ->allowEmpty('at_bat_result')
            ->allowEmpty('stolen_bases')
            ->allowEmpty('rbis')
            ->allowEmpty('runs_scored')
            ->allowEmpty('select_hbp')
            ->add('pitch_count_bals', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('pitch_count_bals')
            ->add('pitch_count_strikes', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('pitch_count_strikes')
            ->add('pitch_thrown', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('pitch_thrown')
            ->add('qab', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('qab')
            ->add('outs', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('outs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_games_id'], 'UserGames'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
