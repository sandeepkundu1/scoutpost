<?php
namespace App\Model\Table;

use App\Model\Entity\MemoryAllocation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MemoryAllocation Model
 */
class MemoryAllocationTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('memory_allocation');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('memory_size', 'valid', ['rule' => 'numeric'])
            ->requirePresence('memory_size', 'create')
            ->notEmpty('memory_size')
            ->add('total_memory', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_memory', 'create')
            ->notEmpty('total_memory')
            ->add('available_memory', 'valid', ['rule' => 'numeric'])
            ->requirePresence('available_memory', 'create')
            ->notEmpty('available_memory');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
