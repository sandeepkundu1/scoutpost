<?php
namespace App\Model\Table;

use App\Model\Entity\SmsAlert;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SmsAlert Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class SmsAlertTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('sms_alert');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('phone_no1', 'create')
            ->notEmpty('phone_no1');
            
        $validator
            ->allowEmpty('phone_no2');
            
        $validator
            ->allowEmpty('phone_no3');
            
        $validator
            ->add('notify_one', 'valid', ['rule' => 'numeric'])
            ->requirePresence('notify_one', 'create')
            ->notEmpty('notify_one');
            
        $validator
            ->add('notify_two', 'valid', ['rule' => 'numeric'])
            ->requirePresence('notify_two', 'create')
            ->notEmpty('notify_two');
            
        $validator
            ->add('notify_three', 'valid', ['rule' => 'numeric'])
            ->requirePresence('notify_three', 'create')
            ->notEmpty('notify_three');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
