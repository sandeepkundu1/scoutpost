<?php
namespace App\Model\Table;

use App\Model\Entity\UserGame;
use App\Error\AppError;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * UserGames Model
 */
class UserGamesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('user_games');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserStats',
            ['foreignKey' => 'user_games_id',
            'dependent' => true,
    'cascadeCallbacks' => true
    ]);     
        $this->belongsTo('Tournaments', [
            'foreignKey' => 'tournament_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('game_date', ['valid'=> ['rule' => ['date'],'message'=>'invalid Date',]])
            ->requirePresence('game_date', 'create')
            ->notEmpty('game_date','Please fill game date')
            ->notEmpty('game_location','Please fill game Location')
            ->allowEmpty('game_type','Please fill game type')
            ->requirePresence('my_team_name', 'create')
            ->notEmpty('my_team_name','Please fill team name')
            ->requirePresence('oponent', 'create')
            ->notEmpty('oponent','Please fill oponent team name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['tournament_id'], 'Tournaments'));
        return $rules;
    }
    
    
   function checkmydate($date) {
	if(preg_match("/-/",$date)){
		$tempDate = explode('-', $date);
		if (checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {//checkdate(month, day, year)
			return true;
		} else {
			return false;
		}
	}
	return false;
}
    
    
     /*
     * function return the AB for,per tournament
     * params is match_type tournament or league
      * option param is for identify all record or per tournament record or for a match
      * if $tournament_id value is zero than all record if not zero then for per tournament
      * or if $tournament_id is an date then record will be filter on the basis of date
     */
    public function fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        //$option if option is not blank then it have tournament_id
        // $condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
        $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }

         $result = $tournaments->find()
                         ->select(['at_bat'=>'sum(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();

         return $result;
               
         
       
       
    }
    
     /*
     * function return the AB for,per tournament
     * params is match_type tournament or league
      * option param is for identify all record or per tournament record or for a match
      * if $tournament_id value is zero than all record if not zero then for per tournament
      * or if $tournament_id is an date then record will be filter on the basis of date
     */
    public function fetch_AB_league($game_type,$year,$user_id,$tournament_id) {
        
        //$option if option is not blank then it have tournament_id
        // $condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
        $tournaments= TableRegistry::get('Tournaments');
        $game_type=2;
        
         
             
              if($tournament_id==0 || empty($tournament_id)) {
              
                    $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


                } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game

                    //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
                    $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];


                } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament

                     $tournament_date=$tournament_id;
                     $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
                }



                $result = $this->find()
                                ->select(['at_bat'=>'sum(us.at_bat)'])
                               ->Where($condition)
                               ->hydrate(false)
                               ->join([
                                   'table' => 'user_stats',
                                   'alias' => 'us',
                                   'type' => 'LEFT',
                                   'conditions' => 'us.user_games_id = UserGames.id',
                               ])->first();
                          
                //pr($result);die;
                return $result;
       
       
    }
    
       /*
     * function return the AB for,per tournament
     * params is match_type tournament or league
      * option param is for identify all record or per tournament record or for a match
      * if $tournament_id value is zero than all record if not zero then for per tournament
     */
    public function fetch_HIT($game_type,$year,$user_id,$tournament_id,$game_id) {
        
       $tournaments= TableRegistry::get('Tournaments');
       $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         $list = $tournaments->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->toArray();
         
         $hit=0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
       
       return $hit;
    } 
    
     /*
     * function return the AB for,per tournament
     * params is match_type tournament or league
      * option param is for identify all record or per tournament record or for a match
      * if $tournament_id value is zero than all record if not zero then for per tournament
     */
    public function fetch_HIT_league($game_type,$year,$user_id,$tournament_id) {
        
          $game_type=2;
          if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         
         $list = $this->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->toArray();
         
         $hit=0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
       
       return $hit;
    } 
    
    //fetch avg
   public function fetch_AVG($game_type,$year,$user_id,$tournament_id,$game_id) {
        
         $ab_result=$this->fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id);
         $at_bat=$ab_result['at_bat']; //print at bat
         
         $hits=$this->fetch_HIT($game_type,$year,$user_id,$tournament_id,$game_id);
         
         $avg=0;
         if($at_bat >0) {
             $avg=$hits/$at_bat;
             $avg=  number_format($avg,3);
         }
         
         return $avg;
        
    }
    
        //fetch avg
   public function fetch_AVG_league($game_type,$year,$user_id,$tournament_id) {
        
         $ab_result=$this->fetch_AB_league($game_type,$year,$user_id,$tournament_id);
         $at_bat=$ab_result['at_bat']; //print at bat
         
         $hits=$this->fetch_HIT_league($game_type,$year,$user_id,$tournament_id);
         
         $avg=0;
         if($at_bat >0) {
             $avg=$hits/$at_bat;
             $avg=  number_format($avg,3);
         }
         
         return $avg;
        
    }
    
    //fetch SLG
    public function fetch_SLG($game_type,$year,$user_id,$tournament_id,$game_id) {
        $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
        
         
         if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         $result = $tournaments->find()
                          ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->toArray();
         
           // pr($result);die;
           $one_b_arr=[];$two_b_arr=[];$three_b_arr=[];$home_run_arr=[];
           foreach ($result as $k=>$val) {
                $at_bat=$val['us']['at_bat_result'];
                if($at_bat=='1B') {
                    $one_b_arr[$k]=1;
                } else if($at_bat=='2B') {
                    $one_b_arr[$k]=2*1;
                } else if($at_bat=='3B') {
                    $one_b_arr[$k]=3*1;
                } else if($at_bat=='HR') {
                    $one_b_arr[$k]=4*1;
                }
          }
          
        //pr($one_b_arr);die;
          
         $ab_result=$this->fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id);
         $at_bat=$ab_result['at_bat']; //print at bat
         
         $slg=0;
         $total=array_sum($one_b_arr);
         if($at_bat>0) {
             
             $slg=$total/$at_bat;
             $slg=  number_format($slg,3);
             
         }
         
         return $slg; 
         
         
        
    }
    
    //fetch SLG
    public function fetch_SLG_league($game_type,$year,$user_id,$tournament_id) {
        
        
         
         //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
           if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         
         
         $result = $this->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->toArray();
           // pr($result);die;
           $one_b_arr=[];$two_b_arr=[];$three_b_arr=[];$home_run_arr=[];
           foreach ($result as $k=>$val) {
                $at_bat=$val['us']['at_bat_result'];
                if($at_bat=='1B') {
                    $one_b_arr[$k]=1;
                } else if($at_bat=='2B') {
                    $one_b_arr[$k]=2*1;
                } else if($at_bat=='3B') {
                    $one_b_arr[$k]=3*1;
                } else if($at_bat=='HR') {
                    $one_b_arr[$k]=4*1;
                }
          }
          
        //pr($one_b_arr);die;
          
         $ab_result=$this->fetch_AB_league($game_type,$year,$user_id,$tournament_id);
         $at_bat=$ab_result['at_bat']; //print at bat
         
         $slg=0;
         $total=array_sum($one_b_arr);
         if($at_bat>0) {
             
             $slg=$total/$at_bat;
             $slg=  number_format($slg,3);
             
         }
         
         return $slg; 
         
         
        
    }
    
    
    //fetch OBP
    public function fetch_OBP($game_type,$year,$user_id,$tournament_id,$game_id) {
        
         $ab_result=$this->fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id);
         $AB=$ab_result['at_bat']; //print at bat
         $HITS=$this->fetch_HIT($game_type,$year,$user_id,$tournament_id,$game_id); //fetch hits
         $BB=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"BB",$game_id); //fetch hits
         $HBP=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"HBP",$game_id); //fetch hits
         $SAC=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"SAC",$game_id); //fetch hits
         
         $result=0;
         if($AB) {
            $result=($HITS + $BB + $HBP) / ($AB + $BB + $HBP + $SAC);
            $result=  number_format($result,3);
         }
         return $result;
         
        
    }
    
    //fetch OBP
    public function fetch_OBP_league($game_type,$year,$user_id,$tournament_id) {
        
         $ab_result=$this->fetch_AB_league($game_type,$year,$user_id,$tournament_id);
         $AB=$ab_result['at_bat']; //print at bat
         $HITS=$this->fetch_HIT_league($game_type,$year,$user_id,$tournament_id); //fetch hits
         $BB=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"BB"); //fetch hits
         $HBP=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"HBP"); //fetch hits
         $SAC=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"SAC"); //fetch hits
         
         $result=0;
         if($AB) {
            $result=($HITS + $BB + $HBP) / ($AB + $BB + $HBP + $SAC);
            $result=  number_format($result,3);
         }
         return $result;
         
        
    }
    
      //fetch OBP
    public function fetch_OPS($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        $OBP=$this->fetch_OBP($game_type,$year,$user_id,$tournament_id,$game_id); //fetch obp
        $SLG=$this->fetch_SLG($game_type,$year,$user_id,$tournament_id,$game_id); //fetch slg
        
        
        
        $result=0;
       $result=$OBP + $SLG;
       
        $result=  number_format($result,3);
        return $result;
        
         
        
    }
    
     //fetch OBP
    public function fetch_OPS_league($game_type,$year,$user_id,$tournament_id) {
        
        $OBP=$this->fetch_OBP_league($game_type,$year,$user_id,$tournament_id); //fetch obp
        $SLG=$this->fetch_SLG_league($game_type,$year,$user_id,$tournament_id); //fetch slg
        
        
        
        $result=0;
       $result=$OBP + $SLG;
       
        $result=  number_format($result,3);
        return $result;
        
         
        
    }
    
    
    //fetch TB
    public function fetch_TB($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        
        $one_B=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"1B",$game_id); //fetch hits
        $two_B=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"2B",$game_id); //fetch hits
        $three_B=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"3B",$game_id); //fetch hits
        $four_B=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"HR",$game_id); //fetch hits
         
         
        $result=0;
        $result=(1*$one_B)+(2*$two_B)+(3*$three_B)+(4*$four_B);
        $result=  number_format($result,1);
        return $result;
         
         
        
    }
    
     //fetch TB
    public function fetch_TB_league($game_type,$year,$user_id,$tournament_id) {
        
        
        $one_B=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"1B"); //fetch hits
        $two_B=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"2B"); //fetch hits
        $three_B=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"3B"); //fetch hits
        $four_B=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"HR"); //fetch hits
         
         
        $result=0;
        $result=(1*$one_B)+(2*$two_B)+(3*$three_B)+(4*$four_B);
        $result=  number_format($result,1);
        return $result;
         
         
        
    }
    
     //fetch RC
    public function fetch_RC($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        
        $BB=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"BB",$game_id); //fetch hits
        
        $TB=$this->fetch_TB($game_type,$year,$user_id,$tournament_id,$game_id); //fetch TB
        
        $HIT=$this->fetch_HIT($game_type,$year,$user_id,$tournament_id,$game_id); //fetch hit
        
        $ab_result=$this->fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id);
        $AB=$ab_result['at_bat']; //print at bat
        
         
        // (H + BB) x TB / (AB + BB)
        
        $result=0;
        if(($AB+$BB)) {
            $result=(($HIT+$BB)*$TB)/($AB+$BB);
            $result=  number_format($result,1);
        }
        return $result;
         
         
        
    }
    
     //fetch RC
    public function fetch_RC_league($game_type,$year,$user_id,$tournament_id) {
        
        
        $BB=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"BB"); //fetch hits
        
        $TB=$this->fetch_TB_league($game_type,$year,$user_id,$tournament_id); //fetch TB
        
        $HIT=$this->fetch_HIT_league($game_type,$year,$user_id,$tournament_id); //fetch hit
        
        $ab_result=$this->fetch_AB_league($game_type,$year,$user_id,$tournament_id);
        $AB=$ab_result['at_bat']; //print at bat
        
         
        // (H + BB) x TB / (AB + BB)
        
        $result=0;
        if(($AB+$BB)) {
            $result=(($HIT+$BB)*$TB)/($AB+$BB);
            $result=  number_format($result,1);
        }
        return $result;
         
         
        
    }
    
     //fetch PA
    /*
     * Equals the SUM of every AT-BAT 
     */
    public function fetch_PA($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
        
       if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }

         $result = $tournaments->find()
                         ->select(['at_bat'=>'count(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();
        
        $AB=$result['at_bat']; //print at bat
        
       
        $result=  $AB;   //number_format($AB,2);
        return $result;
        
    }
    
     //fetch PA
    /*
     * Equals the SUM of every AT-BAT 
     */
    public function fetch_PA_league($game_type,$year,$user_id,$tournament_id) {
        
        
         if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
              
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         
         
         $result = $this->find()
                         ->select(['at_bat'=>'count(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->first();
         
        
        
        $AB=$result['at_bat']; //print at bat
        
       
        $result=  $AB; //number_format($AB,2);
        return $result;
        
    }
    
    
     //fetch PA
    /*Defination: Put in Play  
     *formula: 1- (SO / AB) 
     */
    public function fetch_PIP($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        $ab_result=$this->fetch_AB($game_type,$year,$user_id,$tournament_id,$game_id);
        $AB=$ab_result['at_bat']; //print at bat
        
        $SO=$this->fetch_stats_type($game_type,$year,$user_id,$tournament_id,"SO",$game_id); //fetch SO
        
        
        $result=0;
        if($AB) {
            $result=1- ($SO / $AB);
            $result=  number_format($result,3);
        }
        return $result;
        
    }
    
    //fetch PA
    /*Defination: Put in Play  
     *formula: 1- (SO / AB) 
     */
    public function fetch_PIP_league($game_type,$year,$user_id,$tournament_id) {
        
        $ab_result=$this->fetch_AB_league($game_type,$year,$user_id,$tournament_id);
        $AB=$ab_result['at_bat']; //print at bat
        
        $SO=$this->fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,"SO"); //fetch SO
        
        
        $result=0;
        if($AB) {
            $result=1- ($SO / $AB);
            $result=  number_format($result,3);
        }
        return $result;
        
    }
    
    /*
     * function return the Hit 
     * params is $game_type tournament or league,
     * another param is pitcher if pitcher is righty then we pass 1 if pitcher is lefty then we pass 2 
     */
    public function fetch_HIT_for_pitcher($game_type,$year,$user_id,$tournament_id,$pitcher,$game_id) {
         /**/
          $tournaments= TableRegistry::get('Tournaments');
         $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'us.pitcher'=>$pitcher,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
         }

         $list = $tournaments->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->toArray();
         /**/
         
         $hit=0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
       
       return $hit;
    }
    
    
    
    /*
     * function return the Hit 
     * params is $game_type tournament or league,
     * another param is pitcher if pitcher is righty then we pass 1 if pitcher is lefty then we pass 2 
     */
    public function fetch_HIT_for_pitcher_league($game_type,$year,$user_id,$tournament_id,$pitcher) {
        
        //$option if option is not blank then it have tournament_id
         
         //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
          if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'us.pitcher'=>$pitcher,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
         }
         
         
         $list = $this->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->toArray();
         
         $hit=0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
       
       return $hit;
    }
    
    /*
     * formula:HITS / AB (only for right handed pitchers)
     * DEf: Batting Average versus Righty Pitcher
     * 
     */
    public function fetch_AVG_VS_RHP($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        //passing 1 for righty
        $AB=$this->fetch_AB_for_pitcher($game_type,$year,$user_id,$tournament_id,1,$game_id);
        $HIT=$hits=$this->fetch_HIT_for_pitcher($game_type,$year,$user_id,$tournament_id,1,$game_id);
        
        $result=0;
        if ($AB > 0) {
          $result=($HIT / $AB);
          $result=  number_format($result,3);
        }
        return $result;
        
    }
    
    /*
     * formula:HITS / AB (only for right handed pitchers)
     * DEf: Batting Average versus Righty Pitcher
     * 
     */
    public function fetch_AVG_VS_RHP_league($game_type,$year,$user_id,$tournament_id) {
        
        //passing 1 for righty
        $AB=$this->fetch_AB_for_pitcher_league($game_type,$year,$user_id,$tournament_id,1);
        $HIT=$hits=$this->fetch_HIT_for_pitcher_league($game_type,$year,$user_id,$tournament_id,1);
        
        $result=0;
        if ($AB > 0) {
          $result=($HIT / $AB);
          $result=  number_format($result,3);
        }
        return $result;
        
    }
    
    
      /*
     * formula:HITS / AB (only for left handed pitchers)
     * DEf: Batting Average versus lefty Pitcher
     * 
     */
    public function fetch_AVG_VS_LHP($game_type,$year,$user_id,$tournament_id,$game_id) {
        
        //passing 2 for lefty
        $AB=$this->fetch_AB_for_pitcher($game_type,$year,$user_id,$tournament_id,2,$game_id);
        
        $HIT=$hits=$this->fetch_HIT_for_pitcher($game_type,$year,$user_id,$tournament_id,2,$game_id);
        
        
       $result=0;
        if (($AB > 0)) {
          $result=($HIT / $AB);
          $result=  number_format($result,3);
        }
        return $result;
        
    }
    
    
       /*
     * formula:HITS / AB (only for left handed pitchers)
     * DEf: Batting Average versus lefty Pitcher
     * 
     */
    public function fetch_AVG_VS_LHP_league($game_type,$year,$user_id,$tournament_id) {
        
        //passing 2 for lefty
        $AB=$this->fetch_AB_for_pitcher_league($game_type,$year,$user_id,$tournament_id,2);
        
       $HIT=$hits=$this->fetch_HIT_for_pitcher_league($game_type,$year,$user_id,$tournament_id,2);
        
        
       $result=0;
        if (($AB > 0)) {
          $result=($HIT / $AB);
          $result=  number_format($result,2);
        }
        return $result;
        
    }
    //retrun custom value
    public function fetch_QAB($game_type,$year,$user_id,$tournament_id,$game_id) {
        
       $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }

         $result = $tournaments->find()
                         ->select(['qab'=>'sum(us.qab)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();
       
         
        $qab=$result['qab'];
        return $qab;
        
    }
    
    
    //retrun custom value
    public function fetch_QAB_league($game_type,$year,$user_id,$tournament_id) {
        
        if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
           
         $result = $this->find()
                         ->select(['qab'=>'sum(us.qab)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->first();
         
        $qab=$result['qab'];
        return $qab;
        
    }
    
    
    public function fetch_AVG_W_ROB($game_type,$year,$user_id,$tournament_id,$game_id) {
        
       
        $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }

         $result = $tournaments->find()
                         ->select(['at_bat'=>'sum(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();
         $AB=$result['at_bat'];

          $list = $tournaments->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->toArray();
         $hit=0;
         $rob_result=0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
           
       
        if (($AB > 0)) {
          $rob_result=($hit / $AB);
          $rob_result=  number_format($rob_result,3);
        }
            
        return $rob_result;
        
    }
    
    
     public function fetch_AVG_W_ROB_league($game_type,$year,$user_id,$tournament_id) {
        
       
         if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
           
         $result = $this->find()
                         ->select(['at_bat'=>'sum(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->first();
                        
         $AB=$result['at_bat'];

         $list = $this->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->toArray();
         

         $hit=0;
         $rob_result =0;
         foreach ($list as $val) {
          $at_bat=$val['us']['at_bat_result'];
          if($at_bat=='1B' || $at_bat=='2B' || $at_bat=='3B' || $at_bat=='HR') {
              $hit=$hit+1;
              
          }
        }
        
        if (($AB > 0)) {
          $rob_result=($hit / $AB);
          $rob_result=  number_format($rob_result,3);
        }
            return $rob_result;
        
    }
    
    //fetch AB for pitcher
    /*
     * Param piccher is for left or right pitcher
     * 1: Righty
     * 2: lefty
     * 
     */
    public function fetch_AB_for_pitcher($game_type,$year,$user_id,$tournament_id,$pitcher,$game_id) {
        
       
         
         
         
         
         $tournaments= TableRegistry::get('Tournaments');
        $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'us.pitcher'=>$pitcher,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
         }

         $result = $tournaments->find()
                         ->select(['at_bat'=>'sum(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();
         $AB=$result['at_bat']; 
        return $AB;
       
    }
    
    //fetch AB for pitcher
    /*
     * Param piccher is for left or right pitcher
     * 1: Righty
     * 2: lefty
     * 
     */
    public function fetch_AB_for_pitcher_league($game_type,$year,$user_id,$tournament_id,$pitcher) {
        
        //$option if option is not blank then it have tournament_id
        // $condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id,'us.pitcher'=>$pitcher];
        
         if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'us.pitcher'=>$pitcher,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'us.pitcher'=>$pitcher];
         }
         
         $result = $this->find()
                         ->select(['at_bat'=>'sum(us.at_bat)'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->first();
         $AB=$result['at_bat']; 
        return $AB;
       
    }
    
    
    //fetch stats like BB, HBP, SC,OUT and etc
    /*
    *params type of stats
     * return stats count      
    */
    public function fetch_stats_type($game_type,$year,$user_id,$tournament_id,$stats_type,$game_id) {
        //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
        
         $tournaments= TableRegistry::get('Tournaments');
         $game_type=1;
        
        //fetch on year basis
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }

         $result = $tournaments->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->toArray();
         
 
       
           $temp=[];
           foreach ($result as $k=>$val) {
                $at_bat=$val['us']['at_bat_result'];
                if($at_bat==$stats_type) {
                    $temp[$k]=1;
                } 
          }
          
          $stats=array_sum($temp);
          //$stats=number_format($stats,2);
          return $stats;
        
    }
    
     //fetch stats like BB, HBP, SC,OUT and etc
    /*
    *params type of stats
     * return stats count      
    */
    public function fetch_stats_type_league($game_type,$year,$user_id,$tournament_id,$stats_type) {
        //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
          $game_type=2;
          if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_type];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         
        
         $result = $this->find()
                         ->select(['us.at_bat_result'])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->toArray();
       
           $temp=[];
           foreach ($result as $k=>$val) {
                $at_bat=$val['us']['at_bat_result'];
                if($at_bat==$stats_type) {
                    $temp[$k]=1;
                } 
          }
          
          $stats=array_sum($temp);
          //$stats=number_format($stats,2);
          return $stats;
        
    }
    
    
    //fetch stats like RBI
    /*
    *params type of stats like if we want sum of sum column like RBI,SB and etc
     * return stats sum      
     * pass column name in the param $stats_type
    */
    public function fetch_sum_of_stats($game_type,$year,$user_id,$tournament_id,$stats_type,$game_id) {
        //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
        
         $tournaments= TableRegistry::get('Tournaments');
         $game_type=1;
             
        if($tournament_id==0 || empty($tournament_id)) {

             $condition=['YEAR(UserGames.game_date)' => $year,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id>0)) {   //condition for to check per game
             


             $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.id'=>$game_id,'UserGames.game_type'=>$game_type];


         } else if(($tournament_id >0) && ($game_id==0)) {  ////condition for to check per tournament

              $tournament_date=$tournament_id;
              $condition=['Tournaments.id'=>$tournament_id,'Tournaments.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
        $result = $tournaments->find()
                         ->select(['total_val'=>"sum($stats_type)"])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                             'UserGames' => [
                                 'table' => 'user_games',
                                 'type' => 'LEFT',
                                 'conditions' => 'UserGames.tournament_id = Tournaments.id',
                             ],
                             'us' => [
                                 'table' => 'user_stats',
                                 'type' => 'LEFT',
                                 'conditions' => 'us.user_games_id = UserGames.id',
                             ]
                         ])->first();
         
        
        
           
           $value=$result['total_val'];
        
          return $value;
        
    }
    
    //fetch stats like RBI
    /*
    *params type of stats like if we want sum of sum column like RBI,SB and etc
     * return stats sum      
     * pass column name in the param $stats_type
    */
    public function fetch_sum_of_stats_league($game_type,$year,$user_id,$tournament_id,$stats_type) {
        //$condition=($tournament_id==0 || empty($tournament_id)) ? ['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type]:['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
        
          if($tournament_id==0 || empty($tournament_id)) {
              
             $condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
             
             
         } else if($tournament_id >0 && (!$this->checkmydate($tournament_id))) {   //condition for to check per game
               
             //$condition=['YEAR(UserGames.game_date)' => $year,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type,'UserGames.id'=>$tournament_id];
             $condition=['UserGames.user_id'=>$user_id,'UserGames.id'=>$tournament_id,'UserGames.game_type'=>$game_types];

         
         } else if($this->checkmydate($tournament_id)) {  ////condition for to check per tournament
             
              $tournament_date=$tournament_id;
              $condition=['date(UserGames.game_date)' => $tournament_date,'UserGames.user_id'=>$user_id,'UserGames.game_type'=>$game_type];
         }
         
         
        
         $result = $this->find()
                         ->select(['total_val'=>"sum($stats_type)"])
                        ->Where($condition)
                        ->hydrate(false)
                        ->join([
                            'table' => 'user_stats',
                            'alias' => 'us',
                            'type' => 'LEFT',
                            'conditions' => 'us.user_games_id = UserGames.id',
                        ])->first();
       
           
           
           $value=$result['total_val'];
        
          return $value;
        
    }
    
    
    
    
}
