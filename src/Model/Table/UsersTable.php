<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Network\Email\Email;
use Cake\Routing\Router;
/**
 * Users Model
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasOne('Userprofiles', [
            'className' => 'Userprofiles'

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        
        $validator
            
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->notEmpty('first_name', 'Please fill first name')
             ->notEmpty('last_name', 'Please fill last name')
             ->notEmpty('password', 'Please fill password')
             ->add('password', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'password need to be at least 6 characters long',
                    ]
                ])
              ->add('cpassword', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'confirm password need to be at least 6 characters long',
                    ]
                ])
             ->notEmpty('cpassword', 'Kindly fill confirm password')
             ->notEmpty('age', 'Kindly fill age')
             ->add('age', [
                    'length' => [
                        'rule' => ['maxLength', 3],
                        'message' => 'this age is not acceptable ',
                    ]
                ])
             ->add('age', [
                    'numeric' => [
                        'rule' => ['numeric'],
                        'message' => 'this age is not age ',
                    ]
                ])
             /*->add('age', [
                    'compare' => [
                        'rule' => ['matchAge'],
                        'provider' => 'custom',
                        'message' => 'this age is not match with date of birth ',
                    ]
                ])*/
             ->notEmpty('sex', 'Kindly fill Gender')
             ->notEmpty('dob', 'Kindly fill Date Of Birth')
             ->add('dob', [
                    'valid' => [
                        'rule' => ['date'],
                        'message' => 'this Date Of Birth is invalid ',
                    ]
                ])
             ->notEmpty('address', 'Kindly fill address')
             ->notEmpty('city', 'Kindly fill city')
             ->notEmpty('state', 'Kindly fill state')
             ->notEmpty('zip', 'Kindly fill zip')
             ->add('zip', [
                    'valid' => [
                        'rule' => ['numeric'],
                        'message' => 'Zip is invalid ',
                    ]
                ])
             ->notEmpty('plan_id','Kindly select Plan for your Profile')
             ->add('email', [
                'unique' => [
                    'rule' => ['validateUnique'],
                    'provider' => 'table',
                    'message' => 'Email must be unique'
                ]])
             ->add('email', [
                'valid' => [
                    'rule' => ['email'],
                    'message' => 'Email is invalid'
                ]])
             ->notEmpty('email', 'Kindly Provide Email')
              ->add('cpassword',
                    'compareWith', [
                        'rule' => ['compareWith', 'password'],
                        'message' => 'Passwords not equal.'
                    ]
                )
             ->add('profile_image', 'file', [
                    'rule' => ['extension',['jpeg','jpg','png','gif']],
                    'message' => 'Image must be jpeg/jpg/png/gif Format.'
                    ])
             ->allowEmpty('profile_image','Kindly Provide profile image');
             
             return $validator;

    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    /*
     * return unique user name
     * 
     * */
    public function getUniqueUsernme($firstName,$obj) {
        $userName = $firstName . "_" . rand(101, 999);
        $confirmUsername = $obj->find()->where(['username' => $userName])->count();
        if (!$confirmUsername) {
            return $userName;
        } else {
            $this->getUniqueUsernme($firstName,$obj);

        }
    }
    
    /**
     * Used to send registration mail to user
     *
     * @access public
     * @param array $user user detail array
     * @return void
     */ public function sendRegistrationMail($user,$link) {
        
        
        $body="Hi, You are successfully registered with us you email and password are  \n\n Email: ".$user->email."\n\n"."Password:".$user->password. "\n\n Click the link below to complete your registration \n\n ".$link;
    
        $email = new Email('default');
        
        $email->from(['admin@admin.com' => 'Mail From ScoutPost'])
        ->to($user->email)
        ->subject('Scoutpost SignUp')
        ->send($body);

    }
     
     /**
     * Used to send forgot password mail to user
     *
     * @access public
     * @param array $user user detail
     * @return void
     */
    public function forgotPasswordEmailTemplate($user) {
        
        $userId=$user -> id;
        $activate_key =$user->activation_key;
        $link = Router::url("/categories/resetPassword?ident=$userId&activate=$activate_key",true);
        $body= "Hi ".$user -> first_name.", \n\n Please click the link below to reset your password now \n\n".$link."
        \nIf above link does not work please copy and paste the URL link (above) into your browser address bar to get to the Page to reset password.";
        $email = new Email('default');
        $email->from([EMAIL_FROM_ADDRESS => 'Mail From ScoutPost'])
        ->to($user->email)
        ->subject('Scoutpost Request to Reset Password')
        ->send($body);
        
    }

      public function matchAge(){

             $birthDate = $this->data[$this->alias]['dob'];
  $birthDate = explode("/", $birthDate);
  $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")? ((date("Y") - $birthDate[2]) - 1): (date("Y") - $birthDate[2]));
     
     return $birthDate ===  $this->data[$this->alias]['age'];

      }

    }
