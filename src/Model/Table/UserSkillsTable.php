<?php
namespace App\Model\Table;

use App\Model\Entity\UserSkill;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserSkills Model
 */
class UserSkillsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('user_skills');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('workout_date', ['valid'=> ['rule' => ['date'],'message'=>'invalid date']])
            ->requirePresence('workout_date', 'create')
            ->notEmpty('workout_date','Please fill date')
            ->notEmpty('workout_time','Please fill time')
            ->notEmpty('description','Please fill description')
			->notEmpty('flag','Please fill flag')
            ->add('skills_img', 'file', [
                    'rule' => ['extension',['jpeg','jpg','png','gif']],
                    'message' => 'Image must be jpeg/jpg/png/gif Format.'
                    ])
            
            ->allowEmpty('skills_img','Kindly Provide valid image')
            ->allowEmpty('file','Kindly Provide valid video');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
    
    
    public function fetch_data_skills($skill_type,$user_id,$description,$day_name) {
        
        $str=date("Y").(date("W")-1);
         $result = $this->find()
        ->select(['time_in_minutes'=>'sum(workout_time)','description','day_name'=>'DAYNAME(workout_date)'])
       ->Where(['DAYNAME(workout_date)' => $day_name,'YEARWEEK(`workout_date`)'=>trim($str),'flag'=>$skill_type,'user_id'=>$user_id,'description'=>$description])
       ->first()->toArray();
         
         return $result;
         
    }
    
     public function fetch_data_skills_month($skill_type,$user_id,$description,$week_number) {
        
         $str=$week_number;
         $result = $this->find()
        ->select(['time_in_minutes'=>'sum(workout_time)/60','description','day_name'=>'DAYNAME(workout_date)'])
       ->Where(['YEARWEEK(`workout_date`)'=>trim($str),'flag'=>$skill_type,'user_id'=>$user_id,'description'=>$description])
       ->first()->toArray();
         
         return $result;
         
    }
    
    
    public function fetch_data_skills_year($skill_type,$user_id,$description,$month_name) {
        
        /*SELECT 
                sum(workout_time)/60 AS `time_in_hours`,
                UserSkills.description AS `UserSkills__description`, 
                MONTHNAME(workout_date) AS `day_name`
        FROM user_skills UserSkills 
        WHERE (monthName(`workout_date`) = 'May' 
        AND year(`workout_date`) = '2015' AND flag = '1' 
        AND user_id = 60 AND description = 'agility_training') */

        
        $str=date("Y").(date("W")-1);
         $result = $this->find()
        ->select(['time_in_minutes'=>'sum(workout_time)/60','description','day_name'=>'MONTHNAME(workout_date)'])
       ->Where(['MONTHNAME(workout_date)' => $month_name,'YEAR(`workout_date`)'=>date('Y'),'flag'=>$skill_type,'user_id'=>$user_id,'description'=>$description])
       ->first()->toArray();
         
         return $result;
         
    }
    
  //like ($year==1, $month=5, $start_day_of_week=1 )
  public function weeks_in_month($year, $month, $start_day_of_week)
  {
    // Total number of days in the given month.
    $num_of_days = date("t", mktime(0,0,0,$month,1,$year));
 
    // Count the number of times it hits $start_day_of_week.
    $num_of_weeks = 0;
    for($i=1; $i<=$num_of_days; $i++)
    {
      $day_of_week = date('w', mktime(0,0,0,$month,$i,$year));
      if($day_of_week==$start_day_of_week)
        $num_of_weeks++;
    }
 
    return $num_of_weeks;
  }
    
    
    
    public static function time($check) {
    return self::_check($check, '%^((0?[1-9]|1[012])(:[0-5]\d){0,2} ?([AP]M|[ap]m))$|^([01]\d|2[0-3])(:[0-5]\d){0,2}$%');
      }
}
