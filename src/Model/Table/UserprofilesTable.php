<?php
namespace App\Model\Table;

use App\Model\Entity\Userprofile;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userprofiles Model
 */
class UserprofilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userprofiles');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('sport', 'create')
            ->notEmpty('sport','Kindly select Sport')
            ->requirePresence('bats_from', 'create')
            ->notEmpty('bats_from','Kindly Choose bats From Field')
            ->requirePresence('through_with', 'create')
            ->notEmpty('through_with','Kindly Choose through With Field')
            ->requirePresence('curr_school', 'create')
            ->notEmpty('curr_school','Kindly Fill current school')
            ->requirePresence('schooling', 'create')
            ->allowEmpty('schooling','Kindly Fill high school year')
            ->add('schooling', 'numeric', ['rule' => 'numeric','message'=>'high school year Must be numeric']);
             

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
