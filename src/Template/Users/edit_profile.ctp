<?= $this->Html->script(['custom/datepicker','custom/adminusersignup'],['inline'=>false])?>
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>

  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>
  </li>
      <li>
        <a href="<?= $this->Url->build('/users/viewUser', true); ?>"><i class="fa fa-user"></i> Users</a>
      </li>
  <li class="active">
    <strong>Edit</strong>
  </li>
</ol>

<h2>Edit User</h2>

<div class="clear"></div>
<div><?= $this->Flash->render() ?></div>
<div class="row">
  <div class="col-md-12">

    
<?php echo $this->Form->create($user,['enctype' => 'multipart/form-data','novalidate'])?>
    

        <div data-collapsed="0" class="panel panel-primary">

          <div class="panel-heading">
            <div class="panel-title">
              <i class="fa fa-user"></i>
              <a data-rel="collapse" href="#">Personal Information</a>
            </div>

            <div class="panel-options">
              <a data-rel="collapse" href="#"><i class="entypo-down-open"></i></a>
            </div>
          </div>
          <div class="panel-body">

            <div class="form-group">
              <label class="col-sm-4 control-label">First Name<span style='color:red'>*</span></label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('first_name',['class'=>'form-control','label'=>false]);?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Last Name<span style='color:red'>*</span></label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('last_name',['class'=>'form-control','label'=>false]);?>
              </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Age<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->input('age',['class'=>'form-control','placeholder'=>'Enter Age','label'=>false])?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Gender<span style='color:red'>*</span></label>
                <div class="col-sm-4 gender-label">
                <?php 
                $options = array('M' => ' Male','F' => ' Female');

                $attributes = array('legend' => false);

                echo $this->Form->radio('sex', $options, $attributes); ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">BirthDate<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->input('dob',['class'=>'form-control','placeholder'=>'Birth-Date','label'=>false,'id'=>'date-picker-1','value'=>$user['dob']])?>
                </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Address</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('address',['class'=>'form-control','label'=>false,'type'=>'textarea','value'=>$user['address']]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">City</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('city',['class'=>'form-control','label'=>false,'value'=>$user['city']]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">State</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('state',['class'=>'form-control','label'=>false,'value'=>$user['state']]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Zip</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('zip',['class'=>'form-control','label'=>false,'value'=>$user['zip']]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Email Address</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('email',['class'=>'form-control','label'=>false,'value'=>$user['email']]);?>
              </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Create Password<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->input('password',['class'=>'form-control','placeholder'=>'Create Password','label'=>false])?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Re Enter Password<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->input('cpassword',['type'=>'password','class'=>'form-control','placeholder'=>'Re enter Password','label'=>false])?>
                </div>
            </div>
        
        <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Choose Plan <span style='color:red'>*</span></label>
                <div class="col-sm-4">
                  <?= $this->Form->select('plan_id',['F'=>'FREE','P'=>'PREMIUM'],['class'=>'form-control','empty' => '(choose one)'])?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Sport<span style='color:red'>*</span></label>
                <div class="col-sm-4">

                <?= $this->Form->select('Userprofiles.sport',['1'=>'Baseball','2'=>'Softball'],['class'=>'form-control','empty' => '(choose one)','value'=>$userprofiles['sport']]);?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Fielding Position(s)</label>
                <div class="col-sm-4 fielding-position">
                
                <div>
                  <?=$this->Form->checkbox('Userprofiles.pitcher', ['hiddenField' => false,$userprofiles['pitcher']==1 ? 'checked' : '']);?> Pitcher
                </div>
                <div>
                  <?=$this->Form->checkbox('Userprofiles.outfilder', ['hiddenField' => false,$userprofiles['outfilder']==1 ? 'checked' : '']);?> Outfielder  
                </div>
                <div>
                  <?=$this->Form->checkbox('Userprofiles.infilder', ['hiddenField' => false,$userprofiles['infilder']==1 ? 'checked' : '']);?> Infielder 
                </div>
                <div>                
                  <?=$this->Form->checkbox('Userprofiles.catcher', ['hiddenField' => false,$userprofiles['catcher']==1 ? 'checked' : '']);?> Catcher 
                </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Bats from<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->select('Userprofiles.bats_from',['1'=>'Right','2'=>'Left','3'=>'Both'],['class'=>'form-control','empty' => '(choose one)','value'=>$userprofiles['bats_from']]);?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Throws with<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->select('Userprofiles.through_with',['1'=>'Right Hand','2'=>'Left Hand'],['class'=>'form-control','empty' => '(choose one)','value'=>$userprofiles['through_with']]);?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Current Teams <a class="profile-help"><i class="fa fa-question-circle"></i></a>
            <div role="tooltip" class="popover popovershow fade bottom in" id="popover640679">
              <div class="arrow" ></div>
              <h2 class="popover-title" style="display: block;">? Help Content</h2>
              <div class="popover-content">Team Name:  You can track stats for multiple  teams 
              that you are playing for.  
              Division Major, Minor, AAA, AA, 
              Club, All-Star, Senior, Show Case, Freshman, 
              JV, Varsity, Bronco, Mustang, Pinto etc.
              League Name:  Little League, Travel, 
              Name of Club, Pony, Babe Ruth, City, 
              Middle School, High School etc..</div>
            </div></label>
                <div class="col-sm-4" id="selectDiv">
                <?php
                $divAdd=["Major"=>"Major","Minor"=>"Minor","AAA"=>"AAA","AA"=>"AA","All-Star"=>"All-Star","Senior"=>"Senior","Show-Case"=>"Show Case","Freshman"=>"Freshman","JV"=>"JV","Varsity"=>"Varsity","Bronco"=>"Bronco","Mustang"=>"Mustang","Pinto"=>"Pinto"];
                ?>

                <?php foreach($teamdivision as $single){ ?>
                <?= $this->Form->input('teamdivision.current_team.0',['type'=>'text','class'=>'form-control siming-spc curTeam0','rows'=>'2','placeholder'=>'Current Teams','label'=>false,'onkeyup'=>'autocomplet(0);','value'=>$single['current_team']])?>
                
                
                <?= $this->Form->select('teamdivision.division.0',$divAdd,['class'=>'form-control siming-spc','empty' => '(select Division)','id'=>'divisions','value'=>$single['division']]);?>
                <?php } ?>
                <ul class="list listitem0"></ul>
                </div>

                <?= $this->Form->button('+add more teams',['type'=>'button','class'=>'btn','id'=>'addivisions']);?>
                
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Current School Attending<span style='color:red'>*</span></label>
                <div class="col-sm-4">
                <?= $this->Form->input('Userprofiles.curr_school',['class'=>'form-control','placeholder'=>'Current School Attending','label'=>false,'value'=>$userprofiles['curr_school']])?>

                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Starting High School Year</label>
                <div class="col-sm-4">

                <?php
                $yearArr = ['Or Enter Year']; 
                for($i=2016; $i<2031; $i++){
                  $yearArr[$i]  =$i; 
                }
                  
                ?>
                <span id='passingYearbox' style="display:none;"><?= $this->Form->input('Userprofiles.schooling',['class'=>'form-control','label'=>false,'placeholder'=>'Year of High School Graduation'])?></span> 
                <?= $this->Form->select('Userprofiles.schooling',$yearArr,['class'=>'form-control','empty' => '(High School Year)','id'=>'passingYear','value'=>$userprofiles['schooling']]);?>
                 
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Favorite Sports Team</label>
                <div class="col-sm-4">
                <?= $this->Form->input('Userprofiles.feb_team',['class'=>'form-control','placeholder'=>'Favorite Team','label'=>false,'value'=>$userprofiles['feb_team']])?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Favorite Athlete</label>
                <div class="col-sm-4">
                <?= $this->Form->input('Userprofiles.feb_athlete',['class'=>'form-control','placeholder'=>'Favorite Athlete','label'=>false,'value'=>$userprofiles['feb_athlete']])?>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Profile Photo</label>
            <div class="col-sm-5">
                <?php echo $this->Form->input('profile_image',['type'=>'file','class'=>'form-control','label'=>false]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
              
                <?= $this->Html->image($this->request->Webroot.'/img/profile_pic/'.$user['profile_image'],['width'=>'100','height'=>'100','alt'=>$user['profile_image']]);?>
                <?php echo $this->Form->input('profile_image_old',['type'=>'hidden','class'=>'form-control','label'=>false,'value'=>$user['profile_image']]);?>
              </div>
            </div>
           <div class="form-group">
              <div class="col-sm-5 col-sm-offset-4">
              <div class="alert alert-danger"><?php if(isset($error_list)){foreach($error_list as $error_list){?>
            <div class="error-message"><?=$error_list;?></div><?php }}?></div></div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-5 col-sm-offset-4">
              <div class="alert alert-danger"><?php if(isset($error_listz)){foreach($error_listz as $errores){?>
            <div class="error-message"><?=$errores;?></div><?php }}?></div></div>
            </div>
            <div class="form-group">         
              <div class="col-sm-4"></div>
              <div class="col-sm-5">
                <?= $this->Form->submit('Update',['class'=>'btn btn-primary','label'=>false]);?>
              </div>
            </div>
                          
  
      </div>

    </div>


