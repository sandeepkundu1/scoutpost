
<div class="page-container">
  <div class="sidebar-menu" style="min-height: 712px;">
        <?= $this->element('admin_sidebar');?>
  </div>
  <div class="main-content" style="min-height: 712px;">
    <div class="inner_cont">
  <ol class="breadcrumb bc-3 hidden-xs">
      <li>
        <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>
      </li>
      <li class="active">
          <i class="fa fa-user"></i>
          <strong>School Galery</strong>
      </li>
  </ol>

  <h2 style="margin-top: 0" class="pull-left increase-btn">School Gallery <span class="pull-right"><?php echo  $this->Html->image('add.png', ['alt' => 'Add More','title'=>'add More','url' => ['controller' => 'Users', 'action' => 'addSchool'],'class'=>'btn btn-primary pull-right']); ?></span></h2>

    

<!--h4 class="pull-right">Add New Features</h4-->
  <div style="clear: both;"></div>

    <table class="table table-responsive table-hover table-bordered">
      <thead>
      <tr>

      <th style="width: 1px;">S.no</th>
    <th class="hidden-xs">Images</th>
    <th>Galley Description</th>
    <th style="width: 100px;">Action</th>
      </tr>
      </thead>
    
<?php $i = 1; ?>
  <tbody>
  <?php foreach ($school as $key => $schoolDetail) { ?>
<tr>
<td><?= $i ?></td> 
<td><?=$this->Html->image('school_pic/'.$schoolDetail['photo'],['alt'=>'image_','height'=>'150','width'=>'150'])?></td>
<td><?= $schoolDetail['description']?></td>
<td>

<?php echo $this->Html->image('edit.png', ['alt' => 'Edit','title'=>'Edit','url' => ['controller' => 'Users', 'action' => 'editSchool', $schoolDetail['id']]]); ?>
<?php echo $this->Html->image('delete.png', ['alt' => 'Delete','title'=>'Delete','url' => ['controller' => 'Users', 'action' => 'deleteSchool', $schoolDetail['id']]]); ?>
</td>
</tr>
<?php $i ++; ?>
<?php  } ?>

</tbody>
</table>
</div>
<?php if($this->Paginator->param('count') > @$limit ){ ?>
                    <div id="paginationDivId" class="pull-right">
                        <ul class=" pagination pull-right">
                         <?php 
                            echo $this->Paginator->prev( 'Prev', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled insert-anchor', 'tag' => 'a' ) );
                            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled   insert-anchor', 'tag' => 'a' ) );
                        ?>
                        </ul>
                    </div>
                  <?php } 
                  ?> 

      

    </div>
</div>
