
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">

<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>

  </li>
  <li class="active">
    <i class="fa fa-user"></i>
    <strong>Usermanagement</strong>
  </li>
</ol>

<h2 style="margin-top: 0" class="pull-left">Users </h2>
<?php echo $this->Form->create('users',array('url'=>array('controller'=>'users','action'=>'redirect_url')))?>
    <div class="row filter-form">
		<div class="form-inline col-sm-6">		
		<?php echo $this->Form->input('email',array('type'=>'text','class'=>'form-control','label'=>false,'div'=>false,'placeholder'=>'Search by name or  email','value'=>@$this->request->params['named']['query']))?>
		</div>
		<div class="form-inline col-sm-6">
	<?php echo $this->Form->submit('Filter',array('type'=>'submit','class'=>'btn btn-primary pull-left','label'=>false,'div'=>false));?>
		  </div>
	  </div>
      <?= $this->Form->end(); ?>
    
<div style="clear: both;"></div>

     <div><?= $this->Flash->render() ?></div>  
    <table class="table table-responsive table-hover table-bordered">
      <thead>
      <tr>

      <!--th style="width: 1px;">SL.</th-->
      <th><?= $this->Paginator->sort('id','Sr No.') ?></th>
      <th class="hidden-xs"><?= $this->Paginator->sort('first_name','Name') ?></th>
      <th style="width: 1px;"><?= $this->Paginator->sort('username') ?></th>
      <th class="hidden-xs"><?= $this->Paginator->sort('email','Email') ?></th>
    <!--th class="hidden-xs">Name</th>
    <th style="width: 1px;">Username</th>
    <th class="hidden-xs">Email</th-->
    <th class="hidden-xs">Memory Uses</th>
    <th class="hidden-xs">Email Verified</th>
    <th style="width: 1px;">Status</th>
    <th>Action</th>
      </tr>
      </thead>
    
<?php $i = 1; ?>
  <tbody>
  <?php foreach ($users as $key => $users) { ?>
<tr>
<td><?= $users['id'] ?></td> 
<td><?= $users['first_name']." ".$users['last_name']?></td>
<td><?= $users['username']?></td>
<td><?= $users['email']?></td>
<td><?= $this->Html->link('Checksize',['controller'=>'users','action'=>'checkSize',$users['id']],['class'=>'btn btn-success'])?></td>
<td><?php if($users['email_verified']==1) echo $this->Html->link('Unverify',['controller' => 'Users', 'action' => 'unVerifyEmail',$users['id']],['class'=>'btn btn-success']); else echo $this->Html->link('Verify',['controller' => 'Users', 'action' => 'verifyEmail',$users['id']],['class'=>'btn btn-primary']); ?></td>
<td><?php if($users['active']==1) echo $this->Html->link('makeinActive',['controller' => 'Users', 'action' => 'makeinActive',$users['id']],['class'=>'btn btn-success']); else echo $this->Html->link('makeActive',['controller' => 'Users', 'action' => 'makeActive',$users['id']],['class'=>'btn btn-primary']); ?></td>
<td class="action">
<?php echo $this->Html->image('view.png', ['alt' => 'view','title'=>'View User','url' => ['controller' => 'Users', 'action' => 'viewProfile',$users['id']]]); ?>
<?php echo $this->Html->image('edit.png', ['alt' => 'edit','title'=>'Edit User','url' => ['controller' => 'Users', 'action' => 'editProfile', $users['id']]]); ?>
<?php echo $this->Html->image('password.png', ['alt' => 'change password','title'=>'Change Password','url' => ['controller' => 'Users', 'action' => 'changePassword', $users['id']]]); ?>
<?php echo $this->Html->image('delete.png', ['alt' => 'delete','title'=>'Delete User','url' => ['controller' => 'Users', 'action' => 'deleteProfile', $users['id']],'onclick'=>'return confirm("You are Going to Delete This User! Are you sure?")']); ?>
</td>
</tr>
<?php $i ++; ?>
<?php  } ?>

</tbody>
</table>
</div>
<?php if($this->Paginator->param('count') > @$limit ){ ?>
                    <div id="paginationDivId" class="pull-right">
                        <ul class=" pagination pull-right">
                         <?php 
                            echo $this->Paginator->prev( 'Prev', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled insert-anchor', 'tag' => 'a' ) );
                            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled   insert-anchor', 'tag' => 'a' ) );
                        ?>
                        </ul>
                    </div>
                  <?php } 
                  ?> 
<div class="col-sm-5">

      </div>

    </div>

