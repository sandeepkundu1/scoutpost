<?=$this->Html->css('style',['inline'=>false]);?>
<div class="page-container">

  <div class="sidebar-menu" style="min-height: 712px;">


    
<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>

  </li>
  <li class="active">
    <i class="fa fa-user"></i>
    <strong>Galleries Management</strong>
  </li>
</ol>

<h2 style="margin-top: 0" class="pull-left">Gallery</h2>    
    <div class="content update-profile">
	  <div class="row">
      <div class="col-sm-12">
        <div class="member-service">
          
          <div class="feature_box">
            <div class="current-week-workout">
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                  
                  <div class="">
                    <div class="thumb-box">
                      <div class="row-thumb-box clearfix">
                      <table class="table table-responsive table-hover table-bordered">
      <thead>
      <tr>

      <th style="width: 1px;">S.no</th>
    <th class="hidden-xs">Images</th>
    <th style="width: 100px;">Action</th>
      </tr>
      </thead>
                      <?php $i=1;?>
                        <?php foreach ($galleries as $key => $galleries) { ?>
                          
                         <tr>
                         <td><?= $i;?></td>
                         <td width="100px">
                           <?= $this->Html->image('gallery_pic/'.$galleries["gallery_image"],["alt"=>"gallery_pic",'width'=>'350','height'=>'150'])?>
                         </td>
                         <td>
                          <?=$this->Html->link('Delete',['controller'=>'Users','action'=>'deteteGalery/',$galleries["id"]],['confirm' => 'Are you sure you wish to delete this recipe?','class'=>'btn btn-primary','title'=>'delete image'])?>                               
                            </td>
                         <tr>      
                          
                        <?php $i++;
                         } ?>
                        </table>         
                      </div>
                    </div>
                  </div>          
                </div>
              </div>
            </div>
          </div>  
          <?php if($this->Paginator->param('count') > @$limit ){ ?>
                    <div id="paginationDivId" class="pull-right">
                        <ul class=" pagination pull-right">
                         <?php 
                            echo $this->Paginator->prev( 'Prev', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled insert-anchor', 'tag' => 'a' ) );
                            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled   insert-anchor', 'tag' => 'a' ) );
                        ?>
                        </ul>
                    </div>
                  <?php } 
                  ?> 
          
        </div>
      </div> 
		</div>
  </div>
</div>

    </div>
