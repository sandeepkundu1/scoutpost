
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>

  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>
  </li>
      <li>
        <a href="<?= $this->Url->build('/users/viewGallery', true); ?>"><i class="fa fa-user"></i> Feature</a>
      </li>
  <li class="active">
    <strong>Add Gallery</strong>
  </li>
</ol>

<h2>Add New Feature</h2>

<div class="clear"></div>
<div><?= $this->Flash->render() ?></div>
<div class="row">
  <div class="col-md-12">

    
<?php echo $this->Form->create($user,['id'=>'addfeature','enctype' => 'multipart/form-data'])?>
    

        <div data-collapsed="0" class="panel panel-primary">

          <div class="panel-heading">
            <div class="panel-title">
              <i class="fa fa-file-image-o"></i>
              <a data-rel="collapse" href="#">Gallery Description</a>
            </div>

            <div class="panel-options">
              <a data-rel="collapse" href="#"><i class="fa fa-angle-down"></i></a>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-sm-4 control-label">Gallery Image</label>
            <div class="col-sm-5">
                <?php echo $this->Form->input('gallery_image',['type'=>'file','class'=>'form-control','label'=>false]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
              
                <?= $this->Html->image($this->request->Webroot.'/img/gallery_image/'.$user['gallery_image'],['width'=>'100','height'=>'100','alt'=>$user['feature_image']]);?>
                <?php echo $this->Form->input('gallery_image_old',['type'=>'hidden','class'=>'form-control','label'=>false,'value'=>$user['gallery_image']]);?>
              </div>
            </div>
           
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
                <?= $this->Form->submit('Save',['class'=>'btn btn-primary','label'=>false]);?>
              </div>
            </div>
                          
		</div>
      </div>
	</div>
    </div>


