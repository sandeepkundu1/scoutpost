
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>

  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="entypo-gauge"></i>Dashboard</a>
  </li>
      <li>
        <a href="<?= $this->Url->build('/users/viewSchool', true); ?>"><i class="entypo-user"></i>School</a>
      </li>
  <li class="active">
    <strong>Edit</strong>
  </li>
</ol>

<h2>Edit School Galery Collections</h2>

<div class="clear"></div>
<div><?= $this->Flash->render() ?></div>
<div class="row">
  <div class="col-md-12">

    
<?php echo $this->Form->create($schoolDetail,['enctype' => 'multipart/form-data','novalidate'])?>
    

        <div data-collapsed="0" class="panel panel-primary">
            <div class="panel-heading">
            <div class="panel-title">
              <i class="entypo-user"></i>
              <a data-rel="collapse" href="#">school description</a>
            </div>

            <div class="panel-options">
              <a data-rel="collapse" href="#"><i class="entypo-down-open"></i></a>
            </div>
          </div>
          <div class="panel-body">

            <div class="form-group">
              <label class="col-sm-4 control-label">Description</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('description',['class'=>'form-control','label'=>false]);?>
              </div>
            </div>

            
            <div class="form-group">
              <label class="col-sm-4 control-label">Photo</label>
            <div class="col-sm-5">
                <?php echo $this->Form->input('photo',['type'=>'file','class'=>'form-control','label'=>false]);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
              
                <?= $this->Html->image($this->request->Webroot.'/img/school_pic/'.$schoolDetail['photo'],['width'=>'100','height'=>'100','alt'=>'school']);?>
                <?php echo $this->Form->input('school_image_old',['type'=>'hidden','class'=>'form-control','label'=>false,'value'=>$schoolDetail['photo']]);?>
              </div>
            </div>
           
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
                <?= $this->Form->submit('Update',['class'=>'btn btn-primary','label'=>false]);?>
              </div>
            </div>
                          
  
      </div>

    </div>


