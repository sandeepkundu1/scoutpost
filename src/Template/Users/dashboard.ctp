
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>

  <div class="main-content" style="min-height: 712px;">

    <div class="admin_header">

</div>
<!--div class="inner_cont">
    

        <div class="row">

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products/published';" class="tile-stats tile-blue">
          <div class="icon"><i class="entypo-basket"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="1" data-start="0" class="num">1</div>

          <h3>Published Products</h3>

          <p>Currently published</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products';" class="tile-stats tile-orange">
          <div class="icon"><i class="entypo-attention"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="2" data-start="0" class="num">2</div>

          <h3>Not Published</h3>

          <p>Active but not published</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products/pending_review';" class="tile-stats tile-white-gray">
          <div class="icon"><i class="entypo-alert"></i></div>
          <div data-delay="0" data-duration="1500" data-postfix="" data-end="0" data-start="0" class="num">0</div>

          <h3>Products Pending</h3>

          <p>Pending review</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/users/search?user_status=Online';" class="tile-stats tile-green">
          <div class="icon"><i class="entypo-users"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="1" data-start="0" class="num">1</div>

          <h3>Users Online</h3>

          <p>Currently logged in</p>
        </div>

      </div>

    </div>

    <div class="row">

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products';" class="tile-stats tile-red">
          <div class="icon"><i class="entypo-attention"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="1" data-start="0" class="num">1</div>

          <h3>Low Quantities</h3>

          <p>Currently published</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products';" class="tile-stats tile-red">
          <div class="icon"><i class="entypo-attention"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="2" data-start="0" class="num">2</div>

          <h3>Low Quantities</h3>

          <p>Not published</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/products';" class="tile-stats tile-white-gray">
          <div class="icon"><i class="entypo-cancel-circled"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="0" data-start="0" class="num">0</div>

          <h3>Inactive Products</h3>

          <p>Not activated</p>
        </div>

      </div>

      <div class="col-sm-3">

        <div style="cursor: pointer;" onclick="document.location.href='/users/search?user_status=Pending';" class="tile-stats tile-orange">
          <div class="icon"><i class="entypo-attention"></i></div>
          <div data-delay="600" data-duration="1500" data-postfix="" data-end="1" data-start="0" class="num">1</div>

          <h3>Users Pending</h3>

          <p>Pending activation</p>
        </div-->

      <article id="post-4" class="post-4 page type-page status-publish hentry">       
      <div class="entry-content">
        <div class="col-sm-4">
          <div class="widget socialbox" style="background:#359EEF; height:170px;">
            <div class="logo">
            <i class="fa fa-user"></i> 
            <?=$this->Html->link('Users Management',['controller'=>'Users','action'=>'viewUser'])?></div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="widget socialbox" style="background:#F39C12; height:170px;">
            <div class="logo">
            <i class="fa fa-folder-open"></i> 
            <?=$this->Html->link('CMS',['controller'=>'Users','action'=>'viewFeature'])?></div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="widget socialbox" style="background:#F56954; height:170px;">
            <div class="logo">
            <i class="fa fa-cube"></i> 
            <?=$this->Html->link('GalleryManagement',['controller'=>'Users','action'=>'viewGallery'])?></div>
          </div>
        </div>        
      </div>
    </article>

    </div>

