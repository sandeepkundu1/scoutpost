<?=$this->Html->script('custom/feature-management',['inline'=>false]);?>
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/featureDetail/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>

  </li>
  <li class="active">
    <i class="fa fa-user"></i>
    <strong>New Coming Feature</strong>
  </li>
</ol>

<h2 style="margin-top: 0" class="pull-left increase-btn">New Coming Feature<span class="pull-right"><?php echo  $this->Html->image('add.png', ['alt' => 'Add Feature','title'=>'add features','url' => ['controller' => 'Users', 'action' => 'addFeature'],'class'=>'btn btn-primary pull-right']); ?></span></h2>

    

<!--h4 class="pull-right">Add New Features</h4-->
<div style="clear: both;"></div>

    <table class="table table-responsive table-hover table-bordered">
      <thead>
      <tr>

      <th style="width: 1px;">S.no</th>
    <th class="hidden-xs">Images</th>
    <th>New Coming Feature</th>
    <th style="width: 100px;">Action</th>
      </tr>
      </thead>
    
<?php $i = 1; ?>
  <tbody>
  <?php foreach ($features as $key => $featureDetail) { ?>
<tr>
<td><?= $i ?></td> 
<td><?=$this->Html->image('feature_pic/'.$featureDetail['photo'],['alt'=>'image_','height'=>'150','width'=>'150'])?></td>
<td><?= $featureDetail['description']?></td>
<td>

<?php echo $this->Html->image('edit.png', ['alt' => 'Edit Feature','title'=>'Edit Feature','url' => ['controller' => 'Users', 'action' => 'editFeature', $featureDetail['id']]]); ?>
<?php echo $this->Html->image('delete.png', ['alt' => 'Delete Feature','title'=>'Delete Feature','url' => ['controller' => 'Users', 'action' => 'deleteFeature', $featureDetail['id']]]); ?>
</td>
</tr>
<?php $i ++; ?>
<?php  } ?>

</tbody>
</table>
</div>
<?php if($this->Paginator->param('count') > @$limit ){ ?>
                    <div id="paginationDivId" class="pull-right">
                        <ul class=" pagination pull-right">
                         <?php 
                            echo $this->Paginator->prev( 'Prev', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled insert-anchor', 'tag' => 'a' ) );
                            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled   insert-anchor', 'tag' => 'a' ) );
                        ?>
                        </ul>
                    </div>
                  <?php } 
                  ?> 

      

    </div>
</div>
