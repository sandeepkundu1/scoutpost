<style type="text/css">
	body{background-color: #303641;}
</style>

<?= $this->Html->script(array('custom/login'),array('inline'=>FALSE));?>
<div class="login-progressbar">
  <div></div>  
  </div>


  <div class="login-form">
  <div class="login-content">
      <?= $this->Form->create('null',['accept-charset'=>'UTF-8','id'=>'form_login']);?>
    
        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="&#x2713;" />
            <input name="authenticity_token" type="hidden" value="4DuqJVtHxf7tPROs+vJlLeygbKlcSpdy5eroyarfOA8=" />
        </div>
     <?= $this->Flash->render() ?>
        <div class="form-group">

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-user"></i>
            </div>
               <?= $this->Form->input('email',['type'=>'email','autofocus'=>'autofocus','class'=>'form-control email1', 'id'=>'email','placeholder'=>'Email Address','required'=>'required','label'=>false,'div'=>false]);?>
            

          </div>

        </div>

        <div class="form-group">

          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-key"></i>
            </div>
              <?= $this->Form->input('password',array('type'=>'password','class'=>'form-control', 'id'=>'password','placeholder'=>'Password','required'=>'required','label'=>false,'div'=>false));?>
            <!--<input class="form-control" id="password" name="password" placeholder="Password" required="required" type="password" />-->

          </div>

        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-login">
            Login In
            <i class="fa fa-sign-in"></i>
          </button>
        </div>

        <div class="messages"></div>

        <div class="login-bottom-links">

          <a href="javascript:void(0)" id ="forgot_pass" class="link">Forgot your password?</a>

        </div>

<?= $this->Form->end();?>
  </div>

</div>

<div class="modal fade" id="modal-1" style="display: none;" aria-hidden="true">
   <?= $this->Form->create('User',array('action'=>'send_reset_password_instructions','accept-charset'=>'UTF-8','data-remote'=>'true', 'id'=>"forgot_password_form"));?>
  <!--<form accept-charset="UTF-8" action="/users/send_reset_password_instructions" data-remote="true" id="forgot_password_form" method="post">-->
      <div style="margin:0;padding:0;display:inline">
          <input name="utf8" type="hidden" value="&#x2713;" /></div>

      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Forgot your password?</h4>
          </div>

          <div class="modal-body">

            <div class="alert alert-info">
              <strong>Enter your email address below</strong><br/>
              Password reset instructions will be sent to the email address associated with your account.
            </div>

            <div class="form-group">
              <input autofocus="autofocus" class="form-control email2" id="email" name="email" placeholder="Enter your email address" required="required" type="email" />
            </div>

            <a href="#"  data-toggle="popover" data-placement="top" data-content="If you forgot which email address you used to create your account, please contact our support department.">Forgot your email address?</a>

            <div id="message" class='alert alert-danger' style="display: none; margin-top: 20px; margin-bottom: 0;">
              <strong>Oh snap!</strong> Email address cannot be found. Please try again.
            </div>

          </div>

          <div class="modal-footer" style="margin-top: 0;">
            <button type="submit" id="reset-btn" class="btn btn-warning">Reset My Password</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>

<?= $this->Form->end();?>
</div>

</div>
