
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="entypo-gauge"></i>Dashboard</a>

  </li>
  <li class="active">
    <i class="entypo-user"></i>
    <strong>Check Memory Available for user</strong>
  </li>
</ol>

<h2 style="margin-top: 0" class="pull-left">Users Memory Allocation</h2>
    
<div style="clear: both;"></div>
<?= $this->Html->link('Go Back',['controller'=>'users','action'=>'viewUser'],['class'=>'btn-primary pull-right']);?>
    <table class="table table-responsive table-hover">
    
  <tbody>
  
<tr>
<td>Total Memory : <?=number_format($totalmemory)." Byte" ?></td></tr>
<td>Memory Used : <?= number_format($usedmemory)." Byte"?></td></tr>
<td>Available Memory : <?= number_format($totalmemory-$usedmemory)." Byte"?></td></tr>
</tbody>
</table>
<?= $this->Html->link('Make Change',['controller'=>'users','action'=>'changePermission', '?' => ['id' => $id,'totalmemory'=>$totalmemory,'usedmemory' => $usedmemory]],['class'=>'btn btn-primary']);?>




</div>

<div class="col-sm-5">

      </div>

    </div>

