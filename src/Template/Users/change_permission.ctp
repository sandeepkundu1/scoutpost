<?=$this->Html->script(['memory'],['inline'=>false]) ; ?>
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>

  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>
  </li>
      <li>
        <a href="<?= $this->Url->build('/users/viewUser', true); ?>"><i class="fa fa-user"></i> Users</a>
      </li>
  <li class="active">
    <strong>Edit</strong>
  </li>
</ol>

<h2>Edit User Memory Allocation</h2>

<div class="clear"></div>
<div><?= $this->Flash->render() ?></div>
<div class="row">
  <div class="col-md-12">
  <?php 
      if(!empty($result['total_memory'])){
        $star1 = $result['total_memory'];
      }else{
       $star1 = $totalmemory ; 
        }

        if(!empty($result['memory_size'])){
        $star2 = $result['memory_size'];
      }else{
       $star2 = $usedmemory ; 
        }

        if(!empty($result['available_memory'])){
        $star3 = $result['available_memory'];
      }else{
       $star3 = $totalmemory-$usedmemory ; 
        }

         ?>
<?php echo $this->Form->create()?>
    
<?php echo $this->Form->input('id',['type'=>'hidden','label'=>false,'value'=>$result['id']]);?>
        <div data-collapsed="0" class="panel panel-primary">

          <div class="panel-heading">
            <div class="panel-title">
              <i class="fa fa-user"></i>
              <a data-rel="collapse" href="#">Information</a>
            </div>

            <div class="panel-options">
              <a data-rel="collapse" href="#"><i class="entypo-down-open"></i></a>
            </div>
          </div>
          <div class="panel-body">

            <div class="form-group">
              <label class="col-sm-4 control-label">Total Memory</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('total_memory',['class'=>'form-control','label'=>false,'value'=>$star1]);?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Memory Used</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('memory_size',['class'=>'form-control','label'=>false,'value'=>$star2,'readonly'=>'readonly']);?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-4 control-label">Available Memory</label>

              <div class="col-sm-5">
                <?php echo $this->Form->input('available_memory',['class'=>'form-control','label'=>false,'value'=>$star3,'readonly'=>'readonly']);?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label"></label>

              <div class="col-sm-5">
                <?= $this->Form->submit('Update',['class'=>'btn btn-primary','label'=>false]);?>
              </div>
            </div>
                          
  
      </div>

    </div>


