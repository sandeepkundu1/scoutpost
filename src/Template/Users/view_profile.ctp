
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">
<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="entypo-gauge"></i>Dashboard</a>

  </li>
  <li class="active">
    <i class="entypo-user"></i>
    <strong>Usermanagement</strong>
  </li>
</ol>

<h2 style="margin-top: 0" class="pull-left">Users </h2>
    
<div style="clear: both;"></div>
    <table class="table table-responsive table-hover table-bordered">
    
  <tbody>
  
  <?php 
  foreach ($users as $key => $users) { ?>
<tr>
<td><strong>Name : </strong><?= $users['first_name']." ".$users['last_name']?></td></tr>
<tr><td> <strong>User Name : </strong><?= $users['username']?></td></tr>
<tr><td> <strong>Subscription Package : </strong><?php if($users['plan_id']=='P') {echo "Premium Plan";}else{ echo "Free Plan" ;} ?></td></tr>
<tr><td><h4> Mailing Address :</h4> <?= $users['address']."</br><strong>City:</strong> &nbsp;".$users['city']."</br><strong>State:</strong> &nbsp;".$users['state']."</br><strong>Zip:</strong> &nbsp;".$users['zip']?></td></tr>
<tr><td><strong> Email :</strong> <?= $users['email']?></td></tr>
<tr><td><strong> Photo :</strong> <?= $this->Html->image($this->request->Webroot.'/img/profile_pic/'.$users['profile_image'],['width'=>'100','height'=>'100','alt'=>$users['profile_image']]);?></td></tr>
<tr><td><strong>Sport :</strong> <?php if($userprofiles['sport']==1){ echo "Baseball";}else{echo "Softball";} ?></td></tr>
<tr><td><strong>Fielding Position(s) :</strong> <?php if($userprofiles['outfilder']==1){ echo "Outfielder  ,";}else{echo "";} ?>
<?php if($userprofiles['infilder']==1){ echo "Infielder  ,";}else{echo "";} ?>
<?php if($userprofiles['catcher']==1){ echo "Catcher  ,";}else{echo "";} ?>
<?php if($userprofiles['pitcher']==1){ echo "Pitcher . ";}else{echo "";} ?>
</td></tr>
<tr><td><strong>Bats from :</strong> <?php if($userprofiles['bats_from']==1){ echo "Right";}elseif($userprofiles['bats_from']==2){echo "Left";}else{echo "Both" ;} ?></td></tr>
<tr><td><strong>Throws with : </strong><?php if($userprofiles['through_with']==1){ echo "Right Hand";}else{echo "Left Hand";} ?></td></tr>
 <?php foreach($teamdivision as $i){ ?> 
<tr><td><strong>Current Teams : </strong><?php echo $i['current_team'] ; ?></td></tr>
<tr><td><strong>Division :</strong> <?php echo $i['division'] ; ?></td></tr>
<?php } ?>
<tr><td><strong>Current School Attending :</strong> <?= $userprofiles['curr_school']; ?></td></tr>
<tr><td><strong>Starting High School Year : </strong><?= $userprofiles['schooling']; ?></td></tr>
<tr><td><strong>Favorite Sports Team :</strong> <?= $userprofiles['feb_team']; ?></td></tr>
<tr><td><strong>Favorite Athlete : </strong><?= $userprofiles['feb_athlete']; ?></td></tr>

<?php  } ?>

<?= $this->Html->link('Go Back',['controller'=>'users','action'=>'viewUser'],['class'=>'btn-primary pull-right']);?>
</tbody>
</table>

</div>

<div class="col-sm-5">

      </div>

    </div>

