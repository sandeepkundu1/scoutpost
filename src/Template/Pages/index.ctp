<?php echo $this->Html->script('ckeditor/ckeditor');?>
 <?php echo $this->Html->script('ckfinder/ckfinder');?>

<div class="page-container">

  <div class="sidebar-menu" style="min-height: 712px;">


    
<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>

  </li>
  <li class="active">
    <i class="fa fa-user"></i>
    <strong>Page Management</strong>
  </li>
</ol>
                  <!-- /tile header -->
				  <div class="tile-body tile color transparent-black">
				  <div class="row">
        
          <div class="col-md-12">

              <?php echo $this->Form->create('Page',array('action'=>'add_page','class'=>'form-horizontal','type'=>'file'));?>
                    
            
              <div class="form-group">	
			  
			  <div class="col-md-6">

			  	<label class="control-label" for="typeahead">Title</label>
                <div class="controls">
		<select name="data[Page][pname]" id="selectTitle" class="form-control">
			<option value="">Select</option>
      <option class="controls" value="index"<?php if(@$page_title=="index"){?>selected="selected"<?php } ?>>index</option>
			<option class="controls" value="about"<?php if(@$page_title=="about"){?>selected="selected"<?php } ?>>about</option>
      <option class="controls" value="workoutsGuide"<?php if(@$page_title=="workoutsGuide"){?>selected="selected"<?php } ?>>workoutsGuide</option>
      <option class="controls" value="practiceGuide"<?php if(@$page_title=="practiceGuide"){?>selected="selected"<?php } ?>>practiceGuide</option>
      <option class="controls" value="acadmic"<?php if(@$page_title=="acadmic"){?>selected="selected"<?php } ?>>academic</option>
      <option class="controls" value="termscondition"<?php if(@$page_title=="termscondition"){?>selected="selected"<?php } ?>>termscondition</option>
      <option class="controls" value="mailPage"<?php if(@$page_title=="mailPage"){?>selected="selected"<?php } ?>>FreemailPage</option>
      <option class="controls" value="mailPage2"<?php if(@$page_title=="mailPage2"){?>selected="selected"<?php } ?>>PaidmailPage</option>
      <option class="controls" value="privacy"<?php if(@$page_title=="privacy"){?>selected="selected"<?php } ?>>privacy</option>
      <option class="controls" value="canGo"<?php if(@$page_title=="canGo"){?>selected="selected"<?php } ?>>Block Update Page</option>
      <option class="controls" value="profile"<?php if(@$page_title=="profile"){?>selected="selected"<?php } ?>>profile Page</option>
		</select>
                </div>
			  </div>
			   <div class="col-md-6">
			  
			   <label class="control-label" for="typeahead"> Meta Title</label>
                <div class="controls">
                  <?php echo $this->Form->input('meta_title',array('value'=>@$pageDetail[0]['meta_title'],'type'=>'text', 'class'=>'form-control',
                  'label'=>false));?>
              
                </div>
			  
			  </div>

			</div>

        <div class="form-group">
  					<div class="col-md-6">
  					
  					 <label class="control-label" for="typeahead"> Meta Keyword</label>
                  <div class="controls">
                    <?php echo $this->Form->input('meta_keyword',array('value'=>@$pageDetail[0]['meta_keyword'],'type'=>'textarea', 'class'=>'form-control','label'=>false));?>
                
                  </div>
  					</div>
  					<div class="col-md-6">
  						
  						  <label class="control-label" for="typeahead"> Meta Description</label>
                  <div class="controls">
                    <?php echo $this->Form->input('meta_description',array('value'=>@$pageDetail[0]['meta_description'],'type'=>'textarea', 'class'=>'form-control','label'=>false));?>
                
                  </div>
            </div>
				</div>
        <div class="form-group">
          
          <div class="col-md-6">
        
         <label class="control-label" for="typeahead"> Meta link</label>
                <div class="controls">
                  <?php echo $this->Form->input('meta_link',array('value'=>@$pageDetail[0]['meta_link'],'type'=>'text', 'class'=>'form-control',
                  'label'=>false));?>
              
                </div>
        
        </div>
          <div class="col-md-6">
        
         <label class="control-label" for="typeahead">Link title</label>
                <div class="controls">
                  <?php echo $this->Form->input('link_title',array('value'=>@$pageDetail[0]['link_title'],'type'=>'text', 'class'=>'form-control',
                  'label'=>false));?>
              
                </div>
        
        </div>

      </div>

				<div class="form-group">
              
              <div class="col-md-12">
                <label class="control-label" for="textarea2">Page Contents</label>
                <div class="controls"> 
                <?php echo $this->Form->input('id',array('type'=>'hidden','value'=>@$pageDetail[0]['id'],'label'=>false,'div'=>false));?>
                  <?php echo $this->Form->input('page_content',array('type'=>'textarea','value'=>@$pageDetail[0]['page_content'],'label'=> false,'div'=>false,'id'=>'page_content','class'=>'ckeditor'));?>
                
                </div>
              </div>
			  </div>
			   
			   <script type="text/javascript">
					var CustomHTML = CKEDITOR.replace('page_content',{filebrowserBrowseUrl : '/js/ckfinder/ckfinder.html',filebrowserWindowWidth : '1000',filebrowserWindowHeight : '700'});
				</script>
			  
          
              <div class="form-actions">
            
			         <button class="btn btn-success crick" type="submit">Submit</button>
              
              </div>
             
            <?php echo $this->Form->end();?> 
            
          </div>
        </div>
  </div>


<script type="text/javascript">

$('#selectTitle').change(function(){

window.location.href = '<?=$this->Url->build("/pages/index/"); ?>'+$(this).val();


});
</script>