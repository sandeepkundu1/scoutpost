<div class="content">
			<div class="header-bottom col-sm-offset-2">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
								
			</div>
		</header>
		<div class="clearfix"></div>
		<div class="content update-profile">
			<div class="col-sm-4 col-sm-offset-4">
				<div class="member-service">
					<h1 class="text-center"><span class="user-icon"><i class="fa fa-user"></i></span> User Login</h1>
					<div class="current-week-workout ">
						
						<div class="row">
							<?= $this->Form->create() ?>
							<div class="form-group">
								<div class="col-sm-12">
									<label for="inputEmail3" class="control-label">Email</label>							
									<?= $this->Form->input('email',['class'=>'form-control','placeholder'=>'Enter Email','label'=>false])?>									
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label for="inputEmail3" class="control-label">Password</label>							
									<?= $this->Form->input('password',['class'=>'form-control','placeholder'=>'Enter Password','label'=>false])?>									
								</div>
							</div>
							<div class="col-sm-12 login-button">
								
								<?= $this->Form->button('Login',['class'=>'btn btn-primary  ']) ?>
								<?= $this->Form->button('Forgot Password',['class'=>'btn btn-primary pull-right','data-target'=>'#myModal','data-toggle'=>'modal']) ?>
							</div>

							<?= $this->Form->end() ?>
							
						</div>
						<div class="row">
							<div class="error-message col-sm-12"><?= $this->Flash->render('flash'); ?></div>
						</div>
		   			</div> 
				
				</div>

			</div>
		</div>
	</div>
		<div class="clearfix"></div>
<!-- Forget Password -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog" style="width: 425px;">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Forget Password</h4>
						  </div>
						  <div class="modal-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Email Address</label>
										<?= $this->Form->create('forgotpassword',['action'=>'#','id'=>'forgetPassword'])?><?= $this->Form->input('email',['placeholder'=>'Email Address','class'=>'form-control','label'=>false])?>	
											
										
									</div>
								</div>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<?= $this->Form->button('Send',['type'=>'submit','class'=>'btn btn-primary']); ?>
							<?= $this->Form->end(); ?>
						  </div>
						  
						</div>
					  </div>
					</div>
					<!-- Forget Password -->
					<!-- Reset Password -->
					<div class="modal fade" id="myModalReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" style="width: 425px;">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Reset Password</h4>
							  </div>
							  <div class="modal-body">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputEmail3" class="control-label">Current Password</label>		
											<?= $this->Form->create('resetpassword',['action'=>'#','id'=>'resetPassword'])?><?= $this->Form->input('curr_password',['type'=>'password','placeholder'=>'current Password','class'=>'form-control','label'=>false])?>	
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputEmail3" class="control-label">New Password</label>		
											<?= $this->Form->input('password',['type'=>'password','placeholder'=>'new Password','class'=>'form-control','label'=>false])?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputEmail3" class="control-label">Confirm New Password</label>		
											<?= $this->Form->input('cpassword',['type'=>'password','placeholder'=>'cunfirm new Password','class'=>'form-control','label'=>false])?>
										</div>
									</div>
								</div>
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<?= $this->Form->button('Save',['type'=>'submit','class'=>'btn btn-primary']); ?>
							   <?= $this->Form->end(); ?>

							  </div>
							</div>
						</div>
					</div>
					<!-- Reset Password -->