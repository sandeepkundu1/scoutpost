<?=$this->Html->script(['custom/academics','jquery.bxslider'],['inline'=>false])?>
<div class="content academic">
			<div class="col-sm-12">
				<div class="member-service">
					<h1><?=$pagecontant[0]['meta_title']?></h1>
					<div class="current-week-workout">						
						<div class="workouts">
							<div class="col-sm-8 no-padding-left">
								<h2><?=$pagecontant[0]['meta_description']?></h2>
								<p style="text-align:justify;"><?=$pagecontant[0]['page_content']?></p>
                                                                <div class="animation_image_acadmic" style="text-align:center;display:none;">
                                                                        <?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
                                                                </div>
							</div>
							<div class="col-sm-4 no-padding-right">
								<div class="advertisement current-gpa">
									<h3>Display current GPA</h3>
                                    <h4 id="first_sem_gpa" style="text-align:center;"></h4>
                                    <h4 id="second_sem_gpa" style="text-align:center;"></h4>
								</div>
							</div>
							<div class="col-sm-4 no-padding-right">
								<div class="advertisement current-gpa">
									<h3>image gallery(School Logo) </h3>
                                  	<div class="right_slider">
                              			<div class="getSchool"></div>
                              			<div class="getSchools"></div>
                                  	</div>
								</div>
							</div>							
						</div>
						<div class="clearfix"></div>
						<!-- <div class="blue-border"></div>	 -->					
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>		
	</div>
<!--ul id="getSchool" class="bxsliders">
                                  </ul-->