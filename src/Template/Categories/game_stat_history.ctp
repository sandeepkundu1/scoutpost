<?=$this->Html->script('custom/history-page',['inline'=>false])?>
<div class="content game-stats">
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Game STATS</h1>
					<div class="current-week-workout">
						<h2>Game Stats History</h2>						
						<div class="col-sm-12 no-padding ">
							<div class="batting-statistics game-stats-table">


<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 TerrorObj"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>



							<h3><!-- <b>04-18-2015 TOURNAMENT AT-BAT TOTALS </b> --></h3>
                                                        <div class="table-responsive">
							<table class="table table-bordered table-recorded text-center" >
								<tr>
									<th>YEAR</th>
									<th>AGE</th>
									<th>Team</th>
									<th>GAMES</th>
									<th>PA</th>
									<th>AB</th>
									<th>HITS</th>
									<th>1B</th>
									<th>2B</th>
									<th>3B</th>
									<th>HR</th>
									<th>RBI</th>
									<th>BB</th>
									<th>SB</th>
									<th>RUNS SCORED</th>
									<th>AVG</th>
									<th>OBP</th>
									<th>SLG</th>
									<th>OPS</th>
									<th>RC</th>
									<th>TB</th>
									<th>PIP</th>
								</tr>
								<tbody id='data_table'>
																		
								</tbody>
								<tr>
									<td colspan="3">TOTALS</td>									
									<td id="add1"></td>
									<td id="add2"></td>
									<td id="add3"></td>
									<td id="add4"></td>
									<td id="add5"></td>
									<td id="add6"></td>
									<td id="add7"></td>
									<td id="add8"></td>
									<td id="add9"></td>
									<td id="add10"></td>
									<td id="add11"></td>
									<td id="add12"></td>
									<td id="add13"></td>
									<td id="add14"></td>
									<td id="add15"></td>
									<td id="add16"></td>
									<td id="add17"></td>
									<td id="add18"></td>
									<td id="add19"></td>
									


								</tr> 
								
							</table>
                                                        </div>
							</div>
							<div class="row">
								<div class="form-group" style="margin-top:20px;">
									<div class="col-sm-3">
										
									</div>
								</div>
							</div>
							
						</div>
						
						
						<div class="clearfix"></div>
						
						
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
		
	</div>