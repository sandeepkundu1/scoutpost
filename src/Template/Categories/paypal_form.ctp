<form id = "paypal_checkout" action = "https://www.paypal.com/cgi-bin/webscr" method = "post">
    <input name = "cmd" value = "_cart" type = "hidden">
    <input name = "upload" value = "1" type = "hidden">
    <input name = "no_note" value = "0" type = "hidden">
    <input name = "bn" value = "PP-BuyNowBF" type = "hidden">
    <input name = "tax" value = "0" type = "hidden">
    <input name = "rm" value = "2" type = "hidden">
 
    <input name = "business" value = "jeremy@jdmweb.com" type = "hidden">
    <input name = "handling_cart" value = "0" type = "hidden">
    <input name = "currency_code" value = "GBP" type = "hidden">
    <input name = "lc" value = "GB" type = "hidden">
    <input name = "return" value = "http://mysite/myreturnpage" type = "hidden">
    <input name = "cbt" value = "Return to My Site" type = "hidden">
    <input name = "cancel_return" value = "http://mysite/mycancelpage" type = "hidden">
    <input name = "custom" value = "" type = "hidden">
 
    <div id = "item_1" class = "itemwrap">
        <input name = "item_name_1" value = "Gold Tickets" type = "hidden">
        <input name = "quantity_1" value = "4" type = "hidden">
        <input name = "amount_1" value = "30" type = "hidden">
        <input name = "shipping_1" value = "0" type = "hidden">
    </div>
    <div id = "item_2" class = "itemwrap">
        <input name = "item_name_2" value = "Silver Tickets" type = "hidden">
        <input name = "quantity_2" value = "2" type = "hidden">
        <input name = "amount_2" value = "20" type = "hidden">
        <input name = "shipping_2" value = "0" type = "hidden">
    </div>
    <div id = "item_3" class = "itemwrap">
        <input name = "item_name_3" value = "Bronze Tickets" type = "hidden">
        <input name = "quantity_3" value = "2" type = "hidden">
        <input name = "amount_3" value = "15" type = "hidden">
        <input name = "shipping_3" value = "0" type = "hidden">
    </div>
 
    <input id = "ppcheckoutbtn" value = "Checkout" class = "button" type = "submit">
</form>