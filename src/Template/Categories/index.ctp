<?=$this->Html->script(['custom/index-new-feature'],['inline'=>false]);?>
<div class="content game-stats game-stats-bg">
			<div class="col-sm-12">
				<div class="member-service">
					<h1>BUILD YOUR FIRST IMPRESSION<BR/> FOR COACHES AND SCOUTS</h1>
					<div class="current-week-workout">
						<div class="col-sm-4 no-padding-left">
						<div class="architech">
						<div class="architech-bottom">
						<div class="architech-mdl">
						<div class="in-padd">
						<strong>COMPLETE ATHLETIC PROFILE SYSTEM</strong>
						<ul>
							<li><a href="categories/view_player_post">A fun set of web tools for aspiring athletes ages 8-17</a></li>
							<li><a href="categories/view_player_post">Tracks your game performance with stats and video</a></li>
							<li><a href="categories/view_player_post">Chart measure and view your athletic progress</a></li>
							<li><a href="categories/view_player_post">GPA tool for high school and college admission</a></li>
							<li><a href="categories/view_player_post">A system to assist coaches scouting for scholar athletes.</a></li>
						</ul>
						</div>
						<span class="middle-images-line">&nbsp;</span>
						<div class="in-padd">
						<strong>SMART APP FOR REAL-TIME UPDATES</strong>
						<ul>
							<li><a href="categories/view_player_post">Records and updates your game preparation workouts</a></li>
							<li><a href="categories/view_player_post">Capture game stats plus video and updates your profiles.</a></li>
							<li><a href="categories/view_player_post">Share your box score Views and At-Bat highlights .</a></li>
							<li><a href="categories/view_player_post">Link with all major social media platform.</a></li>
							<li><a href="categories/view_player_post">Efficient use of smart device data storage and usage plan</a></li>
						</ul>
						</div>
						</div>
						 
						
						
						 </div>
						</div>
						
						

						<div class="feature-coming">
					<h1><?php echo $pagecontant[0]['meta_title']; ?></h1>
					<div class="feature-coming-content">
						<div class="images imgdc"></div>
					</div>
					<div class="feature-coming-content2">
						<div class="images2 imgdc"></div>
						
					</div>
					<a class="btn btn-primary orange" href="<?php echo $pagecontant[0]['meta_link']; ?>"><?php echo $pagecontant[0]['link_title']; ?></a>

					<!--?=$this->Html->link('View All Features',['controller'=>'categories','action'=>'viewFeatures'],['class'=>'btn btn-primary orange'])?-->
				</div>
						
																				
						</div>
						
						<div class="col-sm-2 no-add">
						
						</div>
						
						<div class="col-sm-6">
						<div class="lboard">
						<strong>LEADER BOARD AND VIRTUAL TOURNAMENTS</strong>
						<p>compete in our fantasy baseball tournaments</p>
						<p>prizes include: sports gear and rewards cards</p>
						<p>National and international system of player exposure.</p>
						</div>
                       						
						<div class="col-sm-12 no-padding ">
							<div class="batting-statistics game-stats-table bordr-padd-set">							
							<table class="table table-bordered table-striped custom-striped" id="results">
								<tr>
									<th>Rank</th>
									<th>Player</th>
									<th>AT BATS</th>
									<th>AVG</th>
									<th>SLG</th>
									<th>OPS</th>
									<th>RBI</th>
									<th>HR</th>
									<!--th>GPA</th-->
								</tr>
								
							</table>						
							</div>
						</div>
						    
						<div class="col-sm-12">
						  <div class="yold">  
						  <p> “My 10 year old loves to check his box score stats after each tournament. His overall game is improving”</p>

						<p>“My daughter now works on adjusting her swing after viewing her videos” </p>
		
		               <p>“Instead of just watching my ball player I am recording memories and important information instantly posted to his profile” </p>									

						<!--iframe frameborder="0" height="281"  allowfullscreen="" src="https://www.youtube.com/embed/PIoQADWri9s"  class="video_mang"></iframe-->
						<iframe src="https://player.vimeo.com/video/134367834" width="350" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						
						  </div>
						</div>
						
						
						
						</div>
						

						<div class="clearfix"></div>
						
						
					</div>
				
					
		
				</div>
					
			</div>
			 <div class="evolution-footer">
				<strong>DEVELOP YOUR FUTURE -EVOLUTION OF AN ATHLETE</strong>
             <p>The evolution start by profiling at a young age and consistently progresses.</p>
			 <ul>
			  <li><a href="categories/view_player_post">Create a true picture of an evolving at hlete</a> |</li>
			  <li><a href="categories/view_player_post">prep are your profile-Resume at an early age</a> |</li>
			  <li><a href="categories/view_player_post">Share with coaches and scouts</a></li>
			 </ul>
             <p>Qualify for Scholarship Fund- Assistance for collage prep schools and collages</p>			
						
			</div>
			<div class="blk-footer">
			
			</div>
		
		   <div class="profile-premium">
		  <div class="pro-min">  
		   <strong>FREE PROFILE INCLUDES</strong>
		   <ul>
		     <li>Stored Player Information available for  1st year</li>
			 <li>Upload your current Year Profile Image</li>
			 <li>Upload results of one game and video</li>
			 <li>Can be upgraded to Premium at any time</li>
			</ul>
            <em class="pro-th">With this profile you are ready to start updating workouts, practice drills and academic GPA </em>
		   <p>When you decide the time is right Great starting point for players ages 7 to 9 </p>
		   </div>
		   
		    <div class="pro-min spc-lt-rt">
			<strong>PREMIUM PROFILE $10.00 / MONTH</strong>
			<ul>
		      <li>Upload Game Stats, Workouts, Practices and GPA</li>
			  <li>Daily, Weekly, Monthly and Yearly progress tracking</li>
			  <li>Virtual Tournament and Leader Board Rankings</li>
			  <li>Coaches Box (Batting Tips, Training and Advice)</li>
              <li>Share your stats with coaches, family and friends</li>
			</ul>
			<em>With this profile you are ready to start updating workouts, practice drills and academic GPA</em>
			<p>START BUILDING YOUR PROFILE-RESUME AGES 10 AND OLDER </p>
			</div>
		   
		    <div class="pro-min">
			<strong>TEAM PROFILE $70.00 / MONTH </strong>
			<ul>
		      <li>20 Player Profiles or Profile slots</li>
			  <li>Includes all features of the Premium Profile</li>
			  <li>Premium Profiles can migrate into a Team Profile</li>
			  <li>Teams can use new or migrated profiles</li>
			  <li>Players can take their profile when changing teams</li>
			  <li>A great player development tool for coaches</li>
			</ul>
			<p>ORGANIZATIONAL DISCOUNT CLUB AND TRAVEL TEAMS, LEAGUES AND SCHOOLS </p>
			</div>
		   
		 </div>
		
		
		
		   <div class="profile-img-box">
		   <ul>
		   <li>
		   <span>&nbsp;</span>
		   <img src="images/iimg11.jpg" alt=""></li>
		   <li class="rght-spc">
		    <span>&nbsp;</span>
		   <img class="nr-img" src="images/iimg2.jpg" alt="">
		   <img src="images/iimg3.jpg" alt="">
		   </li>
		   <li>
		    <span>&nbsp;</span>
		   <img src="images/iimg4.jpg" alt=""></li>
		   
		   </ul>
		   
		   </div>
		
		
		
		</div>
		<div class="clearfix"></div>
		
	</div>
		<div class="clearfix"></div>
	
		
