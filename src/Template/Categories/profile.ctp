<?=$this->Html->script(['https://www.google.com/jsapi','custom/videoscript','custom/imgscript','custom/profile'],['inline'=>false])?>
<?=$this->Html->css(['video/player','video/videomain'],['inline'=>false])?>
<div class="content">
			<div class="col-sm-7">
			<?= $this->Flash->render('flash'); ?>
				<div class="member-service">
					<h1>This Weeks Activities  <i class="weekly-icon"></i></h1>
					<div class="current-week-workout " id='data_div'>
						
					</div>
				
				</div>
				 <div class="member-service other-member-service">
					<h1>Recent Game Highlights  <i class="weekly-icon"></i></h1>
						<div class="current-week-workout ">
							<h4><b><span class="gameType"></span> --> <span class="gameDate"></span></b> </h4>
								<p><b><span class="team1"></span> Versus <span class="team2"></span></b></p>
								<div class="getVideo"></div>
								
						</div>
				</div>
				<div class="member-service other-member-service">
					<h1>Picture gallery  <i class="weekly-icon"></i></h1>
						<div class="current-week-workout profileGellery">
						<div id="picProfile"></div>
						<!--img id="getpic1" height="150" width="150"></img>
						<img id="getpic2" height="150" width="150"></img>
						<img id="getpic3" height="150" width="150"></img-->
						</div>
				</div>
			</div>

			<div class="col-sm-5" style="overflow: hidden;">
				<div class="col-sm-8 no-padding-left current-batting ">
					<div class="batting-statistics">
						<h3><b><span id="yer"></span> Player Info</b></h3>
						<table class="table">
							<tr>
								<td class="text-left">Age</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg8"></td>
							</tr>
							<tr>
								<td class="text-left">Teams</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg9"></td>
							</tr>
							<tr>
								<td class="text-left">Division</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg11"></td>
							</tr>
							<tr>
								<td class="text-left">Position</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg12"></td>
							</tr>
							<tr>
								<td class="text-left">Throws</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg15"></td>
							</tr>
							<tr>
								<td class="text-left">Bats</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg16"></td>
							</tr>
							<tr>
								<td class="text-left">Favorite Team</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg17"></td>
							</tr>
							<tr>
								<td class="text-left">Favorite Player</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg14"></td>
							</tr>
						</table>
					</div>


					<div class="batting-statistics">
						
						<h3><b>Current Batting Statistics</b></h3>
						<table class="table">
							<tr>
								<td class="text-left">Avg</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg1"></td>
							</tr>
							<tr>
								<td class="text-left">At Bat<span class="lower">s</span></td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg2"></td>
							</tr>
							<tr>
								<td class="text-left">Hits</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg3"></td>
							</tr>
							<tr>
								<td class="text-left">Rbi</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg4"></td>
							</tr>
							<tr>
								<td class="text-left">Slg</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg5"></td>
							</tr>
							<tr>
								<td class="text-left">Obp</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg6"></td>
							</tr>
							<tr>
								<td class="text-left">Pip</td>
								<td class="text-center">-</td>
								<td class="text-right" id="avg7"></td>
							</tr>
						</table>
						<div class="player-icon-bg">
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="advertisement">
					<?php //print_r($pagecontent); die;?>
						<h3><?php echo $pagecontent[0]['meta_title']; ?></h3>
					<div class="feature-coming-content">
						<div class="images imgdc"></div>
					</div>
					<div class="feature-coming-content2">
						<div class="images2 imgdc"></div>
						
					</div>
					<a class="btn btn-primary orange" href="<?php echo $pagecontent[0]['meta_link']; ?>"><?php echo $pagecontent[0]['link_title']; ?></a>
					<!--?=$this->Html->link('View All Images',['controller'=>'categories','action'=>'viewMarkets'],['class'=>'btn btn-primary orange'])?-->	
					</div>
					<!--div class="advertisement">
						<h3>Comparison <br/>Video Gallery <br/>Placeholder</h3>
					</div>
					<div class="advertisement">
						<h3>Batting and Training Tips Area <br/>Placeholder</h3>
					</div-->
				</div>
				<div class="col-sm-4 no-padding-right no-padding-left profile-current">
					<?=$this->Html->image('profile_pic/'.$user_img,['alt'=>'profile_pic']);?>
					<h4 class="text-center"><b>
					
<!--<?=$this->Form->button('Upload Current Picture',['type'=>'button','class'=>'btn btn-primary','data-toggle'=>'modal','data-target'=>'#myModal'])?>-->
</b></h4>
					<p>Current Year Profile Picture</p>

				</div>				
			</div>

		</div>
		<div class="clearfix"></div>
		
	</div>
	<!--image uploading Model code-->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Profile Picture</h4>
      </div>
      <div class="modal-body">

       				<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
       				<?=$this->Form->create('null',['action'=>'javascript:void(0)','id'=>'uploadimage','enctype'=>'multipart/form-data'])?>
					<div id="image_preview">
					<?=$this->Html->image('profile_pic/'.$user_img,['alt'=>'profile_pic','id'=>'previewing']);?>
					</div>
					<hr id="line">
					<div id="selectImage">
					<label>Select Your Image</label><br/>
					
					<?=$this->Form->input('profile_image',['type'=>'file','id'=>'file','label'=>false,'class'=>'form-control no-padding shere'])?>
					
					</div>
					
					</div>
					<div id="message"></div>
      <div class="modal-footer">
      <?=$this->Form->input('Close',['type'=>'button','id'=>'file','class'=>'btn btn-default','data-dismiss'=>'modal','label'=>false])?>
      <?=$this->Form->input('Save changes',['type'=>'submit','id'=>'file','class'=>'btn btn-primary','label'=>false])?>
        <?=$this->Form->end();?>
      </div>
    </div>
  </div>
</div>
				

