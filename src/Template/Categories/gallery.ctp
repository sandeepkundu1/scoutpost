<?=$this->Html->script(['custom/gallery','fresco'],['inline'=>false]);?>
<?=$this->Html->css(['fresco'],['inline'=>false]);?>
<div class="content">

<div class="col-sm-12 gellerTittle"><h2 style="margin-top: 0" class="pull-left">GALLERY</h2>

<?=$this->Html->link('Video Gallery',['controller'=>'Categories','action'=>'videoGallery'],['class'=>'btn btn-primary pull-right'])?></div>

<div style="clear: both;"></div>
<div class="table-responsive">
    <div class="col-sm-12">
		<div class="videoSections imagesSection" >
			<!--h3 class="headingBlue">2016 Images</h3>
		 	<div class="row" id='results2016'></div-->
			<h3 class="headingBlue">2015 Images</h3>
		 	<div class="row" id='results2015'></div>
		 	<h3 class="headingBlue">2014 Images</h3>
		 	<div class="row" id='results2014'></div>
		</div>
	</div>
</div>
</div>	          
		<div class="clearfix"></div>