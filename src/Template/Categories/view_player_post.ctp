<?=$this->Html->css('style',['inline'=>false]);?>
<div class="page-container">
  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">

    <div class="content update-profile">
    <div class="row">
      <div class="col-sm-12">
        <div class="member-service">
          
          <div class="feature_box">
            <div class="current-week-workout">
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                  
                  <div class="">
                    <div class="thumb-box">
                      <div class="row-thumb-box clearfix">
                        <?php foreach ($galleries as $key => $galleries) { ?>
                          <div class="img-box mix Residential" data-myorder="2">
                                                  <a href="<?= $this->Url->build('/img/gallery_pic/'.$galleries["gallery_image"])?>" class="fresco" data-fresco-group='example'>
                        <?= $this->Html->image('gallery_pic/'.$galleries["gallery_image"],["alt"=>"gallery_pic"])?></td>

                        <div class="hover-box">&nbsp;</div>
                                                  </a>      
                                                </div>
                        <?php  } ?>  

                        <?php 
                           
                            if(count($galleries)==0) {
                                echo "<p style='text-align:center;'>No Results Found.</p>";
                            }
                        ?>
                      </div>
                    </div>
                  </div>          
                </div>
              </div>
            </div>
          </div>  
          <?php if($this->Paginator->param('count') > @$limit ){ ?>
                    <div id="paginationDivId" class="pull-right">
                        <ul class=" pagination pull-right">
                         <?php 
                            echo $this->Paginator->prev( 'Prev', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled insert-anchor', 'tag' => 'a' ) );
                            echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) );
                            echo $this->Paginator->next( 'Next', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled   insert-anchor', 'tag' => 'a' ) );
                        ?>
                        </ul>
                    </div>
                  <?php } 
                  ?> 
          
        </div>
      </div> 
