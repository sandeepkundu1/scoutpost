<?=$this->Html->script(['custom/profileSetting','jquery.form'],['inline'=>false])?>
<div class="content">
			<div class="col-sm-12">
				<div class="member-service about">
					<h1>Profile Settings </h1>
					<div class="member-service-content member-svc-cntent">
		         
     <div class="panel-body panel-bodyc">
            <div class="row">
      <div class="col-sm-6">
      <div class="form-group">
        <h2>PERSONAL INFO</h2>
     <?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'personal_info.json'],'id'=>'pInfo','novalidate'])?>

      <?=$this->Form->input('id',['type'=>'hidden' , 'id'=>'pergetId'])?>
              <label class="control-label">Email</label>
             <?php echo $this->Form->input('email',['type'=>'text','class'=>'form-control','label'=>false,'id'=>'emId']);?>
       
       <label class="control-label">Home Address</label>
       <?php echo $this->Form->input('address',['type'=>'textarea','class'=>'form-control frmaid','label'=>false,'id'=>'addId']);?>
       <label class="control-label">Phone Number</label>
       <?php echo $this->Form->input('phone',['type'=>'text','class'=>'form-control','label'=>false,'id'=>'phId']);?> <br/> 
           
      <div class="save-personal">
    <?= $this->Form->button('Save',['class'=>'btn btn-primary','label'=>false,'id'=>'savepersonal','type'=>'button']);?> </div>
              </div>  
               <?php echo $this->Form->end();?>   
       </div>

   <div class="col-sm-6">
    <div class="form-group">

       <h2>PROFILE INFO (Change)</h2>

        <?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'profile_info.json'],'id'=>'proInfo','novalidate'])?>
      <?=$this->Form->input('id',['type'=>'hidden' , 'id'=>'progetId'])?>
              <label class="control-label">Team</label>
             <?php echo $this->Form->input('current_team',['type'=>'text','class'=>'form-control','label'=>false,'id'=>'c_Id']);?>
       
       <label class="control-label">Division</label>
       
      <?php
                $divAdd=["Major"=>"Major","Minor"=>"Minor","AAA"=>"AAA","AA"=>"AA","All-Star"=>"All-Star","Senior"=>"Senior","Show-Case"=>"Show Case","Freshman"=>"Freshman","JV"=>"JV","Varsity"=>"Varsity","Bronco"=>"Bronco","Mustang"=>"Mustang","Pinto"=>"Pinto"];
                ?>
                <?= $this->Form->select('division',$divAdd,['class'=>'form-control frmdid','empty' => '(select Division)','id'=>'d_Id']);?>
      <br/>
      <?= $this->Form->button('Save',['class'=>'btn btn-primary','label'=>false,'id'=>'saveprof','type'=>'button']);?> 
            </div>
             <?php echo $this->Form->end();?>  
          </div>

      </div>     
         
            </div>
<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorpersonal"></div>
                        <div id="personal_success" class="success-msg pro-suc col-sm-3 col-sm-offset-3" style="display:none;"></div>
                        <div class="animation_k" style="text-align:center;display:none;">
              <?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
            </div>
            <?php echo $this->Form->end();?>  

    <div class="panel-body panel-bodyc"></div>

<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorprofile"></div>
                        <div id="profile_success" class="success-msg pro-suc col-sm-12" style="display:none;"></div>
                        <div class="animations" style="text-align:center;display:none;">
              <?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
            </div>
            <?php echo $this->Form->end();?>  

<?php if(@$userDetail['check_plan']==1 || $userDetail['plan_id']=='F'){ ?>
<h3>UPGRADE to PREMIUM :If you are a Free Profile subscriber you can click to upgrade</h3>
<?= $this->Form->input('id',['type'=>'hidden','id'=>'getuser'])?>
<?= $this->Form->button('Upgrade to premium',['class'=>'btn btn-primary','label'=>false,'id'=>'upgradeplan','type'=>'button']);?>
<div class="animat">
<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
</div>
<?php }else{?>
<!--h3>PAUSE / RESUME PREMIUM :  If you are a Premium Profile subscriber you can click to downgrade.  
Note:  Monthly charge will change to $5.00</h3-->
<h3>PAUSE Plan :  you are a Premium Profile subscriber you can click to Delete Plan. </h3>
<?= $this->Form->input('id',['type'=>'hidden','id'=>'getuser'])?>
<?= $this->Form->button('delete Plan ',['class'=>'btn btn-primary','label'=>false,'id'=>'pauseplans','type'=>'button']);?>
<p id="swmsg"></p>
<?php } ?>
<p>&nbsp;</p>
          <h2>select at least 3 profile pic for your website </h2>
					
<?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'profile_pic.json'],'enctype' => 'multipart/form-data','id'=>'profile_form','novalidate'])?>
		<div class="panel-body panel-bodyc">
            <div class="form-group">
			<div class="col-sm-4">

      <?=$this->Form->input('id',['type'=>'hidden' , 'id'=>'getId'])?>
              <label class="control-label">profile Image</label>
             <?php echo $this->Form->input('profile_pic1',['type'=>'file','class'=>'form-control','label'=>false]);?>
			 <img id="getpic1" height="150" width="150" />
			 <?php echo $this->Form->input('profile_pic2',['type'=>'file','class'=>'form-control','label'=>false]);?>
			 <img id="getpic2" height="150" width="150" />
			 <?php echo $this->Form->input('profile_pic3',['type'=>'file','class'=>'form-control','label'=>false]);?>  
       <img id="getpic3" height="150" width="150" />
              </div>
			  <div class="col-sm-8"></div>
            </div>
           <div class="col-sm-12">
       <?= $this->Form->button('Save',['class'=>'btn btn-primary','label'=>false,'id'=>'savedata','type'=>'button']);?>         
              </div>
            </div>
<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorObj_work"></div>
                        <div id="success_work" class="success-msg col-sm-3 col-sm-offset-3" style="display:none;"></div>
                        <div class="animation_image_work" style="text-align:center;display:none;">
              <?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
            </div>
            <?php echo $this->Form->end();?>	
				
			<h2>Add 3 phone numbers to notify friends and family :</h2>
		<?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'setPhone.json'],'id'=>'phone_form','novalidate'])?>
		<div class="panel-body panel-bodyc">
            <div class="form-group">
			<div class="col-sm-6">
      <?=$this->Form->input('id',['type'=>'hidden' , 'id'=>'getnoId'])?>
              <label class="control-label">Phone number #1 (with country code)</label>
      <?php echo $this->Form->input('phone_no1',['type'=>'text','class'=>'form-control txtext','label'=>false,'id'=>'ph1']);?>
      <?php echo $this->Form->input('notify_one',['type'=>'checkbox','class'=>'form-control txtchk','label'=>false,'id'=>'ph4']);?>NOTIFY ON 
			 <label class="control-label">Phone number #2 (with country code)</label>
			 <?php echo $this->Form->input('phone_no2',['type'=>'text','class'=>'form-control txtext','label'=>false,'id'=>'ph2']);?>
			 <?php echo $this->Form->input('notify_two',['type'=>'checkbox','class'=>'form-control txtchk','label'=>false,'id'=>'ph5']);?>NOTIFY ON 
			 <label class="control-label">Phone number #3 (with country code)</label>
			 <?php echo $this->Form->input('phone_no3',['type'=>'text','class'=>'form-control txtext','label'=>false,'id'=>'ph3']);?>
			 <?php echo $this->Form->input('notify_three',['type'=>'checkbox','class'=>'form-control txtchk','label'=>false,'id'=>'ph6']);?>NOTIFY ON 
              </div>
			  <div class="col-sm-6"></div>
            </div>
           <div class="col-sm-12">
       <?= $this->Form->button('Save',['class'=>'btn btn-primary','label'=>false,'id'=>'savephno','type'=>'button']);?>         
              </div>
            </div>
<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorObj"></div>
                        <div id="success" class="success-msg col-sm-3 col-sm-offset-3" style="display:none;"></div>
                        <div class="animation_image" style="text-align:center;display:none;">
              <?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
            </div>
            <?php echo $this->Form->end();?>

        </div>

  </div>          
        </div>
      </div>

		</div>
		<div class="clearfix"></div>
		
	</div>