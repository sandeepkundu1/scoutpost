<?=$this->Html->script(['custom/game_stat_3','https://www.google.com/jsapi','custom/videoscript'],['inline'=>false])?>
<?=$this->Html->css(['video-js','video/player','video/videomain'],['inline'=>false])?>
<div class="content game-stats game-statsto">  
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Game STATS</h1>
					<div class="current-week-workout">
						<div class="col-sm-8 no-padding-left">
						
							<h2><span class="gameType"></span> STATS</h2>
							<h4><b><span class="gameType"></span> --> <span class="gameDate"></span></b> </h4>
							<p><b><span class="team1"></span> Versus <span class="team2"></span></b></p>
							<div class="row">
								<div class="form-group">
									<div class="col-sm-6">
									      <p class="getVideo">
									      </p>
									</div>
									
								</div>
							</div>
						</div>
					<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
						<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj1"></div>
							<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>
							<div class="col-sm-4">
							<h2 class="link-red"><a href="<?= $this->Url->build('/Categories/game_stat_history');?>">HISTORY</a></h2>
							<div class="batting-statistics game-stats-table">
								<h3><b><span class="team1"></span> Versus <span class="team2"></span></b></h3>
								<table class="table">
									
									<tr>
										<td class="text-left">AB</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="ab"></span></td>
									</tr>
									<tr>
										<td class="text-left">1B</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="1b"></span></td>
									</tr>
									<tr>
										<td class="text-left">2B</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="2b"></span></td>
									</tr>
									<tr>
										<td class="text-left">3B</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="3b"></span></td>
									</tr>
									<tr>
										<td class="text-left">HR</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="hr"></span></td>
									</tr>
									<tr>
										<td class="text-left">BB</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="bb"></span></td>
									</tr>
									<tr>
										<td class="text-left">HITS</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="hits"></span></td>
									</tr>
									<tr>
										<td class="text-left">RUNS</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="runs"></span></td>
									</tr>
									<tr>
										<td class="text-left">RBI</td>
										<td class="text-center">-</td>
										<td class="text-right"><span id="rbi"></span></td>
									</tr>



									<tr>
										<td class="text-left">AVG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avg2"></td>
									</tr>
									<!--tr>
										<td class="text-left">SLG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="slg2"></td>
									</tr>
									<tr>
										<td class="text-left">OBP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="obp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">OPS</td>
										<td class="text-center">-</td>
										<td class="text-right" id="ops2"></td>
									</tr-->
									<tr>
										<td class="text-left">TB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="tb2"></td>
									</tr>
									<tr>
										<td class="text-left">RC</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rc2"></td>
									</tr>
									<tr>
										<td class="text-left">PA</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pa2"></td>
									</tr>
									<tr>
										<td class="text-left">PIP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pip2"></td>
									</tr>
                                    <!--tr>
										<td class="text-left">AVG vs RHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rhp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs LHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="lhp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG w/ROB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avgw2"></td>
									</tr-->
                                                                        <tr>
										<td class="text-left">QAB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="qab2"></td>
									</tr>

								</table>
							</div>
						
						</div>
						<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj2"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>
						<div class="clearfix"></div>
						
						
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
		
	</div>

	