<?=$this->Html->script('custom/game_stat_2',['inline'=>false])?>
<div class="content game-stats"> 
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Game STATS</h1>
					<div class="current-week-workout">
						<div class="col-sm-5 no-padding-left">
							<h2><span class="gameType"></span> STATS</h2>
							<h4><b><span class="gameType"></span> --> <span class="gameDate"></span></b> </h4>
							<ul class="tournament-link listLink">
								<!--li><a href="<?= $this->Url->build('/Categories/game_stat_3');?>">Game 1 Versus opponent</a></li-->
								
							</ul>						<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj1"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>							
						</div>
						
						<div class="col-sm-7">
						<h2 class="link-red"><a href="<?= $this->Url->build('/Categories/game_stat_history');?>">HISTORY</a></h2>						
						<div class="col-sm-8 no-padding ">
							<div class="batting-statistics game-stats-table">
							<h3><b><span class="gameDate"></span> <span class="gameType"></span> AT-BAT TOTALS </b></h3>
							<table class="table table-bordered">
								<tr>
									<th>AB</th>
									<th>1B</th>
									<th>2B</th>
									<th>3B</th>
									<th>HR</th>
									<th>HITS</th>
									<th>RUN<span class="lower">s</span></th>
								</tr>
								<tr id="abc">
																		
								</tr>
								<tr>
									<th>BB</th>
									<th>HBP</th>
									<th>ROE</th>
									<th>SO</th>
									<th>SAC</th>
									<th>SB</th>
									<th>RBI</th>
								</tr>
								<tr id="def">
																	
								</tr>
								
							</table>						
							</div>
						</div>
						<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj2"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>
						<div class="col-sm-4 no-padding-right">
							<div class="batting-statistics game-stats-table">
								<h3><b><span class="gameDate"></span> <span class="gameType"></span> STATS</b></h3>
								<table class="table">
									<tr>
										<td class="text-left">AVG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avg1"></td>
									</tr>
									<tr>
										<td class="text-left">SLG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="slg1"></td>
									</tr>
									<tr>
										<td class="text-left">OBP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="obp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">OPS</td>
										<td class="text-center">-</td>
										<td class="text-right" id="ops1"></td>
									</tr>
									<tr>
										<td class="text-left">TB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="tb1"></td>
									</tr>
									<tr>
										<td class="text-left">RC</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rc1"></td>
									</tr>
									<tr>
										<td class="text-left">PA</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pa1"></td>
									</tr>
									<tr>
										<td class="text-left">PIP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pip1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs RHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rhp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs LHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="lhp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG w/ROB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avgw1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">QAB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="qab1"></td>
									</tr>
								</table>
							</div>
							<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj3"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>
						</div>
						</div>
						<div class="clearfix"></div>
						
						
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
		
	</div>
