<?=$this->Html->script(['highcharts','custom/skills'],['inline'=>false])?>
<div class="content game-stats">
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Skills Chart</h1>
					<div class="current-week-workout">
						<h2>WORKOUTS </h2>
						<div class="workouts">
							<div class="col-sm-7 no-padding-left">
                                                                <a href="javascript:void(0);" onclick="load_workout_graph('week');">This Week</a>
								<br>
								<a href="javascript:void(0);" style="text-align:justify;" onclick="load_workout_graph('month');">This Month &nbsp;<?php echo date('M')?>&nbsp;&nbsp;&nbsp;(display weeks and hours)</a>
                                                                <br>
								<a href="javascript:void(0);"  style="text-align:justify;" onclick="load_workout_graph('year');">This Year &nbsp;<?php echo date('Y')?>&nbsp;&nbsp;&nbsp;(display months and hours)</a>
                                                                <br><br>
								<!--p class="btn btn-primary orange work-out-guide">WORKOUT GUIDE</a-->
								<?=$this->Html->link('WORKOUT GUIDE',['controller'=>'Categories','action'=>'workoutsGuide'],['class'=>'btn btn-primary orange work-out-guide'])?>
							</div>
							<div class="col-sm-5 no-padding-right" id="container">
							</div>							
						</div>
						<div class="clearfix"></div>
						<div class="blue-border"></div>
						<h2>PRACTICE </h2>
						<div class="practice">
							<div class="col-sm-7 no-padding-left">
                                                                <a href="javascript:void(0);" onclick="load_practice_graph('week');">This Week</a>
								<br>
								<a href="javascript:void(0);" style="text-align:justify;" onclick="load_practice_graph('month');">This month <?php echo date('M')?>&nbsp;&nbsp;&nbsp;(display weeks and hours)</a>
                                                                <br>
								<a href="javascript:void(0);"  style="text-align:justify;" onclick="load_practice_graph('year');">This year <?php echo date('Y')?>&nbsp;&nbsp;&nbsp;(display months and hours)</a>
                                                                <br><br>
								<!--p class="btn btn-primary orange work-out-guide">PRACTICE GUIDE</a-->
								<?=$this->Html->link('PRACTICE GUIDE',['controller'=>'Categories','action'=>'practiceGuide'],['class'=>'btn btn-primary orange work-out-guide'])?>
							</div>
							<div class="col-sm-5 no-padding-right" id="container_two">
							</div>							
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>		
	</div>