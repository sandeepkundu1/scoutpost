<?=$this->Html->script('custom/allmarketImg',['inline'=>false]);?>
<div class="content">

<div class="col-sm-12"><h2 style="margin-top: 0" class="pull-left">Marketing Area  Detail </h2></div>
<div style="clear: both;"></div>
<div class="table-responsive">
    <div class="col-sm-12">
		<table class="table  table-hover table-bordered">
		 <thead>
		  <tr>
			<th style="width: 1px;">SL.</th>
			<th class="">Photo</th>
			<th style="min-width:300px">Description</th>
		  </tr>
		 </thead>
		 
		 <tbody id="results">
			 
		 </tbody>
		</table>
	</div>
</div>
<div class="animation_image" style="text-align:center;display:none;">
 <?= $this->Html->image('loadinga.gif')?> 

</div>
<div style="text-align:center;color:red;display:none;" id='no_rslt_dv'>No More Results Found.</div>
<div style="text-align:center;color:red;display:none;" id='no_rslt_load'>No Results Found.</div>
<div class="col-sm-12">
	<?= $this->Form->button('Load More',['type'=>'button','id'=>'getMoreFeature','class'=>'pull-right btn btn-primary'])?>
</div>
</div>	          
		<div class="clearfix"></div>