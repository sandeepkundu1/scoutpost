
<div class="content">
			<div class="header-bottom col-sm-offset-2">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
								
			</div>
		</header>
		<div class="clearfix"></div>
		<div class="content update-profile">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="member-service">
					<h1>Reset Password</h1>
					<div class="current-week-workout ">
						
							<div class="row">
							<?= $this->Form->create('resetpassword',['action'=>'#','id'=>'resetPassword']) ?>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">New Password</label>
								<div class="col-sm-8">
								<?= $this->Form->input('ident',['type'=>'hidden','value'=>$ident,'id'=>'identId'])?>
								<?= $this->Form->input('activate',['type'=>'hidden','value'=>$activate,'id'=>'activateCode'])?>
								<?= $this->Form->input('password',['class'=>'form-control','placeholder'=>'Enter New Password','label'=>false])?>
									
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Confirm Password</label>
								<div class="col-sm-8">
								<?= $this->Form->input('cpassword',['type'=>'password','class'=>'form-control','placeholder'=>'Re Enter Password','label'=>false])?>
									
								</div>
							</div>
<div class="col-sm-12"><?= $this->Form->button('Submit',['class'=>'btn btn-primary pull-right ']) ?></div>

<?= $this->Form->end() ?>
</div>
<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorObj"></div>
		   			</div>
				</div>
			</div>
		</div>
	</div>
		<div class="clearfix"></div>