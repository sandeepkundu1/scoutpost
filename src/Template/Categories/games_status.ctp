<?=$this->Html->script('custom/game-stat',['inline'=>false])?>
<div class="content game-stats">
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Game STATS
					<span class="link-red pull-right"><a href="<?= $this->Url->build('/Categories/game_stat_history');?>">HISTORY</a> -- <a href="javascript:void(0);" id ="previousYear"> previous year</a><a href="javascript:void(0);" id ="previousYear1">Click here to view next year</a></span>		
					</h1>
					<div class="current-week-workout">
						<div class="col-sm-5 no-padding-left">
							<h2>TOURNAMENT STATS - <span class="year_prev"></span></h2>
							<h4><b><span class="year_prev"></span> Tournaments</b> </h4>
							<ul class="tournament-link" id="getTDate">

								<!--li><a href="<?= $this->Url->build('/Categories/game_stat_2');?>" class="tournament-link">04-18-2015 – nn Games</a></li-->
								
							</ul>
							<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 TerrorObj"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>


						</div>
						<div class="col-sm-7">
						

						<div class="col-sm-8 no-padding ">
							<div class="batting-statistics game-stats-table">
							<h3><b><span class="year_prev"></span> AT-BAT TOTALS </b></h3>
							<table class="table table-bordered">
								<tr>
									<th>AB</th>
									<th>1B</th>
									<th>2B</th>
									<th>3B</th>
									<th>HR</th>
									<th>HITS</th>
									<th>RUN<span class="lower">s</span></th>
								</tr>
								<tr id="abc">
																		
								</tr>
								<tr>
									<th>BB</th>
									<th>HBP</th>
									<th>ROE</th>
									<th>SO</th>
									<th>SAC</th>
									<th>SB</th>
									<th>RBI</th>
								</tr>
								<tr id="def">
									
								</tr>
							</table>						
							</div>
						</div>
						<div class="col-sm-4 no-padding-right">
							<div class="batting-statistics game-stats-table">
								<h3><b><span class="year_prev"></span> TOURNAMENT STATS</b></h3>
								<table class="table">
									<tr>
										<td class="text-left">AVG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avg1"></td>
									</tr>
									<tr>
										<td class="text-left">SLG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="slg1"></td>
									</tr>
									<tr>
										<td class="text-left">OBP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="obp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">OPS</td>
										<td class="text-center">-</td>
										<td class="text-right" id="ops1"></td>
									</tr>
									<tr>
										<td class="text-left">TB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="tb1"></td>
									</tr>
									<tr>
										<td class="text-left">RC</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rc1"></td>
									</tr>
									<tr>
										<td class="text-left">PA</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pa1"></td>
									</tr>
									<tr>
										<td class="text-left">PIP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pip1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs RHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rhp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs LHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="lhp1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG w/ROB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avgw1"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">QAB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="qab1"></td>
									</tr>
								</table>
							</div>
						</div>
						</div>
						<div class="clearfix"></div>
						<div class="blue-border"></div> 
						<div class="col-sm-5 no-padding-left">
							<h2>LEAGUE STATS - <span class="year_prev"></span></h2>
							<h4><b><span class="year_prev"></span> League Games</b> </h4>
							<ul class="tournament-link" id="getLDate">
								
							</ul>
							<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
								<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 PerrorObj1"></div>
								<div class="animation_image_acadmic" style="text-align:center;display:none;">
 								<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>													
						</div>
						<div class="col-sm-7">
						<!--h2 class="link-red"><a href="<?= $this->Url->build('/Categories/game_stat_history');?>">HISTORY</a></h2-->

						<div class="col-sm-8 no-padding ">
							<div class="batting-statistics game-stats-table">
							<h3><b><span class="year_prev"></span> AT-BAT TOTALS </b></h3>
							<table class="table table-bordered">
								<tr>
									<th>AB</th>
									<th>1B</th>
									<th>2B</th>
									<th>3B</th>
									<th>HR</th>
									<th>HITS</th>
									<th>RUN<span class="lower">s</span></th>
								</tr>
								<tr id="abc1">
									
								</tr>
								<tr>
									<th>BB</th>
									<th>HBP</th>
									<th>ROE</th>
									<th>SO</th>
									<th>SAC</th>
									<th>SB</th>
									<th>RBI</th>
								</tr>
								<tr id="def1">
																		
								</tr>
								
							</table>						
							</div>
						</div>
						<div class="col-sm-4 no-padding-right">
							<div class="batting-statistics game-stats-table">
								<h3><b><span class="year_prev"></span> LEAGUE TOTALS</b></h3>
								<table class="table">
									<tr>
										<td class="text-left">AVG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avg2"></td>
									</tr>
									<tr>
										<td class="text-left">SLG</td>
										<td class="text-center">-</td>
										<td class="text-right" id="slg2"></td>
									</tr>
									<tr>
										<td class="text-left">OBP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="obp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">OPS</td>
										<td class="text-center">-</td>
										<td class="text-right" id="ops2"></td>
									</tr>
									<tr>
										<td class="text-left">TB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="tb2"></td>
									</tr>
									<tr>
										<td class="text-left">RC</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rc2"></td>
									</tr>
									<tr>
										<td class="text-left">PA</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pa2"></td>
									</tr>
									<tr>
										<td class="text-left">PIP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="pip2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs RHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="rhp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG vs LHP</td>
										<td class="text-center">-</td>
										<td class="text-right" id="lhp2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">AVG w/ROB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="avgw2"></td>
									</tr>
                                                                        <tr>
										<td class="text-left">QAB</td>
										<td class="text-center">-</td>
										<td class="text-right" id="qab2"></td>
									</tr>
								</table>
							</div>
						</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
