<?=$this->Html->script('custom/gallery',['inline'=>false]);?>
<div class="content">

<div class="col-sm-12 gellerTittle"><h2 style="margin-top: 0" class="pull-left">GALLERY</h2>

<?=$this->Html->link('Image Gallery',['controller'=>'Categories','action'=>'gallery'],['class'=>'btn btn-primary pull-right'])?></div>

<div style="clear: both;"></div>
<div class="table-responsive">
    <div class="col-sm-12">
		<div class="videoSections" >
			<h3 class="headingBlue">2015 Videos</h3>
		  <div class="row" id='vresults2015'></div>
		  <h3 class="headingBlue">2014 Videos</h3>
		  <div class="row" id='vresults2014'></div>
		</div>
	</div>
</div>
</div>	          
		<div class="clearfix"></div>