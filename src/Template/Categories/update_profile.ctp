<?php //print_r(phpinfo()); die;?>
<?= $this->Html->script(['jquery.form','custom/update-profile','custom/imgscript','vendor/jquery.ui.widget','tmpl.min','jquery.fileupload','jquery.fileupload-process','main','jquery.fileupload-validate'],['inline'=>false])?>
<div class="content update-profile">
			<div class="col-sm-10">
				<div class="member-service">
					<h1>UPDATE profile <a class="profile-help pull-right"><i class="fa fa-question-circle"></i></a>
						<div role="tooltip" class="popover fade bottom in" id="popover640679" >
							<div class="arrow" ></div>
							<h2 class="popover-title" style="display: block;">? Help Content</h2>
							<div class="popover-content">Update Profile allows you to input and upload information to the Player Post Cloud.  Providing a great tracking system of your progress that can be referenced when needed. 
							<b>ACADEMICS will you to enter GPA for each years 
							Semester and upload to your cloud profile 
							WORKOUTs that help in your physical conditioning - Agilities, Running, Strength Training
							Can be uploaded to your cloud profile with a Video 
							PRACTICE that is specific to your sport in preparation for games 
							- Infield, Outfield, Batting and Drills
							Can be uploaded to your cloud profile with a Video 
							GAME STATS of each At Bat of every game can be tracked and 
							uploaded with Video.  Key statistics can be saved and viewed
							at anytime.</b></div>
						</div>
					</h1>
						 <div id="dialog" style="display: none">
    									The state game is on pause mode . click open to resume this.or simply click close to avoid. 
							</div>
					<div class="current-week-workout ">						
						<h4>UPDATE ACADEMICS</h4>
						<div class="row">
						<?php echo $this->Form->create(null,['action'=>'javascript:void(0)','enctype' => 'multipart/form-data','id'=>'acadmics_form','novalidate'])?>
							
							<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Select Year</label>
								<div class="col-sm-3">
								    <?php $setYear=array(date('Y') => date('Y'),date('Y',strtotime('-1 year'))=> date('Y',strtotime('-1 year')));?>

									<?= $this->Form->select('year',$setYear,['class'=>'form-control','id'=>'userYear']);?>
									
								</div>

								<label for="inputEmail3" class="col-sm-3 control-label">Select Semester</label>
								
								<div class="col-sm-3">
									<?= $this->Form->select('semester',['1'=>'1st Semester','2'=>'2nd Semester'],['class'=>'form-control','id'=>'userSem']);?>
							    </div>
							
							</div>
						</div>	
						
						<div class="row">		
							<div class="form-group">
<!--<div class="col-sm-3"><label for="inputEmail3" class="control-label">Subject</label></div>
<div class="col-sm-3"><label for="inputEmail3" class="control-label">Select Grade</label></div>
<div class="col-sm-3"><label for="inputEmail3" class="control-label">Select Weight</label></div>-->
<div class="col-sm-12 txt-right"><?= $this->Form->button('add more',['type'=>'button','class'=>'btn btn-primary','id'=>'addCF']);?></div>
							</div>		
							
						</div>

						<div class="row" id="add-new-div">		
							<div class="form-group">
							        <?php $grade=['4'=>'A','3'=>'B','2'=>'C','1'=>'D','0'=>'F']?>
									<?php $weight=['0'=>'Regular','0.5'=>'Honors','1'=>'College']?>

		<div class="col-sm-3">
		<label for="inputEmail3" class="control-label">Subject</label>
		<?= $this->Form->input('sub.subject',['class'=>'form-control','id'=>'usersub','label'=>false,'placeholder'=>'Enter subject name']);?></div>
		<div class="col-sm-3">
		<label for="inputEmail3" class="control-label">Select Grade</label>
		<?= $this->Form->select('sub.grade',$grade,['class'=>'form-control userSem_0 gpatotal' ,'id'=>'userSems']);?></div>
		<div class="col-sm-3">
		<label for="inputEmail3" class="control-label">Select Weight</label>
		<?= $this->Form->select('sub.weight',$weight,['class'=>'form-control userWt_0 gpatotal','id'=>'userWts']);?></div>
		<div class="col-sm-3"></div>
							</div>
						</div>
						<div class="calculatediv">
						<?= $this->Form->button('claculate',['type'=>'button','class'=>'btn btn-primary','id'=>'calGpa']);?>
                           </div>
						<div class="row">
							<div class="form-group" id ="hide-gpa1">
								<label for="inputEmail3" class="col-sm-3 control-label">Enter GPA For 1st Semester<span style="color:red">*</span></label>
								<div class="col-sm-3">
								<?= $this->Form->input('first_sem_gpa',['class'=>'form-control','label'=>false,'id'=>'Gpa1'])?>
								
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group" id ="hide-gpa2">
								<label for="inputEmail3" class="col-sm-3 control-label">Enter GPA For 2nd Semester<span style="color:red">*</span></label>
								<div class="col-sm-3">
								<?= $this->Form->input('second_sem_gpa',['class'=>'form-control','label'=>false,'id'=>'Gpa2'])?>
								
								</div>
							</div>
							<div style="display:none;" class="success-msg col-sm-3 col-sm-offset-3 succobj"></div>
							<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorObj"></div>
							<div class="animation_image_acadmic" style="text-align:center;display:none;">
 							<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
						     </div>
						</div>
						
						<div class="row">
							<div class="form-group">
								
								<div class="col-sm-6">

								<?= $this->Form->button('Save',['type'=>'submit','id'=>'acadmic_save',"data-loading-text"=>"Loading...",'class'=>'btn btn-primary pull-right']);?>
									
								</div>
								<?=$this->Form->end();?>	
							</div>
						</div>

						<h4>UPDATE SKILLS</h4>
						<div class="row">
					<div id="workcard">
						<div class="front"> 
							<div class="workout col-sm-6">
								<h5>WORKOUT</h5>
								<div class="col-sm-12 workout-inner">

								<?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'save_workout.json'],'enctype' => 'multipart/form-data','id'=>'workout_form','novalidate'])?>	
								<?=$this->Form->input('user_id',['id'=>'getid','value'=>$userId,'type'=>'hidden'])?>	
								<?=$this->Form->input('flag',['id'=>'getid2','value'=>'1','type'=>'hidden'])?>	
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Enter Date<span style="color:red">*</span></label>		
										
									<?= $this->Form->input('workout_date',['class'=>'form-control workdate','label'=>false,'id'=>'workdateId','placeholder'=>'Enter Date'])?>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Enter Time Spent<span style="color:red">*</span></label>

										<?= $this->Form->select('workout_time',['30'=>'30 Mins','60'=>'1 hr'],['class'=>'form-control','id'=>'worktimeId','empty'=>'Select Time']);?>	

										<!--?= $this->Form->input('workout_time',['class'=>'form-control worktimeClass','label'=>false,'id'=>'worktimeId','placeholder'=>'Enter Time Spent'])?-->
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Description of Workout<span style="color:red">*</span></label>

										<?php
										$work_desc = ['agility_training'=>'Agility Training','distance_running'=>'Distance Running','weight_lifting'=>'Weight Lifting','swimming'=>'Swimming'];
										?>
										<?= $this->Form->select('description',$work_desc,['class'=>'form-control','id'=>'practimeId','empty'=>'Select description']);?>

										<!--?= $this->Form->input('description',['type'=>'textarea','class'=>'form-control','label'=>false,'row'=>3,'id'=>'description_1'])?-->
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Upload image</label>

									<?= $this->Form->input('skills_img',['type'=>'file','class'=>'form-control no-padding skilImgcls','label'=>false,'id'=>'skills_img1'])?>

									<img src="" class="img-responsive" id="previewing1" alt=""/>
									<p id="message"></p>


									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-9">
												<label for="inputEmail3" class="control-label">Upload Video</label>		
											<?= $this->Form->input('file.name',['type'=>'hidden','class'=>'fnames'])?>
											<?= $this->Form->input('file.type',['type'=>'hidden','class'=>'types'])?>
											<?= $this->Form->input('file.size',['type'=>'hidden','class'=>'sizes'])?>

											</div>
											
											<div class="col-sm-3 pull-right">
											<?= $this->Form->button('Upload',['type'=>'button','id'=>'work_save',"data-loading-text"=>"Loading...",'class'=>'btn btn-primary','style'=>'margin-top: 28px;']);?>	
											</div>
											<?=$this->Form->end();?>
<!--video start from here -->
<div class="col-lg-9 col-xs-6 pull-left">
<form id="workfileupload" action="" method="POST" enctype="multipart/form-data">
        <div class="fileupload-buttonbar">
            
                <!-- The fileinput-button span is used to style the file input field as button -->
                
                    
                    <?= $this->Form->input('files',['type'=>'file','class'=>'form-control no-padding','label'=>false,'id'=>'fileh'])?>
                    <span id="shnm"></span>
    <div id="work_video" style="text-align:center;display:none;">
 							<?= $this->Html->image('uploading.gif',['height'=>25,'width'=>25])?>
						     </div>            
                
            </div>
            <!-- The global progress state -->
            
        
        <!-- The table listing the files available for upload/download -->
        
    </form>
</div>
<div style="display:none;" class="error-Msg col-sm-5 errorObj_work"></div>
							          <div id="success_work" class="success-msg col-sm-12 " style="display:none;"></div>
							          <div class="animation_image_work" style="text-align:center;display:none;">
 											<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
									  </div>
<!--end from here-->

										</div>
									</div>
								</div>
							</div> 
						<div class="back"></div>
					</div>
				</div>
					<div class="practice col-sm-6">
						<div id="praccard">
							<div class="front"> 
								<h5>PRACTICE</h5>
								<div class="col-sm-12 practice-inner">
								<?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'save_workout.json'],'enctype' => 'multipart/form-data','id'=>'practic_form','novalidate'])?>		<?=$this->Form->input('user_id',['id'=>'getid','value'=>$userId,'type'=>'hidden'])?>	<?=$this->Form->input('flag',['value'=>'2','type'=>'hidden'])?>		
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Enter Date<span style="color:red">*</span></label>		
										<?= $this->Form->input('workout_date',['class'=>'form-control workdate','label'=>false,'id'=>'pracdateId','placeholder'=>'Enter Date'])?>

									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Enter Time Spent<span style="color:red">*</span></label>

										<!--?= $this->Form->input('workout_time',['class'=>'form-control worktimeClass','label'=>false,'id'=>'practimeId','placeholder'=>'Enter Time Spent'])?-->
										<?= $this->Form->select('workout_time',['30'=>'30 Mins','60'=>'1 hr'],['class'=>'form-control','id'=>'practimeId','empty'=>'Select time']);?>	


									</div>
									<div class="form-group">
										<label for="inputEmail3" class="control-label">Description of Practice<span style="color:red">*</span></label>		
										<?php
										$prac_desc = ['hitting_drills'=>'Hitting Drills','offensive_drills'=>'Offensive Drills','defensive_drills'=>'Defensive Drills','pitching'=>'Pitching','catching'=>'Catching'];
										?>
										<?= $this->Form->select('description',$prac_desc,['class'=>'form-control','id'=>'practimeId','empty'=>'Select description']);?>	

										<!--?= $this->Form->input('description',['type'=>'textarea','class'=>'form-control','label'=>false,'row'=>3,'id'=>'description_1'])?-->

									</div>

									<div class="form-group">
										<label for="inputEmail3" class="control-label">Upload image</label>

									<?= $this->Form->input('skills_img',['type'=>'file','class'=>'form-control no-padding skilImgcls2','label'=>false,'id'=>'skills_img2'])?>

									<img src="" class="img-responsive" id="previewing2" alt=""/>
									<p id="message2"></p>
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-9">
												<label for="inputEmail3" class="control-label">Upload Video</label>		
												<?= $this->Form->input('file.name',['type'=>'hidden','class'=>'fnames'])?>
											<?= $this->Form->input('file.type',['type'=>'hidden','class'=>'types'])?>
											<?= $this->Form->input('file.size',['type'=>'hidden','class'=>'sizes'])?>

											</div>
											
											<div class="col-sm-3 pull-right">
												<?= $this->Form->button('Upload',['type'=>'button','id'=>'prac_save',"data-loading-text"=>"Loading...",'class'=>'btn btn-primary','style'=>'margin-top: 28px;']);?>	
											</div>
											<?=$this->Form->end();?>

	
	<div class="col-lg-9 col-xs-6 pull-left">										<!--video start from here -->
<form id="pracfileupload" action="" method="POST" enctype="multipart/form-data">
        <div class="fileupload-buttonbar">
            
                <!-- The fileinput-button span is used to style the file input field as button -->
                
                    
                    <?= $this->Form->input('files',['type'=>'file','class'=>'form-control no-padding','label'=>false,'id'=>'prcvh'])?>
                    <span id="prcvhs"></span>
    <div id="prac_video" style="text-align:center;display:none;">
 							<?= $this->Html->image('uploading.gif',['height'=>25,'width'=>25])?>
						     </div>            
                
            </div>
            <!-- The global progress state -->
            
       
        <!-- The table listing the files available for upload/download -->
        
    </form>
     </div>
<div style="display:none;" class="error-Msg col-sm-5  errorObj_prac"></div>
							<div id="success_prac" class="success-msg col-sm-12 " style="display:none;"></div>
							<div class="animation_image_prac" style="text-align:center;display:none;">
 							<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
							</div>
<!--end from here-->	
										</div>
									</div>
								</div>
							<div class="back"></div>
							</div>
							</div>
							</div>
						</div>
						<div class="blue-border"></div>
						<h4>UPDATE GAME STATS</h4>
						<div id="tempdiv">
						<div class="row">
					<?php echo $this->Form->create(null,['url'=>['controller'=>'api','action'=>'save_user_stats.json'],
					'id'=>'user_stats','novalidate','enctype' => 'multipart/form-data'])?>
						
                                                        <div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter Type of Game</label>		
									<?= $this->Form->select('game.game_type',['1'=>'Tournament','2'=>'League'],['class'=>'form-control','id'=>'gameType']);?>
								</div>
							</div>	
							
                                                        <div class="col-sm-4" id="tourDiv">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter Tournament Name</label>		
									<?=$this->Form->input('tour.name',['type'=>'text','class'=>'form-control','placeholder'=>'Enter Tournament Name','label'=>false,'id'=>'tourName','onkeyup'=>'autocomplet("Tournaments");','onChange'=>"this.value=this.value.toUpperCase();"])?>									
									<?=$this->Form->input('tour.id',['type'=>'hidden','label'=>false,'id'=>'SaveTourId'])?>
									 <ul id="name" class="list"></ul>
								</div>
							</div>
                                                        <div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter Game Date<span style="color:red">*</span></label>
									<?=$this->Form->input('game.game_date',['class'=>'form-control workdateClass','placeholder'=>'Month-Day-Year','id'=>'gameDate','label'=>false,'value'=>date('Y-m-d')])?>
								</div>
							</div>
													
						</div>	
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter My Team Name<span style="color:red">*</span></label>		
									<?=$this->Form->input('game.my_team_name',['type'=>'text','class'=>'form-control','placeholder'=>'Enter My Team Name','label'=>false,'onkeyup'=>'autocomplet("myteam");','id'=>'myteama','onChange'=>"this.value=this.value.toUpperCase();"])?>
									<ul id="my_team_name" class="list"></ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter Opponent<span style="color:red">*</span></label>		
									<?=$this->Form->input('game.oponent',['type'=>'text','class'=>'form-control','placeholder'=>'Enter Opponent','label'=>false,'onkeyup'=>'autocomplet("opponent");','id'=>'opponenta','onChange'=>"this.value=this.value.toUpperCase();"])?>
									<ul id="oponent" class="list"></ul>
								</div>
							</div>
                                                        <div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Enter Location of Game<span style="color:red">*</span></label>		
									<?=$this->Form->input('game.game_location',['class'=>'form-control','placeholder'=>'Field, City and State','label'=>false,'onkeyup'=>'autocomplet("location");','id'=>'locationa','onChange'=>"this.value=this.value.toUpperCase();"])?>
									<ul id="game_location" class="list"></ul>
								</div>
							</div>
							

							<?=$this->Form->input('game.saved_game_id',['type'=>'hidden','label'=>false,'id'=>'SaveGameId','value'=>0])?>
						</div>
						<h5>AT BAT RESULTS</h5>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">AT-BAT#</label>		
									<?=$this->Form->input('stat.at_bat',['type'=>'text','class'=>'form-control','value'=>1,'label'=>false,'id'=>'at_bat_value','readonly'])?>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Number of Outs</label>		
									<?= $this->Form->select('stat.no_of_out',['0','1','2'],['class'=>'form-control','empty'=>'Number of Outs','id'=>'Number-of-Outs']);?>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Pitcher</label>		
									<?= $this->Form->select('stat.pitcher',['1'=>'Righty','2'=>'Lefty'],['class'=>'form-control','empty'=>'Pitcher','id'=>'Pitcher']);?>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">ROB</label>		
									<?= $this->Form->select('stat.rob',['0','1','2','3'],['class'=>'form-control','empty'=>'ROB','id'=>'ROB']);?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group separate-box">
									<div class="row">
										<div class="col-sm-12">
											<label for="inputEmail3" class="control-label">The Count</label>
										</div>
										<div class="col-sm-6">
											<?= $this->Form->select('stat.pitch_count_bals',['0','1','2','3'],['class'=>'form-control','empty'=>'Balls','id'=>'Balls']);?>
										</div>
										<div class="col-sm-6">
										<?= $this->Form->select('stat.pitch_count_strikes',['0','1','2'],['class'=>'form-control','empty'=>'Strikes','id'=>'Strikes']);?>
										</div>
									</div>
								</div>
							</div>							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">AT-BAT Results</label>		
									<?php 
									$bat_result=[
									'1B'=>'1B',
									'2B'=>'2B',
									'3B'=>'3B',
									'HR'=>'HR',
									'BB'=>'BB',
									'HBP'=>'HBP',
									'SO'=>'SO',
									'Ground Out'=>'Ground Out',
									'Line Out'=>'Line Out',
									'Fly Out'=>'Fly Out',
									'FC'=>'FC',
									'SAC'=>'SAC',
									'ROE'=>'ROE',

									];
									?>
									<?= $this->Form->select('stat.at_bat_result',$bat_result,['class'=>'form-control','empty'=>'AT-BAT Results','id'=>'AT-BAT-Results']);?>
																	</div>
							</div>							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">RBIs</label>		
									<?= $this->Form->select('stat.rbis',['0','1','2','3','4'],['class'=>'form-control','empty'=>'RBIs','id'=>'RBIs']);?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Stolen Bases</label>		
									<?= $this->Form->select('stat.stolen_bases',['0','1','2','3'],['class'=>'form-control','empty'=>'Stolen Bases','id'=>'Stolen-Bases']);?>
								</div>
							</div>						
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Runs Scored</label>		
									<?= $this->Form->select('stat.runs_scored',['0','1'],['class'=>'form-control','empty'=>'Runs Scored','id'=>'Runs-Scored']);?>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Pitch Count (Pitches Thrown)</label>	

									<?php for($i=1;$i<=20;$i++){
										$number[]=$i;
									}
										?>	
									<?= $this->Form->select('stat.pitch_thrown',$number,['class'=>'form-control','empty'=>'Pitches Thrown','id'=>'pitchCount']);?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="inputEmail3" class="control-label">Video Upload</label>		
									        <?= $this->Form->input('stat.file.name',['type'=>'hidden','class'=>'fnames'])?>
											<?= $this->Form->input('stat.file.type',['type'=>'hidden','class'=>'types'])?>
											<?= $this->Form->input('stat.file.size',['type'=>'hidden','class'=>'sizes'])?>
								</div>
							</div>
						</div>						
												
						<div class="animation_image_state" style="text-align:center;display:none;">
 							<?= $this->Html->image('loadinga.gif',['height'=>25,'width'=>25])?>
						</div>
							<div id="success_state" class="success-msg col-sm-5 col-sm-offset-5" style="display:none;"></div>
							<div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorObj_state"></div>
							
							<?=$this->Form->end();?>
<div class="col-lg-3">
	
	<form id="fileupload" action="" method="POST" enctype="multipart/form-data">
        <div class="row fileupload-buttonbar">
            
                <!-- The fileinput-button span is used to style the file input field as button -->
                
                    
                    <?= $this->Form->input('files',['type'=>'file','class'=>'form-control no-padding','label'=>false,'id'=>'stapr'])?>
                    <span id="stprcvh"></span>
    <div id="stat_video" style="text-align:center;display:none;">
 							<?= $this->Html->image('uploading.gif',['height'=>25,'width'=>25])?>
						     </div>            
                
            </div>
            <!-- The global progress state -->
            
        
        <!-- The table listing the files available for upload/download -->
        
    </form>
</div>
<!--end from here-->
<!--Check file size-->
<?= $this->Form->input('chkUploadSize',['type'=>'hidden','id'=>'chkUploadSize'])?>
<div class="form-group pull-right col-sm-5 no-padding">
		<?=$this->Form->button('PAUSE',['type'=>'button','class'=>'btn btn-primary','id'=>'pause'])?>
		<?=$this->Form->button('RESUME',['type'=>'button','class'=>'btn btn-danger','id'=>'resume'])?>
		<?=$this->Form->button('NEXT AT-BAT',['type'=>'button','class'=>'btn btn-primary pull-right','id'=>'user_stats_save'])?>								
	</div>

<!--end-->
												
					</div>
					<div class="current-week-workout margin-top-5">
					<?=$this->Form->button('END GAME',['type'=>'button','class'=>'btn btn-primary','id'=>'endGame'])?>
					<?=$this->Form->button('EXIT',['type'=>'button','class'=>'btn btn-danger','id'=>'exit_btn'])?>
				
	                <div style="display:none;" class="error-Msg col-sm-3 col-sm-offset-3 errorsms"></div>
					<div id="succesSms" class="success-msg col-sm-3 col-sm-offset-3" style="display:none;"></div>
					

				<!--
        from there a small hidden form with field is added  
        -->
        <?=$this->Form->input('phone_number1',['type'=>'hidden','id'=>'ph1','label'=>false])?>
        <?=$this->Form->input('phone_number2',['type'=>'hidden','id'=>'ph2','label'=>false])?>
        <?=$this->Form->input('phone_number3',['type'=>'hidden','id'=>'ph3','label'=>false])?>
        <?=$this->Form->input('phone_notify1',['type'=>'hidden','id'=>'ph4','label'=>false])?>
        <?=$this->Form->input('phone_notify2',['type'=>'hidden','id'=>'ph5','label'=>false])?>
        <?=$this->Form->input('phone_notify3',['type'=>'hidden','id'=>'ph6','label'=>false])?>

	<!--
        End  
        -->

					</div>
				</div>
			</div>
			</div>
			<div class="col-sm-2" style="overflow: hidden;">				
				<div class="profile-current">
					<?=$this->Html->image('profile_pic/'.$user_img,['alt'=>'profile_pic']);?>
					<?=$this->Form->button('Upload Current Picture',['type'=>'button','class'=>'btn btn-primary btn-primaryt','data-toggle'=>'modal','data-target'=>'#myModal','style'=>'margin-top:10px;'])?>
					<p>Current Year Profile Picture</p>
				</div>				
			</div>
		</div>
		<div class="clearfix"></div>


		<!--image uploading Model code-->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upload Profile Picture</h4>
      </div>
      <div class="modal-body">

       				<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
       				<?=$this->Form->create('null',['action'=>'javascript:void(0)','id'=>'uploadimage','enctype'=>'multipart/form-data'])?>
					<div id="image_preview">
					<?=$this->Html->image('profile_pic/'.$user_img,['alt'=>'profile_pic','id'=>'previewing','class'=>'img-responsive']);?>
					</div>
					<hr id="line">
					<div id="selectImage">
					<label>Select Your Image</label><br/>
					
					<?=$this->Form->input('profile_image',['type'=>'file','id'=>'filet','label'=>false,'class'=>'form-control no-padding shere'])?>
					
					</div>
					
					</div>
					<div class="imge" style="text-align:center;display:none;">
 							<?= $this->Html->image('loadinga.gif',['height'=>50,'width'=>50])?>
						     </div>

					<div id="message"></div>
      <div class="modal-footer">
      <?=$this->Form->input('Close',['type'=>'button','id'=>'file','class'=>'btn btn-default','data-dismiss'=>'modal','label'=>false])?>
      <?=$this->Form->input('Save changes',['type'=>'submit','id'=>'file','class'=>'btn btn-primary','label'=>false,'style'=>'margin-top:10px;'])?>
        <?=$this->Form->end();?>
      </div>
    </div>
  </div>
</div>