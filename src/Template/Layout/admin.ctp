<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('admin', 'playerpost-admin...');
$this->assign('title', 'Page');
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $cakeDescription ?>
		<?php $this->fetch('title'); ?>
	</title>
	<?= $this->Html->meta('icon',$this->Url->build('/img/log.png',TRUE)) ?>

		
               <?= $this->Html->css(['admin','fonts','font','font-awesome','font-awesome.min','animation','fresco']) ?>

	<?= $this->Html->script(['jquery-1.11.1.min','fresco','jquery.mixitup','main-gsap','admin-162f0a5353902b60718387c766f552fb','jquery-ui-1.10.3.minimal.min','bootstrap.min','joinable','resizeable','jquery.validate.min','neon-api','neon-login','bootstrap-switch.min','neon-custom']);?>
               
		<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	
</head>
<?php if($this->request->params['action'] == 'index'){ ?>
<body class="login-page">
<?php }else{?>
<body class="page-body">
<?php } ?>
	<div id="container">
		<div id="header">
			
                        <?= $this->element('admin_header') ?>
                       
                        
		</div>
		<div id="content">

			 <?= $this->Flash->render() ?>

			<?= $this->fetch('content'); ?>
			
		</div>
		<div id="footer">
			<?= $this->element('admin_footer') ?>
		</div>
		</div>
	</div>
	
		
		<script>

				var SITE_URL = '<?= $this->Url->build('/',TRUE);?>';
				
			</script>
</body>
</html>
