<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('PlayerPost', 'PlayerPost-baseball...');
$this->assign('title', 'Page');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?= $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		<?= $cakeDescription ?>
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon',$this->Url->build('/img/fevi2.png',TRUE)) ?>
<?= $this->Html->css(['bootstrap','custom','jquery-ui','fonts','font-awesome','font-awesome.min','jquery-ui','fresco']) ?>
	<?= $this->Html->script(['jquery-1.10.2.min','jquery-ui.min','custom/datepicker','bootstrap','custom/user-signup','global','jquery.form','fresco']);?>
               
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	
</head>
<body class="bg-main">
	<div class="container">
		<div class="main">
		<div id="header">
		<?= $this->element('user_header') ?>
        </div>
		<div id="content">

			 <?= $this->Flash->render() ?>

			<?= $this->fetch('content'); ?>
			
		</div>
	</div>	
		<div id="footer">
			<?= $this->element('user_footer') ?>
		</div>
	</div>	
			<script>
				var SITE_URL = '<?= $this->Url->build('/',TRUE);?>';
			</script>

</body>

</html>
