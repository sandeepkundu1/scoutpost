<?= $this->Html->script(['custom/coaches','jquery.form'],['inline'=>false])?>
<div class="content game-stats">
			<div class="col-sm-12">
				<div class="member-service">
					<h1>View Game STATS</h1>
					<div class="current-week-workout">
						<div class="col-sm-12 no-padding-left">
							<h2>Select Field To Query</h2>
							<?= $this->Form->Create(null,['url'=>['controller'=>'api','action'=>'topTen.json'],'id'=>'search_form','type'=>'get'])?>
							<ul class="field-query tournament-link stable-ink">
								<li>
								<?= $this->Form->select('query1',['Baseball'=>'Baseball','Softball'=>'Softball'],['class'=>'form-control','empty' => 'SPORT'])?>

								</li>
								<li>
									<?php 

									for($i=7;$i<18;$i++){
										$age[$i] = $i; 
									}
										?>
									<?= $this->Form->select('query2',$age,['class'=>'form-control','empty' => 'AGE'])?>
								</li>
								<li>
									<?= $this->Form->select('query3',['Right'=>'Right','Left'=>'Left','Both'=>'Both'],['class'=>'form-control','empty' => 'BATS FROM'])?>
								</li>
								<!--li>
									<?= $this->Form->select('query4',['Major'=>'Major','AAA'=>'AAA','AA'=>'AA','Bronco'=>'Bronco','Mustang'=>'Mustang','Pinto'=>'Pinto','Minor'=>'Minor','Senior'=>'Senior','Freshman'=>'Freshman','JV'=>'JV','Varsity'=>'Varsity','Show Case'=>'Show Case'],['class'=>'form-control','empty' => 'Division'])?>
								</li-->
								<li>
									<?= $this->Form->select('query5',['AVG'=>'AVG','SLG'=>'SLG','OPS'=>'OPS','OBP'=>'OBP','PIP'=>'PIP','RBI'=>'RBI','HR'=>'HR','BB'=>'BB','Runs Scored'=>'Runs Scored','QAB'=>'QAB','RHP'=>'AVG vs RHP','LHP'=>'AVG vs LHP','ROB-0'=>'AVG W ROB-0','ROB-1'=>'AVG W ROB-1','ROB-2'=>'AVG W ROB-2','ROB-3'=>'AVG W ROB-3','2-outs'=>'AVG W 2 Outs','2-strick'=>'AVG W 2 Stricks'],['class'=>'form-control','empty' => 'Stats'])?>
								</li>
								<!--li>
									<?= $this->Form->select('query6',['Versus Righty'=>'Versus Righty','Versus Lefty'=>'Versus Lefty','With ROB'=>'With ROB','With 2 Outs'=>'With 2 Outs','With 2 Strikes'=>'With 2 Strikes'],['class'=>'form-control','empty' => ' No filter'])?>
								</li-->
								
							</ul>	
                             

								<div class="row srch-disp">
								<div class="col-sm-3">
								match from :<?= $this->Form->input('datep1',['id'=>'gDate','class'=>'form-control picdate','label'=>false])?></div>
									
								<div class="col-sm-3">	
                                to :
								<?= $this->Form->input('datep2',['id'=>'gDates','class'=>'form-control picdate','label'=>false])?></div>
								<div class="col-sm-12">
								<?=$this->Form->button('Search And Display',['class'=>'btn btn-blue btn-lg','id'=>'search','type'=>'button'])?>	
								</div>								
								</div>
							<?=$this->Form->end()?>												
						</div>
						<div class="col-sm-12">								
						
						<div class="clearfix"></div>
						
						
					</div>
					<div class="col-sm-12 no-padding row">
							<div class="batting-statistics bating-stic game-stats-table bordr-padd-set">							
							<table class="table table-bordered table-striped custom-striped blank" id="results">
								<tr>
									<th>Rank</th>  
									<th>Player</th>
									<th>AT BATS</th>
									<th>AVG</th>
									<th>SLG</th>
									<th>OPS</th>
									<th>RBI</th>
									<th>HR</th>
									<th>1B</th>
									<th>2B</th>
									<th>3B</th>
									<th>BB</th>
									<th>OBP</th>
									<th>QAB</th>
									<th>PIP</th>
									<th>RHP</th>
									<th>LHP</th>
									<th>ROB-0</th>
									<th>ROB-1</th>
									<th>ROB-2</th>
									<th>ROB-3</th>
									<th>2OUTS</th>
									<th>2STRICK</th>
									<th>GPA(1<sup>st</sup> Semester)</th>
									<th>GPA(2<sup>nd</sup> Semester)</th>
								</tr>
							</table>						
							</div>
						</div>
						</div>
				</div>
			</div>
			
	</div>
				

