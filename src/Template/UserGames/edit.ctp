<?php
echo $this->Html->script(['custom/admingame','jquery.form','jquery.fileupload','jquery.fileupload-process','main','jquery.fileupload-validate'],['inline'=>false]);
if(isset($next)){
    $nextLink=$next;

 }
  else{
    $nextLink=null;
 }
if(isset($previous)){
   $previousLink=$previous;
 }
else{
    $previousLink=null;
}
 
 ?>


 <div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

  <div class="sidebar-menu" style="min-height: 712px;">

<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>
    /
  </li>
  <i class="fa fa-user"></i>
  <li><?=$this->Html->link('Gamemanagement',['controller'=>'UserGames','action'=>'index'])?></li>
  <li class="active">
    
    <strong>Edit</strong>
  </li>
  
</ol>
    
           
<div class="userGames form large-10 medium-9 columns">
    <?= $this->Form->create($userGame,['class'=>'editUserGameForm form-horzontal','novalidate'=>true]); ?>
    <fieldset>
        <legend><?= __('Edit User Game') ?></legend>
        <?php

            //echo "<div class='col-sm-4'><div class='form-group'><lable>User</lable>";
            //echo $this->Form->input('user_id', ['options' => $users,'label'=>false,'class'=>'form-control']);
            //echo "</div></div>";
            echo "<div class='col-sm-4'><div class='form-group'><lable>Game Type</lable>";
            echo $this->Form->input('game_type', ['options' => ['1'=>'Tournament','2'=>'League'],'label'=>false,'class'=>'form-control','id'=>'gameType','default'=>$userGame->game_type,'disabled'=>'disabled']);
            //echo $this->Form->input('game_type',['label'=>false,'class'=>'form-control']);
            echo "</div></div>";
            echo "<div class='col-sm-4' id='tourDiv'><div class='form-group'><lable>Tournament</lable>";
            echo $this->Form->input('tournament_id', ['options' => $tournaments, 'empty' => true,'label'=>false,'class'=>'form-control']);
            echo "</div></div>";            
            echo "<div class='col-sm-4'><div class='form-group'><lable>Game Location</lable>";
            echo $this->Form->input('game_location',['label'=>false,'class'=>'form-control','id'=>'locationa','onkeyup'=>'autocomplet("location");','onChange'=>"this.value=this.value.toUpperCase();"]);
            echo '<ul id="game_location" class="list"></ul>';
            echo "</div></div>";
            
            echo "<div class='col-sm-4'><div class='form-group'><lable>My Team Name</lable>";
            echo $this->Form->input('my_team_name',['label'=>false,'class'=>'form-control','onkeyup'=>'autocomplet("myteam");','id'=>'myteama','onChange'=>"this.value=this.value.toUpperCase();"]);
            echo '<ul id="my_team_name" class="list"></ul>';
            echo "</div></div>";
            echo "<div class='col-sm-4'><div class='form-group'><lable>Oponent</lable>";
            echo $this->Form->input('oponent',['label'=>false,'class'=>'form-control','onkeyup'=>'autocomplet("opponent");','id'=>'opponenta','onChange'=>"this.value=this.value.toUpperCase();"]);
            echo '<ul id="oponent" class="list"></ul>';
            echo "</div></div>";
            echo "<div class='col-sm-4'><div class='form-group'><lable>Game Date</lable>";
            echo $this->Form->input('game_date',['label'=>false,'class'=>'form-control']);
            echo "</div></div>";
        ?>
    </fieldset>
       
    <fieldset class="form-horzontal editPlayerStateForm">
        <legend><?= __('Player Stats') ?></legend>
        
        <div class="col-sm-4">
            <div class="form-group">
                <label for="inputEmail3" class="control-label">AT-BAT(<small>either 0 or 1 according to rule</small>)</label>      
                <?=$this->Form->input('stat.at_bat',['type'=>'text','class'=>'form-control','label'=>false,'id'=>'at_bat_value','value'=>$UserStatData['at_bat']])?>        
            </div>
        </div>
        
        <div class="col-sm-4">
            <div class="form-group">
                <label for="inputEmail3" class="control-label">Number of Outs</label>      
                <?= $this->Form->select('stat.no_of_out',['0','1','2'],['class'=>'form-control','empty'=>'Number of Outs','id'=>'Number-of-Outs','value'=>$UserStatData['no_of_out']]);?>
            </div>
        </div>
         
        <div class="col-sm-4">
            <div class="form-group">
                <label for="inputEmail3" class="control-label">Pitcher</label>       
                <?= $this->Form->select('stat.pitcher',['1'=>'Righty','2'=>'Lefty'],['class'=>'form-control','empty'=>'Pitcher','id'=>'Pitcher','value'=>$UserStatData['pitcher']]);?>        
            </div>
        </div>                                    
        
        <div class="col-sm-4">
            <div class="form-group">
            <label for="inputEmail3" class="control-label">ROB</label>      
            <?= $this->Form->select('stat.rob',['0','1','2','3'],['class'=>'form-control','empty'=>'ROB','id'=>'ROB','value'=>$UserStatData['rob']]);?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
       <label for="inputEmail3" class="control-label">The Count</label>
                                       <div class="row">
                                            <div class="col-sm-6"><?= $this->Form->select('stat.pitch_count_bals',['0','1','2','3'],['class'=>'form-control','empty'=>'Balls','id'=>'Balls','value'=>$UserStatData['pitch_count_bals']]);?></div>
                                     
                                        <div class="col-sm-6"><?= $this->Form->select('stat.pitch_count_strikes',['0','1','2'],['class'=>'form-control','empty'=>'Strikes','id'=>'Strikes','value'=>$UserStatData['pitch_count_strikes']]);?> </div>
                                        </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
        <label for="inputEmail3" class="control-label">AT-BAT Results</label>      
                                    <?php 
                                    $bat_result=[
                                    '1B'=>'1B',
                                    '2B'=>'2B',
                                    '3B'=>'3B',
                                    'HR'=>'HR',
                                    'BB'=>'BB',
                                    'HBP'=>'HBP',
                                    'SO'=>'SO',
                                    'Ground Out'=>'Ground Out',
                                    'Line Out'=>'Line Out',
                                    'Fly Out'=>'Fly Out',
                                    'FC'=>'FC',
                                    'SAC'=>'SAC',
                                    'ROE'=>'ROE',

                                    ];
                                    ?>
                                    <?= $this->Form->select('stat.at_bat_result',$bat_result,['class'=>'form-control','empty'=>'AT-BAT Results','id'=>'AT-BAT-Results','value'=>$UserStatData['at_bat_result']]);?> 
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">    
        <label for="inputEmail3" class="control-label">RBIs</label>        
                                    <?= $this->Form->select('stat.rbis',['0','1','2','3','4'],['class'=>'form-control','empty'=>'RBIs','id'=>'RBIs','value'=>$UserStatData['rbis']]);?>   
            </div>
        </div>                                        
        <div class="col-sm-4">
            <div class="form-group">
        <label for="inputEmail3" class="control-label">Stolen Bases</label>        
                                    <?= $this->Form->select('stat.stolen_bases',['0','1','2','3'],['class'=>'form-control','empty'=>'Stolen Bases','id'=>'Stolen-Bases','value'=>$UserStatData['stolen_bases']]);?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">                                    
        <label for="inputEmail3" class="control-label">Runs Scored</label>      
                                    <?= $this->Form->select('stat.runs_scored',['0','1'],['class'=>'form-control','empty'=>'Runs Scored','id'=>'Runs-Scored','value'=>$UserStatData['runs_scored']]);?>
            </div>
        </div> 
        <div class="col-sm-4">
            <div class="form-group">                                   
        <label for="inputEmail3" class="control-label">Pitch Count (Pitches Thrown)</label> 

                                    <?php for($i=1;$i<=20;$i++){
                                        $number[]=$i;
                                    }
                                        ?>  
                                    <?= $this->Form->select('stat.pitch_thrown',$number,['class'=>'form-control','empty'=>'Pitches Thrown','id'=>'pitchCount','value'=>$UserStatData['pitch_thrown']]);?>
            </div>
        </div> 
        <div class="col-sm-4">
            <div class="form-group">                                    
                    
                                     <!--?=$this->Form->input('statmedia.media_name',['type'=>'text','class'=>'form-control','label'=>false,'value'=>$UserMedia['media_name']])?-->

              <?= $this->Form->input('statmedia.media_name',['type'=>'hidden','class'=>'fnames'])?>
              <?= $this->Form->input('statmedia.media_type',['type'=>'hidden','class'=>'types'])?>
              <?= $this->Form->input('statmedia.media_size',['type'=>'hidden','class'=>'sizes'])?>                       
            </div>
        </div>
              
                                   
         <?=$this->Form->input('statmedia.id',['type'=>'hidden','class'=>'form-control','label'=>false,'id'=>'at_bat_value','value'=>$UserMedia['id']])?>
        <?= $this->Form->input('id', array('type' => 'hidden', 'value'=>$UserStatData['id'])); ?>                       
                      
   </fieldset>
   <div class="col-sm-12">
    <div class="row">
    <div class="col-sm-4">
    <div class="form-group">
    <?php if($UserMedia['media_name']!= null){?>
    <iframe frameborder="0" height="281"  allowfullscreen="" src="https://www.youtube.com/embed/<?=$UserMedia['media_name'];?>?rel=0"  class="video_mang"></iframe>
    <?php }else {echo "No Video Added...";}?>
    </div>
    </div>
    </div>
    </div>
    <div class="col-sm-4">
    <div class="form-group">
    <label for="inputEmail3" class="control-label" style="visibility: hidden; display: block;">Button</label>  
    <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary submitBtn','Onclick'=>'return stat_val();']) ?>
    <?= $this->Form->end() ?>
    </div>
    </div>
    <div class="col-sm-4 mediaObject">
    
    <form id="adminFileupload" action="" method="POST" enctype="multipart/form-data">
        <div class="row fileupload-buttonbar">
            
                <!-- The fileinput-button span is used to style the file input field as button -->
                
                    <label for="inputEmail3" class="control-label">Media</label>  
                    <?= $this->Form->input('files',['type'=>'file','class'=>'form-control no-padding','label'=>false,'id'=>'stapr'])?>
                    <span id="stprcvh"></span>
    <div id="stat_video" style="text-align:center;display:none;">
                            <?= $this->Html->image('uploading.gif',['height'=>25,'width'=>25])?>
                             </div>            
                
            </div>
            <!-- The global progress state -->
            
        
        <!-- The table listing the files available for upload/download -->
        
    </form>
</div>    



        <div class="col-sm-12 clear">
        <div class="form-group">
                               <?php if($previousLink !=null){echo $this->html->link("Previous",
       array($games_id,$previousLink)); }else{ echo "No Previous Game Record" ;}?>&nbsp;|&nbsp;<?php if($nextLink !=null){echo $this->html->link("Next",
       array($games_id,$nextLink)); }else{ echo "No Next Game Record" ;}?>
       </div>
       </div>
</div> 