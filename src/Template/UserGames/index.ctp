
<div class="page-container">
  <div class="sidebar-menu" style="min-height: 712px;">

<?= $this->element('admin_sidebar');?>
  </div>
  


  <div class="main-content" style="min-height: 712px;">

<div class="inner_cont">
    

    
<ol class="breadcrumb bc-3 hidden-xs">
  <li>
    <a href="<?= $this->Url->build('/users/dashboard', true); ?>"><i class="fa fa-tachometer"></i> Dashboard</a>

  </li>
  <li class="active">
    <i class="fa fa-user"></i>
    <strong>Game management</strong>
  </li>
</ol>

<div class="userGames index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0" class="table table-responsive table-hover table-bordered">
    <thead>
        <tr>

            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('user_id') ?></th> 
            <th><?= $this->Paginator->sort('tournament_id') ?></th>
            <th><?= $this->Paginator->sort('game_date') ?></th>
            <th><?= $this->Paginator->sort('game_location') ?></th>
            <th><?= $this->Paginator->sort('game_type') ?></th>
            <th><?= $this->Paginator->sort('my_team_name') ?></th>
            <th class="actions "><?= __('Actions') ?></th>
            
        </tr>
    </thead>
    <tbody>
    <?php foreach ($userGames as $userGame): ?>
        <tr>

            <td><?= $this->Number->format($userGame->id) ?></td>
            <td>
                <?php //$userGame->has('user') ? $this->Html->link($userGame->user->id, ['controller' => 'Users', 'action' => 'view', $userGame->user->id]) : '' ?>
                <?= $userGame->has('user') ? $userGame->user->username : '' ?>
            </td>                                                   
            <td>
                <?php //$userGame->has('tournament') ? $this->Html->link($userGame->tournament->name, ['controller' => 'Tournaments', 'action' => 'view', $userGame->tournament->id]) : '' ?>
                 <?= $userGame->has('tournament') ? $userGame->tournament->name : '' ?>
            </td>
            <td><?= h(date('Y-m-d',strtotime($userGame->game_date))); ?></td>
            <td><?= h($userGame->game_location) ?></td>
            <td><?= ($userGame->game_type==1)?'Tournament':'League'; ?></td>
            <td><?= h($userGame->my_team_name) ?></td>

            <td class="actions">
                <!--?= $this->Html->link(__('View'), ['action' => 'view', $userGame->id]) ?-->
                
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userGame->id,@$userGame->user_stats[0]->id],['class'=>'btn btn-primary']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userGame->id,@$userGame->user_stats[0]->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userGame->id),'class'=>'btn btn-danger']) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
   
    <div class="paginator pull-right">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <!--p><?= $this->Paginator->counter() ?></p-->
    </div>
</div>

</div></div></div>
