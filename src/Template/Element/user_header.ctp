<header class="header">
			<h3 class="text-center">
			<?= $this->Html->link("MANY PARENTS WILL SKIP THIS IMPORTANT STEP WHILE PARTICIPATING IN YOUTH SPORTS",['controller'=>'Categories','action'=>'aboutUs'])?>

			</h3>
			<div class="header-mid">
				<div class="col-sm-8">
					<h2>Sports Statistics and Video Profile System <br/> 
						Preparing young athletes for High School and Beyond</h2>
				</div>
				<div class="col-sm-4">
					<div class="col-sm-4">
						
						<?= $this->Html->link($this->Html->image('logo.png',['alt'=>'logo']),['controller'=>'Categories','action'=>'index'], array('escape' => false)) ?>
					</div>
					<div class="col-sm-8" style="padding-right:0px;">
						<h3>
						<i class="fa fa-lock"></i><?= $this->Html->link('Player Login',['controller'=>'Categories','action'=>'userLogin'],['class'=>'player-login'])?></h3>	
					</div>
				</div>
			</div>
			<div class="header-bottom">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
				<ul class="profile-nav navbar-collapse collapse" id="navbar">
					<li><i class="coach-icon"></i> <a href="<?= $this->Url->build('/coaches/coachesLogin');?>"> <span>COACHES LOGIN</span></a></li>
					<li><i class="signup-icon"></i>
					<?= $this->Html->link('Sign-Up for free and start recording your progress',['controller'=>'Categories','action'=>'signup'])?>	
					</li>
					<li><i class="blog-icon"></i><?= $this->Html->link('About_us',['controller'=>'Categories','action'=>'aboutUs'],['class'=>''])?></li>
					<li><i class="blog-icon"></i> <a href="#" class=""> <span>COACHES BOX BLOG</span></a></li>
				</ul>						
			</div>
		</header>
		<div class="clearfix"></div>
