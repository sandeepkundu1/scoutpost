<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

	<div class="main">
	<?php if($sport==1){?>
		<header class="profile-header">		
		<h3 class="text-center">Baseball Player</h3>
		<?php  }else {?>
		<header class="profile-header-ladies">
		<?= $this->Html->image('softball-img.png',['alt'=>'logo','class'=>'img-lft']) ?>	
		<h3 class="text-center">SoftBall Player</h3>
		<?php  }?>
			
			<div class="header-mid">
				<div class="col-sm-9 col-sm-offset-2">
					<h2><?=$userDetail['first_name']." ".$userDetail['last_name'];?> Profile Page <?=date('Y')?>  
					<?= $this->Html->image('player-post.png',['alt'=>'logo','class'=>'img-rgt']) ?>
				</h2>
				</div>				
			</div>
			<div class="header-bottom">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>


                <ul class="socialLinks list-inline" id="navbar">
                    <li>
                    <!--div class="fb-share-button" data-href="http://playerpost.net/current_stats?id=41" data-layout="button_count"></div-->
                   			<div class="fb-share-button" data-href="<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" data-layout="button_count"></div>

                   	</li>

                   	
                   	<li>		
							<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</li>

					<li>
						<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
<script type="IN/Share" data-url="http://playerpost.net/current_stats?<?php echo $_SERVER['REDIRECT_QUERY_STRING'];?>" data-counter="right"></script>
					</li>
					<li>
							<div class="g-plus" data-action="share" data-href="<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>"></div>
                   	</li>
				</ul>				


				<!--ul class="profile-nav navbar-collapse collapse" id="navbar">
				<li><i class="fa fa-file-text"></i> <?= $this->Html->link('Home',['controller'=>'Categories','action'=>'profile']); ?></li>
					<li><i class="fa fa-file-text"></i> <?= $this->Html->link('Update Progress',['controller'=>'Categories','action'=>'updateProfile']); ?></li>
					<li><i class="fa fa-gamepad"></i> <?= $this->Html->link('View Game Stats',['controller'=>'Categories','action'=>'gamesStatus']); ?></li>
					<li><i class="fa fa-pie-chart"></i> <?= $this->Html->link('View Skills Chart',['controller'=>'Categories','action'=>'skills']); ?></li>
					<li><i class="fa fa-university"></i> <?= $this->Html->link('Academics',['controller'=>'Categories','action'=>'academic']); ?></li>
					<li><i class="fa fa-university"></i> <?= $this->Html->link('Profile Settings',['controller'=>'Categories','action'=>'profileSetting']); ?></li>
					<li><i class="fa fa-power-off"></i> <?= $this->Html->link('Log out',['controller'=>'Categories','action'=>'palyerLogout']); ?></li>
				</ul-->				
			</div>
		</header>
		<div class="clearfix"></div>
