
	<div class="main">
	<?php if($sport==1){?>
		<header class="profile-header">		
		<h3 class="text-center">Baseball Player</h3>
		<?php  }else {?>
		<header class="profile-header-ladies">
		<?= $this->Html->image('softball-img.png',['alt'=>'logo','class'=>'img-lft']) ?>	
		<h3 class="text-center">SoftBall Player</h3>
		<?php  }?>
			
			<div class="header-mid">
				<div class="col-sm-9 col-sm-offset-2">
					<h2>Welcome <?=$userDetail['first_name']." ".$userDetail['last_name'];?> to your Profile Page <?=date('Y')?>  
					<?= $this->Html->image('player-post.png',['alt'=>'logo','class'=>'img-rgt']) ?>
				</h2>
				</div>				
			</div>
			<div class="header-bottom">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
				<ul class="profile-nav navbar-collapse collapse" id="navbar">
				<li><i class="fa fa-home"></i> <?= $this->Html->link('Home',['controller'=>'Categories','action'=>'profile']); ?></li>
				
				<?php if(isset($checkplan) && $checkplan!=1) {?>
					<li><i class="fa fa-file-text"></i> <?= $this->Html->link('Update Progress',['controller'=>'Categories','action'=>'updateProfile']); ?></li>

				<?php }else {?>

					<li><i class="fa fa-exclamation-triangle"></i> <?= $this->Html->link('Update Progress',['controller'=>'Categories','action'=>'canGo']); ?></li>
				<?php }?>
					<li><i class="fa fa-gamepad"></i> <?= $this->Html->link('View Game Stats',['controller'=>'Categories','action'=>'gamesStatus']); ?></li>
					<li><i class="fa fa-pie-chart"></i> <?= $this->Html->link('View Skills Chart',['controller'=>'Categories','action'=>'skills']); ?></li>
					<li><i class="fa fa-film"></i> <?= $this->Html->link('GALLERY',['controller'=>'Categories','action'=>'gallery']); ?></li>
					<li><i class="fa fa-university"></i> <?= $this->Html->link('Academics',['controller'=>'Categories','action'=>'academic']); ?></li>
					<li><i class="fa fa-cogs"></i> <?= $this->Html->link('Profile Settings',['controller'=>'Categories','action'=>'profileSetting']); ?></li>
					<li><i class="fa fa-power-off"></i> <?= $this->Html->link('Log out',['controller'=>'Categories','action'=>'palyerLogout']); ?></li>
				</ul>				
			</div>
		</header>
		<div class="clearfix"></div>
