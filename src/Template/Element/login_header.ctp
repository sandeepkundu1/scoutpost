<div class="page-body login-page login-form-fall">

<div class="login-container">

  <div class="login-header login-caret">

    <div class="login-content">
      <a href="Categories/index" class="logo">
        
        <?=$this->Html->image('logo.png',['alt'=>'site-logo','height'=>'102','width'=>'86'])?>
      </a>

      <p class="description">Administration Login</p>

      <!-- progress bar indicator -->
      <div class="login-progressbar-indicator">
        <h3></h3>
        <span>logging in...</span>
      </div>
    </div>

  </div>
