<div class="admin_header">
<div class="">
 
  <div class="col-md-6 col-sm-8 clearfix">


  </div>
  <!-- Raw Links -->
  <div class="col-md-6 col-sm-4 clearfix hidden-xs">

    <ul class="list-inline links-list pull-right user-info pull-none-xsm">

        <!-- Profile Info -->
        <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-user"></i> admin@admin.com  &nbsp;<i class="fa fa-angle-down"></i>
          </a>

          <ul class="dropdown-menu">

            <!-- Reverse Caret -->
            <li class="caret"></li>

            <!-- Profile sub-links -->
            <li>
              <a href="<?= $this->Url->build('/users/myprofile/');?>">
                <i class="fa fa-user"></i>
                My Profile
              </a>
            </li>
            <li>
              <a href="<?= $this->Url->build('/users/logout/');?>">
                <i class="fa fa-power-off"></i>
                Logout
              </a>
            </li>


      </ul>

      <ul class="user-info pull-left pull-right-xs pull-none-xsm">


      </ul>
    </li>
      <!--<li>
        <a href="">
          Log Out <i class="entypo-logout right"></i>
        </a>
      </li>-->
    </ul>

  </div>

</div>
</div>

