<header class="logo-env">

      <div class="logo">
      <?php echo $this->Html->link(
    $this->Html->image("/img/logo.png", ["alt" => "logo","width"=>"200","height"=>"150"]),
    "/",
    ['escape' => false]
); ?>
      </div>

      <!-- logo collapse icon -->

      <div class="sidebar-collapse">
        <a class="sidebar-collapse-icon with-animation" href="#"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
          <i class="fa fa-bars"></i>
        </a>
      </div>

      <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
      <div class="sidebar-mobile-menu visible-xs">
        <a class="with-animation" href="#"><!-- add class "with-animation" to support animation -->
          <i class="fa fa-bars"></i>
        </a>
      </div>

    </header>
    <ul class="" id="main-menu">
  <li class="active root-level">
    <a href="<?= $this->Url->build('/users/dashboard');?>">
      <i class="fa fa-tachometer"></i>
      <span>Dashboard</span>
    </a>
  </li>
      <li class="root-level has-sub">
         <a href="#">
          <i class="fa fa-user"></i>
          <span>Users Management</span>
        </a>
        <ul>
          
          <li class="">
            <a href="<?= $this->Url->build('/users/addUser');?>">
              <i class="fa fa-plus-square"></i>
              <span>New User</span>
            </a>
          </li>
          <li class="">
            <a href="<?= $this->Url->build('/users/viewUser');?>">
              <i class="fa fa-list"></i>
              <span>List Users</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="root-level has-sub">
        <a href="#">
          <i class="fa fa-folder-open"></i>
          <span>CMS</span>
        </a>
        <ul>
          <li class="">
            <a href="<?= $this->Url->build('/users/viewFeature');?>">
              <i class="fa fa-plus-square"></i>
              <span>New Features Management</span>
              <span class="badge badge-success pull-right"></span>
            </a>
          </li>
          <li class="">
            <a href="<?= $this->Url->build('/users/viewSchool');?>">
              <i class="fa fa-list"></i>
              <span>School Galery</span>
            </a>
          </li>
          <li class="">
            <a href="<?= $this->Url->build('/users/viewMarket');?>">
              <i class="fa fa-list"></i>
              <span>Market Galery</span>
            </a>
          </li>
          <li class="">
            <a href="<?= $this->Url->build('/pages/index');?>">
              <i class="fa fa-plus-square"></i>
              <span> Manage Cms Page</span>
              <span class="badge badge-success pull-right"></span>
            </a>
          </li>
        </ul>
      </li>
  <li class="root-level has-sub">
    <a href="#">
      <i class="fa fa-file-image-o"></i>
      <span>Gallery Management</span>
    </a>
    <ul>
      <li class="">
        <a href="<?= $this->Url->build('/users/addGallary');?>">
          <i class="fa fa-plus-square"></i>
          <span>Add New Image</span>
        </a>
      </li>
      <li class="">
        <a href="<?= $this->Url->build('/users/viewGallery');?>">
           <i class="fa fa-list"></i>
          <span>List All Image</span>
        </a>
      </li>

    </ul>
    
  </li>
<li class="root-level has-sub">
    <a href="#">
      <i class="fa fa fa-globe"></i>
      <span>Game Management</span>
    </a>
    <ul>
      <li class="">
        <a href="<?= $this->Url->build('/UserGames');?>">
           <i class="fa fa-list"></i>
          <span>List All Games</span>
        </a>
      </li>
    </ul>
  </li>
  </ul>
