
	<div class="main">
	<header class="profile-header">		
		<h3 class="text-center">Coaches Page</h3>

			<div class="header-mid">
				<div class="col-sm-9 col-sm-offset-2">
					<h2>Welcome <?=$user['first_name']." ".$user['last_name'];?> to your Profile Page <?=date('Y')?>  
					<?= $this->Html->image('player-post.png',['alt'=>'logo','class'=>'img-rgt']) ?>
				</h2>
				</div>				
			</div>
			<div class="header-bottom">
				<div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
				<ul class="profile-nav navbar-collapse collapse" id="navbar">
				<li><i class="fa fa-file-text"></i> <?= $this->Html->link('Home',['controller'=>'Coaches','action'=>'index']); ?></li>
					<li><i class="fa fa-power-off"></i> <?= $this->Html->link('Log out',['controller'=>'Coaches','action'=>'logout']); ?></li>
				</ul>				
			</div>
		</header>
		<div class="clearfix"></div>
