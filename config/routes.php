<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('Route');

Router::scope('/', function ($routes) {
	$routes->extensions(['json', 'xml']);
    
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Categories', 'action' => 'index']);
    $routes->connect('/userRegistration', ['controller' => 'Categories', 'action' => 'signup']);
    $routes->connect('/userlogin', ['controller' => 'Categories', 'action' => 'userLogin']);
    $routes->connect('/about', ['controller' => 'Categories', 'action' => 'aboutUs']);
    $routes->connect('/checkout', ['controller' => 'Categories', 'action' => 'makePayment']);
    $routes->connect('/profile', ['controller' => 'Categories', 'action' => 'profile']);
    $routes->connect('/gallery', ['controller' => 'Categories', 'action' => 'gallery']);
    $routes->connect('/current_stats', ['controller' => 'Categories', 'action' => 'profile_url']);
    $routes->connect('/allFeature', ['controller' => 'Categories', 'action' => 'viewFeatures']);
    $routes->connect('/update', ['controller' => 'Categories', 'action' => 'updateProfile']);
    $routes->connect('/gamesStatus', ['controller' => 'Categories', 'action' => 'gamesStatus']);
    $routes->connect('/skills', ['controller' => 'Categories', 'action' => 'skills']);
    $routes->connect('/academicHistory', ['controller' => 'Categories', 'action' => 'academic']);
    $routes->connect('/admin', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard']);
    $routes->connect('/newUser', ['controller' => 'Users', 'action' => 'addUser']);
    $routes->connect('/UserList', ['controller' => 'Users', 'action' => 'viewUser']);
    $routes->connect('/featuresList', ['controller' => 'Users', 'action' => 'viewFeature']);
    $routes->connect('/addNewGallary', ['controller' => 'Users', 'action' => 'addGallary']);
    $routes->connect('/galleryGrid', ['controller' => 'Users', 'action' => 'viewGallery']);
    $routes->connect('/addNewFeature', ['controller' => 'Users', 'action' => 'addFeature']);
    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    //$routes->connect('/Categories/*', ['controller' => 'Categories', 'action' => 'index']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `InflectedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('InflectedRoute');
});
/*Router::connect(
    '/users',
    array('controller' => 'Users','prefix' => 'admin', 'action' => 'login', 'admin' => true)
);
*/
/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
