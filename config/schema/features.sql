-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 15, 2015 at 10:53 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scoutpost`
--

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `photo` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `description`, `photo`, `created`, `modified`) VALUES
(1, 'Lorem ipsum dolor sit amet, coctetuer adipiscing elit, snibh euismod tincied diam nonummy nibh euismod ticidunt ut laoreet dolore magna aliquam erat volutpa {...}', 'default.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Lorem ipsum dolor sit amet, coctetuer adipiscing elit, snibh euismod tincied diam nonummy nibh euismod ticidunt ut laoreet dolore magna aliquam erat volutpa {...}Lorem ipsum dolor sit amet, coctetuer adipiscing elit, snibh euismod tincied diam nonummy nibh euismod ticidunt ut laoreet dolore magna aliquam erat volutpa {...}Lorem ipsum dolor sit amet, coctetuer adipiscing elit, snibh euismod tincied diam nonummy nibh euismod ticidunt ut laoreet dolore magna aliquam erat volutpa {...}', 'default.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'asd', 'default.png', '2015-04-08 00:00:00', '2015-04-14 00:00:00'),
(4, 'fgddfgdf', '6390_4fd597e1-8cd8-4b0f-b0de-0500156366cc.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Lorem ipsum dolor sit amet, coctetuer adipiscing elit, snibh euismod tincied diam nonummy nibh euismod ticidunt ut laoreet dolore magna aliquam erat volutpa {...}', 'default.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'asdfhkjhkldshfdsafsdfgd fdf\r\nsadfdsfdsff', '6722_4fd5926b-4e88-4b9d-a742-0500156366cc.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
