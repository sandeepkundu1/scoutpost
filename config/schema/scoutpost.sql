-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 28, 2015 at 10:38 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scoutpost`
--

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `photo` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `features`
--



-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_image` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `galleries`
--



-- --------------------------------------------------------

--
-- Table structure for table `game_type`
--

CREATE TABLE IF NOT EXISTS `game_type` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `start_time`, `end_time`) VALUES
(20150416091423, '2015-04-15 22:16:15', '2015-04-15 22:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `plan_type` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userprofiles`
--

CREATE TABLE IF NOT EXISTS `userprofiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sport` varchar(255) NOT NULL COMMENT '1=>''Baseball'',2=>''Softball''',
  `pitcher` int(11) NOT NULL,
  `outfilder` int(11) NOT NULL,
  `infilder` int(11) NOT NULL,
  `catcher` int(11) NOT NULL,
  `bats_from` varchar(255) NOT NULL COMMENT '1=>''Right'',2=>''Left'',3=>''Both''',
  `through_with` varchar(255) NOT NULL COMMENT '1=>''Right Hand'',2=>''Left Hand''',
  `curr_team` varchar(255) NOT NULL,
  `curr_school` varchar(255) NOT NULL,
  `schooling` varchar(255) NOT NULL,
  `feb_team` varchar(255) NOT NULL,
  `feb_athlete` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `userprofiles`
--



-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `activation_key` varchar(64) NOT NULL,
  `salt` text,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `sex` char(1) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` int(11) NOT NULL,
  `email_verified` int(1) DEFAULT '0',
  `description` text,
  `profile_image` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`username`),
  KEY `mail` (`email`),
  KEY `users_FKIndex1` (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `users`
--



-- --------------------------------------------------------

--
-- Table structure for table `user_academics`
--

CREATE TABLE IF NOT EXISTS `user_academics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `year` varchar(50) NOT NULL,
  `first_sem_gpa` varchar(100) DEFAULT NULL,
  `second_sem_gpa` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `user_academics`
--

INSERT INTO `user_academics` (`id`, `user_id`, `year`, `first_sem_gpa`, `second_sem_gpa`, `created`, `modified`) VALUES
(40, 37, '2015', '121', '1212', '2015-04-22 08:59:20', '2015-04-22 08:59:20'),
(88, 26, '2014', '12', '12', '2015-04-24 05:22:31', '2015-04-24 05:22:31'),
(90, 26, '2015', '21', '4', '2015-04-24 05:23:07', '2015-04-24 05:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_games`
--

CREATE TABLE IF NOT EXISTS `user_games` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) NOT NULL,
  `game_date` date NOT NULL,
  `game_location` varchar(100) DEFAULT NULL,
  `game_type` varchar(30) NOT NULL COMMENT '1=>tournament,2=>league',
  `my_team_name` varchar(100) NOT NULL,
  `oponent` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `user_games`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_media_info`
--

CREATE TABLE IF NOT EXISTS `user_media_info` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_skills_id` int(30) DEFAULT NULL COMMENT 'foreign key of user skills tbl',
  `user_id` int(30) DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `media_type` varchar(50) DEFAULT NULL,
  `media_size` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;



-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

CREATE TABLE IF NOT EXISTS `user_skills` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) DEFAULT NULL,
  `workout_date` date NOT NULL,
  `workout_time` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `flag` varchar(50) DEFAULT NULL COMMENT '1=>workout,2=>practice',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;



-- --------------------------------------------------------

--
-- Table structure for table `user_stats`
--

CREATE TABLE IF NOT EXISTS `user_stats` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_games_id` int(30) DEFAULT NULL,
  `user_id` int(30) DEFAULT NULL,
  `at_bat` varchar(30) DEFAULT NULL,
  `no_of_out` int(30) DEFAULT NULL,
  `pitcher` varchar(200) DEFAULT NULL,
  `rob` varchar(200) DEFAULT NULL,
  `at_bat_result` varchar(100) DEFAULT NULL,
  `stolen_bases` varchar(100) DEFAULT NULL,
  `rbis` varchar(100) DEFAULT NULL,
  `runs_scored` varchar(200) DEFAULT NULL,
  `select_hbp` varchar(200) DEFAULT NULL,
  `pitch_count_bals` int(30) DEFAULT NULL,
  `pitch_count_strikes` int(30) DEFAULT NULL,
  `pitch_thrown` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `user_stats`
--



-- --------------------------------------------------------

--
-- Table structure for table `user_stats_media`
--

CREATE TABLE IF NOT EXISTS `user_stats_media` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `modified` datetime DEFAULT NULL,
  `user_stats_id` int(30) DEFAULT NULL,
  `user_id` int(30) DEFAULT NULL,
  `media_name` varchar(100) DEFAULT NULL,
  `media_type` varchar(100) DEFAULT NULL,
  `media_size` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `user_stats` ADD `outs` INT( 30 ) NULL AFTER `qab` ;

CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` int(30) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `flag` int(30) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `user_games` ADD `tournament_id` INT( 30 ) NULL AFTER `user_id` ;
ALTER TABLE `tournaments` ADD `user_id` INT( 30 ) NULL AFTER `name` ;

/*
create a new table for user acadmic detail
  created by anshul
  date 09-05-2105
*/

CREATE TABLE IF NOT EXISTS `user_academic_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `year` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


/*
new changes regards the above user_academic_details

*/
ALTER TABLE `user_academic_details` ADD `semester` INT NOT NULL AFTER `year` ;

ALTER TABLE `user_academics` ADD `use_acadmic_details_id` INT NOT NULL AFTER `user_id` ;

ALTER TABLE `tournaments` CHANGE `id` `id` INT( 30 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `tournaments` ADD `user_id` INT( 30 ) NULL AFTER `name` ;
ALTER TABLE `tournaments` ADD PRIMARY KEY ( `id` ) ;


CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(100) NOT NULL,
  `meta_title` text NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  `page_content` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `pname`, `meta_title`, `subject`, `meta_keyword`, `meta_description`, `page_content`, `created`, `modified`) VALUES
(1, 'about', 'About Us', 'ddddd', 'About Us', 'About US', '<p>ScoutPost integrates the latest technologies to provide a national sports statistics and video profile system in a social media forum to promote youth athletes to high school sports programs.</p>\r\n\r\n<p>There are several services that provide various levels of tools to build athletic profiles which include demonstration of skills and statistical information mainly for the college, semi-pro and pro levels. There are also a variety of applications that can be used to provide tracking of specific sports statistics. Although these services may provide some level accuracy as it relates to an athletes performance, none provide a comprehensive stats engine which should also include historical data and none that address a key progression stage which is High School Competition.</p>\r\n\r\n<p>Parents or promoters of young athletes will typically waste many opportunities to capture their player&rsquo;s performance during a team practice, agility workouts and games. With the ScoutPost System, these moments can be captured and automatically uploaded to the Player Profile Database creating a comprehensive view of your athlete starting at a young age and ultimately for coaches to utilize before the showcase season begins.</p>\r\n\r\n<p>If your athlete has a choice of schools to attend the Profile Data can be used to promote your player to multiple coaches providing a huge advantage at an increasingly competitive high school level.</p>\r\n\r\n<p>ScoutPost is a complete service offering focused on capturing the true abilities of an athlete starting at a young age and continually tracking the development process year by year. &nbsp;This is very important as physical abilities are constantly changing. &nbsp;As change occurs, players can understand and continually view how their developed skills affect changes in their performance (good or bad). &nbsp;The statistics are captured with a valid view of the athlete and stored in a secured &quot;Player Post&quot; data base. This secured site can be accessed by a the athlete to view and also compete with other players based on key statistics (Virtual Tournaments). &nbsp;</p>\r\n', '2015-05-11 10:14:44', '2015-05-11 10:55:43'),
(4, 'acadmic', 'acadmic', NULL, 'acadmic', 'acadmic', '<p>IMPORTANCE OF ACADEMICS</p>\r\n\r\n<p>Placing academics at the highest priority level is the biggest key on your road to success. Whether you intend to play sports in high school, college or beyond, working hard to obtain an education and maintain high grades will allow you to compete for a spot on a team.</p>\r\n', '2015-05-11 11:16:59', '2015-05-11 11:16:59'),
(5, 'termscondition', 'Terms and Conditions', NULL, 'Terms and Conditions', 'Terms and Conditions', '<p>ScoutPost integrates the latest technologies to provide a national sports statistics and video profile system in a social media forum to promote youth athletes to high school sports programs.</p>\r\n\r\n<p>There are several services that provide various levels of tools to build athletic profiles which include demonstration of skills and statistical information mainly for the college, semi-pro and pro levels. There are also a variety of applications that can be used to provide tracking of specific sports statistics. Although these services may provide some level accuracy as it relates to an athletes performance, none provide a comprehensive stats engine which should also include historical data and none that address a key progression stage which is High School Competition.</p>\r\n\r\n<p>Parents or promoters of young athletes will typically waste many opportunities to capture their player&rsquo;s performance during a team practice, agility workouts and games. With the ScoutPost System, these moments can be captured and automatically uploaded to the Player Profile Database creating a comprehensive view of your athlete starting at a young age and ultimately for coaches to utilize before the showcase season begins.</p>\r\n\r\n<p>If your athlete has a choice of schools to attend the Profile Data can be used to promote your player to multiple coaches providing a huge advantage at an increasingly competitive high school level.</p>\r\n\r\n<p>ScoutPost is a complete service offering focused on capturing the true abilities of an athlete starting at a young age and continually tracking the development process year by year. &nbsp;This is very important as physical abilities are constantly changing. &nbsp;As change occurs, players can understand and continually view how their developed skills affect changes in their performance (good or bad). &nbsp;The statistics are captured with a valid view of the athlete and stored in a secured &quot;Player Post&quot; data base. This secured site can be accessed by a the athlete to view and also compete with other players based on key statistics (Virtual Tournaments). &nbsp;</p>\r\n', '2015-05-11 11:33:56', '2015-05-11 11:33:56'),
(6, 'mailPage', 'Welcome Page', NULL, 'Mail', 'Mail', '<p>Hi ,<br />\r\nyou have successfully registered on our site now one more things you have to check email and click on the link sent to your mail and activate your Profile for login Successfully.<br />\r\n<br />\r\nBest wishes<br />\r\nScoutpost Team</p>\r\n', '2015-05-11 11:39:05', '2015-05-11 11:39:52'),
(7, 'index', 'Member Services ', NULL, 'Member Services ', 'Member Services ', '<h2><a style="cursor:pointer;margin: 0px;color: #000;font-weight: bold;font-size: 30px;" href="categories/view_player_post">PLAYER POST PROFILE</a></h2>\r\n\r\n<ul>\r\n  <li>A fun and productive profile system focused on youth athletes ages 8 to 13</li>\r\n <li>Measure and track your player&rsquo;s abilities which are upload from your mobile device</li>\r\n <li>Manual and Automated update of training calendar and game stats with video</li>\r\n <li>View and critique progress with your athlete and see historical changes</li>\r\n</ul>\r\n\r\n<h2><a style="cursor:pointer;margin: 0px;color: #000;font-weight: bold;font-size: 30px;" href="categories/view_player_post">PLAYER POST MOBILE APPS</a></h2>\r\n\r\n<ul>\r\n <li>Use our SkillsPost App to record and automatically upload training routines to the Player Profile</li>\r\n  <li>SkillsPost can be used for team practice or any workouts and updates the training calendar</li>\r\n <li>Use our GamePost App to record and automatically upload real game stats to the Player Profile</li>\r\n  <li>GamePost provides game, tournament and historical detailed batting statistics with video</li>\r\n</ul>\r\n\r\n<h2><a style="cursor:pointer;margin: 0px;color: #000;font-weight: bold;font-size: 30px;" href="categories/view_player_post">PLAYER POST VIRTUAL TOURNAMENT</a></h2>\r\n\r\n<ul>\r\n <li>Player from respective age groups can enter to compete for prizes</li>\r\n  <li>Various game stats will be used to determine winners</li>\r\n <li>Prizes include sports gear, equipment and gift cards</li>\r\n</ul>\r\n', '2015-05-11 11:54:24', '2015-05-11 12:49:05');

